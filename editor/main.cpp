#ifdef ENABLE_MEMORY_LEAK_DETECTION
#include <vld/vld.h>
#endif

#include <Biovoid.hpp>
#include <runtime/EditorClient.hpp>
#include <runtime/GameClient.hpp>

int main() {
    bvd::App app("S:/Projects/biovoid-engine");

    bvd::AppInit init;
    bvd::AppWindow& window = init.windows.emplace_back();
    window.width = 2560;
    window.height = 1300;
    window.title = "Biovoid Engine";
    window.mode = bvd::WindowMode::windowed;

    bvd::EditorClient client{app.getRouter()};
    app.init(client, init);

    app.run();
}
