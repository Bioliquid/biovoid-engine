#ifdef ENABLE_MEMORY_LEAK_DETECTION
#include <vld/vld.h>
#endif

#include <Biovoid.hpp>
#include <sstream>
#include <iostream>

#include <boost/iostreams/device/mapped_file.hpp>

class LogViewerScript {
public:
    void receive(bvd::ui::Node& node, bvd::FileDropInd const& msg) {
        using namespace bvd;
        using namespace bvd::ui;

        if (msg.files.size() != 1) {
            BVD_LOG(LogGroup::gen, BE, "More than 1 file is not supported");
            return;
        }

        // std::string text = fs::FileManager::readFile(msg.files[0].data());
        BVD_LOG(LogGroup::gen, BD, "Reading file {}", msg.files[0].data());
        boost::iostreams::mapped_file mmap(msg.files[0].data(), boost::iostreams::mapped_file::readonly);
        char const* f = mmap.const_data();
        char const* l = f + mmap.size();

        std::string lineCol;
        std::string dateCol;
        std::string timeCol;
        std::string logLevelCol;
        std::string componentCol;
        std::string fileCol;
        std::string functionCol;
        std::string messageCol;

        lineCol.reserve(4103826);
        dateCol.reserve(6623463);
        timeCol.reserve(11440523);
        logLevelCol.reserve(3596663);
        componentCol.reserve(24453446);
        fileCol.reserve(6570444);
        functionCol.reserve(6823992);
        messageCol.reserve(62560317);

        uint32_t lineNum = 0;
        while (f && f != l) {
            char const* endLine = static_cast<const char*>(std::memchr(f, '\n', l - f));
            if (not endLine) {
                break;
            }
            std::string_view line(f, uint64_t(endLine - f));

            if (line.substr(0, 4) != "2023") {
                f = endLine + 1;
                continue;
            }

            ++lineNum;
            lineCol += std::to_string(lineNum) + "\n";

            size_t i = 0;
            auto p = line.find(' ', i);
            dateCol += line.substr(i, p - i);
            dateCol += '\n';
            i = p + 1;

            p = line.find(' ', i);
            timeCol += line.substr(i, p - i);
            timeCol += '\n';
            i = p + 1;

            p = line.find(' ', i);
            logLevelCol += line.substr(i, p - i);
            logLevelCol += '\n';
            i = p + 1;
            while (std::isspace(line[i])) ++i;

            p = line.find(':', i);
            componentCol += line.substr(i, p - i);
            componentCol += '\n';
            i = p + 2;

            p = line.find(' ', i);
            fileCol += line.substr(i, p - i);
            fileCol += '\n';
            i = p + 1;

            p = line.find(':', i);
            functionCol += line.substr(i, p - i);
            functionCol += '\n';
            i = p + 2;

            messageCol += line.substr(i, line.size() - i);
            messageCol += '\n';

            f = endLine + 1;
        }

        Node& content = node.children.front().id == "scroll" ? node.children.back() : node.children.front();

        for (Node& child : content.children) {
            if (child.id == "#") {
                child.children.front().setText(lineCol);
            } else if (child.id == "Date") {
                child.children.front().setText(dateCol);
            } else if (child.id == "Time") {
                child.children.front().setText(timeCol);
            } else if (child.id == "LogLvl") {
                child.children.front().setText(logLevelCol);
            } else if (child.id == "Component") {
                child.children.front().setText(componentCol);
            } else if (child.id == "File") {
                child.children.front().setText(fileCol);
            } else if (child.id == "Function") {
                child.children.front().setText(functionCol);
            } else if (child.id == "Message") {
                child.children.front().setText(messageCol);
            }
        }

        node.scale();
    }
};

template<typename... Params>
struct LogView : public bvd::ui::Component<LogView, Params...> {
    static void optHandler(bvd::ui::Node&, bvd::ui::Node&, LogView::Helper helper) {
        using namespace bvd;
        using namespace ui;
        using namespace components;

        helper.setParams<Width, Height, Color>(100.0_pct, 100.0_pct, colors::transparent);

        helper.fragment(
            Div<Width, Height, Color>::top(100.0_pct, autoFit, colors::transparent)(
                TextArea<Title, group::Text, Width, Height>::left("#", 75.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("DATE", 100.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("TIME", 175.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("LOGLVL", 75.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("COMPONENT", 100.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("FILE", 200.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("FUNCTION", 200.0_px, autoFit)(),
                TextArea<Title, group::Text, Width, Height>::left("MESSAGE", 500.0_px, autoFit)()
            ),
            Div<Width, Height, Color, Margin>::top(100.0_pct, 1.0_px, colors::white, glm::vec4{10.0f, 0.0f, 10.0f, 0.0f})(),
            Scrollable<Width, Color, Handler<LogViewerScript>>::vmiddle(100.0_pct, colors::transparent)(
                TextArea<Id, Title, group::Text, Width, Height>::left("#", "", 75.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::left("Date", "", 100.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::left("Time", "", 175.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::left("LogLvl", "", 75.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::left("Component", "", 100.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::left("File", "", 200.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::left("Function", "", 200.0_px, autoFit)(),
                TextArea<Id, Title, group::Text, Width, Height>::hmiddle("Message", "", 500.0_px, autoFit)()
            )
        );
    }
};

template<typename... Params>
struct LogViewer : public bvd::ui::Component<LogViewer, Params...> {
    static void optHandler(bvd::ui::Node&, bvd::ui::Node&, LogViewer::Helper helper) {
        using namespace bvd;
        using namespace ui;
        using namespace components;

        helper.setParams<Width, Height>(100.0_pct, 100.0_pct);

        helper.fragment(
            Tab<Id, Width, Height>::left("Log", 100.0_pct, 100.0_pct)(
                LogView<>::left()()
            )
        );
    }
};

int main() {
    bvd::App app("S:/Projects/biovoid-engine");

    auto sceneRef2 = bvd::fs::FileManager::createScene("uiScene");

    bvd::AppInit init;
    bvd::AppWindow& window = init.windows.emplace_back();
    window.width = 1920;
    window.height = 1080;
    window.title = "Biovoid Engine";
    window.mode = bvd::WindowMode::windowed;
    bvd::AppScene& scene2 = window.scenes.emplace_back();
    scene2.id = 1;
    scene2.scene = "uiScene";

    app.init(init);

    sceneRef2->deserializeUi("dummy");
    sceneRef2.reset();

    app.getUi().create<LogViewer>({ .width = 1920, .height = 1080 });

    app.run();
}