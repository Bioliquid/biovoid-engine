#include "DynamicRhi.hpp"

#include "Rhi.hpp"
#include "GlobalConfig.hpp"
#include "Logger.hpp"

namespace bvd::rhi::common {

void DynamicRhi::selectGraphicsAPI(RhiApi& currentApi) {
    if (GlobalConfig::forceOpengl) {
        currentApi = RhiApi::opengl;
    } else if (GlobalConfig::forceVulkan) {
        currentApi = RhiApi::vulkan;
    } else {
        currentApi = RhiApi::opengl;
    }
}

} // namespace bvd::rhi::common
