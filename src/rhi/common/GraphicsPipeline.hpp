#pragma once

#include "GraphicsPipelineStateInit.hpp"
#include "StaticVector.hpp"
#include "EngineLimits.hpp"
#include "RenderPassInfo.hpp"
#include "UniformType.hpp"
#include "DescriptorType.hpp"
#include "Resource.hpp"

#include <spirv_cross/spirv_glsl.hpp>
#include <cstdint>
#include <vector>
#include <map>

namespace bvd {

namespace renderer {

class Material;

} // namespace renderer

namespace rhi {

namespace common {

class Device;

class GraphicsPipeline : public Resource {
    // TODO: remove when descriptor sets is part of material
    friend renderer::Material;

protected:
    struct VertexInputAttribute {
        std::string name;
        uint32_t    location;
        uint32_t    offset;
        UniformType uniformType;
    };

    struct VertexInputBinding {
        uint32_t stride;
        std::vector<VertexInputAttribute> attributes;
    };

    struct UniformVariable {
        size_t          offset = 0;
        UniformType     type;
    };

    struct DescriptorSetBinding {
        std::string     name;
        DescriptorType  type;
        uint32_t        count = 0;
        uint32_t        size = 0;
        uint32_t        stageFlags = 0;

        std::map<std::string, UniformVariable> variables;
    };

    struct DescriptorSet {
        uint32_t size = 0;
        std::map<uint32_t, DescriptorSetBinding> bindings;
    };

    struct DescriptorSetsInfo {
        bool hasUniforms() const { return !descriptors.empty(); }

        uint32_t numUniformBuffers = 0;
        uint32_t numImageSamplers = 0;
        std::map<uint32_t, DescriptorSet> descriptors;
    };

public:
    GraphicsPipeline() = default;
    GraphicsPipeline(GraphicsPipeline const&) = delete;
    GraphicsPipeline& operator=(GraphicsPipeline const&) = delete;
    
    virtual ~GraphicsPipeline() = default;

    void load(GraphicsPipelineStateInit const&);

    virtual void destroy() = 0;

    virtual void setUniformBuffer(uint32_t, uint32_t, void const*, uint64_t, uint64_t) = 0;
    virtual void cmdSetUniformBuffer(uint32_t, uint32_t, void const*, uint64_t, uint64_t) = 0;

    virtual void setTexture(uint32_t, uint32_t, uint32_t, Texture const&) = 0;

    virtual void bind() const = 0;

    DescriptorSetsInfo const& getInfo() { return descriptorSetsInfo; }

protected:
    static_vector<spirv_cross::CompilerGLSL, maxNumShaderStages> compilers;
    static_vector<VertexInputBinding, maxNumVertexBindings>      vertexInputLayout;

    DescriptorSetsInfo descriptorSetsInfo;

private:
    virtual void loadShaders(GraphicsPipelineStateInit const&) = 0;
    virtual common::Device const& getDevice() const = 0;

    void processVertexInputLayout(spirv_cross::CompilerGLSL&);
    void processDescriptorSetInfo();
};

} // namespace common

} // namespace rhi

} // namespace bvd
