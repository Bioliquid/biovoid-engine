#pragma once

#include "UniformTypeInfo.hpp"
#include "PixelFormatInfo.hpp"

namespace bvd {

namespace rhi {

namespace common {

class Device {
public:
    PixelsFormatInfo pixelFormats;
    UniformTypesInfo uniformTypes;
};

} // namespace common

} // namespace rhi

} // namespace bvd
