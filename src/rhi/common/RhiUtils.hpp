#pragma once

#include <cstdint>
#include <cmath>

namespace bvd {

namespace rhi {

inline uint32_t getMipCount(uint32_t width, uint32_t height) {
    return static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;
}

} // namespace rhi

} // namespace bvd
