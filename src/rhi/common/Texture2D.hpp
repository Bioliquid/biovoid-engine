#pragma once

#include "Texture.hpp"

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class Texture2D : public Texture {
public:
    Texture2D(TextureCreateInfo const& info)
        : Texture(info)
        , width(info.width)
        , height(info.height)
    {}

    Texture2D(Texture2D const&) = delete;
    Texture2D& operator=(Texture2D const&) = delete;

    inline int32_t getWidth() const { return width; }
    inline int32_t getHeight() const { return height; }

private:
    int32_t width;
    int32_t height;
};

} // namespace common

} // namespace rhi

} // namespace bvd
