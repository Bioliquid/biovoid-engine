#pragma once

#include "RhiDefinitions.hpp"
#include "Resource.hpp"
#include "PixelFormat.hpp"
#include "Color.hpp"

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

enum class ClearBinding : uint8_t {
    // No clear color associated with this target. Target will not do hardware clears on most platforms
      none
    // Target has a clear color bound. Clears will use the bound color, and do hardware clears
    , color
    // Target has a depthstencil value bound. Clears will use the bound values and do hardware clears
    , depthStencil
};

struct ClearValueBinding {
    ClearValueBinding() = default;

    ClearValueBinding(Color const& inClearColor)
        : binding(ClearBinding::color)
    {
        color[0] = inClearColor.r;
        color[1] = inClearColor.g;
        color[2] = inClearColor.b;
        color[3] = inClearColor.a;
    }

    Color getClearColor() const { return Color{color}; }

    ClearBinding    binding = ClearBinding::none;
    float           color[4];
};

struct TextureCreateInfo {
    TextureCreateInfo(char const* filePath, bool shouldGenerateMips = false);
    TextureCreateInfo(PixelFormat format, int32_t width, int32_t height, uint8_t* buffer);
    TextureCreateInfo(PixelFormat format, int32_t width, int32_t height, TextureCreateFlags::Type flags = TextureCreateFlags::none, Color const& color = colors::black, uint32_t numSamples = 1);
    ~TextureCreateInfo();

    PixelFormat                 format;
    int32_t                     width;
    int32_t                     height;
    uint32_t                    numMips = 1;
    uint32_t                    numSamples = 1;
    TextureCreateFlags::Type    flags;
    uint8_t*                    pixels = nullptr;
    ClearValueBinding           clearColor;
};

class Texture : public Resource {
public:
    Texture(TextureCreateInfo const& info)
        : clearValue(info.clearColor)
        , numMips(info.numMips)
        , numSamples(info.numSamples)
        , format(info.format)
        , flags(info.flags)
    {}

    Texture(Texture const&) = delete;
    Texture& operator=(Texture const&) = delete;

    uint32_t getNumMips() const { return numMips; }
    uint32_t getNumSamples() const { return numSamples; }
    PixelFormat getFormat() const { return format; }
    TextureCreateFlags::Type getFlags() const { return flags; }

    inline bool hasClearColor() const { return clearValue.binding != ClearBinding::none; }
    inline Color getClearColor() const { return clearValue.getClearColor(); }

protected:
    ClearValueBinding           clearValue;
    uint32_t                    numMips;
    uint32_t                    numSamples;
    PixelFormat                 format;
    TextureCreateFlags::Type    flags;
};

} // namespace common

} // namespace rhi

} // namespace bvd
