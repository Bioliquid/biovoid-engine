#pragma once

#include "RhiDefinitions.hpp"
#include "RenderTargetsInfo.hpp"
#include "Memory.hpp"

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class Texture;

class RenderPassInfo {
public:
    struct ColorEntry {
        Texture*                renderTarget = nullptr;
        Texture*                resolveTarget = nullptr;
        RenderTargetLoadAction  loadAction;
        RenderTargetStoreAction storeAction;
    };

    struct DepthStencilEntry {
        Texture*                depthStencilTarget;
    };

public:
    RenderPassInfo() = default;

    // Color, no depth, optional resolve, optional mip, optional array slice
    explicit RenderPassInfo(Texture* colorRt, RenderTargetLoadAction loadAction, RenderTargetStoreAction storeAction, Texture* resolveRt = nullptr) {
        BVD_CHECK(colorRt, "Color render target is nullptr");
        numColorRenderTargets = 1;
        colorRenderTargets[0].renderTarget = colorRt;
        colorRenderTargets[0].resolveTarget = resolveRt;
        colorRenderTargets[0].loadAction = loadAction;
        colorRenderTargets[0].storeAction = storeAction;
    }

    void convertToRenderTargetsInfo(RenderTargetsInfo& info) const {
        info.numColorRenderTargets = numColorRenderTargets;
        
        for (uint32_t index = 0; index < numColorRenderTargets; ++index) {
            info.colorRenderTargets[index].texture = colorRenderTargets[index].renderTarget;
            info.colorRenderTargets[index].loadAction = colorRenderTargets[index].loadAction;
            info.colorRenderTargets[index].storeAction = colorRenderTargets[index].storeAction;

            if (colorRenderTargets[index].resolveTarget) {
                info.hasResolveAttachments = true;
                info.colorResolveRenderTargets[index] = info.colorRenderTargets[index];
                info.colorResolveRenderTargets[index].texture = colorRenderTargets[index].resolveTarget;
            }
        }
    }

public:
    uint32_t            numColorRenderTargets;
    ColorEntry          colorRenderTargets[maxNumSimultaneousRenderTargets];

    // Depth/Stencil Render Target Info
    // DepthStencilEntry   depthStencilRenderTarget;
};

} // namespace common

} // namespace rhi

} // namespace bvd
