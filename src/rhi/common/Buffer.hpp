#pragma once

#include "Resource.hpp"

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class Buffer : public Resource {
public:
    Buffer() = default;
    Buffer(Buffer const&) = delete;
    Buffer& operator=(Buffer const&) = delete;

    virtual void copy(void const*, int64_t, int64_t) = 0;
    virtual void copy(void const*, int64_t) = 0;
    virtual void cmdCopy(void const*, int64_t) = 0;
    virtual void cmdCopy(void const*, int64_t, int64_t) = 0;
    virtual void destroy() = 0;
};

} // namespace common

} // namespace rhi

} // namespace bvd
