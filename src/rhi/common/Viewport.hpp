#pragma once

#include "Resource.hpp"

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class Viewport : public Resource {
public:
    Viewport() = default;
    virtual ~Viewport() {}

    Viewport(Viewport const&) = delete;
    Viewport& operator=(Viewport const&) = delete;

    virtual void resize(uint32_t, uint32_t) = 0;
};

} // namespace common

} // namespace rhi

} // namespace bvd
