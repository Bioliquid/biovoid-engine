#pragma once

#include "RhiDefinitions.hpp"
#include "RenderTargetView.hpp"

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class RenderTargetsInfo {
public:
    uint32_t            numColorRenderTargets = 0;
    RenderTargetView    colorRenderTargets[maxNumSimultaneousRenderTargets];
    // bool                clearColor;

    bool             hasResolveAttachments = false;
    RenderTargetView colorResolveRenderTargets[maxNumSimultaneousRenderTargets];

    // Depth/Stencil Render Target Info
    // RenderTargetView    depthStencilRenderTarget;
    // bool                clearDepth;
    // bool                clearStencil;
};

} // namespace common

} // namespace rhi

} // namespace bvd
