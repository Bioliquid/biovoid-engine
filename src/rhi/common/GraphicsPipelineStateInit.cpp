#include "GraphicsPipelineStateInit.hpp"
#include "FileManager.hpp"
#include "Logger.hpp"

namespace bvd::rhi::common {

ShaderSource::ShaderSource(ShaderStage inStage, char const* inFilePath)
    : stage(inStage)
    , filePath(inFilePath)
{
    shaderc_shader_kind shaderKind{};
    switch (stage) {
        case ShaderStage::vertex:
            shaderKind = shaderc_vertex_shader;
            break;
        case ShaderStage::fragment:
            shaderKind = shaderc_fragment_shader;
            break;
        default:
            break;
    }

    // vulkan
    {
        shaderc::Compiler compiler;
        shaderc::CompileOptions options;
        options.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_2);
        options.SetWarningsAsErrors();
        options.SetGenerateDebugInfo();

        constexpr bool optimize = true;
        if (optimize) {
            options.SetOptimizationLevel(shaderc_optimization_level_performance);
        }

        std::string shaderSrc = fs::FileManager::readShader(filePath);

        shaderc::SpvCompilationResult shaderModule = compiler.CompileGlslToSpv(shaderSrc, shaderKind, filePath, options);

        if (shaderModule.GetCompilationStatus() != shaderc_compilation_status_success) {
            BVD_LOG(LogGroup::rhi, BE, "Shaderc error {}", shaderModule.GetErrorMessage());
        }

        source.assign(shaderModule.begin(), shaderModule.end());
    }

    constexpr bool isOpengl = false;

    // opengl
    if (isOpengl) {
        shaderc::Compiler compiler;
        shaderc::CompileOptions options;
        options.SetTargetEnvironment(shaderc_target_env_opengl, shaderc_env_version_opengl_4_5);

        constexpr bool optimize = true;
        if (optimize) {
            options.SetOptimizationLevel(shaderc_optimization_level_performance);
        }

        std::string sourceCode;

        spirv_cross::CompilerGLSL glslCompiler(source);
        sourceCode = glslCompiler.compile();

        shaderc::SpvCompilationResult shaderModule = compiler.CompileGlslToSpv(sourceCode, shaderKind, filePath);
        if (shaderModule.GetCompilationStatus() != shaderc_compilation_status_success) {
            BVD_LOG(LogGroup::rhi, BE, "Shaderc error {}", shaderModule.GetErrorMessage());
        }

        source.assign(shaderModule.cbegin(), shaderModule.cend());
    }
}

GraphicsPipelineStateInit::GraphicsPipelineStateInit(std::initializer_list<ShaderSource> const& inShaderSources)
    : shaderSources(inShaderSources)
{}

} // namespace bvd::rhi::common
