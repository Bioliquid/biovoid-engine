#pragma once

#include "ShaderStage.hpp"
#include "PrimitiveTopology.hpp"
#include "PixelFormat.hpp"
#include "RhiDefinitions.hpp"

#include <shaderc/shaderc.hpp>
#include <libshaderc_util/file_finder.h>
#include <spirv_cross/spirv_glsl.hpp>
#include <spirv-tools/libspirv.h>

#include <vector>
#include <array>

namespace bvd {

namespace rhi {

namespace common {

class RenderPassInfo;

struct ShaderSource {
    ShaderSource(ShaderStage, char const*);

    ShaderStage             stage;
    char const*             filePath;
    std::vector<uint32_t>   source;
};

struct GraphicsPipelineStateInit {
    using RenderTargetFormats = std::array<PixelFormat, maxNumSimultaneousRenderTargets>;
    using RenderTargetFlags = std::array<TextureCreateFlags::Type, maxNumSimultaneousRenderTargets>;

    GraphicsPipelineStateInit(std::initializer_list<ShaderSource> const&);

    std::vector<ShaderSource>   shaderSources;
    PrimitiveTopology           primitiveTopology;
    uint32_t                    numSamples = 1;
    uint32_t                    numColorRenderTargets = 0;
    RenderTargetFormats         renderTargetFormats;
    RenderTargetFlags           renderTargetFlags;
};

} // namespace common

} // namespace rhi

} // namespace bvd
