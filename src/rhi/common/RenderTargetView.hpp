#pragma once

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class Texture;

class RenderTargetView {
public:
    Texture*                texture;
    RenderTargetLoadAction  loadAction;
    RenderTargetStoreAction storeAction;
    // uint32_t                mipIndex;
    // uint32_t                arraySliceIndex;
};

} // namespace common

} // namespace rhi

} // namespace bvd
