#include "GraphicsPipeline.hpp"

#include "Device.hpp"
#include "Logger.hpp"

#include <algorithm>

namespace bvd::rhi::common {

// TODO: fill the rest
static UniformType getUniformType(spirv_cross::SPIRType type) {
    if (type.basetype == spirv_cross::SPIRType::Float) {
        if (type.vecsize == 1) {
            return UniformType::vec1;
        } else if (type.vecsize == 2) {
            return UniformType::vec2;
        } else if (type.vecsize == 3) {
            if (type.columns == 1) {
                return UniformType::vec3;
            } else if (type.columns == 3) {
                return UniformType::mat3;
            }
        } else if (type.vecsize == 4) {
            if (type.columns == 1) {
                return UniformType::vec4;
            } else if (type.columns == 4) {
                return UniformType::mat4;
            }
        }
    } else if (type.basetype == spirv_cross::SPIRType::UInt) {
        if (type.vecsize == 1) {
            return UniformType::uvec1;
        } else if (type.vecsize == 2) {
            return UniformType::uvec2;
        } else if (type.vecsize == 3) {
            return UniformType::uvec3;
        } else if (type.vecsize == 4) {
            return UniformType::uvec4;
        }
    }

    return UniformType::num;
}

void GraphicsPipeline::load(GraphicsPipelineStateInit const& init) {
    BVD_LOG(LogGroup::rhi, BD, "=================================================");
    BVD_LOG(LogGroup::rhi, BD, "Graphics Pipeline: {}", init.shaderSources[0].filePath);

    for (common::ShaderSource const& shaderSource : init.shaderSources) {
        spirv_cross::CompilerGLSL& compiler = compilers.emplace_back(shaderSource.source);

        if (shaderSource.stage == ShaderStage::vertex) {
            processVertexInputLayout(compiler);
        }
    }

    processDescriptorSetInfo();
    loadShaders(init);

    BVD_LOG(LogGroup::rhi, BD, "=================================================");
}

void GraphicsPipeline::processVertexInputLayout(spirv_cross::CompilerGLSL& compiler) {
    Device const& device = getDevice();

    spirv_cross::ShaderResources resources = compiler.get_shader_resources();

    if (resources.stage_inputs.size() == 0) {
        BVD_LOG(LogGroup::rhi, BD, "Vertex shader has 0 input bindings");
        return;
    }

    std::ranges::sort(resources.stage_inputs, [&compiler](spirv_cross::Resource const& lhs, spirv_cross::Resource const& rhs) {
        return compiler.get_decoration(lhs.id, spv::DecorationLocation) < compiler.get_decoration(rhs.id, spv::DecorationLocation);
    });

    // find location index where per-instance objects start
    auto perInstanceLocationIterator = std::ranges::find_if(resources.stage_inputs, [](spirv_cross::Resource const& resource) {
        return resource.name[0] == 'i';
    });

    size_t perInstanceLocationIndex = std::distance(std::begin(resources.stage_inputs), perInstanceLocationIterator);

    size_t bindingStart[2] = {0, perInstanceLocationIndex};

    { // usual per-vertex buffer (1st binding)
        VertexInputBinding& vertexInputBinding = vertexInputLayout.emplace_back();
        vertexInputBinding.attributes.resize(perInstanceLocationIndex);
    }

    // per-instance buffer (2nd binding)
    if (perInstanceLocationIterator != std::end(resources.stage_inputs)) {
        VertexInputBinding& vertexInputBinding = vertexInputLayout.emplace_back();
        vertexInputBinding.attributes.resize(resources.stage_inputs.size() - perInstanceLocationIndex);
    }

    BVD_LOG(LogGroup::rhi, BD, "Vertex shader has {} input bindings", vertexInputLayout.size());

    uint32_t vertexInputBindingIndex = 0;
    for (VertexInputBinding& vertexInputBinding : vertexInputLayout) {
        BVD_LOG(LogGroup::rhi, BD, "-Binding[{}] has {} input attributes", vertexInputBindingIndex, vertexInputBinding.attributes.size());

        uint32_t attributeIndex = 0;
        for (size_t vertexInputIndex = bindingStart[vertexInputBindingIndex]; vertexInputIndex < bindingStart[vertexInputBindingIndex] + vertexInputBinding.attributes.size(); ++vertexInputIndex) {
            spirv_cross::Resource& resource = resources.stage_inputs[vertexInputIndex];

            vertexInputBinding.attributes[attributeIndex].name = resource.name;
            vertexInputBinding.attributes[attributeIndex].location = compiler.get_decoration(resource.id, spv::DecorationLocation);

            spirv_cross::SPIRType const& type = compiler.get_type(resource.type_id);

            vertexInputBinding.attributes[attributeIndex].uniformType = getUniformType(type);

            ++attributeIndex;
        }

        uint32_t attributeOffset = 0;
        for (VertexInputAttribute& attribute : vertexInputBinding.attributes) {
            attribute.offset = attributeOffset;

            BVD_LOG(LogGroup::rhi, BD, "-- (location = {}) {} {} [{}..{} bytes]"
                , attribute.location
                , device.uniformTypes[attribute.uniformType].name
                , attribute.name
                , attributeOffset
                , attributeOffset + device.uniformTypes[attribute.uniformType].byteSize);

            attributeOffset += device.uniformTypes[attribute.uniformType].byteSize;
        }

        vertexInputBinding.stride = attributeOffset;

        ++vertexInputBindingIndex;
    }
}

void GraphicsPipeline::processDescriptorSetInfo() {
    for (spirv_cross::CompilerGLSL& glsl : compilers) {
        spirv_cross::ShaderResources resources = glsl.get_shader_resources();

        auto entryPoint = glsl.get_entry_points_and_stages().front();

        for (spirv_cross::Resource& resource : resources.uniform_buffers) {
            uint32_t set = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
            uint32_t binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
            spirv_cross::SPIRType const& type = glsl.get_type(resource.type_id);

            std::map<uint32_t, DescriptorSetBinding>& bindings = descriptorSetsInfo.descriptors[set].bindings;

            bindings[binding].name = resource.name + '.' + glsl.get_name(resource.id);
            bindings[binding].type = DescriptorType::uniformBuffer;
            bindings[binding].count = not type.array.empty() ? glsl.get_type(resource.type_id).array[0] : 1;
            bindings[binding].size = (uint32_t)glsl.get_declared_struct_size(glsl.get_type(resource.base_type_id));
            bindings[binding].stageFlags |= (1 << entryPoint.execution_model);

            BVD_LOG(LogGroup::rhi, BD, "Accessing struct {}", bindings[binding].name);

            spirv_cross::SmallVector<spirv_cross::BufferRange> ranges = glsl.get_active_buffer_ranges(resource.id);
            for (spirv_cross::BufferRange& range : ranges) {
                // TODO: make log more readable
                BVD_LOG(LogGroup::rhi, BD, "Accessing member {} #{}, offset {}, size {}", glsl.get_member_name(resource.base_type_id, range.index), range.index, range.offset, range.range);

                UniformVariable& uniformVariable = bindings[binding].variables[glsl.get_member_name(resource.base_type_id, range.index)];
                uniformVariable.offset = range.offset;
                uniformVariable.type = getUniformType(glsl.get_type(type.member_types[range.index]));
            }
        }

        for (spirv_cross::Resource& resource : resources.sampled_images) {
            uint32_t set = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
            uint32_t binding = glsl.get_decoration(resource.id, spv::DecorationBinding);
            spirv_cross::SPIRType const& type = glsl.get_type(resource.type_id);

            std::map<uint32_t, DescriptorSetBinding>& bindings = descriptorSetsInfo.descriptors[set].bindings;

            bindings[binding].name = resource.name;
            bindings[binding].type = DescriptorType::imageSampler;
            bindings[binding].count = not type.array.empty() ? glsl.get_type(resource.type_id).array[0] : 1;
            // bindings[binding].size = (uint32_t)glsl.get_declared_struct_size(glsl.get_type(resource.base_type_id));
            bindings[binding].stageFlags |= (1 << entryPoint.execution_model);
        }
    }

    for (auto& [dsIndex, ds] : descriptorSetsInfo.descriptors) {
        for (auto& [dsbIndex, dsb] : ds.bindings) {
            ds.size += dsb.size;

            if (dsb.type == DescriptorType::uniformBuffer) {
                ++descriptorSetsInfo.numUniformBuffers;
            } else if (dsb.type == DescriptorType::imageSampler) {
                descriptorSetsInfo.numImageSamplers += dsb.count;
            }
        }
    }

    for (auto& [dsIndex, ds] : descriptorSetsInfo.descriptors) {
        BVD_LOG(LogGroup::rhi, BD, "(set = {}) = {{size: {}, numBindings: {}}}", dsIndex, ds.size, ds.bindings.size());

        for (auto& [dsbIndex, dsb] : ds.bindings) {
            BVD_LOG(LogGroup::rhi, BD, "(set = {}, binding = {}) = {{size: {}, stageFlags: {}}}", dsIndex, dsbIndex, dsb.size, dsb.stageFlags);
        }
    }
}

} // namespace bvd::rhi::common
