#pragma once

#include "PixelFormat.hpp"
#include "Buffer.hpp"
#include "Context.hpp"
#include "DynamicRhi.hpp"
#include "GraphicsPipeline.hpp"
#include "GraphicsPipelineStateInit.hpp"
#include "RenderPassInfo.hpp"
#include "RenderTargetsInfo.hpp"
#include "Texture.hpp"
#include "Texture2D.hpp"
#include "Viewport.hpp"
#include "RhiApi.hpp"

#include <cstdint>
#include <array>

namespace bvd {

struct Rhi {
    void selectGraphicsAPI();
    void init();
    void destroy();

    RhiApi currentApi;
};

namespace rhi {

using namespace common;

} // namespace rhi

} // namespace bvd
