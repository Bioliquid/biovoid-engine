#include "Texture.hpp"

#include "FileManager.hpp"
#include "Memory.hpp"
#include "RhiUtils.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace bvd::rhi::common {

TextureCreateInfo::~TextureCreateInfo() {
    if (flagPresent<TextureCreateFlags::external>(flags)) {
        stbi_image_free(pixels);
    }
}

TextureCreateInfo::TextureCreateInfo(char const* path, bool shouldGenerateMips)
    : numMips(1)
    , numSamples(1)
    , flags(TextureCreateFlags::external)
{
    // TODO: move this to initialization
    stbi_set_flip_vertically_on_load(true);

    std::string fullPath = fs::FileManager::getTexturePath(path).string();

    int inWidth, inHeight, inChannels;
    pixels = stbi_load(fullPath.c_str(), &inWidth, &inHeight, &inChannels, STBI_rgb_alpha);

    format = PixelFormat::r8g8b8a8;
    width = inWidth;
    height = inHeight;
    if (shouldGenerateMips) {
        numMips = getMipCount(width, height);
    }
}

TextureCreateInfo::TextureCreateInfo(PixelFormat inFormat, int32_t inWidth, int32_t inHeight, uint8_t* inPixels)
    : format(inFormat)
    , width(inWidth)
    , height(inHeight)
    , numMips(1)
    , numSamples(1)
    , flags(TextureCreateFlags::none)
    , pixels(inPixels)
{}

TextureCreateInfo::TextureCreateInfo(PixelFormat inFormat, int32_t inWidth, int32_t inHeight, TextureCreateFlags::Type inFlags, Color const& inClearColor, uint32_t inNumSamples)
    : format(inFormat)
    , width(inWidth)
    , height(inHeight)
    , numMips(1)
    , numSamples(inNumSamples)
    , flags(inFlags)
    , pixels(nullptr)
    , clearColor(inClearColor)
{}

} // namespace bvd::rhi::common
