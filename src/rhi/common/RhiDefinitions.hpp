#pragma once

#include <cstdint>

namespace bvd {

namespace rhi {

constexpr uint32_t maxNumSimultaneousRenderTargets = 8;
constexpr uint32_t maxNumVertexElements = 16;
constexpr uint32_t maxNumDescriptorSets = 16;
constexpr uint32_t maxNumShaderStages = 4;

enum class RenderTargetLoadAction {
      noAction
    , load
    , clear
    , num
    , numBits = 2
};
static_assert(uint32_t(RenderTargetLoadAction::num) <= (1 << uint32_t(RenderTargetLoadAction::numBits)), "RenderTargetLoadAction: num will not fit on numBits");

enum class RenderTargetStoreAction {
      noAction
    , store
    , multisampleResolve
    , num
    , numBits = 2
};
static_assert(uint32_t(RenderTargetStoreAction::num) <= (1 << uint32_t(RenderTargetStoreAction::numBits)), "RenderTargetStoreAction: num will not fit on numBits");

struct TextureCreateFlags {
    using Type = uint32_t;

    static constexpr Type none = 0;
    // Texture can be used as a render target
    static constexpr Type renderTargetable = 1 << 0;
    // Texture can be used as a resolve target
    static constexpr Type resolveTargetable = 1 << 1;
    // Texture can be used as a depth-stencil target.
    static constexpr Type depthStencilTargetable = 1 << 2;
    // Texture can be used as a shader resource.
    static constexpr Type shaderResource = 1 << 3;
    // Texture is encoded in sRGB gamma space
    static constexpr Type srgb = 1 << 4;
    // Render target texture that will be displayed on screen (back buffer)
    static constexpr Type presentable = 1 << 5;
    // Texture is loaded from filesystem and needs to release its resources
    static constexpr Type external = 1 << 6;
    // Texture data is accessible by the CPU
    static constexpr Type cpuReadback = 1 << 18;
};

} // namespace rhi

} // namespace bvd
