#include "Rhi.hpp"

#include "Logger.hpp"
#include "Exception.hpp"

namespace bvd {

void Rhi::selectGraphicsAPI() {
    rhi::common::DynamicRhi::selectGraphicsAPI(currentApi);

    BVD_LOG(LogGroup::rhi, BI, "Using {} API", currentApi);
}

void Rhi::init() {
    switch (currentApi) {
    case bvd::RhiApi::opengl:
    {
        rhi::gDynamicRhi = rhi::common::createOpenglDynamicRhi();
        break;
    }
    case bvd::RhiApi::vulkan:
    {
        rhi::gDynamicRhi = rhi::common::createVulkanDynamicRhi();
        break;
    }
    default:
    {
        BVD_THROW("Dynamic rhi is not initialized");
        break;
    }
    }

    rhi::gDynamicRhi->init();
}

void Rhi::destroy() {
    rhi::gDynamicRhi->destroy();
    delete rhi::gDynamicRhi;
}

} // namespace bvd
