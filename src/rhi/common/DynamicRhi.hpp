#pragma once

#include "RhiApi.hpp"
#include "GraphicsPipeline.hpp"
#include "Buffer.hpp"
#include "Viewport.hpp"
#include "Texture2D.hpp"
#include "Ref.hpp"

namespace bvd {

namespace renderer { class RenderCommand; }

namespace rhi {

namespace common {

struct Context;

class DynamicRhi {
    friend class renderer::RenderCommand;

public:
    virtual ~DynamicRhi() {}

    virtual void init() = 0;
    virtual void destroy() = 0;

    virtual Ref<Texture2D> getViewportBackBuffer(Viewport*) = 0;
    virtual void advanceFrameForViewportBackBuffer(Viewport*) = 0;

    virtual Ref<Viewport> createViewport(void*, uint32_t, uint32_t, PixelFormat) = 0;
    virtual Ref<GraphicsPipeline> createGraphicsPipeline() = 0;
    virtual Ref<Buffer> createVertexBuffer(uint64_t) = 0;
    virtual Ref<Buffer> createIndexBuffer(uint64_t) = 0;
    virtual Ref<Buffer> createUniformBuffer(uint64_t) = 0;
    virtual Ref<Texture2D> createTexture2D(TextureCreateInfo const&) = 0;

    static void selectGraphicsAPI(RhiApi&);

protected:
    Context* immediateContext = nullptr;
};

DynamicRhi* createOpenglDynamicRhi();
DynamicRhi* createVulkanDynamicRhi();

} // namespace common

/*
* Initialized and destroyed in Rhi
*/
inline common::DynamicRhi* gDynamicRhi{nullptr};

inline Ref<common::Texture2D> getViewportBackBuffer(common::Viewport* viewport) {
    return gDynamicRhi->getViewportBackBuffer(viewport);
}

inline void advanceFrameForViewportBackBuffer(common::Viewport* viewport) {
    gDynamicRhi->advanceFrameForViewportBackBuffer(viewport);
}

inline Ref<common::Viewport> createViewport(void* windowHandle, uint32_t width, uint32_t height, PixelFormat pixelFormat) {
    return gDynamicRhi->createViewport(windowHandle, width, height, pixelFormat);
}

inline Ref<common::GraphicsPipeline> createGraphicsPipeline() {
    return gDynamicRhi->createGraphicsPipeline();
}

inline Ref<common::Buffer> createVertexBuffer(uint64_t size) {
    return gDynamicRhi->createVertexBuffer(size);
}

inline Ref<common::Buffer> createIndexBuffer(uint64_t size) {
    return gDynamicRhi->createIndexBuffer(size);
}

inline Ref<common::Buffer> createUniformBuffer(uint64_t size) {
    return gDynamicRhi->createUniformBuffer(size);
}

//inline Ref<common::Texture2D> createTexture2D(common::TextureCreateInfo const& info) {
//    return gDynamicRhi->createTexture2D(info);
//}

template<typename... Args>
inline Ref<common::Texture2D> createTexture2D(Args&&... args) {
    common::TextureCreateInfo info(std::forward<Args>(args)...);
    return gDynamicRhi->createTexture2D(info);
}

} // namespace rhi

} // namespace bvd
