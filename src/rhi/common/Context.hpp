#pragma once

#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class RenderPassInfo;
class Viewport;
class Buffer;
class Texture;
class Texture2D;
class GraphicsPipeline;

struct Context {
    virtual ~Context() {}

    virtual void beginDrawingViewport(Viewport*) = 0;
    virtual void endDrawingViewport(Viewport*) = 0;

    virtual void beginRenderPass(RenderPassInfo const&) = 0;
    virtual void endRenderPass() = 0;

    virtual void setStreamSource(uint32_t, Buffer const&, uint32_t) = 0;
    virtual void setGraphicsPipeline(GraphicsPipeline const*) = 0;

    virtual void drawPrimitive(uint32_t, uint32_t, uint32_t) = 0;
    virtual void drawIndexedPrimitive(Buffer const& indexBuffer, int32_t baseVertexIndex, int32_t firstInstance, int32_t startIndex, int32_t numPrimitives, int32_t numInstances) = 0;

    virtual void waitIdle() = 0;
};

} // namespace common

} // namespace rhi

} // namespace bvd
