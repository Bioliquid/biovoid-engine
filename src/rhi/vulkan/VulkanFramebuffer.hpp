#pragma once

#include "VulkanTextureView.hpp"
#include "RhiDefinitions.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>
#include <vector>

namespace bvd {

namespace rhi {

namespace common {

class RenderTargetsInfo;

} // namespace common

namespace vulkan {

class Device;
class RenderTargetLayout;
class RenderPass;

class Framebuffer {
public:
    Framebuffer(Device&, common::RenderTargetsInfo const&, RenderTargetLayout const&, RenderPass const&);
    ~Framebuffer();

    Framebuffer(Framebuffer const&) = delete;
    Framebuffer(Framebuffer&&) = delete;
    Framebuffer& operator=(Framebuffer const&) = delete;
    Framebuffer& operator=(Framebuffer&&) = delete;

    bool matches(common::RenderTargetsInfo const&) const;

    inline bool containsRenderTarget(VkImage image) const {
        for (uint32_t index = 0; index < numColorRenderTargets; ++index) {
            if (colorRenderTargetImages[index] == image) {
                return true;
            }
        }

        // return (depthStencilRenderTargetImage == Image);
        return false;
    }

    inline VkFramebuffer getHandle() { return handle; }
    inline VkRect2D getRenderArea() const { return renderArea; }

private:
    Device&                     device;
    VkFramebuffer               handle;
    VkRect2D                    renderArea;

    uint32_t                    numColorRenderTargets;
    VkImage                     colorRenderTargetImages[maxNumSimultaneousRenderTargets];
    VkImage                     colorResolveTargetImages[maxNumSimultaneousRenderTargets];

    // VkImage                    depthStencilRenderTargetImage;

    std::vector<TextureView>    attachmentTextureViews;
    std::vector<VkImageView>    attachmentViewsToDelete;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
