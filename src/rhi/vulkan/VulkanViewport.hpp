#pragma once

#include "VulkanTextureView.hpp"
#include "VulkanTexture2D.hpp"
#include "Ref.hpp"
#include "Viewport.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>
#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

class Context;
class DynamicRhi;
class Device;
class Semaphore;
class SwapChain;
class CommandBuffer;
class Queue;
class Viewport;

class BackBuffer : public Texture2D {
public:
    BackBuffer(Viewport*, PixelFormat, uint32_t, uint32_t, TextureCreateFlags::Type);

    void onGetBackBufferImage(Context&);
    void acquireBackBufferImage(Context&);
    void onAdvanceBackBufferFrame();
    void releaseAcquiredImage();

private:
    Viewport* viewport;
};

class Viewport : public common::Viewport {
    static constexpr uint32_t numBuffers = 3;

    friend class BackBuffer;

public:
    Viewport(DynamicRhi&, Device&, void*, uint32_t, uint32_t, PixelFormat);
    virtual ~Viewport();

    Ref<common::Texture2D> getBackBuffer(Context&);
    void advanceBackBufferFrame();

    void acquireImageIndex();
    void present(Context*, CommandBuffer&, Queue&, Queue&);

    virtual void resize(uint32_t, uint32_t) override;

private:
    void recreateSwapchain();
    void createSwapchain();
    void destroySwapchain();

    DynamicRhi&                 rhi;
    Device&                     device;

    std::vector<VkImage>        backBufferImages;
    std::vector<Semaphore*>     renderingDoneSemaphores;
    Ref<BackBuffer>             backBuffer;

    // Not owned by this class, returned by SwapChain::acquireImageIndex
    Semaphore*                  acquiredSemaphore;

    SwapChain*                  swapChain;
    void*                       windowHandle;

    int32_t                     acquiredImageIndex = -1;

    uint32_t                    width;
    uint32_t                    height;
    PixelFormat                 pixelFormat;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
