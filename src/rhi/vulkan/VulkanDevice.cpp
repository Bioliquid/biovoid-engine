#include "VulkanDevice.hpp"

#include "VulkanQueue.hpp"
#include "VulkanContext.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "VulkanUtils.hpp"
#include "Logger.hpp"
#include "Exception.hpp"
#include "UniformTypeInfo.hpp"
#include "GlobalConfig.hpp"

#include <string>

namespace bvd::rhi::vulkan {

static inline std::string getQueueInfoStr(VkQueueFamilyProperties const& props) {
    std::string info;
    if ((props.queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT) {
        info += " Gfx";
    }
    if ((props.queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT) {
        info += " Compute";
    }
    if ((props.queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT) {
        info += " Xfer";
    }
    if ((props.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT) {
        info += " Sparse";
    }

    return info;
};

static inline GpuVendorId convertToGpuVendorId(uint32_t VendorId) {
    switch ((GpuVendorId)VendorId) {
        case GpuVendorId::notQueried:
            return GpuVendorId::notQueried;

        case GpuVendorId::amd:
        case GpuVendorId::mesa:
        case GpuVendorId::imgTec:
        case GpuVendorId::nvidia:
        case GpuVendorId::arm:
        case GpuVendorId::qualcomm:
        case GpuVendorId::intel:
            return (GpuVendorId)VendorId;

        default:
            break;
    }

    return GpuVendorId::unknown;
}

Device::Device(DynamicRhi& inRhi, VkPhysicalDevice inGpu)
    : rhi(inRhi)
    , gpu(inGpu)
{
    memzero(gpuProps);
    memzero(physicalFeatures);
    memzero(formatProperties);

    // First get the VendorId. We'll have to get properties again after finding out which extensions we want to use
    vkGetPhysicalDeviceProperties(gpu, &gpuProps);
    vendorId = convertToGpuVendorId(gpuProps.vendorID);
    if (vendorId == GpuVendorId::unknown) {
        BVD_LOG(LogGroup::rhi, BE, "Unknown gpu vendor");
    }
}

void Device::queryGpu(uint32_t gpuIndex) {
    vkGetPhysicalDeviceProperties(gpu, &gpuProps);

    BVD_LOG(LogGroup::rhi, BI, "Device {}: {}", gpuIndex, gpuProps.deviceName);

    // NVIDIA
    if (gpuProps.vendorID == 4318) {
        BVD_LOG(LogGroup::rhi, BI, "Driver version: {}.{}.{}.{}",
            (gpuProps.driverVersion >> 22) & 0x3ff,
            (gpuProps.driverVersion >> 14) & 0x0ff,
            (gpuProps.driverVersion >> 6) & 0x0ff,
            (gpuProps.driverVersion) & 0x003f);
    }

#ifdef _WIN32
    else if (gpuProps.vendorID == 0x8086) {
        BVD_LOG(LogGroup::rhi, BI, "Driver version: {}.{}", gpuProps.driverVersion >> 14, (gpuProps.driverVersion) & 0x3fff);
    }
#endif
    else {
        // Vulkan convertion if vendor mapping is not available
        BVD_LOG(LogGroup::rhi, BI, "Driver version: {}.{}.{}",
            (gpuProps.driverVersion >> 22),
            (gpuProps.driverVersion >> 12) & 0x3ff,
            (gpuProps.driverVersion) & 0xfff);
    }

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, nullptr);

    queueFamilyProps.resize(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(gpu, &queueFamilyCount, queueFamilyProps.data());
}

void Device::initGpu(uint32_t gpuIndex) {
    vkGetPhysicalDeviceFeatures(gpu, &physicalFeatures);

    updateMsaaSamples();

    BVD_LOG(LogGroup::rhi, BI, "Using Device {}: Geometry {} Tessellation {}"
        , gpuIndex
        , physicalFeatures.geometryShader
        , physicalFeatures.tessellationShader);

    BVD_LOG(LogGroup::rhi, BI, "Device limits");
    BVD_LOG(LogGroup::rhi, BI, "- MaxVertexInputAttributes: {}", gpuProps.limits.maxVertexInputAttributes);
    BVD_LOG(LogGroup::rhi, BI, "- MaxVertexInputBindings: {}", gpuProps.limits.maxVertexInputBindings);
    BVD_LOG(LogGroup::rhi, BI, "- MaxPushConstantsSize: {}", gpuProps.limits.maxPushConstantsSize);
    BVD_LOG(LogGroup::rhi, BI, "- MaxFramebufferWidth: {}", gpuProps.limits.maxFramebufferWidth);
    BVD_LOG(LogGroup::rhi, BI, "- MaxFramebufferHeight: {}", gpuProps.limits.maxFramebufferHeight);
    BVD_LOG(LogGroup::rhi, BI, "- MaxFramebufferLayers: {}", gpuProps.limits.maxFramebufferLayers);
    BVD_LOG(LogGroup::rhi, BI, "- MaxColorAttachments: {}", gpuProps.limits.maxColorAttachments);
    BVD_LOG(LogGroup::rhi, BI, "- MaxBoundDescriptorSets: {}", gpuProps.limits.maxBoundDescriptorSets);
    BVD_LOG(LogGroup::rhi, BI, "- MaxPerStageDescriptorUniformBuffers: {}", gpuProps.limits.maxPerStageDescriptorUniformBuffers);
    BVD_LOG(LogGroup::rhi, BI, "- MaxDescriptorSetUniformBuffers: {}", gpuProps.limits.maxDescriptorSetUniformBuffers);
    BVD_LOG(LogGroup::rhi, BI, "- MsaaSamples: {}", msaaSamples);

    createDevice();
    setupFormats();
    setupUniformTypes();

    deviceMemoryManager.init(*this);
    stagingManager.init(*this);

    immediateContext = new Context(rhi, *this, gfxQueue);

    if (gfxQueue->getFamilyIndex() != computeQueue->getFamilyIndex()) {
        computeContext = new Context(rhi, *this, computeQueue);
        enableAsyncCompute = true;
    } else {
        computeContext = immediateContext;
    }

    // Initialization of contexts for all rendering threads
    if (false) {
        // contexts.resize(numRenderThreads);
    }
}

void Device::setupPresentQueue(VkSurfaceKHR surface) {
    if (!presentQueue) {
        const auto SupportsPresent = [surface](VkPhysicalDevice PhysicalDevice, Queue* queue) {
            VkBool32 supportsPresent = VK_FALSE;
            const uint32_t familyIndex = queue->getFamilyIndex();

            if (vkGetPhysicalDeviceSurfaceSupportKHR(PhysicalDevice, familyIndex, surface, &supportsPresent) != VK_SUCCESS) {
                BVD_LOG(LogGroup::rhi, BE, "Failed vkGetPhysicalDeviceSurfaceSupportKHR");
            }

            if (supportsPresent) {
                BVD_LOG(LogGroup::rhi, BD, "Queue Family {}: Supports Present", familyIndex);
            }
            return (supportsPresent == VK_TRUE);
        };

        bool gfxSupportsPresent = SupportsPresent(gpu, gfxQueue);
        if (!gfxSupportsPresent) {
            BVD_LOG(LogGroup::rhi, BE, "Cannot find a compatible Vulkan device that supports surface presentation");
        }

        bool computeSupportsPresent = SupportsPresent(gpu, computeQueue);
        if (transferQueue->getFamilyIndex() != gfxQueue->getFamilyIndex() && transferQueue->getFamilyIndex() != computeQueue->getFamilyIndex()) {
            SupportsPresent(gpu, transferQueue);
        }

        if (computeQueue->getFamilyIndex() != gfxQueue->getFamilyIndex() && computeSupportsPresent) {
            BVD_LOG(LogGroup::rhi, BD, "Initializing PresentQueue on ComputeQueue");
            presentOnComputeQueue = (vendorId == GpuVendorId::amd);
            presentQueue = computeQueue;
        } else {
            BVD_LOG(LogGroup::rhi, BD, "Initializing PresentQueue on GfxQueue");
            presentQueue = gfxQueue;
        }
    }
}

void Device::destroy() {
    if (computeContext != immediateContext) {
        delete computeContext;
    }
    computeContext = nullptr;

    delete immediateContext;
    immediateContext = nullptr;

    stagingManager.destroy();

    delete transferQueue;
    delete computeQueue;
    delete gfxQueue;

    vkDestroyDevice(device, nullptr);
}

void Device::waitUntilIdle() {
    vkDeviceWaitIdle(device);
}

void Device::submitCommands(Context& context) {
    CommandBufferManager& cmdManager = context.getCommandBufferManager();
    if (cmdManager.hasPendingUploadCmdBuffer()) {
        cmdManager.submitUploadCmdBuffer();
    }
    if (cmdManager.hasPendingActiveCmdBuffer()) {
        cmdManager.submitActiveCmdBuffer();
    }
    cmdManager.prepareForNewActiveCommandBuffer();
}

void Device::submitCommandsAndFlushGpu() {
    if (computeContext != immediateContext) {
        submitCommands(*computeContext);
    }

    submitCommands(*immediateContext);
}

void Device::notifyDeletedImage(VkImage image, bool isRenderTarget) {
    if (isRenderTarget) {
        immediateContext->notifyDeletedRenderTarget(image);
    }

    immediateContext->notifyDeletedImage(image);
}

void Device::createDevice() {
    VkDeviceCreateInfo createInfo;
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO);

    createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions.data();

#ifdef USE_VALIDATION_LAYERS
    createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
    createInfo.ppEnabledLayerNames = validationLayers.data();
#else
    createInfo.enabledLayerCount = 0;
#endif

    std::vector<VkDeviceQueueCreateInfo> queueFamilyInfos;
    int32_t gfxQueueFamilyIndex = -1;
    int32_t computeQueueFamilyIndex = -1;
    int32_t transferQueueFamilyIndex = -1;
    uint32_t numPriorities = 0;

    BVD_LOG(LogGroup::rhi, BD, "Requested number of queues");
    BVD_LOG(LogGroup::rhi, BD, "Graphics = {}", GlobalConfig::Vulkan::nrofGfxQueues);
    BVD_LOG(LogGroup::rhi, BD, "Compute = {}", GlobalConfig::Vulkan::nrofComputeQueues);
    BVD_LOG(LogGroup::rhi, BD, "Transfer = {}", GlobalConfig::Vulkan::nrofXferQueues);

    for (int32_t familyIndex = 0; familyIndex < std::ssize(queueFamilyProps); ++familyIndex) {
        VkQueueFamilyProperties const& currentProps = queueFamilyProps[familyIndex];

        uint32_t nrofQueues = 0;

        bool isValidQueue = false;
        if ((currentProps.queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT) {
            if (gfxQueueFamilyIndex == -1) {
                gfxQueueFamilyIndex = familyIndex;
                isValidQueue = true;

                nrofQueues = std::min(GlobalConfig::Vulkan::nrofGfxQueues, currentProps.queueCount);
            } else {
                // TODO: Support for multi-queue/choose the best queue!
            }
        }

        if ((currentProps.queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT) {
            if (computeQueueFamilyIndex == -1 && gfxQueueFamilyIndex != familyIndex) {
                nrofQueues = std::min(GlobalConfig::Vulkan::nrofComputeQueues, currentProps.queueCount);

                if (nrofQueues != 0) {
                    computeQueueFamilyIndex = familyIndex;
                    isValidQueue = true;
                }
            }
        }

        if ((currentProps.queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT) {
            // Prefer a non-gfx transfer queue
            if (transferQueueFamilyIndex == -1 && (currentProps.queueFlags & VK_QUEUE_GRAPHICS_BIT) != VK_QUEUE_GRAPHICS_BIT && (currentProps.queueFlags & VK_QUEUE_COMPUTE_BIT) != VK_QUEUE_COMPUTE_BIT) {
                nrofQueues = std::min(GlobalConfig::Vulkan::nrofXferQueues, currentProps.queueCount);
                
                if (nrofQueues != 0) {
                    transferQueueFamilyIndex = familyIndex;
                    isValidQueue = true;
                }
            }
        }

        if (!isValidQueue) {
            BVD_LOG(LogGroup::rhi, BD, "Skipping unnecessary Queue Family {}: {} queues{}", familyIndex, currentProps.queueCount, getQueueInfoStr(currentProps));
            continue;
        }

        VkDeviceQueueCreateInfo& queueCreateInfo = queueFamilyInfos.emplace_back();
        zeroVulkanStruct(queueCreateInfo, VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
        queueCreateInfo.queueFamilyIndex = familyIndex;
        queueCreateInfo.queueCount = nrofQueues;
        numPriorities += nrofQueues;

        BVD_LOG(LogGroup::rhi, BD, "Initializing Queue Family {}: {}/{} queues{}", familyIndex, nrofQueues, currentProps.queueCount, getQueueInfoStr(currentProps));
        // break;
    }

    std::vector<float> queuePriorities(numPriorities);
    float* currentPriority = queuePriorities.data();
    for (VkDeviceQueueCreateInfo& currentQueue : queueFamilyInfos) {
        currentQueue.pQueuePriorities = currentPriority;

        for (int32_t queueIndex = 0; queueIndex < (int32_t)currentQueue.queueCount; ++queueIndex) {
            *currentPriority++ = 1.0f;
        }
    }

    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueFamilyInfos.size());
    createInfo.pQueueCreateInfos = queueFamilyInfos.data();

    // TODO: investigate
    VkPhysicalDeviceFeatures deviceFeatures{};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    deviceFeatures.sampleRateShading = VK_TRUE;

    createInfo.pEnabledFeatures = &deviceFeatures;

    if (vkCreateDevice(gpu, &createInfo, nullptr, &device) != VK_SUCCESS) {
        BVD_THROW("Failed to create logical device!");
    }

    BVD_LOG(LogGroup::rhi, BD, "Initializing GfxQueue: family index {}", gfxQueueFamilyIndex);
    gfxQueue = new Queue(*this, gfxQueueFamilyIndex);

    if (computeQueueFamilyIndex == -1) {
        // If we didn't find a dedicated Queue, use the default one
        computeQueueFamilyIndex = gfxQueueFamilyIndex;
    } else {
        // Dedicated queue
        asyncComputeQueue = true;
    }

    BVD_LOG(LogGroup::rhi, BD, "Initializing ComputeQueue: family index {}", computeQueueFamilyIndex);
    computeQueue = new Queue(*this, computeQueueFamilyIndex);

    if (transferQueueFamilyIndex == -1) {
        // If we didn't find a dedicated Queue, use the default one
        transferQueueFamilyIndex = computeQueueFamilyIndex;
    }

    BVD_LOG(LogGroup::rhi, BD, "Initializing XferQueue: family index {}", transferQueueFamilyIndex);
    transferQueue = new Queue(*this, transferQueueFamilyIndex);
}

void Device::setupFormats() {
    BVD_LOG(LogGroup::rhi, BD, "Performing setup of formats {}", lastFormatIndex);

    for (uint32_t index = 0; index <= lastFormatIndex; ++index) {
        const VkFormat format = static_cast<VkFormat>(index);
        memzero(formatProperties[index]);
        vkGetPhysicalDeviceFormatProperties(gpu, format, &formatProperties[index]);
    }

    static_assert(sizeof(VkFormat) <= sizeof(pixelFormats[PixelFormat::unknown].platformFormat), "PlatformFormat must be increased!");

    for (uint32_t index = 0; index < PixelsFormatInfo::numFormats; ++index) {
        pixelFormats[index].platformFormat = VK_FORMAT_UNDEFINED;
        pixelFormats[index].supported = false;
    }

    // default formats
    mapFormatSupport(PixelFormat::b8g8r8a8, VK_FORMAT_B8G8R8A8_UNORM);
    mapFormatSupport(PixelFormat::r8g8b8a8, VK_FORMAT_R8G8B8A8_UNORM);
    mapFormatSupport(PixelFormat::r32g32b32a32, VK_FORMAT_R32G32B32A32_SFLOAT);

    // TODO: setComponentMapping
}

void Device::mapFormatSupport(PixelFormat engineFormat, VkFormat vulkanFormat) {
    mapFormatSupportWithFallback(engineFormat, vulkanFormat, {});
}

void Device::mapFormatSupportWithFallback(PixelFormat engineFormat, VkFormat vulkanFormat, std::initializer_list<VkFormat> const& fallbackFormats) {
    BVD_LOG(LogGroup::rhi, BD, "PixelFormat {} - {}", static_cast<uint32_t>(engineFormat), vulkanFormat);

    VkFormat supportedTextureFormat = isTextureFormatSupported(vulkanFormat) ? vulkanFormat : VK_FORMAT_UNDEFINED;
    VkFormat supportedBufferFormat = isBufferFormatSupported(vulkanFormat) ? vulkanFormat : VK_FORMAT_UNDEFINED;

    PixelFormatInfo& formatInfo = pixelFormats[engineFormat];
    formatInfo.supported = (supportedTextureFormat != VK_FORMAT_UNDEFINED || supportedBufferFormat != VK_FORMAT_UNDEFINED);
    formatInfo.platformFormat = supportedTextureFormat;

    if (supportedTextureFormat == VK_FORMAT_UNDEFINED) {
        for (VkFormat fallbackFormat : fallbackFormats) {
            if (isTextureFormatSupported(fallbackFormat)) {
                formatInfo.supported = true;
                formatInfo.platformFormat = fallbackFormat;

                BVD_LOG(LogGroup::rhi, BW, "PixelFormat({}) is not supported with VkFormat {}, falling back to VkFormat {}", engineFormat, vulkanFormat, fallbackFormat);
                break;
            }
        }
    }

    if (!formatInfo.supported) {
        BVD_LOG(LogGroup::rhi, BE, "PixelFormat({}) is not supported with VkFormat {}", engineFormat, vulkanFormat);
    }
}

bool Device::isTextureFormatSupported(VkFormat format) const {
    auto arePropertiesSupported = [](VkFormatProperties const& prop) -> bool {
        return (prop.linearTilingFeatures != 0) || (prop.optimalTilingFeatures != 0);
    };

    if (format >= firstFormatIndex && format <= lastFormatIndex) {
        if (not (formatProperties[format].optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)) {
            BVD_LOG(LogGroup::rhi, BW, "PixelFormat({}) does not support linear blitting", format);
        }

        return arePropertiesSupported(formatProperties[format]);
    }

    // TODO: check formats from extensions

    return false;
}

bool Device::isBufferFormatSupported(VkFormat format) const {
    // auto arePropertiesSupported = [](VkFormatProperties const& Prop) -> bool {
    //     return (Prop.bufferFeatures != 0);
    // };

    if (format >= firstFormatIndex && format <= lastFormatIndex) {
        VkFormatProperties const& prop = formatProperties[format];
        return prop.bufferFeatures != 0;
    }

    // TODO: check formats from extensions

    return false;
}

void Device::setupUniformTypes() {
    BVD_LOG(LogGroup::rhi, BD, "Performing setup of uniform types");

    mapUniformInfo(UniformType::vec1, "float", VK_FORMAT_R32_SFLOAT         , 1, 1 * sizeof(float));
    mapUniformInfo(UniformType::vec2, "vec2" , VK_FORMAT_R32G32_SFLOAT      , 2, 2 * sizeof(float));
    mapUniformInfo(UniformType::vec3, "vec3" , VK_FORMAT_R32G32B32_SFLOAT   , 3, 3 * sizeof(float));
    mapUniformInfo(UniformType::vec4, "vec4" , VK_FORMAT_R32G32B32A32_SFLOAT, 4, 4 * sizeof(float));

    mapUniformInfo(UniformType::uvec1, "uint", VK_FORMAT_R32_UINT           , 1, 1 * sizeof(uint32_t));
    mapUniformInfo(UniformType::uvec2, "uvec2", VK_FORMAT_R32G32_UINT       , 2, 2 * sizeof(uint32_t));
    mapUniformInfo(UniformType::uvec3, "uvec3", VK_FORMAT_R32G32B32_UINT    , 3, 3 * sizeof(uint32_t));
    mapUniformInfo(UniformType::uvec4, "uvec4", VK_FORMAT_R32G32B32A32_UINT , 4, 4 * sizeof(uint32_t));

    mapUniformInfo(UniformType::ivec1, "int", VK_FORMAT_R32_SINT            , 1, 1 * sizeof(int32_t));
    mapUniformInfo(UniformType::ivec2, "ivec2", VK_FORMAT_R32G32_SINT       , 2, 2 * sizeof(int32_t));
    mapUniformInfo(UniformType::ivec3, "ivec3", VK_FORMAT_R32G32B32_SINT    , 3, 3 * sizeof(int32_t));
    mapUniformInfo(UniformType::ivec4, "ivec4", VK_FORMAT_R32G32B32A32_SINT , 4, 4 * sizeof(int32_t));

    mapUniformInfo(UniformType::bvec1, "bool", VK_FORMAT_R8_UINT            , 1, 1 * sizeof(bool));
    mapUniformInfo(UniformType::bvec2, "bvec2", VK_FORMAT_R8G8_UINT         , 2, 2 * sizeof(bool));
    mapUniformInfo(UniformType::bvec3, "bvec3", VK_FORMAT_R8G8B8_UINT       , 3, 3 * sizeof(bool));
    mapUniformInfo(UniformType::bvec4, "bvec4", VK_FORMAT_R8G8B8A8_UINT     , 4, 4 * sizeof(bool));

    mapUniformInfo(UniformType::mat3, "mat3", VK_FORMAT_R32G32B32_SFLOAT    , 4 * 4, 3 * 3 * sizeof(float));
    mapUniformInfo(UniformType::mat4, "mat4", VK_FORMAT_R32G32B32A32_SFLOAT , 4 * 4, 4 * 4 * sizeof(float));
}

void Device::mapUniformInfo(UniformType uniformType, char const* name, VkFormat platformFormat, uint32_t elementSize, uint32_t byteSize) {
    BVD_LOG(LogGroup::rhi, BD, "{}: uniform type {} - {}", name, static_cast<uint32_t>(uniformType), platformFormat);

    uniformTypes[uniformType].name = name;
    uniformTypes[uniformType].platformFormat = platformFormat;
    uniformTypes[uniformType].elementSize = elementSize;
    uniformTypes[uniformType].byteSize = byteSize;
}

void Device::updateMsaaSamples() {
    VkSampleCountFlags counts = gpuProps.limits.framebufferColorSampleCounts & gpuProps.limits.framebufferDepthSampleCounts;
    msaaSamples = (counts & VK_SAMPLE_COUNT_64_BIT) ? VK_SAMPLE_COUNT_64_BIT :
                  (counts & VK_SAMPLE_COUNT_32_BIT) ? VK_SAMPLE_COUNT_32_BIT :
                  (counts & VK_SAMPLE_COUNT_16_BIT) ? VK_SAMPLE_COUNT_16_BIT :
                  (counts & VK_SAMPLE_COUNT_8_BIT) ? VK_SAMPLE_COUNT_8_BIT :
                  (counts & VK_SAMPLE_COUNT_4_BIT) ? VK_SAMPLE_COUNT_4_BIT :
                  (counts & VK_SAMPLE_COUNT_2_BIT) ? VK_SAMPLE_COUNT_2_BIT : VK_SAMPLE_COUNT_1_BIT;
}

} // namespace bvd::rhi::vulkan
