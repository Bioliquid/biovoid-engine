#include "VulkanCommandBufferPool.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"
#include "VulkanCommandBuffer.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

CommandBufferPool::CommandBufferPool(Device& inDevice, CommandBufferManager& inManager)
    : handle(VK_NULL_HANDLE)
    , device(inDevice)
    , manager(inManager)
{
    
}

CommandBufferPool::~CommandBufferPool() {
    for (CommandBuffer* cmdBuffer : cmdBuffers) {
        delete cmdBuffer;
    }

    for (CommandBuffer* cmdBuffer : freeCmdBuffers) {
        delete cmdBuffer;
    }

    vkDestroyCommandPool(device.getLogicalHandle(), handle, nullptr);
    handle = VK_NULL_HANDLE;
}

void CommandBufferPool::init(uint32_t queueFamilyIndex) {
    VkCommandPoolCreateInfo createInfo;
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO);
    createInfo.queueFamilyIndex = queueFamilyIndex;
    createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    verifyVulkanResult(vkCreateCommandPool(device.getLogicalHandle(), &createInfo, nullptr, &handle));
}

CommandBuffer* CommandBufferPool::create(bool isUploadOnly) {
    CommandBuffer* cmdBuffer = new CommandBuffer(device, *this, isUploadOnly);
    cmdBuffers.push_back(cmdBuffer);
    BVD_LOG(LogGroup::rhi, BD, "Command pool (id = no id) created a command buffer (new size = {}, isUploadOnly = {})", cmdBuffers.size(), isUploadOnly);
    return cmdBuffer;
}

void CommandBufferPool::refreshFenceStatus(CommandBuffer& skipCmdBuffer) {
    for (CommandBuffer* cmdBuffer : cmdBuffers)
    {
        if (cmdBuffer != &skipCmdBuffer)
        {
            cmdBuffer->refreshFenceStatus();
        }
    }
}

} // namespace bvd::rhi::vulkan
