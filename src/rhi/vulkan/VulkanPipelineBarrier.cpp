#include "VulkanPipelineBarrier.hpp"

#include "VulkanUtils.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

namespace bvd::rhi::vulkan {

static VkAccessFlags getVkAccessMaskForLayout(VkImageLayout layout) {
    switch (layout) {
        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:              return VK_ACCESS_TRANSFER_READ_BIT;
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:              return VK_ACCESS_TRANSFER_WRITE_BIT;
        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:          return VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:                   return VK_ACCESS_MEMORY_READ_BIT;
        case VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT:  return VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT;
        case VK_IMAGE_LAYOUT_GENERAL:                           return 0;
        case VK_IMAGE_LAYOUT_UNDEFINED:                         return 0;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
#if VULKAN_SUPPORTS_SEPARATE_DEPTH_STENCIL_LAYOUTS
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL_KHR:
        case VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL_KHR:
#endif
            return VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

#if VULKAN_SUPPORTS_MAINTENANCE_LAYER2
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR:
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR:
            return VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
#endif

        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL: return VK_ACCESS_SHADER_READ_BIT;
            break;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL:
#if VULKAN_SUPPORTS_SEPARATE_DEPTH_STENCIL_LAYOUTS
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL_KHR:
        case VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL_KHR:
#endif
            return VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;

        default:
            BVD_THROW("Unknown image layout");
    }
}

static VkPipelineStageFlags getVkStageFlagsForLayout(VkImageLayout layout) {
    switch (layout) {
        case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:              return VK_PIPELINE_STAGE_TRANSFER_BIT;
        case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:              return VK_PIPELINE_STAGE_TRANSFER_BIT;
        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:          return VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:          return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:                   return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        case VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT:  return VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT;
        case VK_IMAGE_LAYOUT_GENERAL:                           return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        case VK_IMAGE_LAYOUT_UNDEFINED:                         return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
#if VULKAN_SUPPORTS_SEPARATE_DEPTH_STENCIL_LAYOUTS
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL_KHR:
        case VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL_KHR:
#endif
            return VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;

        case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL:
#if VULKAN_SUPPORTS_MAINTENANCE_LAYER2
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR:
        case VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR:
#endif
#if VULKAN_SUPPORTS_SEPARATE_DEPTH_STENCIL_LAYOUTS
        case VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL_KHR:
        case VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL_KHR:
#endif
            return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;

        default:
            BVD_THROW("Unknown image layout");
    }
}

void PipelineBarrier::addImageLayoutTransition(VkImage image, VkImageLayout srcLayout, VkImageLayout dstLayout, VkImageSubresourceRange const& subresourceRange) {
    zeroVulkanStruct(imageBarrier, VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER);
    imageBarrier.oldLayout = srcLayout;
    imageBarrier.newLayout = dstLayout;
    imageBarrier.srcAccessMask = getVkAccessMaskForLayout(srcLayout);
    imageBarrier.dstAccessMask = getVkAccessMaskForLayout(dstLayout);
    imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageBarrier.image = image;
    imageBarrier.subresourceRange = subresourceRange;
    imageBarrier.pNext = nullptr;

    srcStageMask |= getVkStageFlagsForLayout(srcLayout);
    dstStageMask |= getVkStageFlagsForLayout(dstLayout);
}

void PipelineBarrier::execute(VkCommandBuffer cmdBuffer) {
    vkCmdPipelineBarrier(cmdBuffer, srcStageMask, dstStageMask, 0, 0, nullptr, 0, nullptr, 1, &imageBarrier);
}

VkImageSubresourceRange PipelineBarrier::makeSubresourceRange(VkImageAspectFlags aspectMask, uint32_t firstMip, uint32_t numMips, uint32_t firstLayer, uint32_t numLayers) {
    VkImageSubresourceRange range;
    range.aspectMask = aspectMask;
    range.baseMipLevel = firstMip;
    range.levelCount = numMips;
    range.baseArrayLayer = firstLayer;
    range.layerCount = numLayers;
    return range;
}

} // namespace bvd::rhi::vulkan
