#pragma once

#include "VulkanTextureBase.hpp"
#include "Texture2D.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

// TODO: should be refCounted
class Texture2D : public common::Texture2D, public TextureBase {
public:
    Texture2D(Device& device, common::TextureCreateInfo const& info, bool target = false)
        : common::Texture2D(info)
        , TextureBase(device, info, VK_IMAGE_VIEW_TYPE_2D, 1, 1)
        , swapChainTarget(target)
    {}

    bool isSwapChainTarget() const { return swapChainTarget; }

private:
    bool swapChainTarget = false;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
