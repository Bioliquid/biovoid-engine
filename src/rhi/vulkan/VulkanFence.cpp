#include "VulkanFence.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"

namespace bvd::rhi::vulkan {

Fence::Fence(Device& inDevice, bool createSignaled)
    : device(inDevice)
{
    VkFenceCreateInfo createInfo{};
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_FENCE_CREATE_INFO);
    createInfo.flags = createSignaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;
    verifyVulkanResult(vkCreateFence(device.getLogicalHandle(), &createInfo, nullptr, &handle));
}

Fence::~Fence() {
    vkDestroyFence(device.getLogicalHandle(), handle, nullptr);
    handle = VK_NULL_HANDLE;
}

bool Fence::waitForFence(uint64_t timeInNanoseconds) {
    VkResult result = vkWaitForFences(device.getLogicalHandle(), 1, &handle, VK_TRUE, timeInNanoseconds);

    switch (result) {
        case VK_SUCCESS:
            state = State::signaled;
            return true;
        case VK_TIMEOUT:
            break;
        default:
            verifyVulkanResult(result);
            break;
    }

    return false;
}

void Fence::reset() {
    if (state != State::notReady) {
        verifyVulkanResult(vkResetFences(device.getLogicalHandle(), 1, &handle));
        state = State::notReady;
    }
}

bool Fence::isSignaled() {
    VkResult Result = vkGetFenceStatus(device.getLogicalHandle(), handle);
    switch (Result) {
        case VK_SUCCESS:
            state = State::signaled;
            return true;

        case VK_NOT_READY:
            break;

        default:
            verifyVulkanResult(Result);
            break;
    }

    return false;
}

} // namespace bvd::rhi::vulkan
