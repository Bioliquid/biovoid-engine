#include "VulkanCommandBufferManager.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanContext.hpp"
#include "VulkanDevice.hpp"
#include "VulkanQueue.hpp"
#include "VulkanSemaphore.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

CommandBufferManager::CommandBufferManager(Device& inDevice, Context& inContext)
    : device(inDevice)
    , pool(inDevice, *this)
    , queue(inContext.getQueue())
    , activeCmdBuffer(nullptr)
    , uploadCmdBuffer(nullptr)
    , activeCmdBufferSemaphore(nullptr)
    , uploadCmdBufferSemaphore(nullptr)
{
    pool.init(queue.getFamilyIndex());

    activeCmdBuffer = pool.create(false);
    activeCmdBuffer->begin();
}

CommandBufferManager::~CommandBufferManager() {}

void CommandBufferManager::init() {
    
}

void CommandBufferManager::prepareForNewActiveCommandBuffer() {
    for (CommandBuffer* cmdBuffer : pool.cmdBuffers) {
        cmdBuffer->refreshFenceStatus();
        if (cmdBuffer->state == CommandBuffer::State::readyForBegin || cmdBuffer->state == CommandBuffer::State::needReset) {
            activeCmdBuffer = cmdBuffer;
            activeCmdBuffer->begin();
            return;
        }
    }

    activeCmdBuffer = pool.create(false);
    activeCmdBuffer->begin();
}

void CommandBufferManager::submitActiveCmdBufferFromPresent(Semaphore* signalSemaphore) {
    VkSemaphore activeSemaphore = signalSemaphore->getHandle();
    queue.submit(*activeCmdBuffer, 1, &activeSemaphore);
}

void CommandBufferManager::submitActiveCmdBuffer() {
    if (!activeCmdBuffer->isSubmitted() && activeCmdBuffer->hasBegun()) {
        if (!activeCmdBuffer->isOutsideRenderPass()) {
            activeCmdBuffer->endRenderPass();
        }

        activeCmdBuffer->end();

        queue.submit(*activeCmdBuffer, 0, nullptr);
    }

    activeCmdBuffer = nullptr;
}

CommandBuffer& CommandBufferManager::getActiveCmdBuffer() {
    return *activeCmdBuffer;
}

void CommandBufferManager::submitUploadCmdBuffer() {
    // TODO: add more security
    if (!uploadCmdBuffer->isSubmitted() && uploadCmdBuffer->hasBegun()) {
        uploadCmdBuffer->end();
    
        queue.submit(*uploadCmdBuffer, 0, VK_NULL_HANDLE);
    }

    uploadCmdBuffer = nullptr;
}

CommandBuffer& CommandBufferManager::getUploadCmdBuffer() {
    if (!uploadCmdBuffer) {
        for (CommandBuffer* cmdBuffer : pool.cmdBuffers) {
            cmdBuffer->refreshFenceStatus();
            if (cmdBuffer->isUploadOnly) {

                if (cmdBuffer->state == CommandBuffer::State::readyForBegin || cmdBuffer->state == CommandBuffer::State::needReset) {
                    uploadCmdBuffer = cmdBuffer;
                    uploadCmdBuffer->begin();

                    return *uploadCmdBuffer;
                }
            }
        }

        uploadCmdBuffer = pool.create(true);
        uploadCmdBuffer->begin();
    }

    return *uploadCmdBuffer;
}

} // namespace bvd::rhi::vulkan
