#pragma once

#include "RhiDefinitions.hpp"
#include "PixelFormatInfo.hpp"
#include "Memory.hpp"

#include <vulkan/vulkan.h>
#include <cstdint>
#include <cstring>

namespace bvd::rhi::vulkan {

inline VkImageAspectFlags getAspectMaskFromEngineFormat(PixelFormat /*format*/, bool /*includeStencil*/, bool /*includeDepth = true*/) {
    /*switch (format) {
        case PF_X24_G8:
            return VK_IMAGE_ASPECT_STENCIL_BIT;
        case PF_DepthStencil:
            return (bIncludeDepth ? VK_IMAGE_ASPECT_DEPTH_BIT : 0) | (bIncludeStencil ? VK_IMAGE_ASPECT_STENCIL_BIT : 0);
        case PF_ShadowDepth:
        case PF_D24:
            return VK_IMAGE_ASPECT_DEPTH_BIT;
        default:
            return VK_IMAGE_ASPECT_COLOR_BIT;
    }*/
    return VK_IMAGE_ASPECT_COLOR_BIT;
}

inline VkAttachmentLoadOp rtLoadActionToVk(RenderTargetLoadAction loadAction) {
    switch (loadAction) {
        case bvd::rhi::RenderTargetLoadAction::noAction:    return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        case bvd::rhi::RenderTargetLoadAction::load:        return VK_ATTACHMENT_LOAD_OP_LOAD;
        case bvd::rhi::RenderTargetLoadAction::clear:       return VK_ATTACHMENT_LOAD_OP_CLEAR;
        default:
            break;
    }

    return VK_ATTACHMENT_LOAD_OP_MAX_ENUM;
}

inline VkAttachmentStoreOp rtStoreActionToVk(RenderTargetStoreAction storeAction) {
    switch (storeAction) {
        case bvd::rhi::RenderTargetStoreAction::noAction:
        case bvd::rhi::RenderTargetStoreAction::multisampleResolve: return VK_ATTACHMENT_STORE_OP_DONT_CARE;
        case bvd::rhi::RenderTargetStoreAction::store:              return VK_ATTACHMENT_STORE_OP_STORE;
        default:
            break;
    }

    return VK_ATTACHMENT_STORE_OP_MAX_ENUM;
}

template<typename T>
static void zeroVulkanStruct(T& Struct, int32_t VkStructureType) {
    // Horrible way to coerce the compiler to not have to know what T::sType is so we can have this header not have to include vulkan.h
    (int32_t&)Struct.sType = VkStructureType;
    memzero(((uint8_t*)&Struct) + sizeof(VkStructureType), sizeof(T) - sizeof(VkStructureType));
}

namespace detail {

extern void verifyVulkanResultImpl(VkResult Result, char const* VkFuntion, char const* Filename, uint32_t Line);

} // namespace detail

} // namespace bvd::rhi::vulkan

#define verifyVulkanResult(VkFunction) \
    { \
        const VkResult ScopedResult = VkFunction; \
        if (ScopedResult != VK_SUCCESS) { \
            bvd::rhi::vulkan::detail::verifyVulkanResultImpl(ScopedResult, #VkFunction, __FILE__, __LINE__); \
        } \
    }
