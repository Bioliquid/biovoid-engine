#pragma once

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

struct PipelineBarrier {
    void addImageLayoutTransition(VkImage image, VkImageLayout srcLayout, VkImageLayout dstLayout, VkImageSubresourceRange const& subresourceRange);
    void execute(VkCommandBuffer);

    static VkImageSubresourceRange makeSubresourceRange(VkImageAspectFlags aspectMask, uint32_t firstMip = 0, uint32_t numMips = VK_REMAINING_MIP_LEVELS, uint32_t firstLayer = 0, uint32_t numLayers = VK_REMAINING_ARRAY_LAYERS);

    VkImageMemoryBarrier imageBarrier;
    VkPipelineStageFlags srcStageMask = 0;
    VkPipelineStageFlags dstStageMask = 0;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
