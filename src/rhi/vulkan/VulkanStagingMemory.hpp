#pragma once

#include <vulkan/vulkan.hpp>
#include <cstdint>
#include <vector>

#include "VulkanMemory.hpp"

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

class StagingBuffer {
    friend class StagingManager;

public:
    StagingBuffer(Device&);
    ~StagingBuffer();

    void allocate(void const*, uint64_t);

    VkBuffer getHandle() const { return buffer; }

private:
    Device&                     device;

    VkBuffer                    buffer;
    // TODO: change to MemoryAllocation
    DeviceMemoryAllocation      allocation;
    uint32_t                    bufferSize;
    VkMemoryPropertyFlagBits    properties;
};

class StagingManager {
    struct BufferEntity {
        StagingBuffer*  stagingBuffer;
        bool            isUsed = false;
    };

public:
    void init(Device&);
    void destroy();

    StagingBuffer* acquireBuffer(uint32_t, VkBufferUsageFlags = VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VkMemoryPropertyFlagBits = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    void releaseBuffer(StagingBuffer*);

private:
    Device*                     device;

    std::vector<BufferEntity>   bufferEntities;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
