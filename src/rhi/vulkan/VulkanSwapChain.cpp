#include "VulkanSwapChain.hpp"
#include "VulkanDevice.hpp"
#include "VulkanQueue.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanFence.hpp"
#include "VulkanUtils.hpp"
#include "VulkanWindowsPlatform.hpp"
#include "Logger.hpp"
#include "Memory.hpp"

#include <algorithm>
#include <cmath>

#include <glfw/glfw3.h>

namespace bvd::rhi::vulkan {

SwapChain::SwapChain(Init const& init, PixelFormat& inOutPixelFormat, uint32_t* desiredNumBackBuffers, std::vector<VkImage>& outImages, SwapChainRecreateInfo* recreateInfo)
    : instance(init.instance)
    , device(init.device)
{
    if (recreateInfo && recreateInfo->swapChain != VK_NULL_HANDLE && recreateInfo->surface != VK_NULL_HANDLE) {
        surface = recreateInfo->surface;
        recreateInfo->surface = VK_NULL_HANDLE;
    } else {
        VkUtils::createSurface(instance, init.windowHandle, surface);
    }

    VkColorSpaceKHR requestedColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;

    // Find Pixel format for presentable images
    VkSurfaceFormatKHR currentFormat;
    memzero(currentFormat);
    {
        uint32_t numFormats;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device.getPhysicalHandle(), surface, &numFormats, nullptr);

        std::vector<VkSurfaceFormatKHR> formats(numFormats);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device.getPhysicalHandle(), surface, &numFormats, formats.data());

        if (inOutPixelFormat != PixelFormat::unknown) {
            bool found = false;
            if (device.pixelFormats[inOutPixelFormat].supported) {
                VkFormat requestedFormat = (VkFormat)device.pixelFormats[inOutPixelFormat].platformFormat;

                for (VkSurfaceFormatKHR format : formats) {
                    if (format.format == requestedFormat) {
                        currentFormat = format;
                        found = true;

                        // TODO: check for requested color space
                        if (currentFormat.colorSpace == requestedColorSpace) {
                            break;
                        }
                    }
                }

                if (!found) {
                    BVD_LOG(LogGroup::rhi, BW, "Requested PixelFormat {} is not supported by this swapchain", inOutPixelFormat);
                    inOutPixelFormat = PixelFormat::unknown;
                }
            } else {
                BVD_LOG(LogGroup::rhi, BW, "Requested PixelFormat {} is not supported by this vulkan implementation", inOutPixelFormat);
                inOutPixelFormat = PixelFormat::unknown;
            }
        }

        if (inOutPixelFormat == PixelFormat::unknown) {
            for (VkSurfaceFormatKHR format : formats) {
                for (uint32_t pixelFormatIndex = 0; pixelFormatIndex < PixelsFormatInfo::numFormats; ++pixelFormatIndex) {
                    if (format.format == (VkFormat)device.pixelFormats[pixelFormatIndex].platformFormat && format.colorSpace == requestedColorSpace) {
                        inOutPixelFormat = static_cast<PixelFormat>(pixelFormatIndex);
                        currentFormat = format;
                    }
                }
            }
        }

        if (inOutPixelFormat == PixelFormat::unknown) {
            BVD_LOG(LogGroup::rhi, BE, "Can't find a proper pixel format for the swapchain");
        }
    }

    device.setupPresentQueue(surface);

    // Fetch present mode
    VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
    if (supportsQuerySurfaceProperties()) {
        uint32_t numFoundPresentModes = 0;
        verifyVulkanResult(vkGetPhysicalDeviceSurfacePresentModesKHR(device.getPhysicalHandle(), surface, &numFoundPresentModes, nullptr));
        BVD_CHECK(numFoundPresentModes > 0, "numFoundPresentModes > 0");

        std::vector<VkPresentModeKHR> foundPresentModes(numFoundPresentModes);
        verifyVulkanResult(vkGetPhysicalDeviceSurfacePresentModesKHR(device.getPhysicalHandle(), surface, &numFoundPresentModes, foundPresentModes.data()));

        BVD_LOG(LogGroup::rhi, BD, "Found {} Surface present modes:", numFoundPresentModes);

        bool bFoundPresentModeMailbox = false;
        bool bFoundPresentModeImmediate = false;
        bool bFoundPresentModeFIFO = false;

        for (size_t i = 0; i < numFoundPresentModes; ++i) {
            switch (foundPresentModes[i]) {
                case VK_PRESENT_MODE_MAILBOX_KHR:
                    bFoundPresentModeMailbox = true;
                    BVD_LOG(LogGroup::rhi, BD, "- VK_PRESENT_MODE_MAILBOX_KHR ({})", (int32_t)VK_PRESENT_MODE_MAILBOX_KHR);
                    break;
                case VK_PRESENT_MODE_IMMEDIATE_KHR:
                    bFoundPresentModeImmediate = true;
                    BVD_LOG(LogGroup::rhi, BD, "- VK_PRESENT_MODE_IMMEDIATE_KHR ({})", (int32_t)VK_PRESENT_MODE_IMMEDIATE_KHR);
                    break;
                case VK_PRESENT_MODE_FIFO_KHR:
                    bFoundPresentModeFIFO = true;
                    BVD_LOG(LogGroup::rhi, BD, "- VK_PRESENT_MODE_FIFO_KHR ({})", (int32_t)VK_PRESENT_MODE_FIFO_KHR);
                    break;
                case VK_PRESENT_MODE_FIFO_RELAXED_KHR:
                    BVD_LOG(LogGroup::rhi, BD, "- VK_PRESENT_MODE_FIFO_RELAXED_KHR ({})", (int32_t)VK_PRESENT_MODE_FIFO_RELAXED_KHR);
                    break;
                default:
                    BVD_LOG(LogGroup::rhi, BD, "- VkPresentModeKHR {}", (int32_t)foundPresentModes[i]);
                    break;
            }
        }

        int32_t RequestedPresentMode = -1;

        if (RequestedPresentMode == -1) {
            // Until FVulkanViewport::Present honors SyncInterval, we need to disable vsync for the spectator window if using an HMD.
            // const bool bDisableVsyncForHMD = (FVulkanDynamicRHI::HMDVulkanExtensions.IsValid()) ? FVulkanDynamicRHI::HMDVulkanExtensions->ShouldDisableVulkanVSync() : false;
            // this should be true in case of vr
            const bool disableVsyncForHMD = false;
            // TODO: make control from above
            const bool lockToVsync = true;

            if (bFoundPresentModeImmediate && (disableVsyncForHMD || !lockToVsync)) {
                presentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
            } else if (bFoundPresentModeMailbox) {
                presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
            } else if (bFoundPresentModeFIFO) {
                presentMode = VK_PRESENT_MODE_FIFO_KHR;
            } else {
                BVD_LOG(LogGroup::rhi, BE, "Couldn't find desired PresentMode! Using {}", static_cast<int32_t>(foundPresentModes[0]));
                presentMode = foundPresentModes[0];
            }
        }

        BVD_LOG(LogGroup::rhi, BI, "Selected VkPresentModeKHR mode {}", presentMode);
    }

    // Check the surface properties and formats
    VkSurfaceCapabilitiesKHR surfaceProperties;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.getPhysicalHandle(), surface, &surfaceProperties);

    VkSurfaceTransformFlagBitsKHR preTransform;
    // if (surfaceProperties.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
    //     preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    // } else {
        preTransform = surfaceProperties.currentTransform;
    // }

    VkCompositeAlphaFlagBitsKHR compositeAlpha = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
    if (surfaceProperties.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR) {
        compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    }

    // 0 means no limit, so use the requested number
    uint32_t desiredNumBuffers = surfaceProperties.maxImageCount > 0 ? std::clamp(*desiredNumBackBuffers, surfaceProperties.minImageCount, surfaceProperties.maxImageCount) : *desiredNumBackBuffers;

    uint32_t sizeX = supportsQuerySurfaceProperties() ? (surfaceProperties.currentExtent.width == 0xFFFFFFFF ? init.width : surfaceProperties.currentExtent.width) : init.width;
    uint32_t sizeY = supportsQuerySurfaceProperties() ? (surfaceProperties.currentExtent.height == 0xFFFFFFFF ? init.height : surfaceProperties.currentExtent.height) : init.height;
    
    VkSwapchainCreateInfoKHR createInfo{};
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR);
    createInfo.surface = surface;
    createInfo.minImageCount = desiredNumBuffers;
    createInfo.imageFormat = currentFormat.format;
    createInfo.imageColorSpace = currentFormat.colorSpace;
    createInfo.imageExtent.width = sizeX;
    createInfo.imageExtent.height = sizeY;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    createInfo.preTransform = preTransform;
    createInfo.imageArrayLayers = 1;
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.presentMode = presentMode;
    createInfo.oldSwapchain = VK_NULL_HANDLE;
    if (recreateInfo != nullptr) {
        createInfo.oldSwapchain = recreateInfo->swapChain;
    }
    createInfo.clipped = VK_TRUE;
    createInfo.compositeAlpha = compositeAlpha;

    *desiredNumBackBuffers = desiredNumBuffers;

    {
        // TODO: crappy workaround
        if (createInfo.imageExtent.width == 0) {
            createInfo.imageExtent.width = init.width;
        }
        if (createInfo.imageExtent.height == 0) {
            createInfo.imageExtent.height = init.height;
        }
    }

    // this is necessary only when VK_QCOM_render_pass_transform which is when device can rotate line a phone
    if (false) {
        imageFormat = createInfo.imageFormat;

        if (createInfo.preTransform == VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR ||
            createInfo.preTransform == VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR) {
            std::swap(createInfo.imageExtent.width, createInfo.imageExtent.height);
        }
    }

    VkBool32 supportsPresent;
    verifyVulkanResult(vkGetPhysicalDeviceSurfaceSupportKHR(device.getPhysicalHandle(), device.getPresentQueue().getFamilyIndex(), surface, &supportsPresent));
    BVD_CHECK(supportsPresent, "supportsPresent");

    BVD_LOG(LogGroup::rhi, BI, "Creating new VK swapchain with present mode {}, format {}, color space {}, num images {}"
        , static_cast<uint32_t>(createInfo.presentMode)
        , static_cast<uint32_t>(createInfo.imageFormat)
        , static_cast<uint32_t>(createInfo.imageColorSpace)
        , static_cast<uint32_t>(createInfo.minImageCount));

    verifyVulkanResult(vkCreateSwapchainKHR(device.getLogicalHandle(), &createInfo, nullptr, &swapChain));

    if (recreateInfo != nullptr) {
        if (recreateInfo->swapChain != VK_NULL_HANDLE) {
            vkDestroySwapchainKHR(device.getLogicalHandle(), recreateInfo->swapChain, nullptr);
            recreateInfo->swapChain = VK_NULL_HANDLE;
        }
        if (recreateInfo->surface != VK_NULL_HANDLE) {
            vkDestroySurfaceKHR(instance, recreateInfo->surface, nullptr);
            recreateInfo->surface = VK_NULL_HANDLE;
        }
    }

    internalWidth = std::min(init.width, createInfo.imageExtent.width);
    internalHeight = std::min(init.height, createInfo.imageExtent.height);
    internalFullScreen = init.isFullscreen;

    uint32_t numSwapChainImages;
    verifyVulkanResult(vkGetSwapchainImagesKHR(device.getLogicalHandle(), swapChain, &numSwapChainImages, nullptr));

    outImages.resize(numSwapChainImages);
    verifyVulkanResult(vkGetSwapchainImagesKHR(device.getLogicalHandle(), swapChain, &numSwapChainImages, outImages.data()));

#if VULKAN_USE_IMAGE_ACQUIRE_FENCES
    imageAcquiredFences.reserve(numSwapChainImages);
    for (uint32_t index = 0; index < numSwapChainImages; ++index) {
        imageAcquiredFences.emplace_back(new Fence(device));
    }
#endif

    imageAcquiredSemaphores.reserve(desiredNumBuffers);
    for (uint32_t index = 0; index < desiredNumBuffers; ++index) {
        imageAcquiredSemaphores.emplace_back(new Semaphore(device));
    }
}

void SwapChain::destroy() {
    vkDestroySwapchainKHR(device.getLogicalHandle(), swapChain, nullptr);
    swapChain = VK_NULL_HANDLE;

#if VULKAN_USE_IMAGE_ACQUIRE_FENCES
    for (Fence* fence : imageAcquiredFences) {
        delete fence;
    }
#endif

    for (Semaphore* semaphore : imageAcquiredSemaphores) {
        delete semaphore;
    }

    vkDestroySurfaceKHR(instance, surface, nullptr);
    surface = VK_NULL_HANDLE;
}

SwapChain::Status SwapChain::acquireImageIndex(Semaphore*& outSemaphore, int32_t* outImageIndex) {
    // Get the index of the next swapchain image we should render to.
    // We'll wait with an "infinite" timeout, the function will block until an image is ready.
    // The ImageAcquiredSemaphore[ImageAcquiredSemaphoreIndex] will get signaled when the image is ready (upon function return).
    uint32_t imageIndex = 0;
    const int32_t prevSemaphoreIndex = semaphoreIndex;
    semaphoreIndex = (semaphoreIndex + 1) % imageAcquiredSemaphores.size();

#if VULKAN_USE_IMAGE_ACQUIRE_FENCES
    imageAcquiredFences[semaphoreIndex]->reset();
    const VkFence acquiredFence = imageAcquiredFences[semaphoreIndex]->getHandle();
#else
    const VkFence acquiredFence = VK_NULL_HANDLE;
#endif

    VkResult result = vkAcquireNextImageKHR(device.getLogicalHandle(), swapChain, UINT64_MAX, imageAcquiredSemaphores[semaphoreIndex]->getHandle(), acquiredFence, &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        semaphoreIndex = prevSemaphoreIndex;
        return Status::outOfDate;
    }

    if (result == VK_ERROR_SURFACE_LOST_KHR) {
        semaphoreIndex = prevSemaphoreIndex;
        BVD_LOG(LogGroup::rhi, BE, "Surface lost");
        return Status::surfaceLost;
    }

    outSemaphore = imageAcquiredSemaphores[semaphoreIndex];

    currentImageIndex = imageIndex;

    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        BVD_LOG(LogGroup::rhi, BE, "Failed to acquire swap chain image");
    }

#if VULKAN_USE_IMAGE_ACQUIRE_FENCES
    bool waitResult = imageAcquiredFences[semaphoreIndex]->waitForFence(UINT64_MAX);
    check(waitResult);
#endif

    *outImageIndex = currentImageIndex;

    return Status::success;
}

SwapChain::Status SwapChain::present(Queue& gfxQueue, Queue& presentQueue, Semaphore* backBufferRenderingDoneSemaphore) {
    (void)gfxQueue;

    VkPresentInfoKHR presentInfo;
    zeroVulkanStruct(presentInfo, VK_STRUCTURE_TYPE_PRESENT_INFO_KHR);

    VkSemaphore Semaphore = VK_NULL_HANDLE;
    if (backBufferRenderingDoneSemaphore) {
        presentInfo.waitSemaphoreCount = 1;
        Semaphore = backBufferRenderingDoneSemaphore->getHandle();
        presentInfo.pWaitSemaphores = &Semaphore;
    }
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &swapChain;
    presentInfo.pImageIndices = (uint32_t*)&currentImageIndex;

    VkResult result = vkQueuePresentKHR(presentQueue.getHandle(), &presentInfo);

    currentImageIndex = -1;

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        return Status::outOfDate;
    }

    if (result == VK_ERROR_SURFACE_LOST_KHR) {
        BVD_LOG(LogGroup::rhi, BE, "Surface lost");
        return Status::surfaceLost;
    }

    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        verifyVulkanResult(result);
    }

    return Status::success;
}

} // namespace bvd::rhi::vulkan
