#include "VulkanGraphicsPipeline.hpp"
#include "VulkanDevice.hpp"
#include "VulkanRenderPass.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "VulkanBuffer.hpp"
#include "VulkanTexture2D.hpp"
#include "VulkanDynamicRhi.hpp"
#include "VulkanUtils.hpp"
#include "VertexInputRate.hpp"
#include "Logger.hpp"
#include "UniformTypeInfo.hpp"
#include "RhiDefinitions.hpp"

#include <vector>

namespace bvd::rhi::vulkan {

static VkPrimitiveTopology castEngineToVkPrimitiveTopology(PrimitiveTopology topology) {
    switch (topology) {
        case bvd::PrimitiveTopology::points:            return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
        case bvd::PrimitiveTopology::lines:             return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
        case bvd::PrimitiveTopology::lineStrips:        return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
        case bvd::PrimitiveTopology::triangles:         return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        case bvd::PrimitiveTopology::triangleStrips:    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
        case bvd::PrimitiveTopology::triangleFans:      return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
        default:                                        return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    }
}

GraphicsPipeline::GraphicsPipeline(Device& inDevice)
    : device(inDevice)
{

}

GraphicsPipeline::~GraphicsPipeline() {

}

void GraphicsPipeline::loadShaders(common::GraphicsPipelineStateInit const& init) {
    const size_t numShaderSources = init.shaderSources.size();

    std::vector<VkShaderModule> shaderModules(numShaderSources);
    std::vector<VkPipelineShaderStageCreateInfo> shaderStages(numShaderSources);

    // This is necessary to keep pointer to pName of VkPipelineShaderStageCreateInfo alive
    std::vector<spirv_cross::EntryPoint> entryPoints(numShaderSources);

    for (size_t index = 0; index < numShaderSources; ++index) {
        VkShaderModule& shaderModule = shaderModules[index];
        common::ShaderSource const& shaderSource = init.shaderSources[index];
        spirv_cross::CompilerGLSL& compiler = compilers[index];

        entryPoints[index] = compiler.get_entry_points_and_stages().front();

        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = shaderSource.source.size() * sizeof(uint32_t);
        createInfo.pCode = shaderSource.source.data();

        verifyVulkanResult(vkCreateShaderModule(device.getLogicalHandle(), &createInfo, nullptr, &shaderModule));

        VkPipelineShaderStageCreateInfo& shaderStageInfo = shaderStages[index];
        zeroVulkanStruct(shaderStageInfo, VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO);
        // TODO: improve safety
        shaderStageInfo.stage = static_cast<VkShaderStageFlagBits>(1 << entryPoints[index].execution_model);
        shaderStageInfo.module = shaderModule;
        shaderStageInfo.pName = entryPoints[index].name.c_str();
    }

    // Vertex input. This structure is mandatory even without vertex attributes
    VkPipelineVertexInputStateCreateInfo vertexInputInfo;
    zeroVulkanStruct(vertexInputInfo, VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO);
    
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
    std::vector<VkVertexInputBindingDescription> bindingDescriptions(vertexInputLayout.size());
    // TODO: delete hardcode
    attributeDescriptions.reserve(10);

    for (uint32_t vertexInputBindingIndex = 0; vertexInputBindingIndex < vertexInputLayout.size(); ++vertexInputBindingIndex) {
        VkVertexInputBindingDescription& bindingDescription = bindingDescriptions[vertexInputBindingIndex];
        VertexInputBinding& vertexInputBinding = vertexInputLayout[vertexInputBindingIndex];

        bindingDescription.binding = vertexInputBindingIndex;
        bindingDescription.stride = vertexInputBinding.stride;
        bindingDescription.inputRate = VertexInputRate::isVertex(vertexInputBindingIndex) ? VK_VERTEX_INPUT_RATE_VERTEX : VK_VERTEX_INPUT_RATE_INSTANCE;

        for (uint32_t vertexInputAttributeIndex = 0; vertexInputAttributeIndex < vertexInputBinding.attributes.size(); ++vertexInputAttributeIndex) {
            VkVertexInputAttributeDescription& attributeDescription = attributeDescriptions.emplace_back();
            VertexInputAttribute& vertexInputAttribute = vertexInputBinding.attributes[vertexInputAttributeIndex];

            attributeDescription.binding = vertexInputBindingIndex;
            attributeDescription.location = vertexInputAttribute.location;
            attributeDescription.format = static_cast<VkFormat>(device.uniformTypes[vertexInputAttribute.uniformType].platformFormat);
            attributeDescription.offset = vertexInputAttribute.offset;
        }
    }

    vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescriptions.size());
    vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();
    vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

    VkPipelineInputAssemblyStateCreateInfo inputAssembly;
    zeroVulkanStruct(inputAssembly, VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO);
    inputAssembly.topology = castEngineToVkPrimitiveTopology(init.primitiveTopology);
    
    VkPipelineViewportStateCreateInfo viewportState{};
    zeroVulkanStruct(viewportState, VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO);
    viewportState.viewportCount = 1;
    viewportState.scissorCount = 1;

    VkPipelineDynamicStateCreateInfo dynamicState;
    zeroVulkanStruct(dynamicState, VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO);
    VkDynamicState dynamicStatesEnabled[dynamicStateRangeSize];
    dynamicState.pDynamicStates = dynamicStatesEnabled;
    memzero(dynamicStatesEnabled);
    dynamicStatesEnabled[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_VIEWPORT;
    dynamicStatesEnabled[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_SCISSOR;

    VkPipelineRasterizationStateCreateInfo rasterizer;
    zeroVulkanStruct(rasterizer, VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO);
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    // TODO: should at least be VK_CULL_MODE_BACK_BIT when Y is flipped (final image)
    rasterizer.cullMode = VK_CULL_MODE_NONE;
    // rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;

    VkPipelineMultisampleStateCreateInfo multisampling;
    zeroVulkanStruct(multisampling, VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO);
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = static_cast<VkSampleCountFlagBits>(init.numSamples);
    // multisampling.alphaToCoverageEnable = VK_TRUE;
    multisampling.sampleShadingEnable = VK_TRUE;
    multisampling.minSampleShading = .6f;

    // TODO: support multiple render targets
    VkPipelineColorBlendAttachmentState colorBlendAttachment[maxNumSimultaneousRenderTargets];
    memzero(colorBlendAttachment);
    colorBlendAttachment[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    // colorBlendAttachment[0].blendEnable = VK_FALSE;
    colorBlendAttachment[0].blendEnable = VK_TRUE;
    /*colorBlendAttachment[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;*/
    colorBlendAttachment[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment[0].srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment[0].dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment[0].alphaBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment[0].colorBlendOp = VK_BLEND_OP_ADD;


    VkPipelineColorBlendStateCreateInfo colorBlending;
    zeroVulkanStruct(colorBlending, VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO);
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = init.numColorRenderTargets;
    colorBlending.pAttachments = colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    createPipelineLayout();

    VkPipelineLayoutCreateInfo pipelineLayoutInfo;
    zeroVulkanStruct(pipelineLayoutInfo, VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO);
    pipelineLayoutInfo.setLayoutCount = (uint32_t)descriptorSetLayouts.size();
    pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts.data();
    pipelineLayoutInfo.pushConstantRangeCount = 0;

    verifyVulkanResult(vkCreatePipelineLayout(device.getLogicalHandle(), &pipelineLayoutInfo, nullptr, &pipelineLayout));

    VkGraphicsPipelineCreateInfo pipelineInfo;
    zeroVulkanStruct(pipelineInfo, VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO);
    pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
    pipelineInfo.pStages = shaderStages.data();
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = &dynamicState;
    pipelineInfo.layout = pipelineLayout;
    pipelineInfo.renderPass = device.getImmediateContext().prepareRenderPassForGraphicsPipelineCreation(init)->getHandle();
    // TODO: custom subpass and numSamples from color target
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

    verifyVulkanResult(vkCreateGraphicsPipelines(device.getLogicalHandle(), VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline));

    for (VkShaderModule shaderModule : shaderModules) {
        vkDestroyShaderModule(device.getLogicalHandle(), shaderModule, nullptr);
    }

    if (descriptorSetsInfo.hasUniforms()) {
        uint32_t numPoolSizes = 0;
        std::array<VkDescriptorPoolSize, 2> poolSizes;

        if (descriptorSetsInfo.numUniformBuffers) {
            VkDescriptorPoolSize& poolSize = poolSizes[numPoolSizes];
            poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            poolSize.descriptorCount = descriptorSetsInfo.numUniformBuffers;

            ++numPoolSizes;
        }

        if (descriptorSetsInfo.numImageSamplers) {
            VkDescriptorPoolSize& poolSize = poolSizes[numPoolSizes];
            poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            poolSize.descriptorCount = descriptorSetsInfo.numImageSamplers;

            ++numPoolSizes;
        }

        VkDescriptorPoolCreateInfo poolInfo;
        zeroVulkanStruct(poolInfo, VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO);
        poolInfo.poolSizeCount = numPoolSizes;
        poolInfo.pPoolSizes = poolSizes.data();
        poolInfo.maxSets = (uint32_t)descriptorSetsInfo.descriptors.size();

        verifyVulkanResult(vkCreateDescriptorPool(device.getLogicalHandle(), &poolInfo, nullptr, &descriptorPool));

        descriptorSets.resize(descriptorSetsInfo.descriptors.size());

        VkDescriptorSetAllocateInfo allocInfo;
        zeroVulkanStruct(allocInfo, VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO);
        allocInfo.descriptorPool = descriptorPool;
        allocInfo.descriptorSetCount = (uint32_t)descriptorSetsInfo.descriptors.size();
        allocInfo.pSetLayouts = descriptorSetLayouts.data();

        verifyVulkanResult(vkAllocateDescriptorSets(device.getLogicalHandle(), &allocInfo, descriptorSets.data()));

        uniformBuffers.resize(descriptorSetsInfo.descriptors.size());
        for (uint32_t dsIndex = 0; dsIndex < descriptorSetsInfo.descriptors.size(); ++dsIndex) {
            DescriptorSet& ds = descriptorSetsInfo.descriptors[dsIndex];

            uniformBuffers[dsIndex].resize(ds.bindings.size());
            for (uint32_t dsbIndex = 0; dsbIndex < ds.bindings.size(); ++dsbIndex) {
                if (ds.bindings[dsbIndex].type == DescriptorType::uniformBuffer) {
                    uniformBuffers[dsIndex][dsbIndex] = static_cast<Buffer*>(device.getImmediateContext().getRhi().createUniformBuffer(ds.bindings[dsbIndex].size).get());
                }
            }
        }

        descriptorSetWrites.descriptorBufferInfo.reserve(descriptorSetsInfo.numUniformBuffers);
        descriptorSetWrites.descriptorImageInfo.reserve(descriptorSetsInfo.numImageSamplers);
        descriptorSetWrites.descriptorWrites.reserve(descriptorSetsInfo.numUniformBuffers + descriptorSetsInfo.numImageSamplers);

        if (descriptorSetsInfo.numUniformBuffers > 0) {
            for (uint32_t dsIndex = 0; dsIndex < descriptorSetsInfo.descriptors.size(); ++dsIndex) {
                DescriptorSet& ds = descriptorSetsInfo.descriptors[dsIndex];

                for (uint32_t dsbIndex = 0; dsbIndex < ds.bindings.size(); ++dsbIndex) {
                    if (ds.bindings[dsbIndex].type != DescriptorType::uniformBuffer) {
                        continue;
                    }

                    VkDescriptorBufferInfo& bufferInfo = descriptorSetWrites.descriptorBufferInfo.emplace_back();
                    bufferInfo.buffer = uniformBuffers[dsIndex][dsbIndex]->getHandle();
                    bufferInfo.offset = 0;
                    bufferInfo.range = ds.bindings[dsbIndex].size;

                    VkWriteDescriptorSet& descriptorWrite = descriptorSetWrites.descriptorWrites.emplace_back();
                    zeroVulkanStruct(descriptorWrite, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET);
                    descriptorWrite.dstSet = descriptorSets[dsIndex];
                    descriptorWrite.dstBinding = dsbIndex;
                    descriptorWrite.dstArrayElement = 0;
                    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    descriptorWrite.descriptorCount = 1;
                    descriptorWrite.pBufferInfo = &bufferInfo;
                }
            }

            vkUpdateDescriptorSets(device.getLogicalHandle(), descriptorSetsInfo.numUniformBuffers, descriptorSetWrites.descriptorWrites.data(), 0, nullptr);
        }
    }
}

void GraphicsPipeline::bind() const {
    CommandBuffer& cmdBuffer = device.getImmediateContext().getCommandBufferManager().getActiveCmdBuffer();
    vkCmdBindPipeline(cmdBuffer.getHandle(), VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

    if (descriptorSetsInfo.hasUniforms()) {
        vkCmdBindDescriptorSets(cmdBuffer.getHandle(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, (uint32_t)descriptorSets.size(), descriptorSets.data(), 0, nullptr);
    }
}

void GraphicsPipeline::destroy() {
    vkDestroyPipeline(device.getLogicalHandle(), graphicsPipeline, nullptr);
    vkDestroyPipelineLayout(device.getLogicalHandle(), pipelineLayout, nullptr);

    for (std::vector<Ref<Buffer>>& dsUniformBuffer : uniformBuffers) {
        for (Ref<Buffer>& dsbUniformBuffer : dsUniformBuffer) {
            // TODO: maybe just make it a map
            if (dsbUniformBuffer) {
                dsbUniformBuffer->destroy();
            }
        }
    }

    if (descriptorSetsInfo.hasUniforms()) {
        vkDestroyDescriptorPool(device.getLogicalHandle(), descriptorPool, nullptr);
    }

    for (VkDescriptorSetLayout& descriptorSetLayout : descriptorSetLayouts) {
        vkDestroyDescriptorSetLayout(device.getLogicalHandle(), descriptorSetLayout, nullptr);
    }
}

common::Device const& GraphicsPipeline::getDevice() const {
    return static_cast<common::Device const&>(device);
}

void GraphicsPipeline::createPipelineLayout() {
    if (!descriptorSetsInfo.hasUniforms()) {
        return;
    }

    std::vector<std::vector<VkDescriptorSetLayoutBinding>> layoutBindings(descriptorSetsInfo.descriptors.size());

    for (uint32_t dsIndex = 0; dsIndex < descriptorSetsInfo.descriptors.size(); ++dsIndex) {
        DescriptorSet& ds = descriptorSetsInfo.descriptors[dsIndex];

        for (uint32_t dsbIndex = 0; dsbIndex < ds.bindings.size(); ++dsbIndex) {
            DescriptorSetBinding& dsb = ds.bindings[dsbIndex];

            VkDescriptorSetLayoutBinding& layoutBinding = layoutBindings[dsIndex].emplace_back();

            layoutBinding.binding = dsbIndex;
            // TODO: remove switch
            switch (dsb.type) {
                case DescriptorType::uniformBuffer: 
                    layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
                    break;
                case DescriptorType::imageSampler:
                    layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
                    break;
            }
            layoutBinding.descriptorCount = dsb.count;
            layoutBinding.stageFlags = dsb.stageFlags;
            layoutBinding.pImmutableSamplers = nullptr;
        }
    }
        
    descriptorSetLayouts.resize(descriptorSetsInfo.descriptors.size());

    for (uint32_t dsIndex = 0; dsIndex < descriptorSetsInfo.descriptors.size(); ++dsIndex) {
        VkDescriptorSetLayoutCreateInfo layoutInfo;
        zeroVulkanStruct(layoutInfo, VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO);
        layoutInfo.pNext = nullptr;
        layoutInfo.bindingCount = (uint32_t)descriptorSetsInfo.descriptors[dsIndex].bindings.size();
        layoutInfo.pBindings = layoutBindings[dsIndex].data();
        layoutInfo.flags = 0;

        if (vkCreateDescriptorSetLayout(device.getLogicalHandle(), &layoutInfo, nullptr, &descriptorSetLayouts[dsIndex]) != VK_SUCCESS) {
            throw std::runtime_error("failed to create descriptor set layout!");
        }
    }
}

void GraphicsPipeline::setUniformBuffer(uint32_t set, uint32_t binding, void const* buffer, uint64_t size, uint64_t offset) {
    uniformBuffers[set][binding]->copy(buffer, size, offset);
}

void GraphicsPipeline::cmdSetUniformBuffer(uint32_t set, uint32_t binding, void const* buffer, uint64_t size, uint64_t offset) {
    uniformBuffers[set][binding]->cmdCopy(buffer, size, offset);
}

void GraphicsPipeline::setTexture(uint32_t set, uint32_t binding, uint32_t arrayElement, common::Texture const& texture) {
    Texture2D const& vulkanTexture = static_cast<Texture2D const&>(texture);

    bool foundDescriptorWrite = false;
    bool foundImageInfo = false;

    for (VkWriteDescriptorSet& descriptorWrite : descriptorSetWrites.descriptorWrites) {
        // TODO: descriptor write can contain range [dstArrayElement, descriptorCount]
        if (descriptorWrite.dstSet == descriptorSets[set] && descriptorWrite.dstBinding == binding && descriptorWrite.dstArrayElement == arrayElement) {
            foundDescriptorWrite = true;
            for (VkDescriptorImageInfo& imageInfo : descriptorSetWrites.descriptorImageInfo) {
                if (descriptorWrite.pImageInfo == &imageInfo) {
                    foundImageInfo = true;
                    imageInfo.imageView = vulkanTexture.defaultView.view;
                    imageInfo.sampler = vulkanTexture.sampler;
                    break;
                }
            }

            if (foundImageInfo) {
                break;
            }
        }
    }

    if (foundDescriptorWrite) {
        if (!foundImageInfo) {
            BVD_LOG(LogGroup::rhi, BE, "Found descriptor write but it seems to be used not for images");
        }
        // TODO: can't update descriptor sets while another backbuffer uses it
        BVD_LOG(LogGroup::rhi, BW, "Updating texture while it might be used by backbuffer");
        // return;
    } else {
        // TODO: make a separate writer
        // TODO: I might recreate the whole descriptorImageInfo by emplacing back by 1 element
        VkDescriptorImageInfo& imageInfo = descriptorSetWrites.descriptorImageInfo.emplace_back();
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = vulkanTexture.defaultView.view;
        imageInfo.sampler = vulkanTexture.sampler;

        VkWriteDescriptorSet& descriptorWrite = descriptorSetWrites.descriptorWrites.emplace_back();
        zeroVulkanStruct(descriptorWrite, VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET);
        descriptorWrite.dstSet = descriptorSets[set];
        descriptorWrite.dstBinding = binding;
        descriptorWrite.dstArrayElement = arrayElement;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.pImageInfo = &imageInfo;
    }

    // TODO: I don't need to update uniform buffers
    vkUpdateDescriptorSets(device.getLogicalHandle(), descriptorSetsInfo.numUniformBuffers + (uint32_t)descriptorSetWrites.descriptorImageInfo.size(), descriptorSetWrites.descriptorWrites.data(), 0, nullptr);
}

} // namespace bvd::rhi::vulkan
