#include "VulkanTextureBase.hpp"
#include "VulkanDevice.hpp"
#include "VulkanBuffer.hpp"
#include "VulkanStagingMemory.hpp"
#include "VulkanUtils.hpp"
#include "VulkanPipelineBarrier.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

TextureBase::TextureBase(Device& inDevice, common::TextureCreateInfo const& info, VkImageViewType inViewType, uint32_t inDepth, uint32_t inArraySize)
    : surface(inDevice, info, inViewType, inDepth, inArraySize)
    , device(inDevice)
{
    if (surface.isPresentable()) {
        return;
    }

    defaultView.create(inDevice, surface.image, surface.viewFormat, surface.getFullAspectMask(), info.numMips);

    VkPhysicalDeviceProperties const& properties = inDevice.getDevicePropetries();

    // TODO: separate sampler from texture
    VkSamplerCreateInfo samplerInfo;
    zeroVulkanStruct(samplerInfo, VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO);
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.minLod = 0;
    samplerInfo.maxLod = (float)info.numMips;
    samplerInfo.mipLodBias = 0.0f;

    verifyVulkanResult(vkCreateSampler(inDevice.getLogicalHandle(), &samplerInfo, nullptr, &sampler));

    if (!info.pixels) {
        return;
    }

    uint32_t size = info.width * info.height * device.pixelFormats[surface.pixelFormat].byteSize;

    StagingBuffer* stagingBuffer = device.getStagingManager().acquireBuffer(size);

    stagingBuffer->allocate(info.pixels, size);

    VkBufferImageCopy region;
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = surface.getFullAspectMask();
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset = {0, 0, 0};
    region.imageExtent = {(uint32_t)info.width, (uint32_t)info.height, 1};

    uint32_t layersPerArrayIndex = (inViewType == VK_IMAGE_VIEW_TYPE_CUBE_ARRAY || inViewType == VK_IMAGE_VIEW_TYPE_CUBE) ? 6 : 1;

    VkImageSubresourceRange subresourceRange;
    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = surface.getNumMips();
    subresourceRange.baseArrayLayer = 0;
    subresourceRange.layerCount = inArraySize * layersPerArrayIndex;

    Surface::internalLockWrite(inDevice.getImmediateContext(), surface, subresourceRange, region, stagingBuffer);
}

TextureBase::~TextureBase() {
    if (not surface.isPresentable()) {
        vkDestroySampler(device.getLogicalHandle(), sampler, nullptr);
        vkDestroyImageView(device.getLogicalHandle(), defaultView.view, nullptr);
    }
}

// TODO: this function might be usefull as an internal engine tool to generate mipmaps
void TextureBase::generateMips() {
    CommandBuffer& cmdBuffer = device.getImmediateContext().getCommandBufferManager().getUploadCmdBuffer();

    VkImageSubresourceRange subresourceRange = PipelineBarrier::makeSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1);

    int32_t mipWidth = surface.width;
    int32_t mipHeight = surface.height;

    for (uint32_t baseMipLevel = 0; baseMipLevel < surface.getNumMips() - 1; ++baseMipLevel) {
        subresourceRange.baseMipLevel = baseMipLevel;

        setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, subresourceRange);

        VkImageBlit blit{};
        blit.srcOffsets[0] = {0, 0, 0};
        blit.srcOffsets[1] = {mipWidth, mipHeight, 1};
        blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.srcSubresource.mipLevel = baseMipLevel;
        blit.srcSubresource.baseArrayLayer = 0;
        blit.srcSubresource.layerCount = 1;
        blit.dstOffsets[0] = {0, 0, 0};
        blit.dstOffsets[1] = {mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1};
        blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        blit.dstSubresource.mipLevel = baseMipLevel + 1;
        blit.dstSubresource.baseArrayLayer = 0;
        blit.dstSubresource.layerCount = 1;

        vkCmdBlitImage(cmdBuffer.getHandle(),
            surface.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1, &blit,
            VK_FILTER_LINEAR);

        setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);

        if (mipWidth > 1) {
            mipWidth /= 2;
        }

        if (mipHeight > 1) {
            mipHeight /= 2;
        }
    }

    subresourceRange.baseMipLevel = surface.getNumMips() - 1;

    setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);
}

} // namespace bvd::rhi::vulkan
