#include "VulkanWindowsPlatform.hpp"

#include "Logger.hpp"
#include "Exception.hpp"

#include <glfw/glfw3.h>

namespace bvd::rhi::vulkan {

void VkUtils::createSurface(VkInstance instance, void* windowHandle, VkSurfaceKHR& surface) {
    if (glfwCreateWindowSurface(instance, reinterpret_cast<GLFWwindow*>(windowHandle), nullptr, &surface) != VK_SUCCESS) {
        BVD_THROW("Failed to create window surface");
    }
}

std::vector<const char*> VkUtils::getRequiredInstanceExtensions() {
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    return std::vector<const char*>(glfwExtensions, glfwExtensions + glfwExtensionCount);
}

} // namespace bvd::rhi::vulkan
