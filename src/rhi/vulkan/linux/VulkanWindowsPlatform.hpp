#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

struct VkUtils {
    static void createSurface(VkInstance, void*, VkSurfaceKHR&);
    static std::vector<const char*> getRequiredInstanceExtensions();
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
