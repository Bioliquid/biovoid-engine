#include "VulkanCommandBuffer.hpp"
#include "VulkanCommandBufferPool.hpp"
#include "VulkanFence.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"
#include "VulkanRenderPass.hpp"
#include "VulkanFramebuffer.hpp"
#include "Memory.hpp"

namespace bvd::rhi::vulkan {

CommandBuffer::CommandBuffer(Device& inDevice, CommandBufferPool& inPool, bool inIsUploadOnly)
    : hasViewport(false)
    , isUploadOnly(inIsUploadOnly)
    , device(inDevice)
    , pool(inPool)
    , state(State::notAllocated)
{
    allocMemory();

    fence = new Fence(device, false);
}

CommandBuffer::~CommandBuffer() {
    delete fence;
}

void CommandBuffer::allocMemory() {
    BVD_CHECK(state == State::notAllocated, "Memory is already allocated");

    memzero(currentViewport);

    VkCommandBufferAllocateInfo allocInfo;
    zeroVulkanStruct(allocInfo, VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO);
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;
    allocInfo.commandPool = pool.getHandle();

    hasViewport = false;

    verifyVulkanResult(vkAllocateCommandBuffers(device.getLogicalHandle(), &allocInfo, &handle));

    state = State::readyForBegin;
}

void CommandBuffer::begin() {
    if (state == State::needReset) {
        vkResetCommandBuffer(handle, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
    } else {
        BVD_CHECK(state == State::readyForBegin, "state == State::readyForBegin");
    }

    VkCommandBufferBeginInfo beginInfo;
    zeroVulkanStruct(beginInfo, VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    verifyVulkanResult(vkBeginCommandBuffer(handle, &beginInfo));
    state = State::isInsideBegin;
}

void CommandBuffer::end() {
    verifyVulkanResult(vkEndCommandBuffer(handle));
    state = State::hasEnded;
}

void CommandBuffer::beginRenderPass(RenderTargetLayout const& rtLayout, RenderPass& renderPass, Framebuffer& framebuffer, VkClearValue const* attachmentClearValues) {
    VkRenderPassBeginInfo renderPassInfo;
    zeroVulkanStruct(renderPassInfo, VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO);
    renderPassInfo.renderPass = renderPass.getHandle();
    renderPassInfo.framebuffer = framebuffer.getHandle();
    renderPassInfo.renderArea = framebuffer.getRenderArea();
    renderPassInfo.clearValueCount = rtLayout.getNumClearValues();
    renderPassInfo.pClearValues = attachmentClearValues;

    vkCmdBeginRenderPass(handle, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    state = State::isInsideRenderPass;
}

void CommandBuffer::endRenderPass() {
    vkCmdEndRenderPass(handle);
    state = State::isInsideBegin;
}

void CommandBuffer::refreshFenceStatus() {
    if (state == State::submitted) {
        if (fence->isSignaled()) {
            fence->reset();

            memzero(currentViewport);
            hasViewport = false;

            state = State::needReset;
        }
    }
}

} // namespace bvd::rhi::vulkan
