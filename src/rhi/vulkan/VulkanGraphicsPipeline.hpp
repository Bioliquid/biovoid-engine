#pragma once

#include "GraphicsPipeline.hpp"

#include <vulkan/vulkan.h>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;
class Buffer;

class GraphicsPipeline : public common::GraphicsPipeline {
    static constexpr uint32_t dynamicStateRangeSize = VK_DYNAMIC_STATE_STENCIL_REFERENCE - VK_DYNAMIC_STATE_VIEWPORT + 1;

    struct DescriptorSetWrites {
        std::vector<VkDescriptorImageInfo>  descriptorImageInfo;
        std::vector<VkDescriptorBufferInfo> descriptorBufferInfo;
        std::vector<VkWriteDescriptorSet>   descriptorWrites;
    };

public:
    GraphicsPipeline(Device&);
    virtual ~GraphicsPipeline();

    virtual void loadShaders(common::GraphicsPipelineStateInit const&) override;
    virtual void bind() const override;
    virtual void destroy() override;
    virtual common::Device const& getDevice() const override;

    virtual void setUniformBuffer(uint32_t, uint32_t, void const*, uint64_t, uint64_t) override;
    virtual void cmdSetUniformBuffer(uint32_t, uint32_t, void const*, uint64_t, uint64_t) override;

    virtual void setTexture(uint32_t, uint32_t, uint32_t, common::Texture const&) override;

private:
    void createPipelineLayout();

    Device&             device;

    VkPipeline          graphicsPipeline;
    VkPipelineLayout    pipelineLayout;

    std::vector<VkDescriptorSetLayout> descriptorSetLayouts;

    // TODO: descriptor pool should be common for all graphics pipelines
    VkDescriptorPool descriptorPool;
    std::vector<VkDescriptorSet> descriptorSets;

    std::vector<std::vector<Ref<Buffer>>> uniformBuffers;

    DescriptorSetWrites descriptorSetWrites;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
