#include "VulkanDynamicRhi.hpp"

#include "VulkanWindowsPlatform.hpp"
#include "VulkanViewport.hpp"
#include "VulkanGraphicsPipeline.hpp"
#include "VulkanBuffer.hpp"
#include "VulkanUtils.hpp"

#include "Logger.hpp"
#include "Exception.hpp"

namespace bvd::rhi::common {

DynamicRhi* createVulkanDynamicRhi() {
    return new vulkan::DynamicRhi;
}

}

namespace bvd::rhi::vulkan {

#ifdef USE_VALIDATION_LAYERS

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT
                                                    , VkDebugUtilsMessageTypeFlagsEXT
                                                    , VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData
                                                    , void*) {
    BVD_LOG(LogGroup::rhi, BE, "validation layer: {}", pCallbackData->pMessage);
    return VK_FALSE;
}

#endif

DynamicRhi::DynamicRhi() {
#ifdef USE_VALIDATION_LAYERS
    zeroVulkanStruct(debugMessengerCreateInfo, VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT);
    debugMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                               VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                           VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                           VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.pfnUserCallback = debugCallback;
#endif
}

DynamicRhi::~DynamicRhi() {

}

void DynamicRhi::init() {
    checkCompatibility();

#ifdef USE_VALIDATION_LAYERS
    if (!checkValidationLayersSupport()) {
        BVD_THROW("Validation layers are not supported");
    }
#endif

    createInstance();
#ifdef USE_VALIDATION_LAYERS
    setupDebugMessenger();
#endif
    selectAndInitDevice();
}

void DynamicRhi::checkCompatibility() {
    BASS(VK_SAMPLE_COUNT_1_BIT == 1, "VkSamples bitmap is broken");
    BASS(VK_SAMPLE_COUNT_2_BIT == 2, "VkSamples bitmap is broken");
    BASS(VK_SAMPLE_COUNT_4_BIT == 4, "VkSamples bitmap is broken");
    BASS(VK_SAMPLE_COUNT_8_BIT == 8, "VkSamples bitmap is broken");
    BASS(VK_SAMPLE_COUNT_16_BIT == 16, "VkSamples bitmap is broken");
    BASS(VK_SAMPLE_COUNT_32_BIT == 32, "VkSamples bitmap is broken");
    BASS(VK_SAMPLE_COUNT_64_BIT == 64, "VkSamples bitmap is broken");
}

void DynamicRhi::createInstance() {
    VkApplicationInfo appInfo;
    zeroVulkanStruct(appInfo, VK_STRUCTURE_TYPE_APPLICATION_INFO);
    appInfo.pApplicationName = "biovoid engine";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "biovoid engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo createInfo{};
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO);
    createInfo.pApplicationInfo = &appInfo;

    auto extensions = getRequiredExtensions();
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

#ifdef USE_VALIDATION_LAYERS
    createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
    createInfo.ppEnabledLayerNames = validationLayers.data();

    createInfo.pNext = reinterpret_cast<VkDebugUtilsMessengerCreateInfoEXT*>(&debugMessengerCreateInfo);
#else
    createInfo.enabledLayerCount = 0;
    createInfo.pNext = nullptr;
#endif

    if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
        BVD_THROW("Failed to create vulkan instance");
    }

    BVD_LOG(LogGroup::rhi, BI, "Vulkan instance has been created with API version {}", appInfo.apiVersion == VK_API_VERSION_1_2 ? "1.2" : "unknown");
    BVD_LOG(LogGroup::rhi, BD, "Required extensions");
    for (char const* extension : extensions) {
        BVD_LOG(LogGroup::rhi, BD, "--- {}", extension);
    }
}

void DynamicRhi::setupDebugMessenger() {
    if (createDebugUtilsMessengerEXT() != VK_SUCCESS) {
        BVD_THROW("Failed to set up debug messenger");
    }

    BVD_LOG(LogGroup::rhi, BI, "Debug messenger was successfully initialized");
}

void DynamicRhi::selectAndInitDevice() {
    uint32_t gpuCount = 0;
    vkEnumeratePhysicalDevices(instance, &gpuCount, nullptr);
    if (gpuCount == 0) {
        BVD_THROW("Failed to find GPUs with Vulkan support!");
    }

    std::vector<VkPhysicalDevice> physicalDevices(gpuCount);
    vkEnumeratePhysicalDevices(instance, &gpuCount, physicalDevices.data());

    for (uint32_t gpuIndex = 0; gpuIndex < gpuCount; ++gpuIndex) {
        Device& newDevice = devices.emplace_back(*this, physicalDevices[gpuIndex]);

        newDevice.queryGpu(gpuIndex);
    }

    uint32_t selectedGpuIndex = 0;
    for (uint32_t gpuIndex = 0; gpuIndex < gpuCount; ++gpuIndex) {
        if (isDeviceSuitable(devices[gpuIndex])) {
            selectedDevice = &devices[gpuIndex];
            selectedGpuIndex = gpuIndex;
            break;
        }
    }

    if (selectedDevice == nullptr) {
        BVD_THROW("Failed to find a suitable GPU!");
    }

    selectedDevice->initGpu(selectedGpuIndex);

    immediateContext = &selectedDevice->getImmediateContext();
}

void DynamicRhi::destroy() {
    selectedDevice->destroy();
    selectedDevice = nullptr;

#ifdef USE_VALIDATION_LAYERS
    destroyDebugUtilsMessengerEXT();
#endif

    vkDestroyInstance(instance, nullptr);
}

bool DynamicRhi::checkValidationLayersSupport() {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

    for (char const* layerName : validationLayers) {
        bool layerFound = false;

        for (auto const& layerProperties : availableLayers) {
            if (strcmp(layerName, layerProperties.layerName) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            return false;
        }
    }

    return true;
}

VkResult DynamicRhi::createDebugUtilsMessengerEXT() {
    auto func = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT"));
    if (func != nullptr) {
        return func(instance, &debugMessengerCreateInfo, nullptr, &debugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void DynamicRhi::destroyDebugUtilsMessengerEXT() {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, debugMessenger, nullptr);
    }
}

std::vector<char const*> DynamicRhi::getRequiredExtensions() {
    std::vector<const char*> extensions = VkUtils::getRequiredInstanceExtensions();

#ifdef USE_VALIDATION_LAYERS
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif

    return extensions;
}

Ref<common::Texture2D> DynamicRhi::getViewportBackBuffer(common::Viewport* viewport) {
    Viewport* vulkanViewport = static_cast<Viewport*>(viewport);
    return vulkanViewport->getBackBuffer(*static_cast<Context*>(immediateContext));
}

void DynamicRhi::advanceFrameForViewportBackBuffer(common::Viewport* viewport) {
    Viewport* vulkanViewport = static_cast<Viewport*>(viewport);
    vulkanViewport->advanceBackBufferFrame();
}

Ref<common::Viewport> DynamicRhi::createViewport(void* windowHandle, uint32_t width, uint32_t height, PixelFormat pixelFormat) {
    return new Viewport(*this, *selectedDevice, windowHandle, width, height, pixelFormat);
}

Ref<common::GraphicsPipeline> DynamicRhi::createGraphicsPipeline() {
    return new GraphicsPipeline(*selectedDevice);
}

Ref<common::Buffer> DynamicRhi::createVertexBuffer(uint64_t size) {
    return new Buffer(*selectedDevice, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, size);
}

Ref<common::Buffer> DynamicRhi::createIndexBuffer(uint64_t size) {
    return new Buffer(*selectedDevice, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, size);
}

Ref<common::Buffer> DynamicRhi::createUniformBuffer(uint64_t size) {
    // TODO: look up what flags should it contain
    return new Buffer(*selectedDevice, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, size);
}

Ref<common::Texture2D> DynamicRhi::createTexture2D(common::TextureCreateInfo const& info) {
    return new Texture2D(*selectedDevice, info);
}

} // namespace bvd::rhi::vulkan
