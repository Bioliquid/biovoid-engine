#pragma once

#include <vulkan/vulkan.h>
#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;
class CommandBuffer;
class CommandBufferManager;

class CommandBufferPool {
    friend class CommandBufferManager;

public:
    CommandBufferPool(Device&, CommandBufferManager&);
    ~CommandBufferPool();

    void init(uint32_t);
    CommandBuffer* create(bool);

    void refreshFenceStatus(CommandBuffer&);

    inline VkCommandPool getHandle() { return handle; }

private:
    VkCommandPool               handle = VK_NULL_HANDLE;

    Device&                     device;
    CommandBufferManager&       manager;

    std::vector<CommandBuffer*> cmdBuffers;
    std::vector<CommandBuffer*> freeCmdBuffers;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
