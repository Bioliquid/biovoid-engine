#pragma once

#include "PixelFormat.hpp"

#include <vulkan/vulkan.h>

#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;
class Semaphore;
class Fence;
class Queue;

struct SwapChainRecreateInfo {
    VkSwapchainKHR  swapChain;
    VkSurfaceKHR    surface;
};

class SwapChain {
public:
    enum class Status : uint8_t {
          success
        , outOfDate
        , surfaceLost
    };

    struct Init {
        VkInstance  instance;
        Device&     device;
        void*       windowHandle;
        uint32_t    width;
        uint32_t    height;
        bool        isFullscreen = false;
    };

public:
    SwapChain(Init const&, PixelFormat&, uint32_t*, std::vector<VkImage>&, SwapChainRecreateInfo*);

    void destroy();

    Status acquireImageIndex(Semaphore*&, int32_t*);
    Status present(Queue&, Queue&, Semaphore*);

private:
    // most platforms can query the surface for the present mode, and size, etc
    // however, android and lumin can't
    // TODO: make proper function
    bool supportsQuerySurfaceProperties() const { return true; }

    VkInstance      instance;
    Device&         device;

    VkSwapchainKHR  swapChain = VK_NULL_HANDLE;
    VkSurfaceKHR    surface = VK_NULL_HANDLE;
    VkFormat        imageFormat = VK_FORMAT_UNDEFINED;

    int32_t         currentImageIndex = -1;
    uint32_t        semaphoreIndex = 0;
    uint32_t        internalWidth = 0;
    uint32_t        internalHeight = 0;
    bool            internalFullScreen = false;

    std::vector<Semaphore*> imageAcquiredSemaphores;
#if VULKAN_USE_IMAGE_ACQUIRE_FENCES
    std::vector<Fence*>     imageAcquiredFences;
#endif
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
