#pragma once

#include "Context.hpp"
#include "VulkanLayoutManager.hpp"

#include <vulkan/vulkan.h>

namespace bvd {

namespace rhi {

namespace vulkan {

class DynamicRhi;
class Device;
class Queue;
class CommandBufferManager;
class RenderPass;
class PendingGfxState;

class Context : public common::Context {
public:
    Context(DynamicRhi&, Device&, Queue*);
    virtual ~Context();

    virtual void beginDrawingViewport(common::Viewport*) override;
    virtual void endDrawingViewport(common::Viewport*) override;

    virtual void beginRenderPass(common::RenderPassInfo const&) override;
    virtual void endRenderPass() override;

    virtual void setStreamSource(uint32_t, common::Buffer const&, uint32_t) override;
    virtual void setGraphicsPipeline(common::GraphicsPipeline const*) override;

    virtual void drawPrimitive(uint32_t, uint32_t, uint32_t) override;
    virtual void drawIndexedPrimitive(common::Buffer const&, int32_t, int32_t, int32_t, int32_t, int32_t) override;

    virtual void waitIdle() override;

    inline void notifyDeletedRenderTarget(VkImage image) { layoutManager.notifyDeletedRenderTarget(image);  }
    inline void notifyDeletedImage(VkImage image) { layoutManager.notifyDeletedRenderTarget(image); }

    inline Queue& getQueue() { return queue; }
    inline CommandBufferManager& getCommandBufferManager() { return *cmdBufferManager; }
    inline DynamicRhi& getRhi() { return rhi; }

    RenderPass* prepareRenderPassForGraphicsPipelineCreation(common::GraphicsPipelineStateInit const&);

private:
    DynamicRhi&                 rhi;
    Device&                     device;
    Queue&                      queue;

    CommandBufferManager*       cmdBufferManager;

    PendingGfxState*            pendingGfxState;

    inline static LayoutManager layoutManager;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
