#include "VulkanContext.hpp"
#include "VulkanDynamicRhi.hpp"
#include "VulkanDevice.hpp"
#include "VulkanQueue.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "VulkanViewport.hpp"
#include "VulkanRenderTargetLayout.hpp"
#include "VulkanPendingGfxState.hpp"
#include "VulkanBuffer.hpp"
#include "VulkanTexture2D.hpp"
#include "VulkanGraphicsPipeline.hpp"
#include "RenderTargetsInfo.hpp"
#include "RenderPassInfo.hpp"
#include "GraphicsPipelineStateInit.hpp"
#include "Logger.hpp"
#include "Memory.hpp"

namespace bvd::rhi::vulkan {

Context::Context(DynamicRhi& inRhi, Device& inDevice, Queue* inQueue)
    : rhi(inRhi)
    , device(inDevice)
    , queue(*inQueue)
{
    cmdBufferManager = new CommandBufferManager(device, *this);
    // cmdBufferManager->init();

    pendingGfxState = new PendingGfxState(device, *this);
}

Context::~Context() {
    layoutManager.destroy();

    delete pendingGfxState;
    delete cmdBufferManager;
}

void Context::beginDrawingViewport(common::Viewport* viewport) {
    Viewport* vulkanViewport = static_cast<Viewport*>(viewport);

    rhi.drawingViewport = vulkanViewport;
}

void Context::endDrawingViewport(common::Viewport* viewport) {
    Viewport* vulkanViewport = static_cast<Viewport*>(viewport);

    // check(rhi.drawingViewport == vulkanViewport);

    CommandBuffer& cmdBuffer = cmdBufferManager->getActiveCmdBuffer();
    // check(!cmdBuffer.hasEnded() && !cmdBuffer.isInsideRenderPass());

    vulkanViewport->present(this, cmdBuffer, queue, device.getPresentQueue());

    rhi.drawingViewport = nullptr;
}

void Context::beginRenderPass(common::RenderPassInfo const& renderPassInfo) {
    CommandBuffer& cmdBuffer = cmdBufferManager->getActiveCmdBuffer();

    RenderTargetLayout rtLayout{device, renderPassInfo};

    RenderPass* renderPass = layoutManager.getOrCreateRenderPass(device, rtLayout);

    common::RenderTargetsInfo rtInfo;
    renderPassInfo.convertToRenderTargetsInfo(rtInfo);

    Framebuffer* framebuffer = layoutManager.getOrCreateFramebuffer(device, rtInfo, rtLayout, renderPass);
    layoutManager.beginRenderPass(cmdBuffer, renderPassInfo, rtLayout, renderPass, framebuffer);

    auto colorEntry = renderPassInfo.colorRenderTargets[0];
    Texture2D const& texture = colorEntry.resolveTarget ? static_cast<Texture2D const&>(*colorEntry.resolveTarget) : static_cast<Texture2D const&>(*colorEntry.renderTarget);
    // TODO: setViewport in command list
    pendingGfxState->setViewport(0.0f, 0.0f, 0.0f, (float)texture.getWidth(), (float)texture.getHeight(), 1.0f, texture.isSwapChainTarget());
}

void Context::endRenderPass() {
    CommandBuffer& cmdBuffer = cmdBufferManager->getActiveCmdBuffer();
    layoutManager.endRenderPass(cmdBuffer);

    // TODO
    // if (not msaa) {
    //     copyToResolveTarget();
    // }
}

void Context::setStreamSource(uint32_t index, common::Buffer const& vertexBuffer, uint32_t offset) {
    Buffer const& vulkanVertexBuffer = static_cast<Buffer const&>(vertexBuffer);

    pendingGfxState->setStreamSource(index, vulkanVertexBuffer.getHandle(), offset);
}

void Context::setGraphicsPipeline(common::GraphicsPipeline const* pipeline) {
    GraphicsPipeline const* vulkanPipeline = static_cast<GraphicsPipeline const*>(pipeline);

    pendingGfxState->setGfxPipeline(vulkanPipeline);
}

void Context::drawPrimitive(uint32_t baseVertexIndex, uint32_t numPrimitives, uint32_t numInstances) {
    CommandBuffer& cmdBuffer = cmdBufferManager->getActiveCmdBuffer();

    pendingGfxState->prepareForDraw(cmdBuffer);
    // uint32 NumVertices = GetVertexCountForPrimitiveCount(NumPrimitives, PendingGfxState->PrimitiveType);
    uint32_t numVertices = numPrimitives * 3;
    vkCmdDraw(cmdBuffer.getHandle(), numVertices, numInstances, baseVertexIndex, 0);
}

void Context::drawIndexedPrimitive(common::Buffer const& indexBuffer, int32_t baseVertexIndex, int32_t firstInstance, int32_t startIndex, int32_t numPrimitives, int32_t numInstances) {
    Buffer const& vkIndexBuffer = static_cast<Buffer const&>(indexBuffer);

    CommandBuffer& cmdBuffer = cmdBufferManager->getActiveCmdBuffer();

    pendingGfxState->prepareForDraw(cmdBuffer);
    vkCmdBindIndexBuffer(cmdBuffer.getHandle(), vkIndexBuffer.getHandle(), 0, VK_INDEX_TYPE_UINT32);

    // TODO: only triangles are supported, no other topology
    vkCmdDrawIndexed(cmdBuffer.getHandle(), numPrimitives * 3, numInstances, startIndex, baseVertexIndex, firstInstance);
}

void Context::waitIdle() {
    vkDeviceWaitIdle(device.getLogicalHandle());
}

RenderPass* Context::prepareRenderPassForGraphicsPipelineCreation(common::GraphicsPipelineStateInit const& initializer) {
    RenderTargetLayout rtLayout{device, initializer};

    return layoutManager.getOrCreateRenderPass(device, rtLayout);
}

} // namespace bvd::vulkan
