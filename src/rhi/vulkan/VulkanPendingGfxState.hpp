#pragma once

#include "RhiDefinitions.hpp"

#include <vulkan/vulkan.h>
#include <algorithm>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;
class Context;
class CommandBuffer;
class GraphicsPipeline;

class PendingGfxState {
public:
    PendingGfxState(Device&, Context&);

    void prepareForDraw(CommandBuffer&);

    void setViewport(float, float, float, float, float, float, bool);
    void setScissorRect(uint32_t, uint32_t, uint32_t, uint32_t);

    void setStreamSource(uint32_t index, VkBuffer buffer, VkDeviceSize offset) {
        vertexOffsets[index] = offset;
        pendingStreams[index] = buffer;
        numPendingStreams = std::max(numPendingStreams, index + 1);
    }

    inline void setGfxPipeline(GraphicsPipeline const* inPipeline) { pipeline = inPipeline; }

private:
    void updateDynamicStates(CommandBuffer&);

    Device&     device;
    Context&    context;

    VkViewport  viewport;
    VkRect2D    scissor;
    bool        swapChainTarget;

    VkDeviceSize    vertexOffsets[2] = {};
    VkBuffer        pendingStreams[2];
    uint32_t        numPendingStreams = 0;

    GraphicsPipeline const* pipeline = nullptr;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
