#include "VulkanViewport.hpp"
#include "VulkanSwapChain.hpp"
#include "VulkanDynamicRhi.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanPipelineBarrier.hpp"
#include "Logger.hpp"
#include "Memory.hpp"

namespace bvd::rhi::vulkan {

BackBuffer::BackBuffer(Viewport* inViewport, PixelFormat inFormat, uint32_t inWidth, uint32_t inHeight, TextureCreateFlags::Type inFlags)
    : Texture2D(inViewport->device, common::TextureCreateInfo(inFormat, inWidth, inHeight, inFlags | TextureCreateFlags::presentable, colors::black), true)
    , viewport(inViewport)
{

}

void BackBuffer::onGetBackBufferImage(Context& context) {
    acquireBackBufferImage(context);
}

void BackBuffer::acquireBackBufferImage(Context& context) {
    if (surface.image == VK_NULL_HANDLE) {
        viewport->acquireImageIndex();

        surface.image = viewport->backBufferImages[viewport->acquiredImageIndex];

        CommandBuffer& cmdBuffer = context.getCommandBufferManager().getActiveCmdBuffer();

        cmdBuffer.addWaitSemaphore(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, viewport->acquiredSemaphore);
    }
}

void BackBuffer::onAdvanceBackBufferFrame() {
    releaseAcquiredImage();
}

void BackBuffer::releaseAcquiredImage() {
    defaultView.view = VK_NULL_HANDLE;
    surface.image = VK_NULL_HANDLE;
}

Viewport::Viewport(DynamicRhi& inRhi, Device& inDevice, void* inWindowHandle, uint32_t inWidth, uint32_t inHeight, PixelFormat preferredPixelFormat)
    : rhi(inRhi)
    , device(inDevice)
    , windowHandle(inWindowHandle)
    , width(inWidth)
    , height(inHeight)
    , pixelFormat(preferredPixelFormat)
{
    createSwapchain();

    renderingDoneSemaphores.resize(backBufferImages.size());

    for (uint32_t index = 0; index < backBufferImages.size(); ++index) {
        renderingDoneSemaphores[index] = new Semaphore(device);
    }
}

Viewport::~Viewport() {
    for (uint32_t index = 0; index < renderingDoneSemaphores.size(); ++index) {
        delete renderingDoneSemaphores[index];
    }

    swapChain->destroy();
    delete swapChain;
}

void Viewport::resize(uint32_t newWidth, uint32_t newHeight) {
    width = newWidth;
    height = newHeight;

    recreateSwapchain();
}

void Viewport::recreateSwapchain() {
    destroySwapchain();
    createSwapchain();
}

void Viewport::createSwapchain() {
    uint32_t desiredNumBackBuffers = 3;
    std::vector<VkImage> images;

    SwapChain::Init init {
         .instance = rhi.getInstance()
        , .device = device
        , .windowHandle = windowHandle
        , .width = width
        , .height = height
        , .isFullscreen = false
    };

    swapChain = new SwapChain(init, pixelFormat, &desiredNumBackBuffers, images, nullptr);

    backBufferImages.resize(images.size());

    for (uint32_t index = 0; index < images.size(); ++index) {
        backBufferImages[index] = images[index];
    }

    // TODO: reconsider used texture flags
    backBuffer = new BackBuffer(this, pixelFormat, width, height, TextureCreateFlags::none);

    acquiredImageIndex = -1;
}

void Viewport::destroySwapchain() {
    device.submitCommandsAndFlushGpu();
    device.waitUntilIdle();

    for (VkImage& image : backBufferImages) {
        device.notifyDeletedImage(image, true);
        image = nullptr;
    }

    swapChain->destroy();
    delete swapChain;

    acquiredImageIndex = -1;
}

Ref<common::Texture2D> Viewport::getBackBuffer(Context& context) {
    backBuffer->onGetBackBufferImage(context);
    return backBuffer.as<common::Texture2D>();
}

void Viewport::advanceBackBufferFrame() {
    backBuffer->onAdvanceBackBufferFrame();
}

void Viewport::acquireImageIndex() {
    SwapChain::Status status = swapChain->acquireImageIndex(acquiredSemaphore, &acquiredImageIndex);
    if (status == SwapChain::Status::outOfDate) {
        BVD_LOG(LogGroup::rhi, BW, "Swapchain is out of date. Trying to recreate");
        recreateSwapchain();
    } else if (status != SwapChain::Status::success) {
        BVD_LOG(LogGroup::rhi, BE, "Swapchain returned unsuccess status");
    }
}

void Viewport::present(Context* context, CommandBuffer& cmdBuffer, Queue& queue, Queue& presentQueue) {
    setImageLayout(cmdBuffer.getHandle(), backBufferImages[acquiredImageIndex], VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, PipelineBarrier::makeSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT));

    cmdBuffer.end();

    context->getCommandBufferManager().submitActiveCmdBufferFromPresent(renderingDoneSemaphores[acquiredImageIndex]);

    swapChain->present(queue, presentQueue, renderingDoneSemaphores[acquiredImageIndex]);

    context->getCommandBufferManager().prepareForNewActiveCommandBuffer();
}

} // namespace bvd::rhi::vulkan
