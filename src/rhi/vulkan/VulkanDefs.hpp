#pragma once

#include <vulkan/vulkan.h>
#include <array>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

constexpr std::array<const char*, 1> deviceExtensions = {
          VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

constexpr std::array<char const*, 1> validationLayers = {
        "VK_LAYER_KHRONOS_validation"
};

constexpr int32_t firstFormatIndex = 0;
constexpr int32_t lastFormatIndex = VK_FORMAT_ASTC_12x12_SRGB_BLOCK;

} // namespace vulkan

} // namespace rhi

} // namespace bvd
