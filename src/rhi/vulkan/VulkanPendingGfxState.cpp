#include "VulkanPendingGfxState.hpp"

#include "VulkanDevice.hpp"
#include "VulkanContext.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanGraphicsPipeline.hpp"
#include "VulkanUtils.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

namespace bvd::rhi::vulkan {

PendingGfxState::PendingGfxState(Device& inDevice, Context& inContext)
    : device(inDevice)
    , context(inContext)
{

}

void PendingGfxState::prepareForDraw(CommandBuffer& cmdBuffer) {
#ifdef DEVELOPMENT_GUIDES_CHECK
    if (!pipeline) {
        BVD_THROW("Pipeline is nullptr");
    }
#endif

    pipeline->bind();

    updateDynamicStates(cmdBuffer);

    if (numPendingStreams != 0) {
        vkCmdBindVertexBuffers(cmdBuffer.getHandle(), 0, numPendingStreams, pendingStreams, vertexOffsets);
    }

    numPendingStreams = 0;
}

void PendingGfxState::setViewport(float minX, float minY, float minZ, float maxX, float maxY, float maxZ, bool target) {
    memzero(viewport);

    viewport.x = minX;
    viewport.y = minY;
    viewport.width = maxX - minX;
    viewport.height = maxY - minY;
    viewport.minDepth = minZ;
    if (minZ == maxZ) {
        // Engine passes have maxZ as 0.0f in some cases
        viewport.maxDepth = minZ + 1.0f;
    } else {
        viewport.maxDepth = maxZ;
    }

    swapChainTarget = target;

    setScissorRect((uint32_t)minZ, (uint32_t)minY, uint32_t(maxX - minX), uint32_t(maxY - minY));
}

void PendingGfxState::setScissorRect(uint32_t minX, uint32_t minY, uint32_t width, uint32_t height) {
    memzero(scissor);

    scissor.offset.x = minX;
    scissor.offset.y = minY;
    scissor.extent.width = width;
    scissor.extent.height = height;
}

void PendingGfxState::updateDynamicStates(CommandBuffer& cmdBuffer) {
    // TODO: store this in command buffer
    // bool cmdNeedsDynamicState = true;

    bool needsUpdateViewport = true || !cmdBuffer.hasViewport || (memcmp(cmdBuffer.currentViewport, viewport) != 0);
    if (needsUpdateViewport) {
        // Flip viewport on Y-axis to be uniform between HLSLcc and DXC generated SPIR-V shaders (requires VK_KHR_maintenance1 extension)
        VkViewport flippedViewport = viewport;

        // TODO: UE doesn't have this
        if (swapChainTarget) {
            flippedViewport.y += flippedViewport.height;
            flippedViewport.height = -flippedViewport.height;
        }

        vkCmdSetViewport(cmdBuffer.getHandle(), 0, 1, &flippedViewport);

        memcpy(cmdBuffer.currentViewport, viewport);
        cmdBuffer.hasViewport = true;
    }

    bool needsUpdateScissor = true;
    if (needsUpdateScissor) {
        vkCmdSetScissor(cmdBuffer.getHandle(), 0, 1, &scissor);
    }
}

} // namespace bvd::rhi::vulkan
