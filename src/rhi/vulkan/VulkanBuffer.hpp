#pragma once

#include "Buffer.hpp"
#include "VulkanMemory.hpp"

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

class Buffer : public common::Buffer {
public:
    Buffer(Device&, VkBufferUsageFlags, int64_t);

    virtual void copy(void const*, int64_t, int64_t) override;
    virtual void copy(void const*, int64_t) override;
    virtual void cmdCopy(void const*, int64_t) override;
    virtual void cmdCopy(void const*, int64_t, int64_t) override;
    virtual void destroy() override;

    inline VkBuffer getHandle() const { return buffer; }

private:
    Device&                 device;
    VkBuffer                buffer;
    DeviceMemoryAllocation  allocation;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
