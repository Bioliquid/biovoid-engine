#pragma once

#include "Device.hpp"
#include "VulkanContext.hpp"
#include "VulkanMemory.hpp"
#include "VulkanStagingMemory.hpp"
#include "VulkanDefs.hpp"

#include <vulkan/vulkan.h>
#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

class DynamicRhi;
class Queue;

enum class GpuVendorId {
      unknown = -1
    , notQueried = 0
    
    , amd = 0x1002
    , imgTec = 0x1010
    , nvidia = 0x10DE
    , arm = 0x13B5
    , qualcomm = 0x5143
    , intel = 0x8086
    , mesa = 0x10005
};

class Device : public common::Device {
public:
    Device(DynamicRhi&, VkPhysicalDevice);

    void queryGpu(uint32_t);
    void initGpu(uint32_t);

    void setupPresentQueue(VkSurfaceKHR);

    void destroy();

    void waitUntilIdle();
    void submitCommandsAndFlushGpu();

    void notifyDeletedImage(VkImage, bool);

    inline VkDevice getLogicalHandle() const { return device; }
    inline VkPhysicalDevice getPhysicalHandle() const { return gpu; }
    inline VkPhysicalDeviceProperties const& getDevicePropetries() const { return gpuProps; }

    inline DeviceMemoryManager& getDeviceMemoryManager() { return deviceMemoryManager; }
    inline StagingManager& getStagingManager() { return stagingManager; }

    inline Queue& getPresentQueue() { return *presentQueue; }

    Context& getImmediateContext() { return *immediateContext; }

    // TODO: redesign
    inline VkFormat engineToVkTextureFormat(PixelFormat engineFormat, const bool isSrgb) {
        VkFormat format = (VkFormat)pixelFormats[engineFormat].platformFormat;
        if (isSrgb) {
            switch (format) {
            case VK_FORMAT_B8G8R8A8_UNORM:  return VK_FORMAT_B8G8R8A8_SRGB;
            case VK_FORMAT_R8G8B8A8_UNORM:  return VK_FORMAT_R8G8B8A8_SRGB;
            default:                        break;
            }
        }

        return format;
    }

private:
    void createDevice();

    void setupFormats();
    void mapFormatSupport(PixelFormat, VkFormat);
    void mapFormatSupportWithFallback(PixelFormat, VkFormat, std::initializer_list<VkFormat> const&);
    bool isTextureFormatSupported(VkFormat) const;
    bool isBufferFormatSupported(VkFormat) const;

    void setupUniformTypes();
    void mapUniformInfo(UniformType, char const*, VkFormat, uint32_t, uint32_t);

    void updateMsaaSamples();

    void submitCommands(Context&);

    DynamicRhi& rhi;

    Context*                immediateContext;
    Context*                computeContext;
    // std::vector<Context*>   contexts;

    DeviceMemoryManager deviceMemoryManager;
    StagingManager      stagingManager;

    VkDevice                             device;
    VkPhysicalDevice                     gpu;
    VkPhysicalDeviceProperties           gpuProps;
    VkPhysicalDeviceFeatures             physicalFeatures;
    std::vector<VkQueueFamilyProperties> queueFamilyProps;
    VkFormatProperties                   formatProperties[lastFormatIndex + 1];

    Queue* gfxQueue = nullptr;
    Queue* computeQueue = nullptr;
    Queue* transferQueue = nullptr;
    Queue* presentQueue = nullptr;
    bool   asyncComputeQueue = false;
    bool   presentOnComputeQueue = false;
    bool   enableAsyncCompute = false;

    GpuVendorId vendorId = GpuVendorId::notQueried;

    VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_1_BIT;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
