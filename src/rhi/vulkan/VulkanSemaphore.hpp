#pragma once

#include <vulkan/vulkan.hpp>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

// TODO: should ref counted 
class Semaphore {
public:
    Semaphore(Device&);
    ~Semaphore();

    inline VkSemaphore getHandle() const { return semaphore; }

private:
    Device&     device;
    VkSemaphore semaphore;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
