#pragma once

#include <vulkan/vulkan.hpp>

namespace bvd {

namespace rhi {

namespace vulkan {

class CommandBufferManager;
class CommandBufferPool;
class Device;
class Fence;
class Semaphore;
class Queue;
class RenderTargetLayout;
class RenderPass;
class Framebuffer;

class CommandBuffer {
    enum class State : uint8_t {
          readyForBegin
        , isInsideBegin
        , isInsideRenderPass
        , hasEnded
        , submitted
        , notAllocated
        , needReset
    };

    friend class CommandBufferManager;
    friend class Queue;

public:
    CommandBuffer(Device&, CommandBufferPool&, bool);
    ~CommandBuffer();

    void allocMemory();

    void begin();
    void end();
    void beginRenderPass(RenderTargetLayout const&, RenderPass&, Framebuffer&, VkClearValue const*);
    void endRenderPass();

    void refreshFenceStatus();

    void addWaitSemaphore(VkPipelineStageFlags flags, Semaphore* semaphore) {
        waitFlags.push_back(flags);
        waitSemaphores.push_back(semaphore);
    }

    VkCommandBuffer getHandle() { return handle; }

    inline bool isInsideRenderPass() const { return state == State::isInsideRenderPass; }
    inline bool isOutsideRenderPass() const { return state == State::isInsideBegin; }
    inline bool hasBegun() const { return state == State::isInsideBegin || state == State::isInsideRenderPass; }
    inline bool hasEnded() const { return state == State::hasEnded; }
    inline bool isSubmitted() const { return state == State::submitted; }
    inline bool isAllocated() const { return state != State::notAllocated; }

    inline CommandBufferPool& getOwner() { return pool; }

private:
    void markSemaphoresAsSubmitted() {
        waitFlags.clear();
        waitSemaphores.clear();
    }

public:
    bool hasViewport{false};
    bool isUploadOnly{false};

    VkViewport currentViewport;

private:
    VkCommandBuffer handle;

    Device&            device;
    CommandBufferPool& pool;

    Fence* fence;

    // Not owned by this class
    // The main semaphore is the one from swapChain (acquireImageIndex)
    std::vector<Semaphore*>           waitSemaphores;
    std::vector<VkPipelineStageFlags> waitFlags;

    State                             state = State::notAllocated;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
