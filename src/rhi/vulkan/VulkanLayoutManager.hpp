#pragma once

#include <vulkan/vulkan.hpp>
#include <cstdint>
#include <map>

namespace bvd {

namespace rhi {

namespace common {

class RenderTargetsInfo;
class RenderPassInfo;
struct GraphicsPipelineStateInit;

} // namespace common

namespace vulkan {

class Device;
class Framebuffer;
class RenderPass;
class RenderTargetLayout;
class CommandBuffer;

class LayoutManager {
public:
    ~LayoutManager();

    Framebuffer* getOrCreateFramebuffer(Device&, common::RenderTargetsInfo const&, RenderTargetLayout const&, RenderPass*);
    RenderPass* getOrCreateRenderPass(Device&, RenderTargetLayout const&);
    
    void destroy();

    void beginRenderPass(CommandBuffer&, common::RenderPassInfo const&, RenderTargetLayout const&, RenderPass*, Framebuffer*);
    void endRenderPass(CommandBuffer&);

    void notifyDeletedRenderTarget(VkImage);
    void notifyDeletedImage(VkImage);

private:
    std::map<uint64_t, RenderPass*> renderPasses;

    struct FramebufferList {
        std::vector<Framebuffer*> framebuffer;
    };
    std::map<uint64_t, FramebufferList*> framebuffers;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
