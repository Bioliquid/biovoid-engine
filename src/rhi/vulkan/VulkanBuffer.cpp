#include "VulkanBuffer.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"

#include <cstring>

namespace bvd::rhi::vulkan {

Buffer::Buffer(Device& inDevice, VkBufferUsageFlags usage, int64_t size)
    : device(inDevice)
{
    VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    verifyVulkanResult(vkCreateBuffer(device.getLogicalHandle(), &bufferInfo, nullptr, &buffer));

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device.getLogicalHandle(), buffer, &memRequirements);

    allocation = device.getDeviceMemoryManager().allocate(memRequirements.size, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    vkBindBufferMemory(device.getLogicalHandle(), buffer, allocation.bufferMemory, 0);
}

void Buffer::copy(void const* bufferData, int64_t size, int64_t offset) {
    void* mappedMemory = allocation.map(size, offset);
    std::memcpy(mappedMemory, bufferData, size);
    allocation.unmap();
}

void Buffer::copy(void const* bufferData, int64_t size) {
    void* mappedMemory = allocation.map(size, 0);
    std::memcpy(mappedMemory, bufferData, size);
    allocation.unmap();
}

void Buffer::cmdCopy(void const* bufferData, int64_t size) {
    Context& context = device.getImmediateContext();
    CommandBuffer& cmdBuffer = context.getCommandBufferManager().getActiveCmdBuffer();

    vkCmdUpdateBuffer(cmdBuffer.getHandle(), buffer, 0, size, bufferData);
}

void Buffer::cmdCopy(void const* bufferData, int64_t size, int64_t offset) {
    Context& context = device.getImmediateContext();
    CommandBuffer& cmdBuffer = context.getCommandBufferManager().getActiveCmdBuffer();

    vkCmdUpdateBuffer(cmdBuffer.getHandle(), buffer, offset, size, bufferData);
}

void Buffer::destroy() {
    vkDestroyBuffer(device.getLogicalHandle(), buffer, nullptr);
    allocation.free();
}

} // namespace bvd::rhi::vulkan
