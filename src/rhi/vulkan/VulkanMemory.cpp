#include "VulkanMemory.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

void* DeviceMemoryAllocation::map(VkDeviceSize size, VkDeviceSize offset) {
    vkMapMemory(device, bufferMemory, offset, size, 0, &mappedPtr);
    return mappedPtr;
}

void DeviceMemoryAllocation::unmap() {
    vkUnmapMemory(device, bufferMemory);
}

void DeviceMemoryAllocation::free() {
    vkFreeMemory(device, bufferMemory, nullptr);
}

void DeviceMemoryManager::init(Device& inDevice) {
    device = &inDevice;

    updateMemoryProperties();
}

DeviceMemoryAllocation DeviceMemoryManager::allocate(VkDeviceSize allocationSize, uint32_t memoryTypeBits, VkMemoryPropertyFlags properties) {
    DeviceMemoryAllocation allocation;
    allocation.device = device->getLogicalHandle();

    VkMemoryAllocateInfo allocInfo{};
    zeroVulkanStruct(allocInfo, VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO);
    allocInfo.allocationSize = allocationSize;
    allocInfo.memoryTypeIndex = getMemoryTypeFromProperties(memoryTypeBits, properties);

    verifyVulkanResult(vkAllocateMemory(device->getLogicalHandle(), &allocInfo, nullptr, &allocation.bufferMemory));

    return allocation;
}

void DeviceMemoryManager::updateMemoryProperties() {
    vkGetPhysicalDeviceMemoryProperties(device->getPhysicalHandle(), &memoryProperties);
}

uint32_t DeviceMemoryManager::getMemoryTypeFromProperties(uint32_t typeBits, VkMemoryPropertyFlags properties) {
    for (uint32_t index = 0; index < memoryProperties.memoryTypeCount && typeBits; ++index) {
        if ((typeBits & 1) == 1) {
            // Type is available, does it match user properties?
            if ((memoryProperties.memoryTypes[index].propertyFlags & properties) == properties) {
                return index;
            }
        }

        typeBits >>= 1;
    }

    BVD_LOG(LogGroup::rhi, BE, "Didn't find memory with such properties");
    return 0;
}

} // namespace bvd::rhi::vulkan
