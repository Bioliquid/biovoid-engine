#pragma once

#include "DynamicRhi.hpp"
#include "VulkanDevice.hpp"

#include <vulkan/vulkan.h>
#include <array>
#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

class Viewport;

class DynamicRhi : public common::DynamicRhi {
    friend class Context;
    friend class Viewport;

public:
    DynamicRhi();
    virtual ~DynamicRhi();

    virtual void init() override;
    virtual void destroy() override;

    virtual Ref<common::Texture2D> getViewportBackBuffer(common::Viewport*) override;
    virtual void advanceFrameForViewportBackBuffer(common::Viewport*) override;

    virtual Ref<common::Viewport> createViewport(void*, uint32_t, uint32_t, PixelFormat) override;
    virtual Ref<common::GraphicsPipeline> createGraphicsPipeline() override;
    virtual Ref<common::Buffer> createVertexBuffer(uint64_t) override;
    virtual Ref<common::Buffer> createIndexBuffer(uint64_t) override;
    virtual Ref<common::Buffer> createUniformBuffer(uint64_t) override;
    virtual Ref<common::Texture2D> createTexture2D(common::TextureCreateInfo const&) override;

    inline VkInstance getInstance() { return instance; }

private:
    // init calls
    void createInstance();
    void setupDebugMessenger();
    void selectAndInitDevice();

    // support functions
    bool checkValidationLayersSupport();
    VkResult createDebugUtilsMessengerEXT();
    void destroyDebugUtilsMessengerEXT();
    std::vector<char const*> getRequiredExtensions();
    bool isDeviceSuitable(Device&) const { return true; }

    void checkCompatibility();

    VkInstance                          instance;
    VkDebugUtilsMessengerCreateInfoEXT  debugMessengerCreateInfo;
    VkDebugUtilsMessengerEXT            debugMessenger;
    std::vector<Device>                 devices;
    Device*                             selectedDevice;

    Ref<Viewport>                       drawingViewport;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
