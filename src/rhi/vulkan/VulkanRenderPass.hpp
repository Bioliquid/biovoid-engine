#pragma once

#include "VulkanRenderTargetLayout.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

class RenderPass {
public:
    RenderPass(Device&, RenderTargetLayout const&);
    ~RenderPass();

    inline VkRenderPass getHandle() const { return handle; }
    inline uint32_t getNumClearValues() const { return layout.getNumClearValues(); }

private:
    RenderTargetLayout  layout;
    VkRenderPass        handle;
    Device&             device;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
