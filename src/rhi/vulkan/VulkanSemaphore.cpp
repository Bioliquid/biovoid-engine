#include "VulkanSemaphore.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"

namespace bvd::rhi::vulkan {

Semaphore::Semaphore(Device& inDevice)
    : device(inDevice)
{
    VkSemaphoreCreateInfo createInfo;
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);
    verifyVulkanResult(vkCreateSemaphore(device.getLogicalHandle(), &createInfo, nullptr, &semaphore));
}

Semaphore::~Semaphore() {
    vkDestroySemaphore(device.getLogicalHandle(), semaphore, nullptr);
    semaphore = VK_NULL_HANDLE;
}

} // namespace bvd::rhi::vulkan
