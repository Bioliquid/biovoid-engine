#include "VulkanQueue.hpp"

#include "VulkanDevice.hpp"
#include "VulkanFence.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanUtils.hpp"
#include "VulkanCommandBufferPool.hpp"

#include "Memory.hpp"

namespace bvd::rhi::vulkan {

Queue::Queue(Device& inDevice, uint32_t inFamilyIndex)
    : device(inDevice)
    , handle(VK_NULL_HANDLE)
    , familyIndex(inFamilyIndex)
    , queueIndex(0)
{
    vkGetDeviceQueue(device.getLogicalHandle(), familyIndex, queueIndex, &handle);
}

void Queue::submit(CommandBuffer& cmdBuffer, uint32_t numSignalSemaphores, VkSemaphore* signalSemaphores) {
    Fence* fence = cmdBuffer.fence;

    const VkCommandBuffer cmdBuffers[] = { cmdBuffer.handle };

    VkSubmitInfo submitInfo;
    zeroVulkanStruct(submitInfo, VK_STRUCTURE_TYPE_SUBMIT_INFO);
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = cmdBuffers;
    submitInfo.signalSemaphoreCount = numSignalSemaphores;
    submitInfo.pSignalSemaphores = signalSemaphores;

    std::vector<VkSemaphore> waitSemaphores;
    if (!cmdBuffer.waitSemaphores.empty()) {
        for (Semaphore* semaphore : cmdBuffer.waitSemaphores) {
            waitSemaphores.push_back(semaphore->getHandle());
        }

        submitInfo.waitSemaphoreCount = (uint32_t)waitSemaphores.size();
        submitInfo.pWaitSemaphores = waitSemaphores.data();
        submitInfo.pWaitDstStageMask = cmdBuffer.waitFlags.data();
    }

    verifyVulkanResult(vkQueueSubmit(handle, 1, &submitInfo, fence->getHandle()));

    cmdBuffer.state = CommandBuffer::State::submitted;
    cmdBuffer.markSemaphoresAsSubmitted();

    cmdBuffer.getOwner().refreshFenceStatus(cmdBuffer);
}

} // namespace bvd::rhi::vulkan
