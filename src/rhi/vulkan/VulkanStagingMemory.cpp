#include "VulkanStagingMemory.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"

namespace bvd::rhi::vulkan {

StagingBuffer::StagingBuffer(Device& inDevice)
    : device(inDevice)
{

}

StagingBuffer::~StagingBuffer() {
    vkDestroyBuffer(device.getLogicalHandle(), buffer, nullptr);
    allocation.free();
}

void StagingBuffer::allocate(void const* bufferData, uint64_t size) {
    void* mappedMemory = allocation.map(size, 0);
    std::memcpy(mappedMemory, bufferData, size);
    allocation.unmap();
}

void StagingManager::init(Device& inDevice) {
    device = &inDevice;
}

void StagingManager::destroy() {
    // TODO: some might be in use
    for (BufferEntity& entity : bufferEntities) {
        delete entity.stagingBuffer;
    }
}

StagingBuffer* StagingManager::acquireBuffer(uint32_t size, VkBufferUsageFlags usage, VkMemoryPropertyFlagBits properties) {
    for (BufferEntity& entity : bufferEntities) {
        if (!entity.isUsed && entity.stagingBuffer->bufferSize == size && entity.stagingBuffer->properties == properties) {
            entity.isUsed = true;
            return entity.stagingBuffer;
        }
    }

    // Add both source and dest flags
    if ((usage & (VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT)) != 0) {
        usage |= (VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
    }

    StagingBuffer* stagingBuffer = new StagingBuffer(*device);

    VkBufferCreateInfo stagingBufferCreateInfo;
    zeroVulkanStruct(stagingBufferCreateInfo, VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO);
    stagingBufferCreateInfo.size = size;
    stagingBufferCreateInfo.usage = usage;

    verifyVulkanResult(vkCreateBuffer(device->getLogicalHandle(), &stagingBufferCreateInfo, nullptr, &stagingBuffer->buffer));

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device->getLogicalHandle(), stagingBuffer->buffer, &memRequirements);

    // Set minimum alignment to 16 bytes, as some buffers are used with CPU SIMD instructions
    memRequirements.alignment = std::max<VkDeviceSize>(16, memRequirements.alignment);

    stagingBuffer->allocation = device->getDeviceMemoryManager().allocate(size, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | properties);
    stagingBuffer->properties = properties;
    stagingBuffer->bufferSize = size;

    // TODO: should have method bindBuffer
    vkBindBufferMemory(device->getLogicalHandle(), stagingBuffer->buffer, stagingBuffer->allocation.bufferMemory, 0);

    bufferEntities.emplace_back(stagingBuffer, true);

    return stagingBuffer;
}

void StagingManager::releaseBuffer(StagingBuffer* stagingBuffer) {
    // TODO: it actually still might be in use
    for (BufferEntity& entity : bufferEntities) {
        if (entity.stagingBuffer == stagingBuffer) {
            entity.isUsed = false;
        }
    }
}

} // namespace bvd::rhi::vulkan
