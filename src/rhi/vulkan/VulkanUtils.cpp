#include "VulkanUtils.hpp"
#include "Logger.hpp"

#include <string>

namespace bvd::rhi::vulkan {

namespace detail {

void verifyVulkanResultImpl(VkResult result, char const* vkFunction, char const* filename, uint32_t line) {
    std::string error;

#define VKERRORCASE(x)    case x: error = #x

    switch (result) {
        VKERRORCASE(VK_NOT_READY); break;
        VKERRORCASE(VK_TIMEOUT); break;
        VKERRORCASE(VK_EVENT_SET); break;
        VKERRORCASE(VK_EVENT_RESET); break;
        VKERRORCASE(VK_INCOMPLETE); break;
        VKERRORCASE(VK_ERROR_OUT_OF_HOST_MEMORY); break;
        VKERRORCASE(VK_ERROR_OUT_OF_DEVICE_MEMORY); break;
        VKERRORCASE(VK_ERROR_INITIALIZATION_FAILED); break;
        VKERRORCASE(VK_ERROR_DEVICE_LOST); break;
        VKERRORCASE(VK_ERROR_MEMORY_MAP_FAILED); break;
        VKERRORCASE(VK_ERROR_LAYER_NOT_PRESENT); break;
        VKERRORCASE(VK_ERROR_EXTENSION_NOT_PRESENT); break;
        VKERRORCASE(VK_ERROR_FEATURE_NOT_PRESENT); break;
        VKERRORCASE(VK_ERROR_INCOMPATIBLE_DRIVER); break;
        VKERRORCASE(VK_ERROR_TOO_MANY_OBJECTS); break;
        VKERRORCASE(VK_ERROR_FORMAT_NOT_SUPPORTED); break;
        VKERRORCASE(VK_ERROR_SURFACE_LOST_KHR); break;
        VKERRORCASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR); break;
        VKERRORCASE(VK_SUBOPTIMAL_KHR); break;
        VKERRORCASE(VK_ERROR_OUT_OF_DATE_KHR); break;
        VKERRORCASE(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR); break;
        VKERRORCASE(VK_ERROR_VALIDATION_FAILED_EXT); break;
#if VK_HEADER_VERSION >= 13
        VKERRORCASE(VK_ERROR_INVALID_SHADER_NV); break;
#endif
#if VK_HEADER_VERSION >= 24
        VKERRORCASE(VK_ERROR_FRAGMENTED_POOL); break;
#endif
#if VK_HEADER_VERSION >= 39
        VKERRORCASE(VK_ERROR_OUT_OF_POOL_MEMORY_KHR); break;
#endif
#if VK_HEADER_VERSION >= 65
        VKERRORCASE(VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR); break;
        VKERRORCASE(VK_ERROR_NOT_PERMITTED_EXT); break;
#endif
        default:
            break;
    }

#undef VKERRORCASE

    BVD_LOG(LogGroup::rhi, BE, "{} failed, VkResult={}\n at {}:{} \n with error {}", vkFunction, (int32_t)result, filename, line, error);
}

} // namespace detail

} // namespace bvd::rhi::vulkan
