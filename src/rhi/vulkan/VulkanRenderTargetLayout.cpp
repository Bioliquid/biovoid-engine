#include "VulkanRenderTargetLayout.hpp"

#include "VulkanDevice.hpp"
#include "VulkanTexture2D.hpp"
#include "RenderPassInfo.hpp"
#include "GraphicsPipelineStateInit.hpp"
#include "VulkanUtils.hpp"
#include "Memory.hpp"

namespace bvd::rhi::vulkan {

struct Hash {
    uint8_t  format = 0;
    uint16_t attachmentsToResolve = 0;

    uint64_t getHash() {
        return (attachmentsToResolve << 8) | format;
    }
};

RenderTargetLayout::RenderTargetLayout(Device& inDevice, common::RenderPassInfo const& renderPassInfo)
    : device(inDevice)
    , numSamples(0)
{
    memzero(descriptions, sizeof(descriptions));
    memzero(colorReferences, sizeof(colorReferences));
    // memzero(&depthStencilReference, sizeof(depthStencilReference));
    memzero(extent);

    Hash hashStruct;

    bool setExtent = false;
    bool foundClearOp = false;
    // bool multiviewRenderTargets = false;

    for (uint32_t index = 0; index < renderPassInfo.numColorRenderTargets; ++index) {
        common::RenderPassInfo::ColorEntry const& colorEntry = renderPassInfo.colorRenderTargets[index];

        // TODO: it's most likely a Texture2D, but need to make sure
        TextureBase* texture = static_cast<TextureBase*>(static_cast<Texture2D*>(renderPassInfo.colorRenderTargets[0].renderTarget));

        if (setExtent) {
            BVD_CHECK(extent.extent3d.width == texture->surface.getWidth(), "extent.extent3d.width == texture->surface.getWidth()");
            BVD_CHECK(extent.extent3d.height == texture->surface.getHeight(), "extent.extent3d.height == texture->surface.getHeight()");
            BVD_CHECK(extent.extent3d.depth == texture->surface.getDepth(), "extent.extent3d.depth == texture->surface.getDepth()");
        } else {
            setExtent = true;
            extent.extent3d.width = texture->surface.getWidth();
            extent.extent3d.height = texture->surface.getHeight();
            extent.extent3d.depth = texture->surface.getDepth();
        }

        numSamples = colorEntry.renderTarget->getNumSamples();

        // multiviewRenderTargets = texture->surface.getNumArrayLevels() > 1;

        VkAttachmentDescription& currentDescription = descriptions[numAttachmentDescriptions];
        currentDescription.samples = static_cast<VkSampleCountFlagBits>(numSamples);
        currentDescription.format = device.engineToVkTextureFormat(colorEntry.renderTarget->getFormat(), texture->surface.isSrgb());
        currentDescription.loadOp = rtLoadActionToVk(colorEntry.loadAction);
        currentDescription.storeOp = rtStoreActionToVk(colorEntry.storeAction);
        currentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        currentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        currentDescription.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        currentDescription.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        colorReferences[numColorAttachments].attachment = numAttachmentDescriptions;
        colorReferences[numColorAttachments].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        foundClearOp = foundClearOp || (currentDescription.loadOp == VK_ATTACHMENT_LOAD_OP_CLEAR);

        if (currentDescription.samples > VK_SAMPLE_COUNT_1_BIT && colorEntry.resolveTarget) {
            descriptions[numAttachmentDescriptions + 1] = descriptions[numAttachmentDescriptions];
            descriptions[numAttachmentDescriptions + 1].samples = VK_SAMPLE_COUNT_1_BIT;
            descriptions[numAttachmentDescriptions + 1].loadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            descriptions[numAttachmentDescriptions + 1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;

            resolveReferences[numColorAttachments].attachment = numAttachmentDescriptions + 1;
            resolveReferences[numColorAttachments].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            hashStruct.attachmentsToResolve |= (uint16_t)(1 << numColorAttachments);
            
            ++numAttachmentDescriptions;
            containsResolveAttachments = true;
        }

        hashStruct.format = (uint8_t)currentDescription.format;

        hash = hashStruct.getHash();

        ++numAttachmentDescriptions;
        ++numColorAttachments;
    }

    // TODO: hardcoded
    // VkAttachmentDescription& depthAttachment = descriptions[1];
    // depthAttachment.format = context.findDepthFormat();
    /*depthAttachment.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;*/

    /*VkAttachmentReference depthAttachmentRef{};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;*/

    // containsDepthStencil = false;

    numUsedClearValues = foundClearOp ? numAttachmentDescriptions : 0;
}

RenderTargetLayout::RenderTargetLayout(Device& inDevice, common::GraphicsPipelineStateInit const& init)
    : device(inDevice)
    , numSamples(init.numSamples)
{
    memzero(descriptions, sizeof(descriptions));
    memzero(colorReferences, sizeof(colorReferences));
    // memzero(&depthStencilReference, sizeof(depthStencilReference));
    memzero(extent);

    Hash hashStruct;

    // bool setExtent = false;
    bool foundClearOp = false;
    // bool multiviewRenderTargets = false;

    for (uint32_t index = 0; index < init.numColorRenderTargets; ++index) {
        VkAttachmentDescription& currentDescription = descriptions[numAttachmentDescriptions];
        currentDescription.samples = static_cast<VkSampleCountFlagBits>(numSamples);
        currentDescription.format = device.engineToVkTextureFormat(init.renderTargetFormats[index], flagPresent<TextureCreateFlags::srgb>(init.renderTargetFlags[index]));
        currentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; // VK_ATTACHMENT_LOAD_OP_DONT_CARE
        currentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE; // VK_ATTACHMENT_STORE_OP_DONT_CARE
        currentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        currentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        // TODO: revisit
        currentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        currentDescription.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        colorReferences[numColorAttachments].attachment = numAttachmentDescriptions;
        colorReferences[numColorAttachments].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        foundClearOp = foundClearOp || (currentDescription.loadOp == VK_ATTACHMENT_LOAD_OP_CLEAR);

        if (currentDescription.samples > VK_SAMPLE_COUNT_1_BIT) {
            descriptions[numAttachmentDescriptions + 1] = descriptions[numAttachmentDescriptions];
            descriptions[numAttachmentDescriptions + 1].samples = VK_SAMPLE_COUNT_1_BIT;
            descriptions[numAttachmentDescriptions + 1].loadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            descriptions[numAttachmentDescriptions + 1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            
            resolveReferences[numColorAttachments].attachment = numAttachmentDescriptions + 1;
            resolveReferences[numColorAttachments].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            
            hashStruct.attachmentsToResolve |= (uint16_t)(1 << numColorAttachments);
            
            ++numAttachmentDescriptions;
            containsResolveAttachments = true;
        }

        hashStruct.format = (uint8_t)currentDescription.format;

        hash = hashStruct.getHash();

        ++numAttachmentDescriptions;
        ++numColorAttachments;
    }

    // TODO: hardcoded
    // VkAttachmentDescription& depthAttachment = descriptions[1];
    // depthAttachment.format = context.findDepthFormat();
    /*depthAttachment.format = VK_FORMAT_D32_SFLOAT_S8_UINT;
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;*/

    /*VkAttachmentReference depthAttachmentRef{};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;*/

    // containsDepthStencil = false;

    numUsedClearValues = foundClearOp ? numAttachmentDescriptions : 0;
}

} // namespace bvd::rhi::vulkan
