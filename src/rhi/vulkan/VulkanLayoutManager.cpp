#include "VulkanLayoutManager.hpp"
#include "VulkanRenderTargetLayout.hpp"
#include "VulkanRenderPass.hpp"
#include "VulkanFramebuffer.hpp"
#include "VulkanContext.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanTexture2D.hpp"
#include "RenderTargetsInfo.hpp"
#include "RenderPassInfo.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

LayoutManager::~LayoutManager() {}

Framebuffer* LayoutManager::getOrCreateFramebuffer(Device& inDevice, common::RenderTargetsInfo const& rtInfo, RenderTargetLayout const& rtLayout, RenderPass* renderPass) {
    uint64_t rtLayoutHash = rtLayout.getRenderPassCompatibleHash();

    auto foundFramebufferList = framebuffers.find(rtLayoutHash);
    FramebufferList* framebufferList = nullptr;
    if (foundFramebufferList != std::end(framebuffers)) {
        framebufferList = foundFramebufferList->second;

        for (uint32_t index = 0; index < framebufferList->framebuffer.size(); ++index) {
            if (framebufferList->framebuffer[index]->matches(rtInfo)) {
                return framebufferList->framebuffer[index];
            }
        }
    } else {
        framebufferList = new FramebufferList;
        framebuffers[rtLayoutHash] = framebufferList;
    }

    Framebuffer* framebuffer = new Framebuffer(inDevice, rtInfo, rtLayout, *renderPass);
    framebufferList->framebuffer.emplace_back(framebuffer);
    BVD_LOG(LogGroup::rhi, BD, "Framebuffers (size={})", framebuffers.size());
    for (auto& [hashId, fb] : framebuffers) {
        BVD_LOG(LogGroup::rhi, BD, "-- Framebuffers[{}] (size={})", hashId, fb->framebuffer.size());
    }
    return framebuffer;
}

RenderPass* LayoutManager::getOrCreateRenderPass(Device& inDevice, RenderTargetLayout const& rtLayout) {
    uint64_t renderPassHash = rtLayout.getRenderPassFullHash();

    auto foundRenderPass = renderPasses.find(renderPassHash);
    if (foundRenderPass != std::end(renderPasses)) {
        return foundRenderPass->second;
    }

    RenderPass* renderPass = new RenderPass(inDevice, rtLayout);
    {
        // TODO: weird behavior
        foundRenderPass = renderPasses.find(renderPassHash);
        if (foundRenderPass != std::end(renderPasses)) {
            delete renderPass;
            return foundRenderPass->second;
        }
        renderPasses[renderPassHash] = renderPass;
    }

    BVD_LOG(LogGroup::rhi, BD, "Render passes (size={})", renderPasses.size());
    return renderPass;
}

void LayoutManager::destroy() {
    for (auto& [hashId, renderPass] : renderPasses) {
        delete renderPass;
    }

    for (auto& [hashId, framebuffer] : framebuffers) {
        for (Framebuffer* fb : framebuffer->framebuffer) {
            delete fb;
        }

        delete framebuffer;
    }

    renderPasses.clear();
    framebuffers.clear();
}

void LayoutManager::beginRenderPass(CommandBuffer& cmdBuffer, common::RenderPassInfo const& renderPassInfo, RenderTargetLayout const& rtLayout, RenderPass* renderPass, Framebuffer* framebuffer) {
    // (numRenderTargets + 1 [depth] ) * 2 [surface + resolve]
    uint32_t clearValueIndex = 0;
    VkClearValue clearValues[(maxNumSimultaneousRenderTargets + 1) * 2];
    memzero(clearValues);

    bool needsClearValues = renderPass->getNumClearValues() > 0;

    uint32_t numColorTargets = renderPassInfo.numColorRenderTargets;
    for (uint32_t index = 0; index < numColorTargets; ++index) {
        common::RenderPassInfo::ColorEntry const& colorEntry = renderPassInfo.colorRenderTargets[index];
        common::Texture* colorTexture = colorEntry.renderTarget;
        
        if (needsClearValues) {
            Color clearColor = colorTexture->hasClearColor() ? colorTexture->getClearColor() : colors::black;

            clearValues[clearValueIndex].color.float32[0] = clearColor.r;
            clearValues[clearValueIndex].color.float32[1] = clearColor.g;
            clearValues[clearValueIndex].color.float32[2] = clearColor.b;
            clearValues[clearValueIndex].color.float32[3] = clearColor.a;

            ++clearValueIndex;
        }
    }

    cmdBuffer.beginRenderPass(rtLayout, *renderPass, *framebuffer, clearValues);
}

void LayoutManager::endRenderPass(CommandBuffer& cmdBuffer) {
    cmdBuffer.endRenderPass();
}

void LayoutManager::notifyDeletedRenderTarget(VkImage image) {
    for (auto& [hashId, framebuffer] : framebuffers) {
        std::erase_if(framebuffer->framebuffer, [image](Framebuffer* fb) {
            if (fb->containsRenderTarget(image)) {
                delete fb;
                return true;
            }
            return false;
        });
    }

    std::erase_if(framebuffers, [](auto const& item) {
        auto const& [hasId, framebuffer] = item;
        if (framebuffer->framebuffer.empty()) {
            delete framebuffer;
            return true;
        }
        return false;
    });
}

void LayoutManager::notifyDeletedImage(VkImage) {

}

} // namespace bvd::rhi::vulkan
