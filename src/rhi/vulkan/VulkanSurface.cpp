#include "VulkanSurface.hpp"
#include "VulkanContext.hpp"
#include "VulkanCommandBufferManager.hpp"
#include "VulkanCommandBuffer.hpp"
#include "VulkanPipelineBarrier.hpp"
#include "VulkanUtils.hpp"
#include "VulkanDevice.hpp"
#include "VulkanMemory.hpp"
#include "VulkanStagingMemory.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

#define VK_IMAGE_VIEW_TYPE_RANGE_SIZE (VK_IMAGE_VIEW_TYPE_CUBE_ARRAY - VK_IMAGE_VIEW_TYPE_1D + 1)

static const VkImageTiling vulkanViewTypeTilingMode[VK_IMAGE_VIEW_TYPE_RANGE_SIZE] =
{
    VK_IMAGE_TILING_LINEAR,     // VK_IMAGE_VIEW_TYPE_1D
    VK_IMAGE_TILING_OPTIMAL,    // VK_IMAGE_VIEW_TYPE_2D
    VK_IMAGE_TILING_OPTIMAL,    // VK_IMAGE_VIEW_TYPE_3D
    VK_IMAGE_TILING_OPTIMAL,    // VK_IMAGE_VIEW_TYPE_CUBE
    VK_IMAGE_TILING_LINEAR,     // VK_IMAGE_VIEW_TYPE_1D_ARRAY
    VK_IMAGE_TILING_OPTIMAL,    // VK_IMAGE_VIEW_TYPE_2D_ARRAY
    VK_IMAGE_TILING_OPTIMAL,    // VK_IMAGE_VIEW_TYPE_CUBE_ARRAY
};

void setImageLayout(VkCommandBuffer cmdBuffer, VkImage image, VkImageLayout srcLayout, VkImageLayout dstLayout, VkImageSubresourceRange const& subresourceRange) {
    PipelineBarrier barrier;
    barrier.addImageLayoutTransition(image, srcLayout, dstLayout, subresourceRange);
    barrier.execute(cmdBuffer);
}

Surface::Surface(Device& inDevice, common::TextureCreateInfo const& info, VkImageViewType inViewType, uint32_t inDepth, uint32_t inArraySize)
    : width(info.width)
    , height(info.height)
    , depth(inDepth)
    , arraySize(inArraySize)
    , pixelFormat(info.format)
    , flags(info.flags)
    , numArrayLevels(inArraySize)
    , viewType(inViewType)
    , numMips(info.numMips)
    , numSamples(info.numSamples)
    , device(inDevice)
{
    VkFormat srgbFormat = device.engineToVkTextureFormat(pixelFormat, flagPresent<TextureCreateFlags::srgb>(flags));
    // VkFormat nonSrgbFormat = device.engineToVkTextureFormat(pixelFormat, false);
    
    viewFormat = srgbFormat;

    fullAspectMask = getAspectMaskFromEngineFormat(pixelFormat, true, true);
    partialAspectMask = getAspectMaskFromEngineFormat(pixelFormat, false, true);

    if (isPresentable()) {
        return;
    }

    VkImageCreateInfo imageInfo;
    zeroVulkanStruct(imageInfo, VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = depth;
    imageInfo.mipLevels = numMips;
    imageInfo.arrayLayers = numArrayLevels;
    imageInfo.format = srgbFormat;
    imageInfo.tiling = vulkanViewTypeTilingMode[viewType];
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    imageInfo.usage = 0;
    imageInfo.usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    imageInfo.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    imageInfo.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;

    if (flagPresent<TextureCreateFlags::renderTargetable>(flags)) {
        imageInfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    }

    imageInfo.samples = static_cast<VkSampleCountFlagBits>(numSamples);
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.queueFamilyIndexCount = 0;
    imageInfo.pQueueFamilyIndices = nullptr;

    verifyVulkanResult(vkCreateImage(inDevice.getLogicalHandle(), &imageInfo, nullptr, &image));

    // TODO: it's the same as in VulkanBuffer except for bind (need to make allocation that can do both)
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(inDevice.getLogicalHandle(), image, &memRequirements);

    allocation = inDevice.getDeviceMemoryManager().allocate(memRequirements.size, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    vkBindImageMemory(inDevice.getLogicalHandle(), image, allocation.bufferMemory, 0);
}

Surface::~Surface() {
    device.notifyDeletedImage(image, isRenderTargetable());

    if (!isPresentable()) {
        vkDestroyImage(device.getLogicalHandle(), image, nullptr);
        allocation.free();
    }
}

void Surface::internalLockWrite(Context& context, Surface& surface, VkImageSubresourceRange const& subresourceRange, VkBufferImageCopy const& region, StagingBuffer* stagingBuffer) {
    CommandBuffer& cmdBuffer = context.getCommandBufferManager().getUploadCmdBuffer();

    setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, subresourceRange);
    vkCmdCopyBufferToImage(cmdBuffer.getHandle(), stagingBuffer->getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    // TODO: revisit this when my own image format with pregenerated mips will exist
    if (surface.getNumMips() == 1) {
        setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange);
    } else {
        VkImageSubresourceRange subresourceRange1 = PipelineBarrier::makeSubresourceRange(VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1);

        int32_t mipWidth = surface.width;
        int32_t mipHeight = surface.height;

        for (uint32_t baseMipLevel = 0; baseMipLevel < surface.getNumMips() - 1; ++baseMipLevel) {
            subresourceRange1.baseMipLevel = baseMipLevel;

            setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, subresourceRange1);

            VkImageBlit blit{};
            blit.srcOffsets[0] = {0, 0, 0};
            blit.srcOffsets[1] = {mipWidth, mipHeight, 1};
            blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            blit.srcSubresource.mipLevel = baseMipLevel;
            blit.srcSubresource.baseArrayLayer = 0;
            blit.srcSubresource.layerCount = 1;
            blit.dstOffsets[0] = {0, 0, 0};
            blit.dstOffsets[1] = {mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1};
            blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            blit.dstSubresource.mipLevel = baseMipLevel + 1;
            blit.dstSubresource.baseArrayLayer = 0;
            blit.dstSubresource.layerCount = 1;

            vkCmdBlitImage(cmdBuffer.getHandle(),
                surface.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1, &blit,
                VK_FILTER_LINEAR);

            setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange1);

            if (mipWidth > 1) {
                mipWidth /= 2;
            }

            if (mipHeight > 1) {
                mipHeight /= 2;
            }
        }

        subresourceRange1.baseMipLevel = surface.getNumMips() - 1;

        setImageLayout(cmdBuffer.getHandle(), surface.image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, subresourceRange1);
    }

    surface.device.getStagingManager().releaseBuffer(stagingBuffer);

    context.getCommandBufferManager().submitUploadCmdBuffer();
}

} // namespace bvd::rhi::vulkan
