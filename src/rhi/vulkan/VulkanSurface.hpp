#pragma once

#include "RhiDefinitions.hpp"
#include "PixelFormat.hpp"
#include "Texture.hpp"
#include "Memory.hpp"
#include "VulkanMemory.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Context;
class Device;
class StagingBuffer;
class TextureBase;
class BackBuffer;
class Framebuffer;

void setImageLayout(VkCommandBuffer, VkImage, VkImageLayout, VkImageLayout, VkImageSubresourceRange const&);

class Surface {
    friend class TextureBase;
    friend class BackBuffer;
    friend class Framebuffer;

public:
    Surface(Device&, common::TextureCreateInfo const&, VkImageViewType, uint32_t, uint32_t);
    ~Surface();

    static void internalLockWrite(Context&, Surface&, VkImageSubresourceRange const&, VkBufferImageCopy const&, StagingBuffer*);

    // Full includes Depth+Stencil
    inline VkImageAspectFlags getFullAspectMask() const { return fullAspectMask; }
    // Only Depth or Stencil
    inline VkImageAspectFlags getPartialAspectMask() const { return partialAspectMask; }
    inline bool isDepthOrStencilAspect() const { return (fullAspectMask & (VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT)) != 0; }

    inline uint32_t getNumMips() const { return numMips; }
    inline bool isPresentable() const { return flagPresent<TextureCreateFlags::presentable>(flags); }
    inline bool isRenderTargetable() const { return flagPresent<TextureCreateFlags::renderTargetable>(flags); }

    inline VkImageViewType getViewType() const { return viewType; }

    inline uint32_t getNumArrayLevels() const { return numArrayLevels; }
    inline bool isSrgb() const { return flagPresent<TextureCreateFlags::srgb>(flags); }

    inline uint32_t getWidth() const { return width; }
    inline uint32_t getHeight() const { return height; }
    inline uint32_t getDepth() const { return depth; }

private:
    VkImage                     image = VK_NULL_HANDLE;

    uint32_t                    width;
    uint32_t                    height;
    uint32_t                    depth;
    uint32_t                    arraySize;
    PixelFormat                 pixelFormat;
    VkFormat                    viewFormat;
    TextureCreateFlags::Type    flags;
    uint32_t                    numArrayLevels;

    VkImageViewType	            viewType;

    uint32_t                    numMips;
    uint32_t                    numSamples;

    VkImageAspectFlags          fullAspectMask;
    VkImageAspectFlags          partialAspectMask;

    // TODO: make an actual allocation class
    DeviceMemoryAllocation      allocation;

public:
    // TODO: delete
    Device&                     device;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
