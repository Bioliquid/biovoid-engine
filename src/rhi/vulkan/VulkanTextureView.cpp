#include "VulkanTextureView.hpp"
#include "VulkanDevice.hpp"
#include "VulkanUtils.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

void TextureView::create(Device& device, VkImage inImage, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t numMips) {
    VkImageViewCreateInfo viewInfo;
    zeroVulkanStruct(viewInfo, VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO);
    viewInfo.image = inImage;
    // TODO: support multiple view types
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = numMips;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    verifyVulkanResult(vkCreateImageView(device.getLogicalHandle(), &viewInfo, nullptr, &view));

    image = inImage;
}

} // namespace bvd::rhi::vulkan
