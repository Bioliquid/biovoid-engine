#pragma once

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

class TextureView {
public:
    void create(Device&, VkImage, VkFormat, VkImageAspectFlags, uint32_t);


    VkImageView view = VK_NULL_HANDLE;
    VkImage     image = VK_NULL_HANDLE;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
