#pragma once

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

struct DeviceMemoryAllocation {
    void* map(VkDeviceSize, VkDeviceSize);
    void unmap();

    void free();

    VkDevice        device;
    VkDeviceMemory  bufferMemory;
    void*           mappedPtr;
};

class DeviceMemoryManager {
public:
    void init(Device&);

    DeviceMemoryAllocation allocate(VkDeviceSize, uint32_t, VkMemoryPropertyFlags);

private:
    void updateMemoryProperties();
    uint32_t getMemoryTypeFromProperties(uint32_t typeBits, VkMemoryPropertyFlags);

    Device* device;

    VkPhysicalDeviceMemoryProperties memoryProperties;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
