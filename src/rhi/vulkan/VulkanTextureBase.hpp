#pragma once

#include "VulkanSurface.hpp"
#include "VulkanTextureView.hpp"
#include "Texture.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

class TextureBase {
public:
    TextureBase(Device&, common::TextureCreateInfo const&, VkImageViewType, uint32_t, uint32_t);
    virtual ~TextureBase();

    void generateMips();

    // TODO: make private
    Surface         surface;
    // View with all mips/layers
    TextureView     defaultView;
    // View with all mips/layers, but if it's a Depth/Stencil, only the Depth view
    TextureView*    partialView = nullptr;

    // TODO: delete
    VkSampler       sampler;

private:
    // TODO: delete
    Device& device;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
