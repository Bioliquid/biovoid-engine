#pragma once

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;

// TODO: should ref counted 
class Fence {
public:
    enum class State {
          // Initial state
          notReady
          // After GPU processed it
        , signaled
    };

public:
    Fence(Device&, bool);
    ~Fence();

    bool waitForFence(uint64_t);
    void reset();

    bool isSignaled();

    inline VkFence getHandle() const { return handle; }

private:
    Device& device;
    VkFence handle;
    State   state;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
