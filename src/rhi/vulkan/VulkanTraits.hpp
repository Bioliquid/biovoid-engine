#pragma once

namespace bvd {

namespace vulkan {

struct VkTraits {
    static constexpr char const* requiredInstanceExtensions[2] = {"VK_KHR_surface", "VK_KHR_win32_surface"};
};

} // namespace vulkan

} // namespace bvd
