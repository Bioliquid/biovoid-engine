#pragma once

#include "VulkanCommandBufferPool.hpp"

#include <vector>

namespace bvd {

namespace rhi {

namespace vulkan {

class Context;
class Device;
class CommandBuffer;
class Queue;
class Semaphore;

class CommandBufferManager {
    struct QueryPoolReset {
        VkQueryPool pool;
        uint32_t    size;
    };

public:
    CommandBufferManager(Device&, Context&);
    ~CommandBufferManager();

    void init();

    void prepareForNewActiveCommandBuffer();

    void submitActiveCmdBufferFromPresent(Semaphore*);
    void submitActiveCmdBuffer();
    CommandBuffer& getActiveCmdBuffer();

    void submitUploadCmdBuffer();
    CommandBuffer& getUploadCmdBuffer();

    inline bool hasPendingUploadCmdBuffer() const { return uploadCmdBuffer != nullptr; }
    inline bool hasPendingActiveCmdBuffer() const { return activeCmdBuffer != nullptr; }

private:
    Device&                 device;
    CommandBufferPool       pool;
    Queue&                  queue;
    CommandBuffer*          activeCmdBuffer;
    CommandBuffer*          uploadCmdBuffer;

    /** This semaphore is used to prevent overlaps between the (current) graphics cmdbuf and next upload cmdbuf. */
    Semaphore*              activeCmdBufferSemaphore;

    /** Holds semaphores associated with the recent upload cmdbuf(s) - waiting to be added to the next graphics cmdbuf as WaitSemaphores. */
    std::vector<Semaphore*> renderingCompletedSemaphores;

    /** This semaphore is used to prevent overlaps between (current) upload cmdbuf and next graphics cmdbuf. */
    Semaphore*              uploadCmdBufferSemaphore;

    /** Holds semaphores associated with the recent upload cmdbuf(s) - waiting to be added to the next graphics cmdbuf as WaitSemaphores. */
    std::vector<Semaphore*> uploadCompletedSemaphores;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
