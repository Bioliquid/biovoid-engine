#include "VulkanFramebuffer.hpp"
#include "RenderTargetsInfo.hpp"
#include "VulkanUtils.hpp"
#include "VulkanRenderPass.hpp"
#include "VulkanRenderTargetLayout.hpp"
#include "VulkanDevice.hpp"
#include "VulkanTexture2D.hpp"
#include "Memory.hpp"

namespace bvd::rhi::vulkan {

Framebuffer::Framebuffer(Device& inDevice, common::RenderTargetsInfo const& rtInfo, RenderTargetLayout const& rtLayout, RenderPass const& renderPass)
    : device(inDevice)
    , numColorRenderTargets(rtInfo.numColorRenderTargets)
{
    memzero(colorRenderTargetImages, sizeof(colorRenderTargetImages));
    memzero(colorResolveTargetImages, sizeof(colorResolveTargetImages));

    attachmentTextureViews.reserve(rtLayout.getNumAttachmentDescriptions());

    VkExtent3D const& rtExtent = rtLayout.getExtent3D();

    for (uint32_t index = 0; index < rtInfo.numColorRenderTargets; ++index) {
        common::Texture* rhiTexture = rtInfo.colorRenderTargets[index].texture;
        if (!rhiTexture) {
            continue;
        }

        TextureBase* texture = static_cast<TextureBase*>(static_cast<Texture2D*>(rhiTexture));
        BVD_CHECK(texture->surface.image != VK_NULL_HANDLE, "Color render target is nullptr");

        colorRenderTargetImages[index] = texture->surface.image;

        // TODO: create based on view type (see UE)
        // also there is pixel format
        TextureView& rtView = attachmentTextureViews.emplace_back();
        rtView.create(device, texture->surface.image, texture->surface.viewFormat, texture->surface.getFullAspectMask(), 1);

        attachmentViewsToDelete.push_back(rtView.view);

        if (rtInfo.hasResolveAttachments && rtLayout.hasResolveAttachments() && rtLayout.getResolveAttachmentReferences()[index].layout != VK_IMAGE_LAYOUT_UNDEFINED) {
            common::Texture* resolveRhiTexture = rtInfo.colorResolveRenderTargets[index].texture;
            TextureBase* resolveTexture = static_cast<TextureBase*>(static_cast<Texture2D*>(resolveRhiTexture));
            colorResolveTargetImages[index] = resolveTexture->surface.image;

            // resolve attachments only supported for 2d/2d array textures
            TextureView& resolveRtView = attachmentTextureViews.emplace_back();
            if (resolveTexture->surface.getViewType() == VK_IMAGE_VIEW_TYPE_2D || resolveTexture->surface.getViewType() == VK_IMAGE_VIEW_TYPE_2D_ARRAY) {
                resolveRtView.create(device, resolveTexture->surface.image, resolveTexture->surface.viewFormat, resolveTexture->surface.getFullAspectMask(), 1);
            }

            attachmentViewsToDelete.push_back(resolveRtView.view);
        }
    }

    // if (rtLayout.hasDepthStencil()) {

    // }

    std::vector<VkImageView> attachments;
    for (TextureView& view : attachmentTextureViews) {
        attachments.push_back(view.view);
    }

    VkFramebufferCreateInfo createInfo;
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO);
    createInfo.renderPass = renderPass.getHandle();
    createInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    createInfo.pAttachments = attachments.data();
    createInfo.width = rtExtent.width;
    createInfo.height = rtExtent.height;
    createInfo.layers = rtExtent.depth;

    // if (VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR || VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR) {
    //     std::swap(createInfo.width, createInfo.height);
    // }

    verifyVulkanResult(vkCreateFramebuffer(device.getLogicalHandle(), &createInfo, nullptr, &handle));

    renderArea.offset.x = 0;
    renderArea.offset.y = 0;
    renderArea.extent.width = rtExtent.width;
    renderArea.extent.height = rtExtent.height;
    

    BVD_LOG(LogGroup::rhi, BD, "Framebuffer(width={}, height={}) was created", createInfo.width, createInfo.height);
}

bool Framebuffer::matches(common::RenderTargetsInfo const& rtInfo) const {
    if (numColorRenderTargets != rtInfo.numColorRenderTargets) {
        return false;
    }

    /*{
        common::RenderTargetView const& b = rtInfo.depthStencilRenderTarget;
        if (b.texture) {
            VkImage fbImage = depthStencilRenderTargetImage;
            VkImage rtImage = static_cast<TextureBase*>(b.texture->getTextureBaseRhi())->surface.image;
            if (fbImage != rtImage) {
                return false;
            }
        }
    }*/

    for (uint32_t index = 0; index < rtInfo.numColorRenderTargets; ++index) {
        if (rtInfo.hasResolveAttachments) {
            common::RenderTargetView const& r = rtInfo.colorResolveRenderTargets[index];
            if (r.texture) {
                VkImage aImage = colorResolveTargetImages[index];
                VkImage bImage = static_cast<TextureBase*>(static_cast<Texture2D*>(r.texture))->surface.image;
                if (aImage != bImage) {
                    return false;
                }
            }
        }

        common::RenderTargetView const& b = rtInfo.colorRenderTargets[index];
        if (b.texture) {
            VkImage fbImage = colorRenderTargetImages[index];
            VkImage rtImage = static_cast<TextureBase*>(static_cast<Texture2D*>(b.texture))->surface.image;
            if (fbImage != rtImage) {
                return false;
            }
        }
    }

    return true;
}

Framebuffer::~Framebuffer() {
    for (VkImageView view : attachmentViewsToDelete) {
        vkDestroyImageView(device.getLogicalHandle(), view, nullptr);
    }

    vkDestroyFramebuffer(device.getLogicalHandle(), handle, nullptr);
}

} // namespace bvd::rhi::vulkan
