#pragma once

#include "RhiDefinitions.hpp"
#include "VulkanUtils.hpp"

#include <vulkan/vulkan.hpp>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace common {

class RenderPassInfo;
struct GraphicsPipelineStateInit;

} // namespace common

namespace vulkan {

class Device;

class RenderTargetLayout {
public:
    RenderTargetLayout(Device&, common::RenderPassInfo const&);
    RenderTargetLayout(Device&, common::GraphicsPipelineStateInit const&);

    RenderTargetLayout(RenderTargetLayout const&) = default;
    RenderTargetLayout(RenderTargetLayout&&) = delete;
    RenderTargetLayout& operator=(RenderTargetLayout const&) = delete;
    RenderTargetLayout& operator=(RenderTargetLayout&&) = delete;

    inline VkExtent2D const& getExtent2D() const { return extent.extent2d; }
    inline VkExtent3D const& getExtent3D() const { return extent.extent3d; }
    inline uint32_t getNumClearValues() const { return numUsedClearValues; }

    uint64_t getRenderPassCompatibleHash() const { return hash; }
    uint64_t getRenderPassFullHash() const { return hash; }

    inline uint32_t getNumAttachmentDescriptions() const { return numAttachmentDescriptions; }
    inline VkAttachmentDescription const* getAttachmentDescriptions() const { return descriptions; }

    inline uint32_t getNumColorAttachments() const { return numColorAttachments; }
    inline VkAttachmentReference const* getColorAttachmentReferences() const { return numColorAttachments > 0 ? colorReferences : nullptr; }

    inline bool hasResolveAttachments() const { return containsResolveAttachments; }
    inline VkAttachmentReference const* getResolveAttachmentReferences() const { return containsResolveAttachments ? resolveReferences : nullptr; }

    // inline bool hasDepthStencil() const { return containsDepthStencil; }
    // inline VkAttachmentReference const* getDepthStencilAttachmentReference() const { return containsDepthStencil ? &depthStencilReference : nullptr; }

private:
    Device& device;

    // Depth goes in the "+1" slot and the Fixed Foveation texture goes in the "+2" slot.
    uint32_t                numAttachmentDescriptions = 0;
    VkAttachmentDescription descriptions[maxNumSimultaneousRenderTargets * 2 + 2];

    uint32_t                numColorAttachments = 0;
    VkAttachmentReference   colorReferences[maxNumSimultaneousRenderTargets];

    bool                    containsResolveAttachments = false;
    VkAttachmentReference   resolveReferences[maxNumSimultaneousRenderTargets];

    // bool                    containsDepthStencil;
    // VkAttachmentReference   depthStencilReference;

    uint32_t numUsedClearValues;
    uint32_t numSamples;

    union {
        VkExtent3D extent3d;
        VkExtent2D extent2d;
    } extent;

    uint64_t hash;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
