#pragma once

#include <vulkan/vulkan.h>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace vulkan {

class Device;
class CommandBuffer;

class Queue {
public:
    Queue(Device&, uint32_t);

    void submit(CommandBuffer&, uint32_t, VkSemaphore*);

    inline uint32_t getFamilyIndex() const { return familyIndex; }
    inline uint32_t getQueueIndex() const { return queueIndex; }

    inline VkQueue getHandle() { return handle; }

private:
    Device&     device;

    VkQueue     handle;
    uint32_t    familyIndex;
    uint32_t    queueIndex;
};

} // namespace vulkan

} // namespace rhi

} // namespace bvd
