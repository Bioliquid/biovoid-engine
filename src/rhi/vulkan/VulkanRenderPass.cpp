#include "VulkanRenderPass.hpp"
#include "VulkanUtils.hpp"
#include "VulkanDevice.hpp"
#include "Logger.hpp"

namespace bvd::rhi::vulkan {

RenderPass::RenderPass(Device& inDevice, RenderTargetLayout const& inRtLayout)
    : layout(inRtLayout)
    , handle(VK_NULL_HANDLE)
    , device(inDevice)
{
    VkRenderPassCreateInfo createInfo;
    zeroVulkanStruct(createInfo, VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO);

    uint32_t numSubpasses = 0;
    VkSubpassDescription subpassDescriptions[8];
    uint32_t numDependencies = 0;
    VkSubpassDependency subpassDependencies[8];

    memzero(subpassDescriptions, sizeof(subpassDescriptions));
    memzero(subpassDependencies, sizeof(subpassDependencies));

    // main sub-pass
    {
        VkSubpassDescription& subpassDesc = subpassDescriptions[numSubpasses];
        memzero(&subpassDesc, sizeof(VkSubpassDescription));

        subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpassDesc.colorAttachmentCount = layout.getNumColorAttachments();
        subpassDesc.pColorAttachments = layout.getColorAttachmentReferences();
        subpassDesc.pDepthStencilAttachment = nullptr;
        subpassDesc.pResolveAttachments = layout.getResolveAttachmentReferences();
        // subpassDesc.pDepthStencilAttachment = layout.getDepthStencilAttachmentReference();

        ++numSubpasses;
    }

    // hardcoded
    {
        VkSubpassDependency& subpassDep = subpassDependencies[numDependencies];

        subpassDep.srcSubpass = VK_SUBPASS_EXTERNAL;
        subpassDep.dstSubpass = 0;
        // subpassDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        subpassDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDep.srcAccessMask = 0;
        // subpassDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        subpassDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        // subpassDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        subpassDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        ++numDependencies;
    }

    createInfo.attachmentCount = layout.getNumAttachmentDescriptions();
    createInfo.pAttachments = layout.getAttachmentDescriptions();
    createInfo.subpassCount = numSubpasses;
    createInfo.pSubpasses = subpassDescriptions;
    createInfo.dependencyCount = numDependencies;
    createInfo.pDependencies = subpassDependencies;

    verifyVulkanResult(vkCreateRenderPass(device.getLogicalHandle(), &createInfo, nullptr, &handle));

    BVD_LOG(LogGroup::rhi, BD, "Render pass was created");
}

RenderPass::~RenderPass() {
    vkDestroyRenderPass(device.getLogicalHandle(), handle, nullptr);
}

} // namespace bvd::rhi::vulkan
