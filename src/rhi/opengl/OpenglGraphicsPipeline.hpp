#pragma once

#include "OpenglBuffer.hpp"
#include "GraphicsPipeline.hpp"
#include "Ref.hpp"

#include <glad/glad.h>
#include <map>

namespace bvd {

namespace rhi {

namespace opengl {

class Device;

class GraphicsPipeline : public common::GraphicsPipeline {
public:
    GraphicsPipeline(Device&);

    GraphicsPipeline(GraphicsPipeline const&) = delete;
    GraphicsPipeline(GraphicsPipeline&&) = delete;
    GraphicsPipeline& operator=(GraphicsPipeline const&) = delete;
    GraphicsPipeline& operator=(GraphicsPipeline&&) = delete;

    virtual void loadShaders(common::GraphicsPipelineStateInit const&) override;
    virtual void bind() const override;
    virtual void destroy() override;
    virtual common::Device const& getDevice() const override;

    virtual void setUniformBuffer(uint32_t, uint32_t, void const*, uint64_t, uint64_t) override;
    virtual void cmdSetUniformBuffer(uint32_t, uint32_t, void const*, uint64_t, uint64_t) override;
    virtual void setTexture(uint32_t, uint32_t, uint32_t, common::Texture const&) override;

    void prepareVao(uint32_t, uint32_t, uint32_t (&)[2], uint32_t(&)[2]) const;

private:
    Device& device;

    uint32_t handle;
    uint32_t vao;

    std::vector<std::vector<Ref<Buffer>>> uniformBuffers;
    std::map<GLuint, std::map<uint32_t, GLuint>> textures;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
