#pragma once

#include "Texture2D.hpp"
#include "Memory.hpp"

#include <glad/glad.h>
#include <cstdint>

namespace bvd {

namespace rhi {

namespace opengl {

class DynamicRhi;
class Device;

class Texture2D : public common::Texture2D {
public:
    Texture2D(DynamicRhi&, Device&, common::TextureCreateInfo const&);
    ~Texture2D();

    Texture2D(Texture2D const&) = delete;
    Texture2D(Texture2D&&) = delete;
    Texture2D& operator=(Texture2D const&) = delete;
    Texture2D& operator=(Texture2D&&) = delete;

    GLuint getHandle() const { return handle; }

    inline bool isRenderTargetable() const { return flagPresent<TextureCreateFlags::renderTargetable>(flags); }

private:
    DynamicRhi& rhi;
    Device&     device;
    GLuint      handle;

    TextureCreateFlags::Type flags;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
