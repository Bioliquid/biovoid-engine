#include "OpenglViewport.hpp"
#include "OpenglDynamicRhi.hpp"
#include "OpenglContext.hpp"
#include "OpenglFramebuffer.hpp"
#include "Logger.hpp"

#include <glad/glad.h>

namespace bvd::rhi::opengl {

Viewport::Viewport(DynamicRhi& inRhi, Device& inDevice, uint32_t inWidth, uint32_t inHeight, PixelFormat preferredPixelFormat)
    : rhi(inRhi)
    , device(inDevice)
    , width(inWidth)
    , height(inHeight)
    , pixelFormat(preferredPixelFormat)
{
    createBackBuffer();
}

void Viewport::createBackBuffer() {
    backbuffer = new Texture2D(rhi, device, common::TextureCreateInfo(pixelFormat, width, height, TextureCreateFlags::presentable, colors::black));
}

void Viewport::recreateBackBuffer() {
    rhi.notifyDeletedImage(static_cast<Texture2D&>(*backbuffer).getHandle(), true);

    backbuffer.reset();
    createBackBuffer();
}

void Viewport::resize(uint32_t inWidth, uint32_t inHeight) {
    width = inWidth;
    height = inHeight;

    recreateBackBuffer();
}

void Viewport::present(Context& context) {
    BVD_LOG(LogGroup::rhi, BD, "Presenting");

    glDisable(GL_FRAMEBUFFER_SRGB);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDrawBuffer(GL_BACK);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, context.getLastFramebuffer()->getHandle());
    glReadBuffer(GL_COLOR_ATTACHMENT0);

    // context.resetLastFramebuffer();

    GLint backbufferSizeX = backbuffer->getWidth();
    GLint backbufferSizeY = backbuffer->getHeight();

    glBlitFramebuffer(
        0, 0, backbufferSizeX, backbufferSizeY,
        0, 0, backbufferSizeX, backbufferSizeY,
        GL_COLOR_BUFFER_BIT,
        GL_NEAREST
    );

    glEnable(GL_FRAMEBUFFER_SRGB);
}

} // namespace bvd::rhi::opengl
