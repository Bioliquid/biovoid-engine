#pragma once

#include "Context.hpp"
#include "OpenglLayoutManager.hpp"

namespace bvd {

namespace rhi {

namespace opengl {

class Device;
class DynamicRhi;
class PendingGfxState;
class Framebuffer;

class Context : public common::Context {
public:
    Context(DynamicRhi&, Device&);
    ~Context();

    Context(Context const&) = delete;
    Context(Context&&) = delete;
    Context& operator=(Context const&) = delete;
    Context& operator=(Context&&) = delete;

    virtual void beginDrawingViewport(common::Viewport*) override;
    virtual void endDrawingViewport(common::Viewport*) override;

    virtual void beginRenderPass(common::RenderPassInfo const&) override;
    virtual void endRenderPass() override;

    virtual void setStreamSource(uint32_t, common::Buffer const&, uint32_t) override;
    virtual void setGraphicsPipeline(common::GraphicsPipeline const*) override;

    virtual void drawPrimitive(uint32_t, uint32_t, uint32_t) override;
    virtual void drawIndexedPrimitive(common::Buffer const&, int32_t, int32_t, int32_t, int32_t, int32_t) override;

    virtual void waitIdle() override {}

    inline void notifyDeletedRenderTarget(uint32_t image) { layoutManager.notifyDeletedRenderTarget(image); }
    inline void notifyDeletedImage(uint32_t image) { layoutManager.notifyDeletedRenderTarget(image); }

    Framebuffer const* getLastFramebuffer() const { return lastFramebuffer; }
    void resetLastFramebuffer() { lastFramebuffer = nullptr; }

private:
    DynamicRhi&         rhi;
    Device&             device;
    PendingGfxState*    pendingGfxState;
    Framebuffer const*  lastFramebuffer;

    inline static LayoutManager layoutManager;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
