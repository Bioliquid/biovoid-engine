#pragma once

#include "Buffer.hpp"

namespace bvd {

namespace rhi {

namespace opengl {

class Buffer : public common::Buffer {
public:
    Buffer(int64_t);

    Buffer(Buffer const&) = delete;
    Buffer(Buffer&&) = delete;
    Buffer& operator=(Buffer const&) = delete;
    Buffer& operator=(Buffer&&) = delete;

    virtual void copy(void const*, int64_t, int64_t) override;
    virtual void copy(void const*, int64_t) override;
    virtual void cmdCopy(void const*, int64_t) override;
    virtual void cmdCopy(void const*, int64_t, int64_t) override;
    virtual void destroy() override;

    uint32_t getHandle() const { return handle; }

private:
    uint32_t handle;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
