#include "OpenglGraphicsPipeline.hpp"

#include "OpenglDevice.hpp"
#include "OpenglBuffer.hpp"
#include "OpenglTexture2D.hpp"
#include "OpenglUtils.hpp"

#include "VertexInputRate.hpp"
#include "UniformTypeInfo.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <vector>

namespace bvd::rhi::opengl {

GraphicsPipeline::GraphicsPipeline(Device& inDevice) : device(inDevice) {}

void GraphicsPipeline::loadShaders(common::GraphicsPipelineStateInit const& init) {
    const size_t numShaderSources = init.shaderSources.size();

    handle = glCreateProgram();
    
    std::vector<GLuint> shaderIDs;
    for (size_t index = 0; index < numShaderSources; ++index) {
        common::ShaderSource const& shaderSource = init.shaderSources[index];
        GLuint stage{};
        switch (shaderSource.stage) {
            case ShaderStage::vertex: {
                stage = GL_VERTEX_SHADER;
                break;
            }
            case ShaderStage::fragment: {
                stage = GL_FRAGMENT_SHADER;
                break;
            }
            default:
                break;
        }
        

        GLuint shaderID = shaderIDs.emplace_back(glCreateShader(stage));
        glShaderBinary(1, &shaderID, GL_SHADER_BINARY_FORMAT_SPIR_V, shaderSource.source.data(), GLsizei(shaderSource.source.size() * sizeof(uint32_t)));
        glSpecializeShader(shaderID, "main", 0, nullptr, nullptr);
        glAttachShader(handle, shaderID);
    }
    

    glLinkProgram(handle);

    GLint isLinked;
    verifyOpenglResult(glGetProgramiv(handle, GL_LINK_STATUS, &isLinked));
    if (isLinked == GL_FALSE) {
        GLint maxLength;
        verifyOpenglResult(glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &maxLength));

        std::vector<GLchar> infoLog(maxLength);
        verifyOpenglResult(glGetProgramInfoLog(handle, maxLength, &maxLength, infoLog.data()));

        verifyOpenglResult(glDeleteProgram(handle));

        for (auto id : shaderIDs) {
            verifyOpenglResult(glDeleteShader(id));
        }

        BVD_THROW("Shader linking failed: {}", infoLog.data());
    }


    for (auto id : shaderIDs) {
        verifyOpenglResult(glDetachShader(handle, id));
        verifyOpenglResult(glDeleteShader(id));
    }


    // vertex array
    verifyOpenglResult(glCreateVertexArrays(1, &vao));

    for (uint32_t vertexInputBindingIndex = 0; vertexInputBindingIndex < vertexInputLayout.size(); ++vertexInputBindingIndex) {
        VertexInputBinding const& vertexInputBinding = vertexInputLayout[vertexInputBindingIndex];
        for (uint32_t vertexInputAttributeIndex = 0; vertexInputAttributeIndex < vertexInputBinding.attributes.size(); ++vertexInputAttributeIndex) {
            VertexInputAttribute const& vertexInputAttribute = vertexInputBinding.attributes[vertexInputAttributeIndex];

            verifyOpenglResult(glEnableVertexArrayAttrib(vao, vertexInputAttribute.location));
            glVertexArrayAttribBinding(vao, vertexInputAttribute.location, vertexInputBindingIndex);

            // TODO: make more clear
            GLenum type = device.uniformTypes[vertexInputAttribute.uniformType].platformFormat;
            switch (type) {
                case GL_FLOAT:
                    glVertexArrayAttribFormat(vao, vertexInputAttribute.location, device.uniformTypes[vertexInputAttribute.uniformType].elementSize, type, GL_FALSE, vertexInputAttribute.offset);
                    break;
                case GL_UNSIGNED_INT:
                    glVertexArrayAttribIFormat(vao, vertexInputAttribute.location, device.uniformTypes[vertexInputAttribute.uniformType].elementSize, type, vertexInputAttribute.offset);
                    break;
                default:
                    break;
            }
            BVD_LOG(LogGroup::rhi, BD, "Vertex array attrib id={} loc={} size={} type={} offset={}"
                , vao
                , vertexInputAttribute.location
                , device.uniformTypes[vertexInputAttribute.uniformType].elementSize
                , type
                , vertexInputAttribute.offset);
        }

        if (VertexInputRate::isInstance(vertexInputBindingIndex)) {
            BVD_THROW("Instancing is not supported on opengl yet");
        }

        // glVertexAttribDivisor(id, 1) for instancing
    }

    uniformBuffers.resize(descriptorSetsInfo.descriptors.size());
    for (uint32_t dsIndex = 0; dsIndex < descriptorSetsInfo.descriptors.size(); ++dsIndex) {
        DescriptorSet& ds = descriptorSetsInfo.descriptors[dsIndex];

        uniformBuffers[dsIndex].resize(ds.bindings.size());
        for (uint32_t dsbIndex = 0; dsbIndex < ds.bindings.size(); ++dsbIndex) {
            if (ds.bindings[dsbIndex].type == DescriptorType::uniformBuffer) {
                if (ds.bindings[dsbIndex].type == DescriptorType::uniformBuffer) {
#ifdef DEVELOPMENT_GUIDES_CHECK
                    // TODO: uniform block index != binding
                    // GL_INVALID_INDEX
                    GLuint blockIndex = glGetUniformBlockIndex(handle, ds.bindings[dsbIndex].name.c_str());
                    if (blockIndex != dsbIndex) {
                        BVD_LOG(LogGroup::rhi, BE, "{}: Uniform block index {} is not equal to uniform block binding {}", ds.bindings[dsbIndex].name.c_str(), blockIndex, dsbIndex);
                    }
#endif
                    uniformBuffers[dsIndex][dsbIndex] = new Buffer(ds.bindings[dsbIndex].size);
                }
            }
        }
    }
}

void GraphicsPipeline::destroy() {
    for (std::vector<Ref<Buffer>>& dsUniformBuffer : uniformBuffers) {
        for (Ref<Buffer>& dsbUniformBuffer : dsUniformBuffer) {
            // TODO: maybe just make it a map
            if (dsbUniformBuffer) {
                dsbUniformBuffer->destroy();
            }
        }
    }

    verifyOpenglResult(glDeleteProgram(handle));
    verifyOpenglResult(glDeleteVertexArrays(1, &vao));
}

common::Device const& GraphicsPipeline::getDevice() const {
    return static_cast<common::Device const&>(device);
}

void GraphicsPipeline::bind() const {
    verifyOpenglResult(glUseProgram(handle));

    for (auto [binding, textureArray] : textures) {
        for (auto [arrayElement, textureId] : textureArray) {
            verifyOpenglResult(glBindTextureUnit(binding + arrayElement, textureId));
        }
    }

    verifyOpenglResult(glBindVertexArray(vao));
}

void GraphicsPipeline::prepareVao(uint32_t indexBuffer, uint32_t numVertexBuffers, uint32_t(&vertexBufferOffset)[2], uint32_t(&vertexBuffer)[2]) const {
    verifyOpenglResult(glVertexArrayElementBuffer(vao, indexBuffer));

    for (uint32_t bindingIndex = 0; bindingIndex < numVertexBuffers; ++bindingIndex) {
        verifyOpenglResult(glVertexArrayVertexBuffer(vao, bindingIndex, vertexBuffer[bindingIndex], vertexBufferOffset[bindingIndex], vertexInputLayout[bindingIndex].stride));
    }
}

void GraphicsPipeline::setUniformBuffer(uint32_t set, uint32_t binding, void const* buffer, uint64_t size, uint64_t offset) {
#ifdef DEVELOPMENT_GUIDES_CHECK
    if (descriptorSetsInfo.descriptors[set].bindings[binding].type == DescriptorType::imageSampler) {
        BVD_THROW("Setting uniform buffer to location of image sampler");
    }
#endif

    // TODO: is it the correct way to call glBindBufferRange every time
    verifyOpenglResult(glBindBufferRange(GL_UNIFORM_BUFFER, binding, uniformBuffers[set][binding]->getHandle(), 0, descriptorSetsInfo.descriptors[set].bindings[binding].size));
    uniformBuffers[set][binding]->copy(buffer, size, offset);
}

void GraphicsPipeline::cmdSetUniformBuffer(uint32_t /*set*/, uint32_t /*binding*/, void const* /*buffer*/, uint64_t /*size*/, uint64_t /*offset*/) {
    BVD_THROW("This method is not implemented yet");
}

void GraphicsPipeline::setTexture(uint32_t set, uint32_t binding, uint32_t arrayElement, common::Texture const& texture) {
#ifdef DEVELOPMENT_GUIDES_CHECK
    if (descriptorSetsInfo.descriptors[set].bindings[binding].type == DescriptorType::uniformBuffer) {
        BVD_THROW("Setting image sampler to location of uniform buffer");
    }
#endif

    textures[binding][arrayElement] = static_cast<Texture2D const&>(texture).getHandle();
}

} // namespace bvd::rhi::opengl
