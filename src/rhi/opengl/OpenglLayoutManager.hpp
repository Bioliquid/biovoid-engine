#pragma once

#include <cstdint>
#include <vector>
#include <map>

namespace bvd {

namespace rhi {

namespace common {

class RenderTargetsInfo;
class RenderPassInfo;

} // namespace common

namespace opengl {

class Device;

class Framebuffer;

class LayoutManager {
public:
    ~LayoutManager();

    Framebuffer* getOrCreateFramebuffer(Device&, common::RenderTargetsInfo const&);

    void beginRenderPass(common::RenderPassInfo const&, Framebuffer const*);
    void endRenderPass(Framebuffer const*);

    void destroy();

    void notifyDeletedRenderTarget(uint32_t);
    void notifyDeletedImage(uint32_t);

private:
    struct FramebufferList {
        std::vector<Framebuffer*> framebuffer;
    };
    std::map<uint64_t, FramebufferList*> framebuffers;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
