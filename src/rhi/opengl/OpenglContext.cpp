#include "OpenglContext.hpp"

#include "OpenglViewport.hpp"
#include "OpenglGraphicsPipeline.hpp"
#include "OpenglPendingGfxState.hpp"
#include "OpenglBuffer.hpp"
#include "RenderTargetsInfo.hpp"
#include "RenderPassInfo.hpp"
#include "Logger.hpp"
#include "OpenglUtils.hpp"

namespace bvd::rhi::opengl {

Context::Context(DynamicRhi& inRhi, Device& inDevice)
    : rhi(inRhi)
    , device(inDevice)
{
    pendingGfxState = new PendingGfxState();
}

Context::~Context() {
    layoutManager.destroy();

    delete pendingGfxState;
}

void Context::beginDrawingViewport(common::Viewport*) {
    
}

void Context::endDrawingViewport(common::Viewport* viewport) {
    Viewport* openglViewport = static_cast<Viewport*>(viewport);

    openglViewport->present(*this);
}

void Context::beginRenderPass(common::RenderPassInfo const& renderPassInfo) {
    common::RenderTargetsInfo rtInfo;
    renderPassInfo.convertToRenderTargetsInfo(rtInfo);

    Framebuffer* framebuffer = layoutManager.getOrCreateFramebuffer(device, rtInfo);
    lastFramebuffer = framebuffer;

    layoutManager.beginRenderPass(renderPassInfo, framebuffer);
}

void Context::endRenderPass() {
    layoutManager.endRenderPass(lastFramebuffer);
}

void Context::setStreamSource(uint32_t index, common::Buffer const& vertexBuffer, uint32_t offset) {
    Buffer const& openglVertexBuffer = static_cast<Buffer const&>(vertexBuffer);

    pendingGfxState->setStreamSource(index, openglVertexBuffer.getHandle(), offset);
}

void Context::setGraphicsPipeline(common::GraphicsPipeline const* pipeline) {
    GraphicsPipeline const* openglPipeline = static_cast<GraphicsPipeline const*>(pipeline);

    pendingGfxState->setGfxPipeline(openglPipeline);
}

void Context::drawPrimitive(uint32_t /*baseVertexIndex*/, uint32_t /*numPrimitives*/, uint32_t /*numInstances*/) {
    
}

void Context::drawIndexedPrimitive(common::Buffer const& indexBuffer, int32_t /*baseVertexIndex*/, int32_t /*firstInstance*/, int32_t /*startIndex*/, int32_t numPrimitives, int32_t numInstances) {
    pendingGfxState->prepareForDraw(static_cast<Buffer const&>(indexBuffer));

    // TODO: only triangles are supported, no other topology
    verifyOpenglResult(glDrawElementsInstanced(GL_TRIANGLES, numPrimitives * 3, GL_UNSIGNED_INT, nullptr, numInstances));
}

} // namespace bvd::opengl
