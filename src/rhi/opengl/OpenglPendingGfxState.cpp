#include "OpenglPendingGfxState.hpp"

#include "OpenglGraphicsPipeline.hpp"
#include "OpenglBuffer.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

namespace bvd::rhi::opengl {

void PendingGfxState::prepareForDraw(Buffer const& indexBuffer) {
#ifdef DEVELOPMENT_GUIDES_CHECK
    if (!pipeline) {
        BVD_THROW("Pipeline is nullptr");
    }
#endif

    pipeline->prepareVao(indexBuffer.getHandle(), numPendingStreams, vertexOffsets, pendingStreams);
    pipeline->bind();

    numPendingStreams = 0;
}

} // namespace bvd::rhi::opengl
