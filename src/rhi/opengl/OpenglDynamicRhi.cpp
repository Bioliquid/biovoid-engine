#include "OpenglDynamicRhi.hpp"

#include "OpenglContext.hpp"
#include "OpenglViewport.hpp"
#include "OpenglGraphicsPipeline.hpp"
#include "OpenglBuffer.hpp"
#include "OpenglTexture2D.hpp"
#include "OpenglUtils.hpp"

#include "PixelFormatInfo.hpp"
#include "Logger.hpp"
#include "Exception.hpp"
#include "GlobalConfig.hpp"

// Be sure to include GLAD before GLFW. 
// The include file for GLAD includes the required OpenGL headers behind the scenes (like GL/gl.h)
// so be sure to include GLAD before other header files that require OpenGL (like GLFW). 
#include <glfw/glfw3.h>

namespace bvd::rhi::common {

DynamicRhi* createOpenglDynamicRhi() {
    return new opengl::DynamicRhi;
}

}

namespace bvd::rhi::opengl {

static void GLAPIENTRY messageCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const* message, void const* /*userParam*/) {
    if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
        std::string_view sw{message, (size_t)length};
        BVD_LOG(LogGroup::rhi, BE, "validation layer: source={} type={} id={} {}", getOpenglSource(source), getOpenglMsgType(type), id, sw);
    }
}

void DynamicRhi::init() {
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        BVD_THROW("Failed to initialize GLAD");
    }

    verifyOpenglResult(glEnable(GL_DEBUG_OUTPUT));
    verifyOpenglResult(glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS));
    verifyOpenglResult(glDebugMessageCallback(messageCallback, nullptr));
    verifyOpenglResult(glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE));

    // glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    // verifyOpenglResult(glEnable(GL_DEPTH_TEST));
    verifyOpenglResult(glEnable(GL_BLEND));
    verifyOpenglResult(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

    if (GlobalConfig::Graphics::msaaSamples > 1) {
        verifyOpenglResult(glEnable(GL_MULTISAMPLE));
    }

    selectedDevice = new Device(*this);

    immediateContext = &selectedDevice->getImmediateContext();
}

void DynamicRhi::destroy() {
    delete immediateContext;
}

Ref<common::Texture2D> DynamicRhi::getViewportBackBuffer(common::Viewport* viewport) {
    Viewport* vulkanViewport = static_cast<Viewport*>(viewport);
    return vulkanViewport->getBackBuffer();
}

Ref<common::Viewport> DynamicRhi::createViewport(void*, uint32_t width, uint32_t height, PixelFormat pixelFormat) {
    return new Viewport(*this, *selectedDevice, width, height, pixelFormat);
}

Ref<common::GraphicsPipeline> DynamicRhi::createGraphicsPipeline() {
    return new GraphicsPipeline(*selectedDevice);
}

Ref<common::Buffer> DynamicRhi::createVertexBuffer(uint64_t size) {
    return new Buffer(size);
}

Ref<common::Buffer> DynamicRhi::createIndexBuffer(uint64_t size) {
    return new Buffer(size);
}

Ref<common::Buffer> DynamicRhi::createUniformBuffer(uint64_t size) {
    return new Buffer(size);
}

Ref<common::Texture2D> DynamicRhi::createTexture2D(common::TextureCreateInfo const& info) {
    return new Texture2D(*this, *selectedDevice, info);
}

void DynamicRhi::notifyDeletedImage(GLuint image, bool isRenderTarget) {
    if (isRenderTarget) {
        static_cast<Context*>(immediateContext)->notifyDeletedRenderTarget(image);
    }

    static_cast<Context*>(immediateContext)->notifyDeletedImage(image);
}

} // namespace bvd::rhi::opengl
