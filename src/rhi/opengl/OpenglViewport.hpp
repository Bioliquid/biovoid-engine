#pragma once

#include "Ref.hpp"
#include "Viewport.hpp"
#include "OpenglTexture2D.hpp"

#include <cstdint>
#include <vector>

namespace bvd {

namespace rhi {

namespace opengl {

class DynamicRhi;
class Device;
class Context;

class Viewport : public common::Viewport {
public:
    Viewport(DynamicRhi&, Device&, uint32_t, uint32_t, PixelFormat);

    Viewport(Viewport const&) = delete;
    Viewport(Viewport&&) = delete;
    Viewport& operator=(Viewport const&) = delete;
    Viewport& operator=(Viewport&&) = delete;

    inline Ref<common::Texture2D> getBackBuffer() { return backbuffer; }

    virtual void resize(uint32_t, uint32_t) override;

    void present(Context&);

private:
    void createBackBuffer();
    void recreateBackBuffer();

private:
    DynamicRhi& rhi;
    Device&     device;

    Ref<common::Texture2D>  backbuffer;

    uint32_t    width;
    uint32_t    height;
    PixelFormat pixelFormat;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
