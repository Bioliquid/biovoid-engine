#include "OpenglTexture2D.hpp"

#include "OpenglDynamicRhi.hpp"
#include "OpenglUtils.hpp"

#include "PixelFormatInfo.hpp"

namespace bvd::rhi::opengl {

Texture2D::Texture2D(DynamicRhi& inRhi, Device& inDevice, common::TextureCreateInfo const& info)
    : common::Texture2D(info)
    , rhi(inRhi)
    , device(inDevice)
    , flags(info.flags)
{
    GLenum target = numSamples > 1 ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;
    verifyOpenglResult(glCreateTextures(target, 1, &handle));

    if (numSamples == 1) {
        verifyOpenglResult(glTextureParameteri(handle, GL_TEXTURE_WRAP_S, GL_REPEAT));
        verifyOpenglResult(glTextureParameteri(handle, GL_TEXTURE_WRAP_T, GL_REPEAT));
        verifyOpenglResult(glTextureParameteri(handle, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
        verifyOpenglResult(glTextureParameteri(handle, GL_TEXTURE_MAG_FILTER, GL_LINEAR));

        verifyOpenglResult(glTextureStorage2D(handle, 1, device.pixelFormats[info.format].platformFormat, info.width, info.height));
    } else {
        verifyOpenglResult(glTextureStorage2DMultisample(handle, numSamples, device.pixelFormats[info.format].platformFormat, info.width, info.height, GL_TRUE));
    }

    BVD_LOG(LogGroup::rhi, BD, "Created texture2d {}", handle);

    if (info.pixels) {
        if (device.pixelFormats[info.format].platformFormat == GL_RGBA32F) {
            verifyOpenglResult(glTextureSubImage2D(handle, 0, 0, 0, info.width, info.height, device.pixelFormats[info.format].formatOrder, GL_FLOAT, info.pixels));
        } else {
            verifyOpenglResult(glTextureSubImage2D(handle, 0, 0, 0, info.width, info.height, device.pixelFormats[info.format].formatOrder, GL_UNSIGNED_BYTE, info.pixels));
        }
        // glGenerateTextureMipmap(handle);
    }
}

Texture2D::~Texture2D() {
    BVD_LOG(LogGroup::rhi, BD, "Destroying texture2d {} renderTargerable={}", handle, isRenderTargetable());

    rhi.notifyDeletedImage(handle, isRenderTargetable());

    verifyOpenglResult(glDeleteTextures(1, &handle));
}

} // namespace bvd::rhi::opengl
