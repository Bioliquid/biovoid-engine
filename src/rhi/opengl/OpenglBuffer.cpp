#include "OpenglBuffer.hpp"

#include "Exception.hpp"
#include "OpenglUtils.hpp"

#include <glad/glad.h>

namespace bvd::rhi::opengl {

Buffer::Buffer(int64_t size) {
    verifyOpenglResult(glCreateBuffers(1, &handle));
    verifyOpenglResult(glNamedBufferData(handle, size, nullptr, GL_STATIC_DRAW));

    BVD_LOG(LogGroup::rhi, BD, "Created buffer {} size={}", handle, size);
}

void Buffer::copy(void const* data, int64_t size, int64_t offset) {
    BVD_LOG(LogGroup::rhi, BD, "Copying data to buffer {} offset={} size={}", handle, offset, size);

    verifyOpenglResult(glNamedBufferSubData(handle, offset, size, data));
}

void Buffer::copy(void const* data, int64_t size) {
    BVD_LOG(LogGroup::rhi, BD, "Copying data to buffer {} offset=0 size={}", handle, size);

    verifyOpenglResult(glNamedBufferSubData(handle, 0, size, data));
}

void Buffer::cmdCopy(void const*, int64_t) {
    BVD_THROW("CmdCopy is not implemented yet");
}

void Buffer::cmdCopy(void const*, int64_t, int64_t) {
    BVD_THROW("CmdCopy is not implemented yet");
}

void Buffer::destroy() {
    BVD_LOG(LogGroup::rhi, BD, "Destroying buffer {}", handle);

    verifyOpenglResult(glDeleteBuffers(1, &handle));
}

} // namespace bvd::opengl
