#pragma once

#include "DynamicRhi.hpp"
#include "UniformTypeInfo.hpp"
#include "OpenglDevice.hpp"

#include <glad/glad.h>

namespace bvd {

namespace rhi {

namespace opengl {

class DynamicRhi : public common::DynamicRhi {
public:
    virtual void init() override;
    virtual void destroy() override;

    virtual Ref<common::Texture2D> getViewportBackBuffer(common::Viewport*) override;
    virtual void advanceFrameForViewportBackBuffer(common::Viewport*) override {}

    virtual Ref<common::Viewport> createViewport(void*, uint32_t, uint32_t, PixelFormat) override;
    virtual Ref<common::GraphicsPipeline> createGraphicsPipeline() override;
    virtual Ref<common::Buffer> createVertexBuffer(uint64_t) override;
    virtual Ref<common::Buffer> createIndexBuffer(uint64_t) override;
    virtual Ref<common::Buffer> createUniformBuffer(uint64_t) override;
    virtual Ref<common::Texture2D> createTexture2D(common::TextureCreateInfo const&) override;

    void notifyDeletedImage(GLuint, bool);

private:
    Device* selectedDevice;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
