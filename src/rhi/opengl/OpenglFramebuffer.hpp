#pragma once

#include "RhiDefinitions.hpp"

#include <glad/glad.h>
#include <cstdint>
#include <vector>

namespace bvd {

namespace rhi {

namespace common {

class RenderTargetsInfo;

} // namespace common

namespace opengl {

class Device;

class Framebuffer {
public:
    Framebuffer(Device&, common::RenderTargetsInfo const&);
    ~Framebuffer();

    bool matches(common::RenderTargetsInfo const&) const;

    bool containsRenderTarget(GLuint image) const {
        for (uint32_t index = 0; index < numColorRenderTargets; ++index) {
            if (colorRenderTargetImages[index] == image) {
                return true;
            }
        }

        // return (depthStencilRenderTargetImage == Image);
        return false;
    }

    void bind() const;

    GLuint getHandle() const { return handle; }

public:
    Device& device;
    GLuint  handle;
    GLuint  multisampleHandle;
    bool    isMultisampled{false}; // to delete

    uint32_t    numColorRenderTargets;
    GLuint      colorRenderTargetImages[maxNumSimultaneousRenderTargets];
    GLuint      colorResolveTargetImages[maxNumSimultaneousRenderTargets];

    int32_t    width;
    int32_t    height;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
