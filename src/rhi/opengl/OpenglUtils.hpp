#pragma once

#include "Logger.hpp"

#include <glad/glad.h>
#include <string_view>

inline std::string_view getOpenglError(int errorCode) {
    switch (errorCode) {
        case GL_INVALID_ENUM:                  return "INVALID_ENUM";
        case GL_INVALID_VALUE:                 return "INVALID_VALUE";
        case GL_INVALID_OPERATION:             return "INVALID_OPERATION";
        case GL_STACK_OVERFLOW:                return "STACK_OVERFLOW";
        case GL_STACK_UNDERFLOW:               return "STACK_UNDERFLOW";
        case GL_OUT_OF_MEMORY:                 return "OUT_OF_MEMORY";
        case GL_INVALID_FRAMEBUFFER_OPERATION: return "INVALID_FRAMEBUFFER_OPERATION";
    }
    return "unknown";
}

inline std::string_view getOpenglSource(GLenum source) {
    switch (source) {
        case GL_DEBUG_SOURCE_API:             return "api";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   return "window system";
        case GL_DEBUG_SOURCE_SHADER_COMPILER: return "shader compiler";
        case GL_DEBUG_SOURCE_THIRD_PARTY:     return "third party";
        case GL_DEBUG_SOURCE_APPLICATION:     return "application";
        case GL_DEBUG_SOURCE_OTHER:           return "unknown";
        default:                              return "unknown";
    }
}

inline std::string_view getOpenglMsgType(GLenum type) {
    switch (type) {
        case GL_DEBUG_TYPE_ERROR:               return "error";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "deprecated behavior";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  return "undefined behavior";
        case GL_DEBUG_TYPE_PORTABILITY:         return "portability";
        case GL_DEBUG_TYPE_PERFORMANCE:         return "performance";
        case GL_DEBUG_TYPE_OTHER:               return "other";
        case GL_DEBUG_TYPE_MARKER:              return "marker";
        default:                                return "unknown";
    }
}

#define verifyOpenglResult(OpenglFunction)                     \
    {                                                          \
        OpenglFunction;                                        \
        int error = glGetError();                              \
        if (error != GL_NO_ERROR) {                            \
            BVD_LOG(bvd::LogGroup::rhi, BE, "Opengl error {}", \
                getOpenglError(error));                        \
        }                                                      \
    }
