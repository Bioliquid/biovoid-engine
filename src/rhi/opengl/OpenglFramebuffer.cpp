#include "OpenglFramebuffer.hpp"

#include "OpenglTexture2D.hpp"
#include "OpenglUtils.hpp"
#include "OpenglDevice.hpp"
#include "OpenglContext.hpp"

#include "RenderTargetsInfo.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

namespace bvd::rhi::opengl {

Framebuffer::Framebuffer(Device& inDevice, common::RenderTargetsInfo const& rtInfo)
    : device(inDevice)
    , numColorRenderTargets(rtInfo.numColorRenderTargets)
{
    verifyOpenglResult(glCreateFramebuffers(1, &handle));

    BVD_LOG(LogGroup::rhi, BD, "Created framebuffer {}", handle);

    for (uint32_t index = 0; index < rtInfo.numColorRenderTargets; ++index) {
        common::Texture* rhiTexture = rtInfo.colorRenderTargets[index].texture;
        if (!rhiTexture) {
            continue;
        }

        Texture2D* texture = static_cast<Texture2D*>(rhiTexture);

        width = texture->getWidth();
        height = texture->getHeight();

        colorRenderTargetImages[index] = texture->getHandle();

        // TODO: check colorResolveRenderTargets[index]
        if (rtInfo.hasResolveAttachments) {
            isMultisampled = true;

            common::Texture* resolveRhiTexture = rtInfo.colorResolveRenderTargets[index].texture;
            // TODO: make textureBase
            // TextureBase* resolveTexture = static_cast<TextureBase*>(static_cast<Texture2D*>(resolveRhiTexture));
            Texture2D* resolveTexture = static_cast<Texture2D*>(resolveRhiTexture);
            colorResolveTargetImages[index] = resolveTexture->getHandle();

            verifyOpenglResult(glCreateFramebuffers(1, &multisampleHandle));
            verifyOpenglResult(glNamedFramebufferTexture(multisampleHandle, GL_COLOR_ATTACHMENT0 + index, texture->getHandle(), 0));
            verifyOpenglResult(glNamedFramebufferTexture(handle, GL_COLOR_ATTACHMENT0 + index, resolveTexture->getHandle(), 0));

            if (glCheckNamedFramebufferStatus(multisampleHandle, GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
                BVD_THROW("Framebuffer is not complete");
            }
        } else {
            verifyOpenglResult(glNamedFramebufferTexture(handle, GL_COLOR_ATTACHMENT0 + index, texture->getHandle(), 0));
        }
    }

    if (glCheckNamedFramebufferStatus(handle, GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        BVD_THROW("Framebuffer is not complete");
    }
}

Framebuffer::~Framebuffer() {
    BVD_LOG(LogGroup::rhi, BD, "Destroying framebuffer {}", handle);

    verifyOpenglResult(glDeleteFramebuffers(1, &handle));
    if (isMultisampled) {
        verifyOpenglResult(glDeleteFramebuffers(1, &multisampleHandle));
    }
}

bool Framebuffer::matches(common::RenderTargetsInfo const& rtInfo) const {
    if (numColorRenderTargets != rtInfo.numColorRenderTargets) {
        return false;
    }

    for (uint32_t index = 0; index < rtInfo.numColorRenderTargets; ++index) {
        common::RenderTargetView const& b = rtInfo.colorRenderTargets[index];
        if (b.texture) {
            GLuint fbImage = colorRenderTargetImages[index];
            GLuint rtImage = static_cast<Texture2D*>(b.texture)->getHandle();
            if (fbImage != rtImage) {
                return false;
            }
        }
    }

    return true;
}

void Framebuffer::bind() const {
    if (isMultisampled) {
        BVD_LOG(LogGroup::rhi, BD, "Binding framebuffer {}", multisampleHandle);
        glBindFramebuffer(GL_FRAMEBUFFER, multisampleHandle);
    } else {
        BVD_LOG(LogGroup::rhi, BD, "Binding framebuffer {}", handle);
        glBindFramebuffer(GL_FRAMEBUFFER, handle);
    }
    glViewport(0, 0, width, height);
}

} // namespace bvd::rhi::opengl
