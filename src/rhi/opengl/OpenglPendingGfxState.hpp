#pragma once

#include "RhiDefinitions.hpp"

#include <glad/glad.h>
#include <algorithm>

namespace bvd {

namespace rhi {

namespace opengl {

class Context;
class Buffer;
class GraphicsPipeline;

class PendingGfxState {
public:
    void prepareForDraw(Buffer const&);

    void setStreamSource(uint32_t index, uint32_t buffer, uint32_t offset) {
        vertexOffsets[index] = offset;
        pendingStreams[index] = buffer;
        numPendingStreams = std::max(numPendingStreams, index + 1);
    }

    inline void setGfxPipeline(GraphicsPipeline const* inPipeline) { pipeline = inPipeline; }

private:
    bool            swapChainTarget;

    uint32_t    vertexOffsets[2] = {};
    uint32_t    pendingStreams[2];
    uint32_t    numPendingStreams = 0;

    GraphicsPipeline const* pipeline = nullptr;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
