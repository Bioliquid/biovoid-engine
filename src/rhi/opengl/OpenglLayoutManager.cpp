#include "OpenglLayoutManager.hpp"

#include "OpenglFramebuffer.hpp"
#include "OpenglDevice.hpp"
#include "OpenglUtils.hpp"

#include "RenderTargetsInfo.hpp"
#include "RenderPassInfo.hpp"
#include "Texture.hpp"
#include "Logger.hpp"

#include <glad/glad.h>

namespace bvd::rhi::opengl {

LayoutManager::~LayoutManager() {}

Framebuffer* LayoutManager::getOrCreateFramebuffer(Device& device, common::RenderTargetsInfo const& rtInfo) {
    uint64_t rtLayoutHash = 0;

    auto foundFramebufferList = framebuffers.find(rtLayoutHash);
    FramebufferList* framebufferList = nullptr;
    if (foundFramebufferList != std::end(framebuffers)) {
        framebufferList = foundFramebufferList->second;

        for (uint32_t index = 0; index < framebufferList->framebuffer.size(); ++index) {
            if (framebufferList->framebuffer[index]->matches(rtInfo)) {
                return framebufferList->framebuffer[index];
            }
        }
    } else {
        framebufferList = new FramebufferList;
        framebuffers[rtLayoutHash] = framebufferList;
    }

    Framebuffer* framebuffer = new Framebuffer(device, rtInfo);
    framebufferList->framebuffer.emplace_back(framebuffer);
    BVD_LOG(LogGroup::rhi, BD, "Framebuffers (size={})", framebuffers.size());
    for (auto& [hashId, fb] : framebuffers) {
        BVD_LOG(LogGroup::rhi, BD, "-- Framebuffers[{}] (size={})", hashId, fb->framebuffer.size());
    }
    return framebuffer;
}

void LayoutManager::beginRenderPass(common::RenderPassInfo const& renderPassInfo, Framebuffer const* framebuffer) {
    framebuffer->bind();

    uint32_t numColorTargets = renderPassInfo.numColorRenderTargets;
    for (uint32_t index = 0; index < numColorTargets; ++index) {
        common::RenderPassInfo::ColorEntry const& colorEntry = renderPassInfo.colorRenderTargets[index];
        common::Texture* colorTexture = colorEntry.renderTarget;

        Color clearColor = colorTexture->hasClearColor() ? colorTexture->getClearColor() : colors::black;

        glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClear(GL_COLOR_BUFFER_BIT);
    }
}

void LayoutManager::endRenderPass(Framebuffer const* framebuffer) {
    for (uint32_t i = 0; i < framebuffer->numColorRenderTargets; ++i) {
        if (framebuffer->isMultisampled) {
            verifyOpenglResult(glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer->handle));
            verifyOpenglResult(glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer->multisampleHandle));

            verifyOpenglResult(glBlitFramebuffer(
                0, 0, framebuffer->width, framebuffer->height,
                0, 0, framebuffer->width, framebuffer->height,
                GL_COLOR_BUFFER_BIT,
                GL_NEAREST
            ));
        }
    }
}

void LayoutManager::destroy() {
    for (auto& [hashId, framebuffer] : framebuffers) {
        for (Framebuffer* fb : framebuffer->framebuffer) {
            delete fb;
        }

        delete framebuffer;
    }

    framebuffers.clear();
}

void LayoutManager::notifyDeletedRenderTarget(GLuint image) {
    for (auto& [hashId, framebuffer] : framebuffers) {
        std::erase_if(framebuffer->framebuffer, [image](Framebuffer* fb) {
            if (fb->containsRenderTarget(image)) {
                delete fb;
                return true;
            }
            return false;
        });
    }

    std::erase_if(framebuffers, [](auto const& item) {
        auto const& [hasId, framebuffer] = item;
        if (framebuffer->framebuffer.empty()) {
            delete framebuffer;
            return true;
        }
        return false;
    });
}

void LayoutManager::notifyDeletedImage(GLuint) {

}

} // namespace bvd::rhi::opengl
