#include "OpenglDevice.hpp"
#include "OpenglContext.hpp"
#include "Logger.hpp"

namespace bvd::rhi::opengl {

Device::Device(DynamicRhi& inRhi)
    : rhi(inRhi)
{
    mapUniformInfo(UniformType::vec1, "float", GL_FLOAT, 1, 1 * sizeof(float));
    mapUniformInfo(UniformType::vec2, "vec2", GL_FLOAT, 2, 2 * sizeof(float));
    mapUniformInfo(UniformType::vec3, "vec3", GL_FLOAT, 3, 3 * sizeof(float));
    mapUniformInfo(UniformType::vec4, "vec4", GL_FLOAT, 4, 4 * sizeof(float));

    mapUniformInfo(UniformType::uvec1, "uint", GL_UNSIGNED_INT, 1, 1 * sizeof(uint32_t));
    mapUniformInfo(UniformType::uvec2, "uvec2", GL_UNSIGNED_INT, 2, 2 * sizeof(uint32_t));
    mapUniformInfo(UniformType::uvec3, "uvec3", GL_UNSIGNED_INT, 3, 3 * sizeof(uint32_t));
    mapUniformInfo(UniformType::uvec4, "uvec4", GL_UNSIGNED_INT, 4, 4 * sizeof(uint32_t));

    mapUniformInfo(UniformType::ivec1, "int", GL_INT, 1, 1 * sizeof(int32_t));
    mapUniformInfo(UniformType::ivec2, "ivec2", GL_INT, 2, 2 * sizeof(int32_t));
    mapUniformInfo(UniformType::ivec3, "ivec3", GL_INT, 3, 3 * sizeof(int32_t));
    mapUniformInfo(UniformType::ivec4, "ivec4", GL_INT, 4, 4 * sizeof(int32_t));

    mapUniformInfo(UniformType::bvec1, "bool", GL_UNSIGNED_BYTE, 1, 1 * sizeof(bool));
    mapUniformInfo(UniformType::bvec2, "bvec2", GL_UNSIGNED_BYTE, 2, 2 * sizeof(bool));
    mapUniformInfo(UniformType::bvec3, "bvec3", GL_UNSIGNED_BYTE, 3, 3 * sizeof(bool));
    mapUniformInfo(UniformType::bvec4, "bvec4", GL_UNSIGNED_BYTE, 4, 4 * sizeof(bool));

    mapUniformInfo(UniformType::mat3, "mat3", GL_FLOAT, 3 * 3, 3 * 3 * sizeof(float));
    mapUniformInfo(UniformType::mat4, "mat4", GL_FLOAT, 4 * 4, 4 * 4 * sizeof(float));

    // default formats
    mapFormatSupport(PixelFormat::b8g8r8a8, GL_RGBA8, GL_BGRA);
    mapFormatSupport(PixelFormat::r8g8b8a8, GL_RGBA8, GL_RGBA);
    mapFormatSupport(PixelFormat::r32g32b32a32, GL_RGBA32F, GL_RGBA);

    immediateContext = new Context(rhi, *this);
}

void Device::mapUniformInfo(UniformType uniformType, char const* name, int32_t platformFormat, int32_t elementSize, int32_t byteSize) {
    BVD_LOG(LogGroup::rhi, BD, "{}: uniform type {} - none", name, static_cast<int32_t>(uniformType));

    uniformTypes[uniformType].name = name;
    uniformTypes[uniformType].platformFormat = platformFormat;
    uniformTypes[uniformType].elementSize = elementSize;
    uniformTypes[uniformType].byteSize = byteSize;
}

void Device::mapFormatSupport(PixelFormat engineFormat, GLenum openglFormat, GLenum openglFormatOrder) {
    pixelFormats[engineFormat].platformFormat = openglFormat;
    pixelFormats[engineFormat].formatOrder = openglFormatOrder;
    pixelFormats[engineFormat].supported = true;
}

} // namespace bvd::rhi::opengl
