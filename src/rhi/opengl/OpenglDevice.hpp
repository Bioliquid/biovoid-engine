#pragma once

#include "Device.hpp"

#include <glad/glad.h>

namespace bvd {

namespace rhi {

namespace opengl {

class DynamicRhi;
class Context;

class Device : public common::Device {
public:
    Device(DynamicRhi&);

    Device(Device const&) = delete;
    Device(Device&&) = delete;
    Device& operator=(Device const&) = delete;
    Device& operator=(Device&&) = delete;

    Context& getImmediateContext() { return *immediateContext; }

private:
    void mapUniformInfo(UniformType, char const*, int32_t, int32_t, int32_t);
    void mapFormatSupport(PixelFormat, GLenum, GLenum);

    DynamicRhi&         rhi;
    Context*            immediateContext;
};

} // namespace opengl

} // namespace rhi

} // namespace bvd
