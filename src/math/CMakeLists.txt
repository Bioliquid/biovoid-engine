include(CommonOptions)

create_library(math
    SRCS
        Collision.cpp
    DIRS
        .
    LIBS
        inc::core
)
