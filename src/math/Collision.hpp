#pragma once

#include <glm/glm.hpp>

namespace bvd {

namespace math {

bool testDotInRect(glm::vec2 const& dot, glm::vec2 const& rectPos, glm::vec2 const& rectSize);

} // namespace math

} // namespace bvd
