#include "Collision.hpp"

namespace bvd::math {

bool testDotInRect(glm::vec2 const& dot, glm::vec2 const& rectPos, glm::vec2 const& rectSize) {
    const bool fitX = dot.x > rectPos.x && dot.x < rectPos.x + rectSize.x;
    const bool fitY = dot.y > rectPos.y && dot.y < rectPos.y + rectSize.y;

    return fitX && fitY;
}

} // namespace bvd
