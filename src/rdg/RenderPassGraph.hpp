#pragma once

#include "Scope.hpp"
#include "Rhi.hpp"
#include "RenderCommand.hpp"
#include "Ref.hpp"
#include "GlobalConfig.hpp"
#include "RenderCommand.hpp"

#include <hml/algo/at.hpp>
#include <vector>
#include <functional>
#include <map>

namespace bvd {

namespace rdg {

class RenderGraph;

struct CanvasInfo {
    auto operator<=>(CanvasInfo const&) const = default;

    int32_t width;
    int32_t height;
};

struct RenderGraphResourceAllocator {
    rhi::Texture2D& allocTexture(std::string const&, rhi::common::TextureCreateInfo const&);

    std::map<std::string, Ref<rhi::Texture2D>> textures;
};

class RenderGraphPass {
    friend class RenderGraph;

    using RenderCallback = std::function<void()>;

public:
    RenderGraphPass(RenderGraphResourceAllocator& inAllocator) : allocator(inAllocator) {}

    void setOutput(std::string const&, rhi::Texture2D&);
    void setOutput(std::string const&, rhi::common::TextureCreateInfo const&);
    void updateOutput(rhi::Texture2D&);

    void setCallback(RenderCallback const& callback) {
        render = callback;
    }

private:
    void completeRenderPassInfo() {
        renderPassInfo = rhi::RenderPassInfo(colorAttachment, rhi::RenderTargetLoadAction::clear, rhi::RenderTargetStoreAction::store, resolveAttachment);
    }

private:
    // CanvasInfo          canvas;
    RenderGraphResourceAllocator& allocator;
    RenderCallback      render;
    rhi::RenderPassInfo renderPassInfo;
    rhi::Texture2D*     colorAttachment{nullptr};
    rhi::Texture2D*     resolveAttachment{nullptr};
};

class RenderGraph {
public:
    RenderGraph(RenderGraphResourceAllocator& inAllocator) : allocator(inAllocator) {}

    RenderGraphPass& addPass(std::string const&) {
        return passes.emplace_back(allocator);
    }

    RenderGraphPass& getFinalPass() {
        return passes.back();
    }

    void execute() {
        using namespace renderer;

        for (auto& pass : passes) {
            pass.completeRenderPassInfo();

            RenderCommand::beginRenderPass(pass.renderPassInfo);
            pass.render();
            RenderCommand::endRenderPass();
        }
    }

private:
    RenderGraphResourceAllocator&     allocator;
    static_vector<RenderGraphPass, 2> passes;
};

} // namespace rdg

} // namespace bvd
