#include "RenderPassGraph.hpp"

#include "FileManager.hpp"
#include "GlobalConfig.hpp"

namespace bvd::rdg {

rhi::Texture2D& RenderGraphResourceAllocator::allocTexture(std::string const& name, rhi::common::TextureCreateInfo const& info) {
    textures[name] = rhi::createTexture2D(info);
    fs::FileManager::setTexture(name, textures.at(name)); // TODO: temporary
    return *textures.at(name);
}

void RenderGraphPass::setOutput(std::string const& name, rhi::Texture2D& renderTarget) {
    if (GlobalConfig::Graphics::msaaSamples == 1) {
        colorAttachment = &renderTarget;
    } else {
        rhi::common::TextureCreateInfo msaaInfo{
            renderTarget.getFormat(),
            renderTarget.getWidth(),
            renderTarget.getHeight(),
            rhi::TextureCreateFlags::renderTargetable,
            colors::black,
            GlobalConfig::Graphics::msaaSamples
        };
        colorAttachment = &allocator.allocTexture(name + "-msaa", msaaInfo);
        resolveAttachment = &renderTarget;
    }
}

void RenderGraphPass::setOutput(std::string const& name, rhi::common::TextureCreateInfo const& info) {
    setOutput(name, allocator.allocTexture(name, info));
}

void RenderGraphPass::updateOutput(rhi::Texture2D& renderTarget) {
    if (colorAttachment->getWidth() == renderTarget.getWidth() and colorAttachment->getHeight() == renderTarget.getHeight()) {
        return;
    }

    setOutput("temp", renderTarget);
}

} // namespace bvd::rdg
