#pragma once

#include "IEngineClient.hpp"
#include "SceneView.hpp"
#include "Messages.hpp"

namespace bvd {

class EditorClient : public IEngineClient {
public:
    EditorClient(srvm::Router&);

    void initialize(renderer::Component&, ui::UiComponent&);

    void receive(ViewportResizeInd const&);

private:
    srvm::Receiver       receiver;
    renderer::ViewInfo view;
};

} // namespace bvd
