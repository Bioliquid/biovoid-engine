#pragma once

#include "UiScript.hpp"
#include "DivComponent.hpp"
#include "FileManager.hpp"
#include "IdComponent.hpp"
#include "TextAreaComponent.hpp"

#include "Entity.hpp"

#include "EditorStyle.hpp"

namespace bvd {

namespace ui {

namespace components {

struct EntityScript : public Script {
    EntityScript(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const& msg) {
        if (not node.isHovered()) {
            if (isSelected) {
                unselect();
            }
            return;
        }

        select();

        if (msg.btnKey == buttons::left) {
            UiEntitySelectedInd ind;
            ind.scene = "mainScene";
            ind.name = node["textArea"].getText();
            node.getManager().internalSend(ind);
        } else if (msg.btnKey == buttons::right) {
            BVD_LOG(LogGroup::gen, BE, "HELLO");
        }
    }

    void select() {
        isSelected = true;
        node.getRegularStyle().setColor(colors::lightGrey);
        // node.setBorderColor(colors::lightGrey);
    }

    void unselect() {
        node.getRegularStyle().setColor(colors::transparent);
        isSelected = false;
    }

    bool isSelected{false};
};

struct CreateEntityScript : public Script {
    CreateEntityScript(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const&) {
        if (not node.isHovered()) {
            return;
        }

        EntityCreateReq req;
        node.getManager().internalSend(req);
    }
};

// TODO: list does not need to be a separate element/Component
// it can just apply settings to children
template<typename... Params>
struct List : public Component<List, Params...>, ComponentInfo {
    template<typename Component, typename... Components>
    void render(auto, Component child, Components... children) {
        setParams(Width{autoFit}, Height{autoFit}, colors::transparent);

        fragment(
            child,
            children...
        );
    }
};

template<typename... Params>
struct DropBox : public Component<DropBox, Params...>, ComponentInfo {
    template<typename... Components>
    void render(auto, Components... children) {
        setParams(Height{autoFit}, Padding{glm::vec4{5.0f, 5.0f, 5.0f, 5.0f}}, colors::midGrey);

        fragment(
            // List<>::top()(
            children...
            // )
        );
    }
};

struct SceneHierarchyScript : public Script {
    struct Init {
        ecs::Scene& scene;
    };

    SceneHierarchyScript(Node& n, Init const& init) : Script(n), scene(init.scene) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const& msg) {
        if (optionsActive and not node["options"].isHovered()) {
            node.getManager().removeElement(node["options"]);

            optionsActive = false;
        }

        if (not node.isLastHovered()) {
            return;
        }

        if (msg.btnKey == buttons::right) {
            glm::vec4 pos{msg.pos.y, 0.0f, 0.0f, msg.pos.x};
            pos[0] -= node.getAbsolutePos().y;
            pos[3] -= node.getAbsolutePos().x;
            DropBox<>::blank(Id{"options"}, Width{100.0_px}, Margin{pos})(
                TextArea<group::SelectableItem, Handler<CreateEntityScript>>::top(Title{"Create entity"})(),
                TextArea<group::SelectableItem>::top(Title{"Rename"})(),
                TextArea<group::SelectableItem>::top(Title{"Copy"})(),
                TextArea<group::SelectableItem>::top(Title{"Paste"})()
            ).inPlace(node);

            optionsActive = true;
        }
    }

    void receive(EntityCreateReq const&) {
        std::string entityName = "New entity";
        ecs::Entity entity = scene.createEntity(entityName);

        Node* bindNode{nullptr};
        for (Node& child : node.view() | std::views::filter([](Node& n) { return n.getId() == "entity"; })) {
            bindNode = &child;
        }

        TextArea<Handler<EntityScript>, group::Entity>::blank(Id{"entity"}, Title{entityName}, MarginTop{5.0f}, PaddingLeft{ 10.0f })().inPlace(node).bindTop(bindNode);
    }

    bool optionsActive{false};

    ecs::Scene& scene;
};

template<typename... Params>
struct SceneHierarchy : public Component<SceneHierarchy, Params...>, ComponentInfo {
    void render(auto helper) {
        setParams(Width{100.0_pct}, Height{100.0_pct}, colors::transparent, Margin{glm::vec4{10.0f, 0.0f, 0.0f, 0.0f}});

        ecs::Scene& scene = *fs::FileManager::getScene(helper.template arg<SceneId>());
        setParams(Handler<SceneHierarchyScript>{{scene}});

        std::vector<NodeCreator> proxies;
        auto view = scene.registry.view<ecs::IdComponent>().each();
        for (auto [entity, idComponent] : view) {
            proxies.push_back(TextArea<Handler<EntityScript>, group::Entity>::top(Id{"entity"}, Title{idComponent.tag}, PaddingLeft{10.0f})());
        };

        fragment(
            proxies
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
