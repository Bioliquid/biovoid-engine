#pragma once

#include "DivComponent.hpp"
#include "FileManager.hpp"
#include "IdComponent.hpp"
#include "TextAreaComponent.hpp"
#include "TextInputComponent.hpp"
#include "UiScript.hpp"

#include "TransformComponent.hpp"

namespace bvd {

namespace ui {

namespace components {

struct ShadowBoxScript : public Script {
    ShadowBoxScript(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const& msg) {
        if (not node.isHovered()) {
            return;
        }

        if (msg.btnKey != buttons::left) {
            return;
        }

        if (node.getRotateAngle() == -90) {
            node.setRotateAngle(0);
        } else if (node.getRotateAngle() == 0) {
            node.setRotateAngle(-90);
        }

        Node& content = node.getParent().getParent()["content"];
        if (content.isEnabled()) {
            content.disable();
        } else {
            content.enable();
        }
    }
};

template<typename... Params>
struct ShadowBox : public Component<ShadowBox, Params...>, ComponentInfo {
    template<typename... Components>
    void render(auto helper, Components... children) {
        setParams(Width{100.0_pct}, autoHeight, noRender);

        fragment(
            Div<>::top(Id{"titleBar"}, Width{100.0_pct}, autoHeight, noRender)(
                Div<Handler<ShadowBoxScript>>::left(Id{"arrow"}, Width{15.0_px}, Height{15.0_px}, RotateAngle{-90}, Alignment::y, Texture{"icon_fwrd.png"})(),
                TextArea<>::left(Id{"title"}, helper.param<Title>(), Alignment::y, Margin{glm::vec4{0.0f, 0.0f, 0.0f, 15.0f}})()
            ),
            Div<>::top(Id{"content"}, Width{100.0_pct}, autoHeight, noRender, Padding{ glm::vec4{5.0f, 0.0f, 0.0f, 15.0f} })(
                children...
            )
        );
    }
};

template<typename... Params>
struct Inspector : public Component<Inspector, Params...>, ComponentInfo {
    void render(auto helper) {
        setParams(Width{100.0_pct}, Height{autoFit}, noRender, Padding{glm::vec4{10.0f, 10.0f, 10.0f, 10.0f}});

        ecs::Scene& scene = *fs::FileManager::getScene(helper.template arg<SceneId>());
        ecs::Entity entity = scene.getEntity(helper.template arg<EntityId>());

        std::vector<NodeCreator> proxies;
        if (entity.hasComponent<ecs::IdComponent>()) {
            auto& id = entity.getComponent<ecs::IdComponent>().tag;
            proxies.push_back(TDiv<>::top(Width{100.0_pct}, Height{autoFit})(
                TextArea<>::left(Title{std::string("ID")}, Alignment::y, Width{autoFit}, Height{autoFit}, colors::grey)(),
                TextInput<>::right(String{&id}, Width{100.0_px}, Height{autoFit}, Alignment::y, colors::grey)()
            ));
        }

        if (entity.hasComponent<ecs::TransformComponent>()) {
            auto& transform = entity.getComponent<ecs::TransformComponent>();

            Margin margin{glm::vec4{0.0f, 5.0f, 0.0f, 0.0f}};

            proxies.push_back(
                ShadowBox<>::top(Title{"Transform"}, Margin{ glm::vec4{10.0f, 0.0f, 0.0f, 0.0f} })(
                    TDiv<>::top(Width{ 100.0_pct }, autoHeight, Margin{ glm::vec4{5.0f, 5.0f, 0.0f, 0.0f} })(
                        TextArea<>::left(Title{ std::string("Position") }, Alignment::y, autoWidth, autoHeight, colors::grey)(),
                        TextArea<>::right(Title{ "X" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.translation.x }, margin)(),
                        TextArea<>::right(Title{ "Y" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.translation.y }, margin)(),
                        TextArea<>::right(Title{ "Z" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.translation.z }, Margin{ glm::vec4{0.0f, 0.0f, 0.0f, 0.0f} })()
                    ),
                    TDiv<>::top(Width{ 100.0_pct }, autoHeight, Margin{ glm::vec4{5.0f, 0.0f, 0.0f, 0.0f} })(
                        TextArea<>::left(Title{ std::string("Rotation") }, Alignment::y, autoWidth, autoHeight, colors::grey)(),
                        TextArea<>::right(Title{ "X" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.rotation.x }, margin)(),
                        TextArea<>::right(Title{ "Y" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.rotation.y }, margin)(),
                        TextArea<>::right(Title{ "Z" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.rotation.z }, Margin{ glm::vec4{0.0f, 0.0f, 0.0f, 0.0f} })()
                    ),
                    TDiv<>::top(Width{ 100.0_pct }, autoHeight, Margin{ glm::vec4{5.0f, 0.0f, 0.0f, 0.0f} })(
                        TextArea<>::left(Title{ std::string("Scale") }, Alignment::y, autoWidth, autoHeight, colors::grey)(),
                        TextArea<>::right(Title{ "X" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.scale.x }, margin)(),
                        TextArea<>::right(Title{ "Y" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.scale.y }, margin)(),
                        TextArea<>::right(Title{ "Z" }, Alignment::y, autoWidth, autoHeight, colors::transparent, margin)(),
                        TextInput<>::right(Alignment::y, Width{ 100.0_px }, autoHeight, Float{ &transform.scale.z }, Margin{ glm::vec4{0.0f, 0.0f, 0.0f, 0.0f} })()
                    )
                )
            );
        }

        fragment(
            proxies
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
