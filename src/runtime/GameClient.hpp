#pragma once

#include "IEngineClient.hpp"
#include "SceneView.hpp"
#include "Messages.hpp"

namespace bvd {

class GameClient : public IEngineClient {
public:
    GameClient(srvm::Router&);

    void initialize(renderer::Component&, ui::UiComponent&);

private:
    srvm::Receiver       receiver;
};

} // namespace bvd
