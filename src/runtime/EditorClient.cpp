#include "EditorClient.hpp"

#include "FileManager.hpp"

#include "Ui.hpp"
#include "DivComponent.hpp"
#include "DockPlaceComponent.hpp"
#include "FsComponent.hpp"
#include "FilecComponent.hpp"
#include "HorizontalSeparator.hpp"
#include "TabComponent.hpp"
#include "TextAreaComponent.hpp"
#include "VerticalSeparator.hpp"
#include "SceneHierarchyComponent.hpp"
#include "EntityPropertiesHandler.hpp"

#include "RendererComponent.hpp"
#include "RenderViewport.hpp"

#include "RenderPassGraph.hpp"

#include "Scene.hpp"

namespace bvd {

struct ViewportScript : public ui::Script {
    ViewportScript(ui::Node& n) : Script(n) {
        receiver.init<MessageMap>(25, *this);
    }

    void receive(UiNodeResizeInd const& msg) {
        ViewportResizeInd ind;
        ind.width = (int32_t)msg.width;
        ind.height = (int32_t)msg.height;
        node.getManager().send(ind);
    }

    void receive(FrameTickInd const&) {
        if (not node.isHovered()) {
            return;
        }

        if (not Input::isMouseButtonPressed(buttons::right)) {
            return;
        }

        auto scene = fs::FileManager::getScene("mainScene");
        auto entity = scene->getEntity("myPicture");
        auto& translate = entity.getComponent<ecs::TransformComponent>().translation;

        float velocity = 25.0f * 0.016f;
        if (Input::isKeyPressed(keys::w)) {
            translate.y -= velocity;
        }
        if (Input::isKeyPressed(keys::s)) {
            translate.y += velocity;
        }
        if (Input::isKeyPressed(keys::a)) {
            translate.x += velocity;
        }
        if (Input::isKeyPressed(keys::d)) {
            translate.x -= velocity;
        }
    }
};

template<typename... Params>
struct EditorComponent : public ui::Component<EditorComponent, Params...>, ui::ComponentInfo {
    void render(auto) {
        using namespace ui;
        using namespace ui::components;

        fragment(
            TDiv<>::vmiddle(Width{ 100.0_pct })(
                Tab<>::left(Id{ "Scene Hierarchy Panel" }, Width{ 300.0_px }, Height{ 100.0_pct })(
                    SceneHierarchy<>::top(SceneId{ "mainScene" })()
                    ),
                VerticalSeparator<>::left()(),
                Div<Handler<ViewportScript>>::hmiddle(Id{"Viewport"}, Texture{ "scene-raw" }, Height{ 100.0_pct })(),
                VerticalSeparator<>::right()(),
                Tab<>::right(Id{ "File" }, Width{ 300.0_px }, Height{ 100.0_pct })(
                    Filec<>::left()()
                    ),
                VerticalSeparator<>::right()(),
                Tab<Handler<handlers::EntityProperties>>::right(Id{ "Properties" }, Width{ 450.0_px }, Height{ 100.0_pct })()
                ),
            HorizontalSeparator<>::bottom()(),
            Tab<>::bottom(Id{ "Content Browser" }, Height{ 450.0_px }, Width{ 100.0_pct })(
                Scrollable<>::vmiddle(Width{ 100.0_pct }, colors::transparent)(
                    Filesystem<>::left(Id{ "Filesystem" }, Path{ "S:/Projects/biovoid-engine/" }, Height{ autoFit })()
                    )
                )
        );
    }
};

EditorClient::EditorClient(srvm::Router& router)
    : receiver(router)
{
    receiver.init<MessageMap>(0, *this);
}

void EditorClient::initialize(renderer::Component& rendererComponent, ui::UiComponent& uiComponent) {
    auto sceneRef1 = fs::FileManager::createScene("mainScene");
    auto sceneRef2 = fs::FileManager::createScene("uiScene");

    uint32_t numSamples = GlobalConfig::Graphics::msaaSamples;
    sceneRef1->deserialize({.format = PixelFormat::r8g8b8a8, .flags = rhi::TextureCreateFlags::renderTargetable, .numSamples = numSamples });
    sceneRef2->deserializeUi({ .format = PixelFormat::b8g8r8a8, .flags = numSamples == 1 ? rhi::TextureCreateFlags::presentable : rhi::TextureCreateFlags::renderTargetable, .numSamples = numSamples });

    uiComponent.create<EditorComponent>({ .width = 2560, .height = 1300 });

    renderer::Viewport& viewport = rendererComponent.getViewport();
    rdg::RenderGraph& renderGraph = viewport.getRenderGraph();
    rhi::Texture2D& renderTarget = *viewport.getRenderTargetTexture();

    rhi::common::TextureCreateInfo info{
            PixelFormat::r8g8b8a8,
            1920,
            1080,
            rhi::TextureCreateFlags::renderTargetable,
            colors::black,
            1
    };

    view.viewMatrices.projectionMatrix = glm::ortho(-0.5f * 1920, 0.5f * 1920, -0.5f * 1080, 0.5f * 1080);

    auto& scenePass = renderGraph.addPass("scene");
    scenePass.setCallback([this, scene = sceneRef1]() mutable {
        scene->updateAllPrimitiveSceneInfos(view.viewMatrices.projectionMatrix * view.viewMatrices.viewMatrix);
        scene->render();
    });
    scenePass.setOutput("scene-raw", info);

    auto& uiPass = renderGraph.addPass("ui");
    uiPass.setCallback([&viewport, scene = sceneRef2]() mutable {
        scene->updateAllPrimitiveSceneInfos(viewport.getView().viewMatrices.projectionMatrix * viewport.getView().viewMatrices.viewMatrix);
        scene->render();
    });
    uiPass.setOutput("ui-raw", renderTarget);
}

void EditorClient::receive(ViewportResizeInd const& msg) {
    view.viewMatrices.projectionMatrix = glm::ortho(-0.5f * msg.width, 0.5f * msg.width, -0.5f * msg.height, 0.5f * msg.height);
}

} // namespace bvd
