#include "GameClient.hpp"

#include "FileManager.hpp"

#include "RendererComponent.hpp"
#include "RenderViewport.hpp"

#include "RenderPassGraph.hpp"

#include "Scene.hpp"

namespace bvd {

GameClient::GameClient(srvm::Router& router)
    : receiver(router)
{
    // receiver.init(0, *this);
}

void GameClient::initialize(renderer::Component& rendererComponent, ui::UiComponent&) {
    auto sceneRef1 = fs::FileManager::createScene("mainScene");

    uint32_t numSamples = GlobalConfig::Graphics::msaaSamples;
    sceneRef1->deserialize({ .format = PixelFormat::b8g8r8a8, .flags = numSamples == 1 ? rhi::TextureCreateFlags::presentable : rhi::TextureCreateFlags::renderTargetable, .numSamples = numSamples });

    renderer::Viewport& viewport = rendererComponent.getViewport();
    rdg::RenderGraph& renderGraph = viewport.getRenderGraph();
    rhi::Texture2D& renderTarget = *viewport.getRenderTargetTexture();

    auto& scenePass = renderGraph.addPass("scene");
    scenePass.setCallback([&viewport, scene = sceneRef1]() mutable {
        scene->updateAllPrimitiveSceneInfos(viewport.getView().viewMatrices.projectionMatrix);
        scene->render();
        });
    scenePass.setOutput("scene-raw", renderTarget);
}

} // namespace bvd
