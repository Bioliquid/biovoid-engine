#pragma once

#include "UiStyle.hpp"

namespace bvd::ui::group {

using Entity = ComponentClass<
      Property<TextColor, colors::blue>
    , Property<Width, 100.0_pct>
    , Property<Height, 20.0_px>
    , Property<TextAlignment, Alignment::y>
>;

} // namespace bvd
