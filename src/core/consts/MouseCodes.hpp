#pragma once

#include <cstdint>

namespace bvd {

using MouseCode = int32_t;

namespace buttons {

constexpr MouseCode btn1 = 0;
constexpr MouseCode btn2 = 1;
constexpr MouseCode btn3 = 2;
constexpr MouseCode btn4 = 3;
constexpr MouseCode btn5 = 4;
constexpr MouseCode btn6 = 5;
constexpr MouseCode btn7 = 6;
constexpr MouseCode btn8 = 7;
constexpr MouseCode last = 7;
constexpr MouseCode left = 0;
constexpr MouseCode right = 1;
constexpr MouseCode middle = 2;

} // namespace buttons

} // namespace bvd
