#pragma once

#include <cstdint>

namespace bvd {

constexpr int32_t maxNumVertexBindings = 4;

} // namespace bvd
