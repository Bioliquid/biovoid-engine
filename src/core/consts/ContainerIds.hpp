#pragma once

#include <cstdint>

namespace bvd {

namespace containerId {

constexpr uint16_t any = 0;

//constexpr uint8_t renderer = 0;
//constexpr uint8_t rdg = 1;
//constexpr uint8_t ui = 2;
//constexpr uint8_t wsi = 3;
//
//constexpr uint16_t get(uint8_t id, uint8_t arrayId = 0) {
//    return (arrayId << 8) | id;
//}

} // namespace containerId

} // namespace bvd