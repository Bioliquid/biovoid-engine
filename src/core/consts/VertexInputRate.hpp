#pragma once

#include <cstdint>

namespace bvd {

struct VertexInputRate {
    using Type = uint32_t;

    static constexpr Type vertex = 0;
    static constexpr Type instace = 1;

    static inline bool isVertex(Type bindingIndex) { return bindingIndex == vertex; }
    static inline bool isInstance(Type bindingIndex) { return bindingIndex == instace; }
};

} // namespace bvd
