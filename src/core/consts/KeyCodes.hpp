#pragma once

#include <cstdint>

namespace bvd {

using KeyCode = int32_t;

namespace keys {

// From glfw3.h
constexpr KeyCode space = 32;
constexpr KeyCode apostrophe = 39;  /* ' */
constexpr KeyCode comma = 44;  /* , */
constexpr KeyCode minux = 45;  /* - */
constexpr KeyCode period = 46;  /* . */
constexpr KeyCode slash = 47;  /* / */
constexpr KeyCode key0 = 48;
constexpr KeyCode key1 = 49;
constexpr KeyCode key2 = 50;
constexpr KeyCode key3 = 51;
constexpr KeyCode key4 = 52;
constexpr KeyCode key5 = 53;
constexpr KeyCode key6 = 54;
constexpr KeyCode key7 = 55;
constexpr KeyCode key8 = 56;
constexpr KeyCode key9 = 57;
constexpr KeyCode semicolon = 59;  /* ; */
constexpr KeyCode equal = 61;  /* = */
constexpr KeyCode a = 65;
constexpr KeyCode b = 66;
constexpr KeyCode c = 67;
constexpr KeyCode d = 68;
constexpr KeyCode e = 69;
constexpr KeyCode f = 70;
constexpr KeyCode g = 71;
constexpr KeyCode h = 72;
constexpr KeyCode i = 73;
constexpr KeyCode j = 74;
constexpr KeyCode k = 75;
constexpr KeyCode l = 76;
constexpr KeyCode m = 77;
constexpr KeyCode n = 78;
constexpr KeyCode o = 79;
constexpr KeyCode p = 80;
constexpr KeyCode q = 81;
constexpr KeyCode r = 82;
constexpr KeyCode s = 83;
constexpr KeyCode t = 84;
constexpr KeyCode u = 85;
constexpr KeyCode v = 86;
constexpr KeyCode w = 87;
constexpr KeyCode x = 88;
constexpr KeyCode y = 89;
constexpr KeyCode z = 90;
constexpr KeyCode leftBracket = 91;  /* [ */
constexpr KeyCode backslash = 92;  /* \ */
constexpr KeyCode rightBracket = 93;  /* ] */
constexpr KeyCode graveAccent = 96;  /* ` */
constexpr KeyCode world1 = 161; /* non-US #1 */
constexpr KeyCode world2 = 162; /* non-US #2 */

/* Function keys */
constexpr KeyCode escape = 256;
constexpr KeyCode enter = 257;
constexpr KeyCode tab = 258;
constexpr KeyCode backspace = 259;
constexpr KeyCode insert = 260;
constexpr KeyCode del = 261;
constexpr KeyCode right = 262;
constexpr KeyCode left = 263;
constexpr KeyCode down = 264;
constexpr KeyCode up = 265;
constexpr KeyCode pageUp = 266;
constexpr KeyCode pageDown = 267;
constexpr KeyCode home = 268;
constexpr KeyCode end = 269;
constexpr KeyCode capsLock = 280;
constexpr KeyCode scrollLock = 281;
constexpr KeyCode numLock = 282;
constexpr KeyCode printScreen = 283;
constexpr KeyCode pause = 284;
constexpr KeyCode F1 = 290;
constexpr KeyCode F2 = 291;
constexpr KeyCode F3 = 292;
constexpr KeyCode F4 = 293;
constexpr KeyCode F5 = 294;
constexpr KeyCode F6 = 295;
constexpr KeyCode F7 = 296;
constexpr KeyCode F8 = 297;
constexpr KeyCode F9 = 298;
constexpr KeyCode F10 = 299;
constexpr KeyCode F11 = 300;
constexpr KeyCode F12 = 301;
constexpr KeyCode F13 = 302;
constexpr KeyCode F14 = 303;
constexpr KeyCode F15 = 304;
constexpr KeyCode F16 = 305;
constexpr KeyCode F17 = 306;
constexpr KeyCode F18 = 307;
constexpr KeyCode F19 = 308;
constexpr KeyCode F20 = 309;
constexpr KeyCode F21 = 310;
constexpr KeyCode F22 = 311;
constexpr KeyCode F23 = 312;
constexpr KeyCode F24 = 313;
constexpr KeyCode F25 = 314;
constexpr KeyCode kp0 = 320;
constexpr KeyCode kp1 = 321;
constexpr KeyCode kp2 = 322;
constexpr KeyCode kp3 = 323;
constexpr KeyCode kp4 = 324;
constexpr KeyCode kp5 = 325;
constexpr KeyCode kp6 = 326;
constexpr KeyCode kp7 = 327;
constexpr KeyCode kp8 = 328;
constexpr KeyCode kp9 = 329;
constexpr KeyCode kpDecimal = 330;
constexpr KeyCode kpDivide = 331;
constexpr KeyCode kpMultiply = 332;
constexpr KeyCode kpSubtract = 333;
constexpr KeyCode kpAdd = 334;
constexpr KeyCode kpEnter = 335;
constexpr KeyCode kpEqual = 336;
constexpr KeyCode leftShift = 340;
constexpr KeyCode leftControl = 341;
constexpr KeyCode leftAlt = 342;
constexpr KeyCode leftSuper = 343;
constexpr KeyCode rightShift = 344;
constexpr KeyCode rightControl = 345;
constexpr KeyCode rightAlt = 346;
constexpr KeyCode rightSuper = 347;
constexpr KeyCode menu = 348;

} // namespace keys

} // namespace bvd
