#include "Timer.hpp"

#include "Logger.hpp"

namespace bvd::rtc {

void Timer::start(uint32_t newPeriod) {
    BVD_LOG(LogGroup::gen, BD, "Starting timer-{} with period {}", id, newPeriod);
    period = newPeriod;
    waited = 0;
    if (not isRunning) {
        isRunning = true;
        pool->usedTimers.push_back(*this);
    }
}

void Timer::restart(uint32_t newPeriod) {
    start(newPeriod ? newPeriod : period);
}

void Timer::stop() {
    BVD_LOG(LogGroup::gen, BD, "Stoped timer-{}", id);
    isRunning = false;
    pool->usedTimers.erase(pool->usedTimers.iterator_to(*this));
    // pool->freeTimers.push_back(*this);
}

TimerPool::TimerPool() {
    for (uint32_t timerId = 0; Timer& timer : timers) {
        timer.id = timerId++;
        timer.isRunning = false;
        timer.pool = this;
        freeTimers.push_back(timer);
    }
}

TimerPtr TimerPool::create(TimerCallback const& callback) {
    Timer& timer = freeTimers.front();
    timer.callback = callback;
    freeTimers.pop_front();
    return &timer;
}

void TimerPool::processTick(uint32_t time) {
    for (auto timerIt = usedTimers.begin(); timerIt != usedTimers.end();) {
        Timer& timer = *timerIt;
        timerIt++;
        timer.waited += time;
        if (timer.waited >= timer.period) {
            timer.callback(timer);
        }
    }
}

} // namespace bvd::rtc
