#include "RtcProfile.hpp"

#include "Logger.hpp"

#include <Windows.h>

namespace bvd::rtc {

Profile::Profile(std::string_view s)
    : name(s)
{
    LARGE_INTEGER startingTime;
    QueryPerformanceCounter(&startingTime);
    startTime = startingTime.QuadPart;
}

Profile::~Profile() {
    LARGE_INTEGER frequency;
    QueryPerformanceFrequency(&frequency);

    uint64_t elapsedMicroseconds;

    LARGE_INTEGER endingTime;
    QueryPerformanceCounter(&endingTime);
    elapsedMicroseconds = endingTime.QuadPart - startTime;
    elapsedMicroseconds *= 1000000;
    elapsedMicroseconds /= frequency.QuadPart;

    BVD_LOG(LogGroup::gen, BD, "{}: {}", name, elapsedMicroseconds);
}

} // namespace bvd::rtc
