#pragma once

#include <boost/intrusive/list.hpp>
#include <array>
#include <functional>

namespace bvd {

namespace rtc {

class Timer;
class TimerPool;

using TimerId = uint32_t;
using TimerCallback = std::function<void(Timer&)>;
using TimerHook = boost::intrusive::list_base_hook<>;

class Timer : public TimerHook {
    friend class TimerPool;

public:
    void start(uint32_t);
    void restart(uint32_t = 0);
    void stop();

public:
    TimerId       id;
    uint32_t      period{0};
    uint32_t      waited{0};
    bool          isRunning;
    TimerCallback callback;

private:
    TimerPool* pool{nullptr};
};

using TimerPtr = Timer*;
using TimerList = boost::intrusive::list<Timer>;

class TimerPool {
    friend class Timer;

    static constexpr uint32_t maxNumTimers = 1024;

    using TimerStorage = std::array<Timer, maxNumTimers>;

public:
    TimerPool();

    TimerPtr create(TimerCallback const&);
    void processTick(uint32_t);

private:
    TimerStorage timers;
    TimerList    freeTimers;
    TimerList    usedTimers;
};

// TODO: make non-global
// TODO: this timer is more like nrt (mili not micro or even nano)
inline TimerPool timerPool;

} // namespace rtc

} // namespace bvd
