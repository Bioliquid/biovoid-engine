#pragma once

#include "RtcTime.hpp"

namespace bvd {

namespace rtc {

class Delta {
public:
    inline float getSeconds() const { return step; }
    inline float getMilliseconds() const { return step * 1000.0f; }

    void advance() {
        float currentFrame = getTime();
        step = currentFrame - lastFrameTime;
        lastFrameTime = currentFrame;
    }

private:
    float step = 0.0f;
    float lastFrameTime = 0.0f;
};

} // namespace rtc

} // namespace bvd
