#include "RtcProfile.hpp"

#include "Logger.hpp"

namespace bvd::rtc {

Profile::Profile(std::string_view s)
    : name(s)
{
}

Profile::~Profile() {
    // BVD_LOG(LogGroup::gen, BD, "{}: {}", name, elapsedMicroseconds);
}

} // namespace bvd::rtc
