#include "RtcTime.hpp"

#include <glfw/glfw3.h>

namespace bvd::rtc {

float getTime() {
    return (float)glfwGetTime();
}

} // namespace bvd::rtc
