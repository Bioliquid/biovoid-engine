#pragma once

#include "RtcTime.hpp"

#include <string_view>

namespace bvd {

namespace rtc {

class Profile {
public:
    Profile(std::string_view);

    ~Profile();

private:
    std::string_view name;
    uint64_t         startTime;
};

} // namespace rtc

} // namespace bvd
