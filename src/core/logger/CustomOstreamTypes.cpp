#include "CustomOstreamTypes.hpp"

//auto fmt::formatter<bvd::PixelFormat>::format(bvd::PixelFormat format, format_context& ctx) -> decltype(ctx.out()) {
//    return format_to(ctx.out(), "{}", toString(format));
//}

auto fmt::formatter<glm::vec2>::format(glm::vec2 const& vec, format_context& ctx) -> decltype(ctx.out()) {
    return fmt::format_to(ctx.out(), "[{} {}]", vec.x, vec.y);
}

auto fmt::formatter<glm::vec4>::format(glm::vec4 const& vec, format_context& ctx) -> decltype(ctx.out()) {
    return fmt::format_to(ctx.out(), "[{} {} {} {}]", vec.x, vec.y, vec.z, vec.w);
}
