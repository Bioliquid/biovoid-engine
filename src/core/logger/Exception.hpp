#pragma once

#include "Logger.hpp"

#include <exception>
#include <string_view>

namespace bvd {

struct Exception {
    Exception(fmt::memory_buffer& mb) : buffer(mb) {}
    Exception(Exception const&) = default;

    std::string_view message() const {
        return {buffer.data(), buffer.size()};
    }

    fmt::memory_buffer& buffer;
};

#define BVD_THROW(tempstr, ...)                                                                          \
    {                                                                                                    \
        BVD_LOG(LogGroup::gen, BE, tempstr __VA_OPT__(,) __VA_ARGS__);                                   \
        fmt::memory_buffer bvdBuffer;                                                                    \
        Exception bvdException{bvdBuffer};                                                               \
        fmt::format_to(std::back_inserter(bvdException.buffer), tempstr "\n" __VA_OPT__(,) __VA_ARGS__); \
        throw bvdException;                                                                              \
    }

//template<typename... Args>
//struct BVD_ASSERT {
//    constexpr BVD_ASSERT(bool condition, fmt::format_string<Args...> sw, Args&&... args, std::source_location = std::source_location::current()) {
//        static_assert(condition, fmt::format(sw, std::forward<Args>(args)...));
//    }
//};

#define BVD_ASSERT(CONDITION, MESSAGE) \
    static_assert(CONDITION, MESSAGE);

} // namespace bvd
