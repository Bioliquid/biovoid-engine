#pragma once

#include "LogInfo.hpp"
#include "CustomOstreamTypes.hpp"

#if ENABLE_INTERNAL_DEBUG

#include <fmt/core.h>
#include <fmt/xchar.h>
#include <source_location>

constexpr std::string_view baseFilename(std::string_view filename)
{
    return filename.substr(filename.find_last_of('\\') + 1);
}

template<typename... Args>
struct BVD_LOG {
    BVD_LOG(bvd::LogGroup group, bvd::LogLevel lvl, fmt::format_string<Args...> sw, Args&&... args, std::source_location loc = std::source_location::current()) {
        if (bvd::logLevels[static_cast<uint32_t>(group)] <= lvl) {
            fmt::memory_buffer loggerBuffer;
            fmt::format_to(std::back_inserter(loggerBuffer), sw, std::forward<Args>(args)...);
            std::string_view logStr(loggerBuffer.data(), loggerBuffer.size());
            fmt::print(fg(bvd::getLogLvlColor(lvl)), "{} {} {:<30}[{:<4}]: {}\n", bvd::getLogLvlName(lvl), bvd::getLogGroupName(group), baseFilename(loc.file_name()), loc.line(), logStr);
        }
    }
};

template <typename... Args>
BVD_LOG(bvd::LogGroup, bvd::LogLevel, fmt::format_string<Args...>, Args&&...) -> BVD_LOG<Args...>;

#else

#define BVD_LOG

#endif
