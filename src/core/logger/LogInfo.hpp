#pragma once

#include <fmt/color.h>

#include <cstdint>

namespace bvd {

using LogLevel = uint8_t;

constexpr uint8_t BD = 0;
constexpr uint8_t BI = 1;
constexpr uint8_t BW = 2;
constexpr uint8_t BE = 3;
constexpr uint8_t BS = 4;
constexpr uint8_t BF = 5;

enum class LogGroup : uint8_t {
      init = 0
    , gen
    , render
    , rhi
    , wcs
    , fs
    , srvm
    , ui
    , num
};

inline LogLevel logLevels[static_cast<uint8_t>(LogGroup::num)];

template<uint8_t>
struct LogTraits;

template<>
struct LogTraits<BD> {
    static constexpr char const* str = "DEBUG";
    static constexpr wchar_t const* wstr = L"DEBUG";
    static constexpr fmt::color color = fmt::color::gray;
};

template<>
struct LogTraits<BI> {
    static constexpr char const* str = "INFO ";
    static constexpr wchar_t const* wstr = L"INFO ";
    static constexpr fmt::color color = fmt::color::light_gray;
};

template<>
struct LogTraits<BW> {
    static constexpr char const* str = "WARN ";
    static constexpr wchar_t const* wstr = L"WARN ";
    static constexpr fmt::color color = fmt::color::orange;
};

template<>
struct LogTraits<BE> {
    static constexpr char const* str = "ERROR";
    static constexpr wchar_t const* wstr = L"ERROR";
    static constexpr fmt::color color = fmt::color::red;
};

template<>
struct LogTraits<BS> {
    static constexpr char const* str = "INFO ";
    static constexpr wchar_t const* wstr = L"INFO ";
    static constexpr fmt::color color = fmt::color::green;
};

template<>
struct LogTraits<BF> {
    static constexpr char const* str = "FATAL";
    static constexpr wchar_t const* wstr = L"FATAL";
    static constexpr fmt::color color = fmt::color::red;
};

constexpr char const* getLogLvlName(bvd::LogLevel lvl) {
    switch (lvl)
    {
    case BD: return LogTraits<BD>::str;
    case BI: return LogTraits<BI>::str;
    case BW: return LogTraits<BW>::str;
    case BE: return LogTraits<BE>::str;
    case BS: return LogTraits<BS>::str;
    case BF: return LogTraits<BF>::str;
    }
    return "unknown";
}

constexpr fmt::color getLogLvlColor(bvd::LogLevel lvl) {
    switch (lvl)
    {
    case BD: return LogTraits<BD>::color;
    case BI: return LogTraits<BI>::color;
    case BW: return LogTraits<BW>::color;
    case BE: return LogTraits<BE>::color;
    case BS: return LogTraits<BS>::color;
    case BF: return LogTraits<BF>::color;
    }
    return {};
}

constexpr char const* getLogGroupName(bvd::LogGroup group) {
    switch (group) {
    case bvd::LogGroup::init:    return "init  ";
    case bvd::LogGroup::gen:     return "gen   ";
    case bvd::LogGroup::render:  return "render";
    case bvd::LogGroup::rhi:     return "rhi   ";
    case bvd::LogGroup::wcs:     return "wcs   ";
    case bvd::LogGroup::fs:      return "fs    ";
    case bvd::LogGroup::srvm:    return "srvm  ";
    case bvd::LogGroup::ui:      return "ui    ";
    case bvd::LogGroup::num:     return "unknown";
    }

    return "unknown";
}

constexpr wchar_t const* getLogGroupNameUtf8(bvd::LogGroup group) {
    switch (group) {
    case bvd::LogGroup::init:    return L"init  ";
    case bvd::LogGroup::gen:     return L"gen   ";
    case bvd::LogGroup::render:  return L"render";
    case bvd::LogGroup::rhi:     return L"rhi   ";
    case bvd::LogGroup::wcs:     return L"wcs   ";
    case bvd::LogGroup::fs:      return L"fs    ";
    case bvd::LogGroup::srvm:    return L"srvm  ";
    case bvd::LogGroup::ui:      return L"ui    ";
    case bvd::LogGroup::num:     return L"unknown";
    }

    return L"unknown";
}

} // namespace bvd
