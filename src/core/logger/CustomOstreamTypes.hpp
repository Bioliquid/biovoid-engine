#pragma once

#include <fmt/format.h>
#include <fmt/ostream.h>
#include <magic_enum.hpp>
#include <glm/glm.hpp>

namespace fmt {

// neccessary for packed struct members
template<class T>
typename std::remove_reference<T>::type val(T t) {
    return t;
}

} // namespace fmt

template <typename T>
struct fmt::formatter<T, std::enable_if_t<magic_enum::is_scoped_enum<T>::value, char>> : formatter<std::string_view> {
    auto format(T const& value, format_context& ctx) {
        if (std::string_view name = magic_enum::enum_name(value); !name.empty()) {
            return fmt::format_to(ctx.out(), "{}", name);
        } else {
            return fmt::format_to(ctx.out(), "{}", magic_enum::enum_integer(value));
        }
    }
};

template <typename T>
struct fmt::formatter<T, std::enable_if_t<magic_enum::is_unscoped_enum<T>::value, char>> : formatter<std::string_view> {
    auto format(T const& value, format_context& ctx) {
        return fmt::format_to(ctx.out(), "{}", magic_enum::enum_integer(value));
    }
};

// declaration of all custom-formatted types
namespace bvd {

namespace ui {

class Node;
struct Val;

} // namespace ui

} // namespace bvd

// declaration of all methods used by the type (parse and/or format)
template <>
struct fmt::formatter<bvd::ui::Node> : formatter<string_view> {
    auto format(bvd::ui::Node const&, format_context& ctx) -> decltype(ctx.out());
};

template <>
struct fmt::formatter<bvd::ui::Val> : formatter<string_view> {
    auto format(bvd::ui::Val const&, format_context& ctx) -> decltype(ctx.out());
};

template <>
struct fmt::formatter<glm::vec2> : formatter<string_view> {
    auto format(glm::vec2 const& vec, format_context& ctx) -> decltype(ctx.out());
};

template <>
struct fmt::formatter<glm::vec4> : formatter<string_view> {
    auto format(glm::vec4 const& vec, format_context& ctx) -> decltype(ctx.out());
};

// formatter class instanciation for all custom types
// extern template struct fmt::formatter<bvd::PixelFormat>;
