#pragma once

#if defined(BVD_BUILD_DLL)
#define BVDAPI __declspec(dllexport)
#elif defined(BVD_USE_DLL)
#define BVDAPI __declspec(dllimport)
#else
#define BVDAPI
#endif
