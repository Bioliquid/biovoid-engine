#pragma once

#include "PixelFormat.hpp"

#include <cstdint>
#include <string>

namespace bvd {

struct GlobalConfig {
    static void load(char const*);

    struct Graphics {
        inline static uint32_t msaaSamples;
    };

    struct Vulkan {
        inline static uint32_t nrofGfxQueues;
        inline static uint32_t nrofComputeQueues;
        inline static uint32_t nrofXferQueues;
    };

    inline static bool          forceVulkan;
    inline static bool          forceOpengl;
    inline static PixelFormat   defaultBackBufferPixelFormat;
};

} // namespace bvd
