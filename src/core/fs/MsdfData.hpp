#pragma once

#undef INFINITE
#include <msdf/msdf-atlas-gen.h>
#include <vector>

namespace bvd {

struct MsdfData {
    msdf_atlas::FontGeometry                fontGeometry;
    std::vector<msdf_atlas::GlyphGeometry>  glyphs;
};

} // namespace bvd
