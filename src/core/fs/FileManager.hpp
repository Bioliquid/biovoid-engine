#pragma once

#include "FilePath.hpp"
#include "Font.hpp"
#include "Scene.hpp"
#include "Ref.hpp"

#include <string>
#include <vector>

namespace bvd {

namespace fs {

class FileManager {
public:
    static void initialize(FilePath const& inRoot);
    static void initializeDefaultResources();
    static void destroyDefaultResources();
    static void destroy();

    static std::string readFile(FilePath const&);

    inline static std::string readShader(char const* path) { return readFile(getShaderPath(path)); }
    static Ref<Font> getFont(std::string const&);

    static Ref<rhi::Texture2D> initializeTexture(std::string const&);
    static Ref<rhi::Texture2D> getTexture(std::string const&);
    static void setTexture(std::string const&, Ref<rhi::Texture2D> const&);

    static Ref<ecs::Scene> createScene(std::string);
    static Ref<ecs::Scene> getScene(std::string);

    inline static FilePath getRootPath() { return root; }
    inline static FilePath getAbsolutePath(char const* path) { return root / path; }
    inline static FilePath getConfigPath(char const* path) { return root / "configs" / path; }
    inline static FilePath getShaderPath(char const* path) { return root / "shaders" / path; }
    inline static FilePath getTexturePath() { return root / "textures"; }
    inline static FilePath getTexturePath(char const* path) { return root / "textures" / path; }
    inline static FilePath getFontPath(char const* path) { return root / "fonts" / path; }

    static FilePath getCurrentPath();

private:
    inline static FilePath root; 

    inline static std::map<std::string, Ref<Font>> fontCache;
    inline static std::map<std::string, Ref<rhi::Texture2D>> textureCache;

    inline static std::map<std::string, Ref<ecs::Scene>> scenes;
};

} // namespace fs

} // namespace bvd
