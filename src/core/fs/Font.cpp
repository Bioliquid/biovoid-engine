﻿#include "Font.hpp"

#include "Exception.hpp"

#include <glm/glm.hpp>

namespace bvd {

using namespace msdf_atlas;

struct FontInput {
    char const*         fontFilename;
    GlyphIdentifierType glyphIdentifierType;
    char const*         charsetFilename;
    double              fontScale;
    char const*         fontName;
};

struct Configuration {
    ImageType               imageType;
    msdf_atlas::ImageFormat imageFormat;
    YDirection              yDirection;
    int                     width;
    int                     height;
    double                  emSize;
    double                  pxRange;
    double                  angleThreshold;
    double                  miterLimit;
    void (*edgeColoring)(msdfgen::Shape&, double, unsigned long long);
    bool                    expensiveColoring;
    unsigned long long      coloringSeed;
    GeneratorAttributes     generatorAttributes;
};

#define DEFAULT_ANGLE_THRESHOLD 3.0
#define DEFAULT_MITER_LIMIT 1.0
#define LCG_MULTIPLIER 6364136223846793005ull
#define LCG_INCREMENT 1442695040888963407ull
#define THREADS 8

template <typename T, typename S, int N, GeneratorFunction<S, N> GEN_FN>
static Ref<rhi::Texture2D> makeAtlas(const std::vector<GlyphGeometry>& glyphs, const FontGeometry& /*fontGeometry*/, const Configuration& config) {
    ImmediateAtlasGenerator<S, N, GEN_FN, BitmapAtlasStorage<T, N>> generator(config.width, config.height);
    generator.setAttributes(config.generatorAttributes);
    generator.setThreadCount(THREADS);
    generator.generate(glyphs.data(), (int)glyphs.size());

    msdfgen::BitmapConstRef<T, N> bitmap = (msdfgen::BitmapConstRef<T, N>) generator.atlasStorage();
    Ref<rhi::Texture2D> texture = rhi::createTexture2D(rhi::TextureCreateInfo(PixelFormat::r32g32b32a32, bitmap.width, bitmap.height, (uint8_t*)bitmap.pixels));
    return texture;
}

Font::Font(FilePath const& inFilepath)
    : filePath(inFilepath)
    , msdfData(new MsdfData()) 
{
    // int result = 0;
    FontInput fontInput = { };
    Configuration config = { };
    fontInput.glyphIdentifierType = GlyphIdentifierType::UNICODE_CODEPOINT;
    fontInput.fontScale = -1;
    config.imageType = ImageType::MTSDF;
    config.imageFormat = msdf_atlas::ImageFormat::BINARY_FLOAT;
    config.yDirection = YDirection::BOTTOM_UP;
    config.edgeColoring = msdfgen::edgeColoringInkTrap;
    // const char* imageFormatName = nullptr;
    int fixedWidth = -1, fixedHeight = -1;
    config.generatorAttributes.config.overlapSupport = true;
    config.generatorAttributes.scanlinePass = true;
    double minEmSize = 0;
    double rangeValue = 2.0;
    TightAtlasPacker::DimensionsConstraint atlasSizeConstraint = TightAtlasPacker::DimensionsConstraint::MULTIPLE_OF_FOUR_SQUARE;
    config.angleThreshold = DEFAULT_ANGLE_THRESHOLD;
    config.miterLimit = DEFAULT_MITER_LIMIT;

    std::string fontFilepath = filePath.string();
    fontInput.fontFilename = fontFilepath.c_str();

    config.emSize = 40;

    // Load fonts
    bool anyCodepointsAvailable = false;
    {
        class FontHolder {
            msdfgen::FreetypeHandle*    ft;
            msdfgen::FontHandle*        font;
            const char*                 fontFilename;
        public:
            FontHolder()
                : ft(msdfgen::initializeFreetype())
                , font(nullptr)
                , fontFilename(nullptr)
            {}

            ~FontHolder() {
                if (ft) {
                    if (font) {
                        msdfgen::destroyFont(font);
                    }
                    msdfgen::deinitializeFreetype(ft);
                }
            }

            bool load(char const* newFontFilename) {
                if (ft && newFontFilename) {
                    if (this->fontFilename && !strcmp(this->fontFilename, newFontFilename)) {
                        return true;
                    }

                    if (font) {
                        msdfgen::destroyFont(font);
                    }

                    font = msdfgen::loadFont(ft, newFontFilename);
                    if (font) {
                        this->fontFilename = newFontFilename;
                        return true;
                    }

                    this->fontFilename = nullptr;
                }
                return false;
            }
            operator msdfgen::FontHandle* () const {
                return font;
            }
        } font;

        if (!font.load(fontInput.fontFilename)) {
            BVD_LOG(LogGroup::gen, BE, "Error");
        }

        if (fontInput.fontScale <= 0) {
            fontInput.fontScale = 1;
        }

        // Load character set
        Charset charset;
        if (fontInput.charsetFilename) {
            if (!charset.load(fontInput.charsetFilename, fontInput.glyphIdentifierType != GlyphIdentifierType::UNICODE_CODEPOINT)) {
                BVD_LOG(LogGroup::gen, BE, "Error");
            }
        } else {
            charset = Charset::ASCII;
            fontInput.glyphIdentifierType = GlyphIdentifierType::UNICODE_CODEPOINT;
        }
        // From ImGui
        //static const uint32_t charsetRanges[] =
        //{
        //    0x0020, 0x00FF, // Basic Latin + Latin Supplement
        //    0x0400, 0x052F, // Cyrillic + Cyrillic Supplement
        //    0x2DE0, 0x2DFF, // Cyrillic Extended-A
        //    0xA640, 0xA69F, // Cyrillic Extended-B
        //    0,
        //};

        //for (int range = 0; range < 8; range += 2) {
        //    for (int c = charsetRanges[range]; c <= charsetRanges[range + 1]; c++)
        //        charset.add(c);
        //}

        // Load glyphs
        msdfData->fontGeometry = FontGeometry(&msdfData->glyphs);
        int glyphsLoaded = -1;
        switch (fontInput.glyphIdentifierType) {
            case GlyphIdentifierType::GLYPH_INDEX:
                glyphsLoaded = msdfData->fontGeometry.loadGlyphset(font, fontInput.fontScale, charset);
                break;
            case GlyphIdentifierType::UNICODE_CODEPOINT:
                glyphsLoaded = msdfData->fontGeometry.loadCharset(font, fontInput.fontScale, charset);
                anyCodepointsAvailable |= glyphsLoaded > 0;
                break;
        }

        if (glyphsLoaded < 0) {
            BVD_LOG(LogGroup::gen, BE, "Error");
        }
        BVD_LOG(LogGroup::gen, BD, "Loaded geometry of {} out of {} glyphs", glyphsLoaded, (int)charset.size());
        // List missing glyphs
        if (glyphsLoaded < (int)charset.size()) {
            BVD_LOG(LogGroup::gen, BW, "Missing {} {}", (int)charset.size() - glyphsLoaded, fontInput.glyphIdentifierType == GlyphIdentifierType::UNICODE_CODEPOINT ? "codepoints" : "glyphs");
        }

        if (fontInput.fontName) {
            msdfData->fontGeometry.setName(fontInput.fontName);
        }
    }

    // Determine final atlas dimensions, scale and range, pack glyphs
    {
        double pxRange = rangeValue;
        bool fixedDimensions = fixedWidth >= 0 && fixedHeight >= 0;
        bool fixedScale = config.emSize > 0;
        TightAtlasPacker atlasPacker;
        if (fixedDimensions)
            atlasPacker.setDimensions(fixedWidth, fixedHeight);
        else
            atlasPacker.setDimensionsConstraint(atlasSizeConstraint);
        atlasPacker.setPadding(config.imageType == ImageType::MSDF || config.imageType == ImageType::MTSDF ? 0 : -1);
        // TODO: In this case (if padding is -1), the border pixels of each glyph are black, but still computed. For floating-point output, this may play a role.
        if (fixedScale) {
            atlasPacker.setScale(config.emSize);
        } else {
            atlasPacker.setMinimumScale(minEmSize);
        }
        atlasPacker.setPixelRange(pxRange);
        atlasPacker.setMiterLimit(config.miterLimit);
        if (int remaining = atlasPacker.pack(msdfData->glyphs.data(), (int)msdfData->glyphs.size())) {
            if (remaining < 0) {
                BVD_LOG(LogGroup::gen, BE, "Error");
            } else {
                BVD_LOG(LogGroup::gen, BE, "Error: Could not fit {} out of {} glyphs into the atlas.", remaining, (int)msdfData->glyphs.size());
            }
        }
        atlasPacker.getDimensions(config.width, config.height);
        // HZ_CORE_ASSERT(config.width > 0 && config.height > 0);
        config.emSize = atlasPacker.getScale();
        config.pxRange = atlasPacker.getPixelRange();
        if (!fixedScale) {
            BVD_LOG(LogGroup::gen, BD, "Glyph size: {} pixels/EM", config.emSize);
        }
        if (!fixedDimensions) {
            BVD_LOG(LogGroup::gen, BD, "Atlas dimensions: {} x {}", config.width, config.height);
        }
    }


    // Edge coloring
    if (config.imageType == ImageType::MSDF || config.imageType == ImageType::MTSDF) {
        if (config.expensiveColoring) {
            Workload([&glyphs = msdfData->glyphs, &config](int i, int /*threadNo*/) -> bool
            {
                unsigned long long glyphSeed = (LCG_MULTIPLIER * (config.coloringSeed ^ i) + LCG_INCREMENT) * !!config.coloringSeed;
                glyphs[i].edgeColoring(config.edgeColoring, config.angleThreshold, glyphSeed);
                return true;
            }, (int)msdfData->glyphs.size()).finish(THREADS);
        } else {
            unsigned long long glyphSeed = config.coloringSeed;
            for (GlyphGeometry& glyph : msdfData->glyphs) {
                glyphSeed *= LCG_MULTIPLIER;
                glyph.edgeColoring(config.edgeColoring, config.angleThreshold, glyphSeed);
            }
        }
    }

    bool floatingPointFormat = true;
    switch (config.imageType) {
        case ImageType::MSDF:
            if (floatingPointFormat) {
                textureAtlas = makeAtlas<float, float, 3, msdfGenerator>(msdfData->glyphs, msdfData->fontGeometry, config);
            } else {
                textureAtlas = makeAtlas<byte, float, 3, msdfGenerator>(msdfData->glyphs, msdfData->fontGeometry, config);
            }
            break;
        case ImageType::MTSDF:
            if (floatingPointFormat) {
                textureAtlas = makeAtlas<float, float, 4, mtsdfGenerator>(msdfData->glyphs, msdfData->fontGeometry, config);
            } else {
                textureAtlas = makeAtlas<byte, float, 4, mtsdfGenerator>(msdfData->glyphs, msdfData->fontGeometry, config);
            }
            break;
        case ImageType::HARD_MASK:
        case ImageType::SOFT_MASK:
        case ImageType::SDF:
        case ImageType::PSDF:
            BVD_THROW("Invalid ImageType");
    }

    const auto& metrics = msdfData->fontGeometry.getMetrics();
    fsScale = 1.0f / float(metrics.ascenderY - metrics.descenderY);
}

Font::~Font() {
    delete msdfData;
}

float Font::getTextWidth(std::string_view const& view, float fontSize, float maxWidth) const {
    auto& fontGeometry = msdfData->fontGeometry;

    float width = 0.0f;
    float x = 0.0f;
    for (uint32_t i = 0; i < view.size(); ++i) {
        char character = view[i];
        if (character == '\n') {
            width = std::max(width, x);
            x = 0;
            continue;
        }

        auto glyph = fontGeometry.getGlyph(character);
        if (!glyph) {
            glyph = fontGeometry.getGlyph('?');
        }

        if (i != view.size() - 1) {
            double advance = glyph->getAdvance();
            fontGeometry.getAdvance(advance, character, view[i + 1]);
            x += fontSize * (fsScale * (float)advance + kerningOffset);
        } else {
            double pl, pb, pr, pt;
            glyph->getQuadPlaneBounds(pl, pb, pr, pt);
            x += fontSize * (fsScale * float(pr) + kerningOffset);
        }
    }

    return std::min(std::max(width, x), maxWidth);
}

float Font::getTextHeight(std::string_view const& view, float fontSize, float maxWidth) const {
    auto& fontGeometry = msdfData->fontGeometry;
    const auto& metrics = fontGeometry.getMetrics();

    float lineHeightOffset = 0.0f;

    std::vector<int> nextLines;
    {
        double x = 0.0;
        double y = 0.0;
        int lastSpace = -1;
        for (uint32_t i = 0; i < view.size(); i++) {
            char character = view[i];
            if (character == '\n') {
                x = 0;
                y -= fontSize * fsScale * metrics.lineHeight + lineHeightOffset;
                lastSpace = -1;
                continue;
            }

            if (std::abs(y) > 10000.0f) {
                break;
            }

            auto glyph = fontGeometry.getGlyph(character);
            if (!glyph)
                glyph = fontGeometry.getGlyph('?');
            if (!glyph)
                continue;

            if (character != ' ') {
                // Calc geo
                double pl, pb, pr, pt;
                glyph->getQuadPlaneBounds(pl, pb, pr, pt);
                glm::vec2 quadMax((float)pr, (float)pt);

                quadMax *= fsScale * fontSize;
                quadMax += glm::vec2(x, y);

                if (quadMax.x > maxWidth) {
                    if (lastSpace != -1) {
                        i = lastSpace;
                        nextLines.emplace_back(lastSpace);
                        lastSpace = -1;
                    } else {
                        nextLines.emplace_back(i);
                        i = i - 1;
                    }
                    x = 0;
                    y -= fontSize * (fsScale * metrics.lineHeight + lineHeightOffset);
                    continue;
                }
            } else {
                lastSpace = i;
            }

            if (i != view.size() - 1) {
                double advance = glyph->getAdvance();
                fontGeometry.getAdvance(advance, character, view[i + 1]);
                x += fontSize * (fsScale * advance + kerningOffset);
            }
        }
    }

    auto nextLine = [](int index, const std::vector<int>& lines) {
        for (int line : lines) {
            if (line == index)
                return true;
        }
        return false;
    };

    uint32_t numOfLines = 0;
    {
        for (uint32_t i = 0; i < view.size(); ++i) {
            char character = view[i];
            if (character == '\n' || nextLine(i, nextLines)) {
                ++numOfLines;
            }
        }
    }

    float yMax = 0.0f;
    {
        // TODO: height = fontSize * (fsScale * (float)metrics.lineHeight + lineHeightOffset) * NUMBER_OF_LINES
        for (int32_t i = (int32_t)view.size() - 1; i >= 0; --i) {
            char character = view[i];
            if (character == '\n' || nextLine(i, nextLines)) {
                break;
            }

            auto glyph = fontGeometry.getGlyph(character);
            if (!glyph) {
                glyph = fontGeometry.getGlyph('?');
            }

            double pl, pb, pr, pt;
            glyph->getQuadPlaneBounds(pl, pb, pr, pt);
            pb *= fsScale * fontSize, pt *= fsScale * fontSize;
            yMax = std::max(yMax, (float)(pt));
        }
    }

    return numOfLines * fontSize * (fsScale * (float)metrics.lineHeight + lineHeightOffset) + yMax;
}

float Font::getAdvance(std::string const& str, uint32_t pos, float fontSize) const {
    auto& fontGeometry = msdfData->fontGeometry;
    auto glyph = fontGeometry.getGlyph(str[pos]);
    double advance = glyph->getAdvance();
    if (pos != str.size() - 1) {
        fontGeometry.getAdvance(advance, str[pos], str[pos + 1]);
    }
    return fontSize * (fsScale * float(advance) + kerningOffset);
}

std::pair<float, uint32_t> Font::getCursorPos(std::string_view const& view, float fontSize, float cursorX) const
{
    auto& fontGeometry = msdfData->fontGeometry;

    float x = 0.0f;
    uint32_t charNum = 0;
    for (; charNum < view.size() && x < cursorX; ++charNum) {
        char character = view[charNum];
        if (character == '\n') {
            BVD_LOG(LogGroup::fs, BE, "getCursorPos on multiple lines is not supported");
            x = 0;
            continue;
        }

        auto glyph = fontGeometry.getGlyph(character);
        if (!glyph) {
            glyph = fontGeometry.getGlyph('?');
        }

        if (charNum != view.size() - 1) {
            double advance = glyph->getAdvance();
            fontGeometry.getAdvance(advance, character, view[charNum + 1]);
            float adv = fontSize * (fsScale * (float)advance + kerningOffset);
            if (x + adv > cursorX && cursorX - x <= x + adv - cursorX) {
                break;
            }
            x += adv;
        } else {
            double pl, pb, pr, pt;
            glyph->getQuadPlaneBounds(pl, pb, pr, pt);
            x += fontSize * (fsScale * float(pr) + kerningOffset);
        }
    }

    return std::make_pair(x, charNum);
}

} // namespace bvd
