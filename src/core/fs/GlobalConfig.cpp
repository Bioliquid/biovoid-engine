#include "GlobalConfig.hpp"
#include "FileManager.hpp"
#include "Logger.hpp"

#include <toml++/toml.h>
#include <string_view>

namespace bvd {

void GlobalConfig::load(char const* path) {
    using namespace std::string_view_literals;

    FilePath absolutePath = fs::FileManager::getConfigPath(path);

    try {
        toml::table table = toml::parse_file(absolutePath.native());

        Graphics::msaaSamples = table["graphics"]["msaaSamples"].value_or(1);

        std::string_view forceGraphicsAPI = table["GraphicsAPI"].value_or("auto"sv);
        GlobalConfig::forceOpengl = false;
        GlobalConfig::forceVulkan = false;
        if (forceGraphicsAPI == "opengl"sv) {
            GlobalConfig::forceOpengl = true;
        } else if (forceGraphicsAPI == "vulkan"sv) {
            GlobalConfig::forceVulkan = true;
        } else if (forceGraphicsAPI == "auto"sv) {

        }

        std::string_view backBufferPixelFormat = table["graphics"]["DefaultBackBufferPixelFormat"].value_or("auto"sv);
        if (backBufferPixelFormat == "B8G8R8A8"sv) {
            GlobalConfig::defaultBackBufferPixelFormat = PixelFormat::b8g8r8a8;
        } else if (backBufferPixelFormat == "R8G8B8A8"sv) {
            GlobalConfig::defaultBackBufferPixelFormat = PixelFormat::r8g8b8a8;
        } else if (backBufferPixelFormat == "auto"sv) {
            GlobalConfig::defaultBackBufferPixelFormat = PixelFormat::unknown;
        }

        Vulkan::nrofGfxQueues     = table["vulkan"]["NrofGfxQueues"].value_or(128);
        Vulkan::nrofComputeQueues = table["vulkan"]["NrofComputeQueues"].value_or(0);
        Vulkan::nrofXferQueues    = table["vulkan"]["NrofXferQueues"].value_or(0);

        logLevels[static_cast<uint32_t>(LogGroup::init)]    = table["logger"]["init"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::gen)]     = table["logger"]["gen"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::render)]  = table["logger"]["render"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::rhi)]     = table["logger"]["rhi"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::wcs)]     = table["logger"]["wcs"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::fs)]      = table["logger"]["fs"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::srvm)]    = table["logger"]["srvm"].value_or<uint8_t>(2);
        logLevels[static_cast<uint32_t>(LogGroup::ui)]      = table["logger"]["ui"].value_or<uint8_t>(2);
    } catch (toml::parse_error const& err) {
        // BLG(BE, "Error parsing file '{}':\n{}\n({})\n", *err.source().path, err.description(), err.source().begin);
        BVD_LOG(LogGroup::gen, BE, "Error parsing file '{}':\n{}", *err.source().path, err.description());
    }
}

} // namespace bvd
