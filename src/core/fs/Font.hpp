#pragma once

#include "MsdfData.hpp"
#include "FilePath.hpp"
#include "Resource.hpp"
#include "Ref.hpp"
#include "Rhi.hpp"

#include <filesystem>

namespace bvd {

struct MsdfData;

class Font : public Resource {
public:
    Font(FilePath const&);
    ~Font();

    Font(Font const&) = delete;
    Font& operator=(Font const&) = delete;

    inline Ref<rhi::Texture2D> getFontAtlas() const { return textureAtlas; }
    inline MsdfData const* getMsdfData() const { return msdfData; }

    float getTextWidth(std::string_view const&, float, float) const;
    float getTextHeight(std::string_view const&, float, float) const;

    float getAdvance(std::string const&, uint32_t, float) const;

    std::pair<float, uint32_t> getCursorPos(std::string_view const&, float, float) const;

private:
    FilePath            filePath;
    Ref<rhi::Texture2D> textureAtlas;
    MsdfData*           msdfData = nullptr;

    float kerningOffset{0.01f};
    float fsScale;
};

} // namespace bvd
