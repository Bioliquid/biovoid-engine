#include "FileManager.hpp"

#include "FileEntry.hpp"
#include "FileIterator.hpp"
#include "Logger.hpp"

#include <fstream>

namespace bvd::fs {

void FileManager::initialize(FilePath const& inRoot) {
    root = inRoot;

    BVD_LOG(LogGroup::fs, BI, "Initializing FileManager::assetsPath: {}", root.string());
}

void FileManager::initializeDefaultResources() {
    BVD_LOG(LogGroup::fs, BI, "Initializing textures");
    for (FileEntry const& entry : FileIterator{getTexturePath()}) {
        BVD_LOG(LogGroup::fs, BD, "-- {}", entry.path().filename().string());
        initializeTexture(entry.path().filename().string());
    }

    BVD_LOG(LogGroup::fs, BD, "-- dummy");
    uint8_t pixel[64 * 4] = {};
    std::ranges::fill(pixel, uint8_t(255));
    setTexture("dummy", rhi::createTexture2D(PixelFormat::r8g8b8a8, 8, 8, pixel));
}

void FileManager::destroyDefaultResources() {
    for (auto& cache : textureCache) {
        Ref<rhi::Texture2D>& texture = cache.second;
        texture.reset();
    }
}

void FileManager::destroy() {
    for (auto& cache : fontCache) {
        Ref<Font>& font = cache.second;
        font.reset();
    }

    for (auto& [name, scene] : scenes) {
        scene->destroy();
        scene.reset();
    }
}

std::string FileManager::readFile(FilePath const& path) {
    std::ifstream in(path);
    std::stringstream buffer;
    buffer << in.rdbuf();

    return buffer.str();
}

FilePath FileManager::getCurrentPath() {
    return std::filesystem::current_path();
}

Ref<Font> FileManager::getFont(std::string const& path) {
    if (fontCache.count(path) == 0) {
        Font* font = new Font(getFontPath(path.c_str()));
        fontCache.emplace(path, font);
        setTexture(path, font->getFontAtlas());
    }

    return fontCache[path];
}

Ref<rhi::Texture2D> FileManager::initializeTexture(std::string const& filename) {
    if (textureCache.count(filename) == 0) {
        textureCache.emplace(filename, rhi::createTexture2D(filename.c_str()));
    }

    return textureCache[filename];
}

Ref<rhi::Texture2D> FileManager::getTexture(std::string const& name) {
    if (textureCache.count(name) == 0) {
        BVD_LOG(LogGroup::fs, BE, "Trying to get texture which has not been initialized yet name={}", name);
        return nullptr;
    }

    return textureCache[name];
}

void FileManager::setTexture(std::string const& name, Ref<rhi::Texture2D> const& texture) {
    textureCache[name] = texture;
}

Ref<ecs::Scene> FileManager::createScene(std::string name) {
    scenes[name] = new ecs::Scene;
    return scenes[name];
}

Ref<ecs::Scene> FileManager::getScene(std::string name) {
    return scenes[name];
}

} // namespace bvd::fs
