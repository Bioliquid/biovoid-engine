#pragma once

namespace bvd {

namespace ui {
class UiComponent;
} // namespace ui

namespace renderer {
class Component;
} // namespace renderer

struct IEngineClient {
    virtual void initialize(renderer::Component&, ui::UiComponent&) = 0;
};

} // namespace bvd
