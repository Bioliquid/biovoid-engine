#pragma once

#include "Logger.hpp"

#include <cstring>

namespace bvd {

inline void memzero(void* dst, size_t count) {
    std::memset(dst, 0, count);
}

template<typename T>
inline void memzero(T& dst) {
    memzero(&dst, sizeof(T));
}

template<typename T>
inline int32_t memcmp(T const& lhs, T const& rhs) {
    return std::memcmp(reinterpret_cast<void const*>(&lhs), reinterpret_cast<void const*>(&rhs), sizeof(T));
}

template<typename T>
inline void memcpy(T& dst, T const& src) {
    std::memcpy(reinterpret_cast<void*>(&dst), reinterpret_cast<void const*>(&src), sizeof(T));
}

template<uint64_t flag>
inline bool flagPresent(uint64_t flags) {
    return (flags & flag) == flag;
}

#if ENABLE_INTERNAL_DEBUG

template<typename... Args>
struct BVD_CHECK {
    BVD_CHECK(bool condition, fmt::format_string<Args...> sw, Args&&... args, std::source_location loc = std::source_location::current()) {
        if (!condition) [[unlikely]] {
            BVD_LOG<Args...>(LogGroup::gen, BE, sw, std::forward<Args>(args)..., loc);
        }
    }
};

template <std::convertible_to<bool> CondType, typename... Args>
BVD_CHECK(CondType, fmt::format_string<Args...>, Args&&...) -> BVD_CHECK<Args...>;

template<typename... Args>
struct BVD_ICHECK {
    BVD_ICHECK(bool condition, fmt::format_string<Args...> sw, Args&&... args, std::source_location loc = std::source_location::current()) {
        if (condition) [[unlikely]] {
            BVD_LOG<Args...>(LogGroup::gen, BE, sw, std::forward<Args>(args)..., loc);
        }
    }
};

template <std::convertible_to<bool> CondType, typename... Args>
BVD_ICHECK(CondType, fmt::format_string<Args...>, Args&&...) -> BVD_ICHECK<Args...>;

#define BASS(condition, ...) \
    if constexpr (!(condition)) \
        BVD_LOG(bvd::LogGroup::gen, BE, __VA_ARGS__)

#else

#define BVD_CHECK(condition, ...)
#define BVD_ICHECK(condition, ...)

#endif

} // namespace bvd
