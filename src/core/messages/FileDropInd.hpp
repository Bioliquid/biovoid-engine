#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

#include "StaticVector.hpp"

namespace bvd {

using FileList = static_vector<std::string_view, 10>;

struct FileDropInd {
    srvm::MessageHeader header{.type = msgId::fileDropInd};
    FileList files;
};

} // namespace bvd
