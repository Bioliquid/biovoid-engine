#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct ViewportResizeInd {
    srvm::MessageHeader header{.type = msgId::viewportResizeInd};
    int32_t width;
    int32_t height;
};

} // namespace bvd
