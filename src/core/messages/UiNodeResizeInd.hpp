#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

#include <string_view>

namespace bvd {

struct UiNodeResizeInd {
    srvm::MessageHeader header{.type = msgId::uiNodeResizeInd};
    float width;
    float height;
};

struct UiNodeHoverInd {
    srvm::MessageHeader header{.type = msgId::uiNodeHoverInd};
    bool isEnter;
};

struct UiEntitySelectedInd {
    srvm::MessageHeader header{.type = msgId::uiEntitySelectedInd};
    std::string_view scene;
    std::string_view name;
};

} // namespace bvd
