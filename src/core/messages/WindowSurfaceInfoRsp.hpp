#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct WindowSurfaceInfoRsp {
    srvm::MessageHeader header{.type = msgId::windowSurfaceInfoRsp};
    uint32_t width;
    uint32_t height;
    void*    surfaceHandle;
};

} // namespace bvd
