#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct FrameEndInd {
    srvm::MessageHeader header{.type = msgId::frameEndInd};
};

} // namespace bvd
