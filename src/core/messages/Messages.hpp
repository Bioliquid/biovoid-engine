#pragma once

#include "KeyboardEvents.hpp"
#include "MouseEvents.hpp"
#include "WindowEvents.hpp"

#include "UiNodeResizeInd.hpp"
#include "ViewportResizeInd.hpp"
#include "WindowActionReq.hpp"
#include "WindowSurfaceInfoReq.hpp"
#include "WindowSurfaceInfoRsp.hpp"
#include "FrameTickInd.hpp"
#include "FrameEndInd.hpp"
#include "RenderSetupReq.hpp"
#include "WsiSetupReq.hpp"
#include "FileDropInd.hpp"
#include "EntityCreateReq.hpp"

#include "SrvmReceiver.hpp"
#include "SrvmSender.hpp"

#include <hml/data/List.hpp>
#include <hml/data/Int.hpp>
#include <type_traits>

namespace bvd {

using MessageMap = hml::List<
    hml::List<hml::Int<msgId::keyPressInd>, KeyPressInd>,
    hml::List<hml::Int<msgId::keyReleaseInd>, KeyReleaseInd>,
    hml::List<hml::Int<msgId::keyTypeInd>, KeyTypeInd>,
    hml::List<hml::Int<msgId::mouseMoveInd>, MouseMoveInd>,
    hml::List<hml::Int<msgId::mousePressInd>, MousePressInd>,
    hml::List<hml::Int<msgId::mouseReleaseInd>, MouseReleaseInd>,
    hml::List<hml::Int<msgId::mouseScrollInd>, MouseScrollInd>,
    hml::List<hml::Int<msgId::windowCloseInd>, WindowCloseInd>,
    hml::List<hml::Int<msgId::windowMinimizeInd>, WindowMinimizeInd>,
    hml::List<hml::Int<msgId::windowRefreshInd>, WindowRefreshInd>,
    hml::List<hml::Int<msgId::windowResizeInd>, WindowResizeInd>,
    hml::List<hml::Int<msgId::uiNodeResizeInd>, UiNodeResizeInd>,
    hml::List<hml::Int<msgId::uiNodeHoverInd>, UiNodeHoverInd>,
    hml::List<hml::Int<msgId::uiEntitySelectedInd>, UiEntitySelectedInd>,
    hml::List<hml::Int<msgId::viewportResizeInd>, ViewportResizeInd>,
    hml::List<hml::Int<msgId::windowActionReq>, WindowActionReq>,
    hml::List<hml::Int<msgId::windowSurfaceInfoReq>, WindowSurfaceInfoReq>,
    hml::List<hml::Int<msgId::windowSurfaceInfoRsp>, WindowSurfaceInfoRsp>,
    hml::List<hml::Int<msgId::frameTickInd>, FrameTickInd>,
    hml::List<hml::Int<msgId::frameEndInd>, FrameEndInd>,
    hml::List<hml::Int<msgId::renderSetupReq>, RenderSetupReq>,
    hml::List<hml::Int<msgId::wsiSetupReq>, WsiSetupReq>,
    hml::List<hml::Int<msgId::fileDropInd>, FileDropInd>,
    hml::List<hml::Int<msgId::entityCreateReq>, EntityCreateReq>
>;

} // namespace bvd
