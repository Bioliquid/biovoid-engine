#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"
#include "WindowMode.hpp"

#include <string_view>

namespace bvd {

struct WsiSetupReq {
    srvm::MessageHeader header{.type = msgId::wsiSetupReq};
    int32_t          width;
    int32_t          height;
    WindowMode       mode;
    std::string_view title;
};

} // namespace bvd
