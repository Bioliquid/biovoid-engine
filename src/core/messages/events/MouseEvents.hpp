#pragma once

#include "mouse/MousePressInd.hpp"
#include "mouse/MouseReleaseInd.hpp"
#include "mouse/MouseMoveInd.hpp"
#include "mouse/MouseScrollInd.hpp"
