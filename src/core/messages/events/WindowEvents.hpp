#pragma once

#include "window/WindowCloseInd.hpp"
#include "window/WindowMinimizeInd.hpp"
#include "window/WindowRefreshInd.hpp"
#include "window/WindowResizeInd.hpp"
