#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct WindowRefreshInd {
    srvm::MessageHeader header{.type = msgId::windowRefreshInd};
};

} // namespace bvd
