#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct WindowCloseInd {
    srvm::MessageHeader header{.type = msgId::windowCloseInd};
};

} // namespace bvd
