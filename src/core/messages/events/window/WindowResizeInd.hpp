#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct WindowResizeInd {
    srvm::MessageHeader header{.type = msgId::windowResizeInd};
    int32_t width;
    int32_t height;
};

} // namespace bvd
