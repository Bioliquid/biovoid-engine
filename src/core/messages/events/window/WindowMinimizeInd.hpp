#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct WindowMinimizeInd {
    srvm::MessageHeader header{.type = msgId::windowMinimizeInd};
};

} // namespace bvd
