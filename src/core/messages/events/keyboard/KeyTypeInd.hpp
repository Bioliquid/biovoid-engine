#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct KeyTypeInd {
    srvm::MessageHeader header{.type = msgId::keyTypeInd};
    int keyCode;
};

} // namespace bvd
