#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct KeyPressInd {
    srvm::MessageHeader header{.type = msgId::keyPressInd};
    int keyCode;
    int repeatCount;
};

} // namespace bvd
