#pragma once

#include "SrvmMessage.hpp"
#include "KeyCodes.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct KeyReleaseInd {
    srvm::MessageHeader header{.type = msgId::keyReleaseInd};
    KeyCode keyCode;
};

} // namespace bvd
