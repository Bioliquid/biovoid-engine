#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

#include <glm/glm.hpp>

namespace bvd {

struct MousePressInd {
    srvm::MessageHeader header{.type = msgId::mousePressInd};
    int       btnKey;
    bool      isDoubleClick;
    glm::vec2 pos;

};

} // namespace bvd
