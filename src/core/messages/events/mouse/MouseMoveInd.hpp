#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

#include <glm/glm.hpp>

namespace bvd {

struct MouseMoveInd {
    srvm::MessageHeader header{.type = msgId::mouseMoveInd};
    glm::vec2 pos;
    glm::vec2 delta;
};

} // namespace bvd
