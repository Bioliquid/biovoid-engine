#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct MouseReleaseInd {
    srvm::MessageHeader header{.type = msgId::mouseReleaseInd};
    int btnKey;
};

} // namespace bvd
