#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct MouseScrollInd {
    srvm::MessageHeader header{.type = msgId::mouseScrollInd};
    float xOffset;
    float yOffset;
};

} // namespace bvd
