#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct WindowSurfaceInfoReq {
    srvm::MessageHeader header{.type = msgId::windowSurfaceInfoReq};
    void* surfaceHandle;
};

} // namespace bvd
