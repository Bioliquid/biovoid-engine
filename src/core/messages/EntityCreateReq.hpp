#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct EntityCreateReq {
    srvm::MessageHeader header{.type = msgId::entityCreateReq};
};

} // namespace bvd
