#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"
#include "StaticVector.hpp"

#include <array>
#include <string_view>

namespace bvd {

struct RenderSceneInfo {
    uint32_t          id;
    std::string       scene;
};

using RenderScenesInfo = static_vector<RenderSceneInfo, 2>;

struct RenderSetupReq {
    srvm::MessageHeader header{.type = msgId::renderSetupReq};
    int32_t          width;
    int32_t          height;
    RenderScenesInfo scenes{};
};

} // namespace bvd
