#pragma once

#include "SrvmDefs.hpp"

namespace bvd {

namespace msgId {

constexpr srvm::MsgId keyPressInd = 0;
constexpr srvm::MsgId keyReleaseInd = 1;
constexpr srvm::MsgId keyTypeInd = 2;

constexpr srvm::MsgId mouseMoveInd = 3;
constexpr srvm::MsgId mousePressInd = 4;
constexpr srvm::MsgId mouseReleaseInd = 5;
constexpr srvm::MsgId mouseScrollInd = 6;

constexpr srvm::MsgId windowCloseInd = 7;
constexpr srvm::MsgId windowMinimizeInd = 8;
constexpr srvm::MsgId windowRefreshInd = 9;
constexpr srvm::MsgId windowResizeInd = 10;

constexpr srvm::MsgId uiNodeResizeInd = 11;
constexpr srvm::MsgId uiNodeHoverInd = 12;
constexpr srvm::MsgId uiEntitySelectedInd = 13;
constexpr srvm::MsgId viewportResizeInd = 14;
constexpr srvm::MsgId windowActionReq = 15;
constexpr srvm::MsgId windowSurfaceInfoReq = 16;
constexpr srvm::MsgId windowSurfaceInfoRsp = 17;
constexpr srvm::MsgId frameTickInd = 18;
constexpr srvm::MsgId frameEndInd = 19;
constexpr srvm::MsgId renderSetupReq = 20;
constexpr srvm::MsgId wsiSetupReq = 21;
constexpr srvm::MsgId fileDropInd = 22;
constexpr srvm::MsgId entityCreateReq = 23;

constexpr srvm::MsgId userDefinedIdStart = 100;

} // namespace msgId

} // namespace bvd
