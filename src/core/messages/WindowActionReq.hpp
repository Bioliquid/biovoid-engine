#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

enum class WindowAction {
      swapBuffers
    , close
    , makeActive
};

struct WindowActionReq {
    srvm::MessageHeader header{.type = msgId::windowActionReq};
    WindowAction action;
};

} // namespace bvd
