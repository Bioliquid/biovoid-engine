#pragma once

#include "SrvmMessage.hpp"
#include "MsgIds.hpp"

namespace bvd {

struct FrameTickInd {
    srvm::MessageHeader header{.type = msgId::frameTickInd};
    float ms;
};

} // namespace bvd
