#pragma once

namespace bvd::traits {

template <std::size_t... Is>
struct Indices {};

template <std::size_t N, std::size_t... Is>
struct build_indices : build_indices<N - 1, N - 1, Is...> {};

template <std::size_t... Is>
struct build_indices<0, Is...> : Indices<Is...> {};

} // namespace bvd::traits
