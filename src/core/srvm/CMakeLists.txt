include(CommonOptions)

create_library(srvm
    SRCS
        SrvmRouter.cpp
    DIRS
        .
        ${Boost_INCLUDE_DIRS}
    PVT_LIBS
    	inc::messages
        inc::types
        inc::consts
        logger
        hml::hml
)
