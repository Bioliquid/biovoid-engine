#pragma once

#include "SrvmDefs.hpp"
#include "SrvmMessage.hpp"

#include <boost/intrusive/list.hpp>
#include <functional>

namespace bvd {

namespace srvm {

struct MsgReceiver : public boost::intrusive::list_base_hook<> {
    using Handler = std::function<void(MessageHeader const&)>;

    MsgId       msgId;
    ContainerId containerId;
    Handler     handler;
    bool        enabled{true};
};

} // namespace srvm

} // namespace bvd
