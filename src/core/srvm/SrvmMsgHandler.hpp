#pragma once

#include "SrvmDefs.hpp"
#include "SrvmRouter.hpp"
#include "SrvmMsgReceiver.hpp"
#include "Logger.hpp"

namespace bvd {

namespace srvm {

template<typename>
struct MsgHandler;

template<typename MsgInfoType>
struct MsgHandler {
    using MsgType = hml::at<hml::Int<1>, MsgInfoType>::type;
    static constexpr uint32_t msgId = hml::at<hml::Int<0>, MsgInfoType>::type::value;

public:
    MsgHandler(MsgHandler const&) = delete;
    MsgHandler& operator=(MsgHandler const&) = delete;

    template<typename T> requires HasReceive<T, MsgType>
    MsgHandler(ContainerId contId, T& comp, Router& msgRouter)
        : router(msgRouter)
    {
        receiver.msgId = msgId;
        receiver.containerId = contId;
        receiver.handler = [&comp](MessageHeader const& header) {
            comp.receive(*reinterpret_cast<MsgType const*>(&header));
        };
        
        constexpr bool subscribeOnInit = true;
        if constexpr (subscribeOnInit) {
            msgRouter.registerReceiver(receiver);
        }
    }

    bool tryReceive(MessageHeader const& msg) {
        if (msg.type == receiver.msgId) {
            receiver.handler(msg);
            return true;
        }
        return false;
    }

    void deregister() {
        router.deregisterReceiver(receiver);
    }

private:
    Router&     router;
    MsgReceiver receiver;
};

// TODO: both versions are equal but this one does not work on linux
template<MsgId msgId, typename MsgType>
struct MsgHandler<IndMessage<msgId, MsgType>> {
public:
    MsgHandler(MsgHandler const&) = delete;
    MsgHandler& operator=(MsgHandler const&) = delete;

    template<typename T> requires HasReceive<T, MsgType>
    MsgHandler(ContainerId contId, T& comp, Router& msgRouter)
        : router(msgRouter)
    {
        receiver.msgId = msgId;
        receiver.containerId = contId;
        receiver.handler = [&comp](MessageHeader const& header) {
            comp.receive(*reinterpret_cast<MsgType const*>(&header));
        };
        
        constexpr bool subscribeOnInit = true;
        if constexpr (subscribeOnInit) {
            msgRouter.registerReceiver(receiver);
        }
    }

    bool tryReceive(MessageHeader const& msg) {
        if (msg.type == receiver.msgId) {
            receiver.handler(msg);
            return true;
        }
        return false;
    }

    void disable() {
        receiver.enabled = false;
    }

    void deregister() {
        router.deregisterReceiver(receiver);
    }

private:
    Router&     router;
    MsgReceiver receiver;
};

} // namespace srvm

} // namespace bvd
