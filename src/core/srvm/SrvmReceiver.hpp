#pragma once

#include "SrvmMsgHandler.hpp"
#include "SrvmDefs.hpp"
#include "Logger.hpp"

#include <hml/algo/dot.hpp>
#include <hml/algo/filter.hpp>
#include <hml/algo/find.hpp>
#include <hml/algo/cons.hpp>
#include <hml/algo/map.hpp>
#include <hml/algo/length.hpp>
#include <hml/algo/at.hpp>
#include <hml/algo/equal.hpp>
#include <hml/algo/head.hpp>
#include <hml/algo/fromJust.hpp>
#include <hml/algo/range.hpp>

#include <ranges>
#include <initializer_list>
#include <any>

namespace bvd {

namespace srvm {

template<typename>
struct MsgHandlers;

template<>
struct MsgHandlers<hml::List<>> {
    template<typename T>
    explicit MsgHandlers(ContainerId, T&, Router&) {}

    void receive(MessageHeader const&) {}
    void disable() {}
    void free() {}
};

template<typename Message, typename... Messages>
struct MsgHandlers<hml::List<Message, Messages...>> : MsgHandler<Message>, MsgHandlers<hml::List<Messages...>> {
    template<typename T>
    explicit MsgHandlers(ContainerId contId, T& comp, Router& msgRouter)
        : MsgHandler<Message>(contId, comp, msgRouter)
        , MsgHandlers<hml::List<Messages...>>(contId, comp, msgRouter)
    {}

    void receive(MessageHeader const& msg) {
        if (MsgHandler<Message>::tryReceive(msg)) {
            return;
        }
        MsgHandlers<hml::List<Messages...>>::receive(msg);
    }

    void disable() {
        MsgHandler<Message>::disable();
        MsgHandlers<hml::List<Messages...>>::disable();
    }

    void free() {
        MsgHandler<Message>::deregister();
        MsgHandlers<hml::List<Messages...>>::free();
    }
};

class Receiver {
public:
    explicit Receiver(Router& inRouter) : router(inRouter) {}

    Receiver(Receiver const&) = delete;
    Receiver& operator=(Receiver const&) = delete;

    template<typename MessageList, typename T>
    void init(ContainerId contId, T& comp) {
        using FilteredMessages = hml::filter<HmlHasReceive<T, GetMessageType<>>, MessageList>::type;
        using Base = MsgHandlers<FilteredMessages>;

        static_assert(hml::length<FilteredMessages>::value != 0);

        containerId = contId;

        base = new Base(contId, comp, router);
        destroyCallback = [this]() {
            Base* handlers = reinterpret_cast<Base*>(base);
            handlers->free();
            delete handlers;
        };
        disableCallback = [this]() {
            Base* handlers = reinterpret_cast<Base*>(base);
            handlers->disable();
        };
    }

    void disable() {
        disableCallback();
    }

    ~Receiver() {
        if (destroyCallback) {
            destroyCallback();
        }
    }

    ContainerId getContainerId() const { return containerId; }

private:
    Router& router;
    ContainerId containerId;
    void* base;
    std::function<void()> disableCallback;
    std::function<void()> destroyCallback;
};

} // namespace srvm

} // namespace bvd
