#pragma once

#include "SrvmDefs.hpp"
#include "SrvmRouter.hpp"

#include <hml/algo/find.hpp>
#include <hml/algo/at.hpp>
#include <hml/algo/fromJust.hpp>
#include <hml/algo/equal.hpp>

namespace bvd {

namespace srvm {

class Sender {
public:
    Sender(Router& inRouter) : router(inRouter) {}

    Sender& operator=(Sender const&) = delete;

    // TODO: make concept for messages
    template<typename T>
    void send(T msg) {
        msg.header.size = sizeof(msg);
        msg.header.dst = invalidContainerId;
        router.submit(msg.header);
    }

    template<typename T>
    void send(T msg, ContainerId containerId) {
        msg.header.size = sizeof(msg);
        msg.header.dst = containerId;
        router.submit(msg.header);
    }

private:
    Router& router;
};

} // namespace srvm

} // namespace bvd
