#pragma once

#include "SrvmDefs.hpp"

namespace bvd {

namespace srvm {

struct MessageHeader {
    MsgId       type;
    uint16_t    size;
    // ContainerId src;
    ContainerId dst{invalidContainerId};
};

} // namespace srvm

} // namespace bvd
