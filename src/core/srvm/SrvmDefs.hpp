#pragma once

#include <hml/data/List.hpp>
#include <hml/data/Int.hpp>
#include <hml/data/Bool.hpp>
#include <hml/algo/at.hpp>

#include <concepts>

namespace bvd {

namespace srvm {

using MsgId       = uint16_t;
using ContainerId = uint16_t;

constexpr ContainerId invalidContainerId = 0;

template<typename T, typename... MsgsType>
concept HasReceive = requires(T & receiver, MsgsType&&... msg) {
    { receiver.receive(std::forward<MsgsType>(msg)...) } -> std::same_as<void>;
};

template<typename T, typename MsgType = hml::Placeholder>
struct HmlHasReceive : hml::Bool<HasReceive<T, MsgType>> {};

template<MsgId msgId, typename MessageType>
using IndMessage = hml::List<hml::Int<msgId>, MessageType>;

template<typename MessageListType = hml::Placeholder>
using GetMessageId = hml::at<hml::Int<0>, MessageListType>;

template<typename MessageListType = hml::Placeholder>
using GetMessageType = hml::at<hml::Int<1>, MessageListType>;

} // namespace srvm

} // namespace bvd
