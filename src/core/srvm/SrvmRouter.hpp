#pragma once

#include "SrvmMsgReceiver.hpp"

#include <array>

namespace bvd {

namespace srvm {

class Router {
    static constexpr uint32_t maxNumMessages = 128;
    using MsgReceivers = boost::intrusive::list<MsgReceiver>;
    using Receivers = std::array<MsgReceivers, maxNumMessages>;

public:
    Router() = default;
    Router(Router const&) = delete;
    Router& operator=(Router const&) = delete;

    void registerReceiver(MsgReceiver&);
    void deregisterReceiver(MsgReceiver&);

    void submit(MessageHeader const&);

private:
    void decodeAndReceive(MessageHeader const&);

    Receivers receivers;
    bool      activeReceive{false};
};

} // namespace srvm

} // namespace bvd
