#include "SrvmRouter.hpp"

#include "Exception.hpp"

namespace bvd::srvm {

void Router::registerReceiver(MsgReceiver& receiver) {
    MsgReceivers& msgReceivers = receivers[receiver.msgId];
    msgReceivers.push_back(receiver);
}

void Router::deregisterReceiver(MsgReceiver& receiver) {
    if (activeReceive) {
        BVD_THROW("Deregistration while receive is not supported");
    }

    MsgReceivers& msgReceivers = receivers[receiver.msgId];
#ifdef DEVELOPMENT_GUIDES_CHECK
    bool found = false;
    for (MsgReceiver& msgReceiver : msgReceivers) {
        if (&msgReceiver == &receiver) {
            found = true;
        }
    }
    if (not found) {
        BVD_THROW("No such receiver {}", uint64_t(&receiver));
    }
#endif
    msgReceivers.erase(msgReceivers.iterator_to(receiver));
}

void Router::submit(MessageHeader const& msg) {
    decodeAndReceive(msg);
}

void Router::decodeAndReceive(MessageHeader const& header) {
    MsgReceivers& msgReceivers = receivers[header.type];
    activeReceive = true;
    for (MsgReceiver& receiver : msgReceivers) {
        if (receiver.enabled and (receiver.containerId == invalidContainerId or receiver.containerId == header.dst)) {
            receiver.handler(header);
        }
    }
    activeReceive = false;
}

} // namespace bvd::srvm
