#pragma once

#include "PixelFormat.hpp"

#include <cstddef>

namespace bvd {

struct PixelFormatInfo {
    uint32_t    platformFormat;
    uint32_t    formatOrder;
    uint32_t    byteSize = 0;
    bool        supported = false;
};

struct PixelsFormatInfo {
    static constexpr size_t numFormats = static_cast<size_t>(PixelFormat::num);

    constexpr PixelsFormatInfo() {
        for (size_t i = static_cast<size_t>(PixelFormat::unknown); i < static_cast<size_t>(PixelFormat::num); ++i) {
            info[i].byteSize = getPixelFormatByteSize(static_cast<PixelFormat>(i));
        }
    }

    PixelFormatInfo& operator[](PixelFormat format) {
        return info[static_cast<size_t>(format)];
    }

    PixelFormatInfo const& operator[](PixelFormat format) const {
        return info[static_cast<size_t>(format)];
    }

    PixelFormatInfo& operator[](size_t format) {
        return info[format];
    }

    PixelFormatInfo const& operator[](uint64_t format) const {
        return info[format];
    }

private:
    constexpr uint32_t getPixelFormatByteSize(PixelFormat format) {
        switch (format) {
            case PixelFormat::b8g8r8a8:     return 4;
            case PixelFormat::r8g8b8a8:     return 4;
            case PixelFormat::r32g32b32a32: return 4 * sizeof(float);
            default:                        return 0;
        }
    }

public:
    PixelFormatInfo info[numFormats];
};

} // namespace bvd
