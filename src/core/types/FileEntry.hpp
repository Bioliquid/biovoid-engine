#pragma once

#include <filesystem>

namespace bvd {

using FileEntry = std::filesystem::directory_entry;

} // namespace bvd
