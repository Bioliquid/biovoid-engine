#pragma once

namespace bvd {

enum class DescriptorType {
      uniformBuffer
    , imageSampler
};

} // namespace bvd
