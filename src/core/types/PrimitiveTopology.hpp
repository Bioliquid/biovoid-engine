#pragma once

namespace bvd {

enum class PrimitiveTopology {
      points
    , lines
    , lineStrips
    , triangles
    , triangleStrips
    , triangleFans
};

} // namespace bvd
