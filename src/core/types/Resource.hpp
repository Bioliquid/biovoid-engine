#pragma once

#include "ThreadSafeCounter.hpp"

namespace bvd {

//class Resource {
//public:
//    virtual ~Resource() {}
//
//    void addRef() const {
//        numRefs.inc();
//    }
//
//    void release() const {
//        numRefs.dec();
//
//        if (numRefs.get() == 0) {
//            delete this;
//        }
//    }
//
//    uint32_t getRefCount() const {
//        return static_cast<uint32_t>(numRefs.get());
//    }
//
//private:
//    mutable ThreadSafeCounter numRefs;
//};

template<typename T>
class Ref;

class Resource {
    template<typename T>
    friend class Ref;

public:
    Resource() = default;
    virtual ~Resource() {}

    Resource(Resource const&) = delete;
    Resource& operator=(Resource const&) = delete;

private:
    void addRef() const {
        ++numRefs;
    }

    void decRef() const {
        --numRefs;
    }

    uint32_t getRefCount() const {
        return numRefs.load();
    }

    mutable std::atomic<uint32_t> numRefs;
};

} // namespace bvd
