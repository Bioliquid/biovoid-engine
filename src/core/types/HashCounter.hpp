#pragma once

#include <map>
#include <concepts>

namespace bvd {

template<typename T>
class HashCounter {
public:
    template<std::convertible_to<T> R>
    bool containts(R const& val) const {
        return hashes.contains(val);
    }

    template<std::convertible_to<T> R>
    void update(R const& val) {
        if (not containts(val)) {
            hashes[val] = lastHash;
            ++lastHash;
        }
    }

    template<std::convertible_to<T> R>
    uint32_t get(R const& val) const {
        return hashes.at(val);
    }

private:
    uint32_t              lastHash{0};
    std::map<T, uint32_t> hashes;
};

} // namespace bvd
