#pragma once

#include <atomic>
#include <cstdint>

namespace bvd {

class ThreadSafeCounter {
public:
    ThreadSafeCounter(ThreadSafeCounter const&) = delete;
    ThreadSafeCounter(ThreadSafeCounter&&) = delete;
    ThreadSafeCounter& operator=(ThreadSafeCounter const&) = delete;
    ThreadSafeCounter& operator=(ThreadSafeCounter&&) = delete;

    void inc() {
        ++counter;
    }
    void dec() {
        --counter;
    }

    int32_t get() { 
        return counter.load();
    }

private:
    std::atomic<int32_t> counter = 0;
};


} // namespace bvd
