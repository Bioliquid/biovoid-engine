#pragma once

namespace bvd {

enum class RhiApi {
      opengl
    , vulkan
};

} // namespace bvd
