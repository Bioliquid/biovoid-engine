#pragma once

namespace bvd {

enum class UniformType {
      vec1
    , vec2
    , vec3
    , vec4
    , ivec1
    , ivec2
    , ivec3
    , ivec4
    , uvec1
    , uvec2
    , uvec3
    , uvec4
    , bvec1
    , bvec2
    , bvec3
    , bvec4
    , mat3
    , mat4
    , num
};

} // namespace bvd
