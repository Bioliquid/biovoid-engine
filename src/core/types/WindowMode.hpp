#pragma once

namespace bvd {

enum class WindowMode {
      windowed
    , fullscreen
    , borderless
};

} // namespace bvd
