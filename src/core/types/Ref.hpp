#pragma once

#include <cstdint>

namespace bvd {

template<typename T>
class Ref {
    template<typename OtherType>
    friend class Ref;

public:
    Ref() : instance(nullptr) {}

    Ref(T* InInstance)
        : instance(InInstance)
    {
        incRef();
    }

    Ref(Ref<T> const& other)
        : instance(other.instance)
    {
        incRef();
    }

    template<typename R>
    Ref(Ref<R> const& other) {
        instance = (T*)other.instance;
        incRef();
    }

    Ref& operator=(Ref<T> const& other) {
        other.incRef();
        decRef();

        instance = other.instance;
        return *this;
    }

    template<typename R>
    Ref& operator=(Ref<R> const& other) {
        other.incRef();
        decRef();

        instance = other.instance;
        return *this;
    }

    ~Ref() {
        decRef();
    }

    T* get() { return  instance; }
    T const* get() const { return  instance; }

    template<typename R>
    R* as() { return static_cast<R*>(instance); }

    template<typename R>
    R const* as() const { return static_cast<R const*>(instance); }

    void reset(T* InInstance = nullptr) {
        decRef();
        instance = InInstance;
    }

    operator bool() const { return instance != nullptr; }

    T* operator->() { return instance; }
    T const* operator->() const { return instance; }

    T& operator*() { return *instance; }
    T const& operator*() const { return *instance; }

private:
    void incRef() const {
        if (instance) {
            instance->addRef();
        }
    }

    void decRef() const {
        if (instance) {
            instance->decRef();
            if (instance->getRefCount() == 0) {
                delete instance;
                instance = nullptr;
            }
        }
    }

    mutable T* instance;
};

} // namespace bvd
