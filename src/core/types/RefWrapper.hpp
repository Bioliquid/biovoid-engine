#pragma once

namespace bvd {

template<typename T>
class RefWrapper {
public:
    RefWrapper(T& r) : ref(r) {}
    RefWrapper(RefWrapper const& other) : ref(other.ref) {}

    T& get() { return ref; }

private:
    T& ref;
};

} // namespace bvd
