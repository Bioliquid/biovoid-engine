#pragma once

namespace bvd {

enum class EngineModule {
      none
    , opengl
    , vulkan
};

} // namespace bvd
