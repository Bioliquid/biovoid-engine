#pragma once

#include <memory>

namespace bvd {

template<typename T>
using Scope = std::unique_ptr<T>;

} // namespace bvd
