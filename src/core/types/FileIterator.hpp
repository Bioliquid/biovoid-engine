#pragma once

#include <filesystem>

namespace bvd {

using FileIterator = std::filesystem::directory_iterator;

} // namespace bvd
