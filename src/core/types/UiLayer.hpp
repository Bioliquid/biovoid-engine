#pragma once

namespace bvd {

enum class UiLayer {
      general
    , overlay
};

} // namespace bvd
