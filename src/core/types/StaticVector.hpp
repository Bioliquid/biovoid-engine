#pragma once

#include <boost/container/static_vector.hpp>

namespace bvd {

template<typename T, size_t size>
using static_vector = boost::container::static_vector<T, size>;

} // namespace bvd
