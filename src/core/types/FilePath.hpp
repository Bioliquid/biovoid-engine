#pragma once

#include <filesystem>

namespace bvd {

using FilePath = std::filesystem::path;

} // namespace bvd
