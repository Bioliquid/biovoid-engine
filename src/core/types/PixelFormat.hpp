#pragma once

namespace bvd {

enum class PixelFormat {
      unknown
    , b8g8r8a8
    , r8g8b8a8
    , r32g32b32a32
    , num
};

} // namespace bvd
