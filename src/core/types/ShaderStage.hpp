#pragma once

namespace bvd {

enum class ShaderStage {
      vertex
    , fragment
    , numStages
};

} // namespace bvd
