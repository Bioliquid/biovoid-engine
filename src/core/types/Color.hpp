#pragma once

#include <compare>

namespace bvd {

struct Color {
    using type = Color;

    constexpr Color() = default;

    constexpr Color(uint32_t color, float alpha)
        : r(float((color & 0xFF0000) >> 16) / 255.f)
        , g(float((color & 0xFF00) >> 8) / 255.f)
        , b(float(color & 0xFF) / 255.f)
        , a(alpha)
    {}

    constexpr Color(float colorR, float colorG, float colorB, float colorA)
        : r(colorR)
        , g(colorG)
        , b(colorB)
        , a(colorA)
    {}

    constexpr Color(float const (&color)[4])
        : r(color[0])
        , g(color[1])
        , b(color[2])
        , a(color[3])
    {}

    auto operator<=>(Color const&) const = default;

    float r;
    float g;
    float b;
    float a;
};

namespace colors {

inline constexpr Color red{0xf44336, 1.0f};
inline constexpr Color pink{0xe91e63, 1.0f};
inline constexpr Color indigo{0x3f51b5, 1.0f};
inline constexpr Color blue{0x2196f3, 1.0f};
inline constexpr Color lightBlue{0x03a9f4, 1.0f};
inline constexpr Color cyan{0x00bcd4, 1.0f};
inline constexpr Color teal{0x009688, 1.0f};
inline constexpr Color green{0x4caf50, 1.0f};
inline constexpr Color lightGreen{0x8bc34a, 1.0f};
inline constexpr Color lime{0xcddc39, 1.0f};
inline constexpr Color yellow{0xffeb3b, 1.0f};
inline constexpr Color amber{0xffc107, 1.0f};
inline constexpr Color orange{0xff9800, 1.0f};
inline constexpr Color deepOrange{0xff5722, 1.0f};
inline constexpr Color brown{0x795548, 1.0f};

inline constexpr Color black{0.0f,0.0f,0.0f,1.0f};
inline constexpr Color purple{0x9c27b0, 1.0f};
inline constexpr Color deepPurple{0x673ab7, 1.0f};
inline constexpr Color grey{0x242424, 1.0f};
inline constexpr Color midGrey{0x1f1f1f, 1.0f};
inline constexpr Color darkGrey{0x151515, 1.0f};
inline constexpr Color lightGrey{0x4f4f4f, 1.0f};
inline constexpr Color white{1.0f,1.0f,1.0f,1.0f};
inline constexpr Color transparent{1.0f, 1.0f, 1.0f, 0.0f};

} // namespace colors

} // namespace bvd
