#pragma once

#include "UniformType.hpp"

#include <cstdint>
#include <string_view>

namespace bvd {

struct UniformTypeInfo {
    std::string_view name;
    int32_t          platformFormat;
    int32_t          elementSize;
    int32_t          byteSize;
};

struct UniformTypesInfo {
    static constexpr size_t numUniformTypes = static_cast<size_t>(UniformType::num);

    UniformTypeInfo& operator[](UniformType type) {
        return info[static_cast<size_t>(type)];
    }

    UniformTypeInfo const& operator[](UniformType type) const {
        return info[static_cast<size_t>(type)];
    }

    UniformTypeInfo& operator[](size_t type) {
        return info[type];
    }

    UniformTypeInfo const& operator[](size_t type) const {
        return info[type];
    }

    UniformTypeInfo info[numUniformTypes];
};

} // namespace bvd
