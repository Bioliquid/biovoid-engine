#pragma once

namespace bvd {

enum class CursorType {
      arrow
    , beam
    , crosshair
    , hand
    , hResize
    , vResize
    , nwseResize
};

} // namespace bvd
