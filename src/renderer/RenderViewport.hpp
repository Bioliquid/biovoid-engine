#pragma once

#include "Rhi.hpp"
#include "SceneRenderer.hpp"
#include "Messages.hpp"
#include "Ref.hpp"
#include "RenderPassGraph.hpp"

namespace bvd {

namespace srvm {

class Sender;

} // namespace srvm

namespace renderer {

class RenderTarget {
public:
    RenderTarget() = default;
    virtual ~RenderTarget() {}

    Ref<rhi::Texture2D> const& getRenderTargetTexture() const {
        return renderTargetTexture;
    }

    Ref<rhi::Texture2D>& getRenderTargetTexture() {
        return renderTargetTexture;
    }

protected:
    int32_t width = 0;
    int32_t height = 0;
    Ref<rhi::Texture2D> renderTargetTexture;
};

class Viewport : public RenderTarget {
public:
    struct Init {
        srvm::Sender& msgSender;
    };

public:
    Viewport(Init const&);
    ~Viewport();

    Viewport(Viewport const&) = delete;
    Viewport& operator=(Viewport const&) = delete;

    void update(FrameTickInd const& ind) {
        sceneRenderer.update(ind);
    }

    void makeActive();
    void render();

    void setupRender(uint32_t, uint32_t);
    void process(WindowSurfaceInfoRsp const&);
    void process(WindowResizeInd const&);
    void process(WindowRefreshInd const&);
    void process(KeyPressInd const&);

    inline rdg::RenderGraph& getRenderGraph() { return renderGraph; }
    inline ViewInfo const& getView() const { return sceneRenderer.getView(); }

private:
    void beginRenderFrame();
    void endRenderFrame();

    void updateRenderTargetSurfaceRHIToCurrentBackBuffer();

private:
    srvm::Sender&            msgSender;
    Ref<rhi::Viewport>     viewport;
    rdg::RenderGraphResourceAllocator allocator;
    rdg::RenderGraph       renderGraph{allocator};
    // TODO: renderer is not part of viewport
    ForwardShadingRenderer sceneRenderer;
};

class ViewportClient {
public:
    void draw(Viewport&);
};

} // namespace renderer

} // namespace bvd
