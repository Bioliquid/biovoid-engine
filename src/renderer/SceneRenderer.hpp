#pragma once

#include "Material.hpp"
#include "Mesh.hpp"
#include "Ref.hpp"

#include "Entity.hpp"
#include "Scene.hpp"
#include "SceneView.hpp"

#include "Messages.hpp"

namespace bvd {

namespace rdg {
class RenderGraph;
} // namespace rdg

namespace renderer {

class Viewport;

class SceneRenderer {
public:
    SceneRenderer() = default;
    SceneRenderer(SceneRenderer const&) = delete;
    SceneRenderer(SceneRenderer&&) = delete;
    SceneRenderer& operator=(SceneRenderer const&) = delete;
    SceneRenderer& operator=(SceneRenderer&&) = delete;

    void init(Viewport&);

    void update(FrameTickInd const&) {
        // scene->onUpdate();
    }

    void render();

    void destroy();

    ViewInfo& getView() { return view; }
    ViewInfo const& getView() const { return view; }

private:
    // ecs::Scene*      scene;
    rdg::RenderGraph* renderGraph;
    SceneViewFamily  viewFamily;
    ViewInfo         view;
};

class ForwardShadingRenderer : public SceneRenderer {};

} // namespace renderer

} // namespace bvd
