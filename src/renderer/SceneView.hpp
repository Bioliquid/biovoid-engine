#pragma once

#include <glm/glm.hpp>

namespace bvd {

namespace ecs {

class Scene;

} // namespace ecs

namespace renderer {

class RenderTarget;

struct ViewMatrices {
    glm::mat4 projectionMatrix;
    glm::mat4 viewMatrix{1.0f};
};

struct SceneView {
    ViewMatrices viewMatrices;
};

struct ViewInfo : public SceneView {

};

struct SceneViewFamily {
    RenderTarget* renderTarget;
    SceneView*    view;
};

} // namespace renderer

} // namespace bvd
