#pragma once

#include <glm/glm.hpp>

namespace bvd {

namespace rhi::common {

class Buffer;
class Viewport;
class RenderPassInfo;

} // namespace rhi::common

namespace renderer {

class Viewport;
class Mesh;
class Material;

class RenderCommand {
public:
    static void beginDrawingViewport(rhi::common::Viewport* viewport);
    static void endDrawingViewport(rhi::common::Viewport* viewport);

    static void beginRenderPass(rhi::common::RenderPassInfo const& rpInfo);
    static void endRenderPass();

    inline static void beginFrame() {}
    inline static void endFrame() {}

    static void setStreamSource(uint32_t index, rhi::common::Buffer const& buffer, uint32_t offset = 0);
    static void setGraphicsPipeline(Material const&);

    static void drawPrimitive(uint32_t baseVertexIndex, uint32_t numPrimitives, uint32_t numInstances);
    static void drawIndexedPrimitive(rhi::common::Buffer const& indexBuffer, int32_t baseVertexIndex, uint32_t firstInstance, uint32_t startIndex, uint32_t numPrimitives, uint32_t numInstances);

    static void submitMesh(Mesh const&, Material const&);

    static void waitIdle();
};

} // namespace renderer

} // namespace bvd
