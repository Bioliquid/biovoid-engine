#include "RenderViewport.hpp"

#include "RenderCommand.hpp"
#include "KeyCodes.hpp"
#include "GlobalConfig.hpp"
#include "SrvmSender.hpp"
#include "ContainerIds.hpp"
#include "FileManager.hpp"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace bvd::renderer {

Viewport::Viewport(Init const& init)
    : msgSender(init.msgSender)
{}

Viewport::~Viewport() {
    sceneRenderer.destroy();

    viewport.reset();
}

void Viewport::updateRenderTargetSurfaceRHIToCurrentBackBuffer() {
    renderTargetTexture = rhi::getViewportBackBuffer(viewport.get());
}

void Viewport::beginRenderFrame() {
    RenderCommand::beginDrawingViewport(viewport.get());
    updateRenderTargetSurfaceRHIToCurrentBackBuffer();
}

void Viewport::endRenderFrame() {
    RenderCommand::endDrawingViewport(viewport.get());

    // opengl specific request
    // should be moved to OpenglViewport
    WindowActionReq req;
    req.action = WindowAction::swapBuffers;
    msgSender.send(req);
}

void Viewport::makeActive() {
    WindowActionReq req;
    req.action = WindowAction::makeActive;
    msgSender.send(req);
}

void Viewport::render() {
    beginRenderFrame();

    sceneRenderer.render();

    endRenderFrame();
}

void Viewport::setupRender(uint32_t inWidth, uint32_t inHeight) {
    width = inWidth;
    height = inHeight;
}

void Viewport::process(WindowSurfaceInfoRsp const& rsp) {
    width = rsp.width;
    height = rsp.height;

    viewport = rhi::createViewport(rsp.surfaceHandle, width, height, GlobalConfig::defaultBackBufferPixelFormat);
    updateRenderTargetSurfaceRHIToCurrentBackBuffer();

    sceneRenderer.getView().viewMatrices.projectionMatrix = glm::ortho(-0.5f * rsp.width, 0.5f * rsp.width, -0.5f * rsp.height, 0.5f * rsp.height);

    sceneRenderer.init(*this);
}

void Viewport::process(WindowResizeInd const& msg) {
    RenderCommand::waitIdle();

    BVD_LOG(LogGroup::render, BI, "Resizing viewport {}, {}", msg.width, msg.height);

    sceneRenderer.getView().viewMatrices.projectionMatrix = glm::ortho(-0.5f * msg.width, 0.5f * msg.width, -0.5f * msg.height, 0.5f * msg.height);

    viewport->resize(msg.width, msg.height);
    updateRenderTargetSurfaceRHIToCurrentBackBuffer();

    renderGraph.getFinalPass().updateOutput(*renderTargetTexture);
}

void Viewport::process(WindowRefreshInd const&) {
    if (width != 0 && height != 0) {
        render();
    }
}

void Viewport::process(KeyPressInd const& msg) {
    if (msg.keyCode == keys::escape) {
        WindowActionReq req;
        req.action = WindowAction::close;
        msgSender.send(req);
    }
}

} // namespace renderer
