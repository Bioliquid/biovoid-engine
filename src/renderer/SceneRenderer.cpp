#include "SceneRenderer.hpp"

#include "RenderViewport.hpp"
#include "RenderCommand.hpp"
#include "RenderPassGraph.hpp"

#include "FileManager.hpp"

namespace bvd::renderer {

void SceneRenderer::init(Viewport& renderTarget) {
    viewFamily.renderTarget = &renderTarget;
    viewFamily.view = &view;
    renderGraph = &renderTarget.getRenderGraph();
}

void SceneRenderer::render() {
    renderGraph->execute();
}

void SceneRenderer::destroy() {
    // scene->destroy();
}

} // namespace bvd::renderer
