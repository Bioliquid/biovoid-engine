#pragma once

#include "Rhi.hpp"
#include "Ref.hpp"
#include "HashCounter.hpp"

#include <vector>

namespace bvd {

namespace renderer {

class RenderCommand;

class Material {
    friend class RenderCommand;

public:
    void prepareShaders(rhi::GraphicsPipelineStateInit const&);

    void destroy();

    template<typename T>
    void setUniformBuffer(char const* name, uint32_t set, uint32_t binding, T const& data) {
        graphicsPipeline->setUniformBuffer(set, binding, reinterpret_cast<void const*>(&data), sizeof(T), graphicsPipeline->descriptorSetsInfo.descriptors[set].bindings[binding].variables[name].offset);
    }

    template<typename T>
    void cmdSetUniformBuffer(char const* name, uint32_t set, uint32_t binding, T const& data) {
        graphicsPipeline->cmdSetUniformBuffer(set, binding, reinterpret_cast<void const*>(&data), sizeof(T), graphicsPipeline->descriptorSetsInfo.descriptors[set].bindings[binding].variables[name].offset);
    }

    void setTexture(uint32_t, uint32_t, uint32_t, rhi::Texture const&);

    void processTextures(ui::Node const&, HashCounter<std::string>&);

    inline auto getInfo() { return graphicsPipeline->getInfo(); }

private:
    Ref<rhi::GraphicsPipeline>      graphicsPipeline;
    std::vector<Ref<rhi::Texture>>  textures;
};

} // namespace renderer

} // namespace bvd
