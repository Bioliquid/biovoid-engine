#include "RendererComponent.hpp"

#include "ContainerIds.hpp"
#include "RenderViewport.hpp"
#include "RenderPassGraph.hpp"
#include "RenderCommand.hpp"

namespace bvd::renderer {

Component::Component(srvm::Router& inRouter)
    : sender(inRouter)
    , receiver(inRouter)
{
    receiver.init<MessageMap>(containerId::any, *this);
}

Component::~Component() {
    destroy();
}

void Component::destroy() {
    RenderCommand::waitIdle();

    viewport.reset();
}

void Component::receive(RenderSetupReq const& msg) {
    viewport.reset(new Viewport(Viewport::Init{sender}));
    viewport->setupRender(msg.width, msg.height);

    WindowSurfaceInfoReq req;
    sender.send(req);
}

void Component::receive(WindowSurfaceInfoRsp const& msg) {
    viewport->process(msg);
}

void Component::receive(FrameTickInd const& ind) {
    if (not isMinimized) {
        viewport->makeActive();

        // scene.onUpdate();
        viewport->update(ind);
        viewport->render();
    }
}

void Component::receive(WindowMinimizeInd const&) {
    isMinimized = true;
}

void Component::receive(WindowResizeInd const& msg) {
    isMinimized = false;
    viewport->process(msg);
}

void Component::receive(WindowRefreshInd const& msg) {
    viewport->process(msg);
}

void Component::receive(KeyPressInd const& msg) {
    viewport->process(msg);
}

} // namespace bvd::renderer
