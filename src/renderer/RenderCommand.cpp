#include "RenderCommand.hpp"

#include "RenderViewport.hpp"
#include "Material.hpp"
#include "Mesh.hpp"
#include "Rhi.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <string_view>

namespace bvd::renderer {

void RenderCommand::beginDrawingViewport(rhi::Viewport* viewport) {
    rhi::gDynamicRhi->immediateContext->beginDrawingViewport(viewport);
}

void RenderCommand::endDrawingViewport(rhi::Viewport* viewport) {
    rhi::gDynamicRhi->immediateContext->endDrawingViewport(viewport);
    rhi::gDynamicRhi->advanceFrameForViewportBackBuffer(viewport);
}

void RenderCommand::setStreamSource(uint32_t index, rhi::Buffer const& buffer, uint32_t offset) {
    rhi::gDynamicRhi->immediateContext->setStreamSource(index, buffer, offset);
}

void RenderCommand::setGraphicsPipeline(Material const& material) {
    rhi::gDynamicRhi->immediateContext->setGraphicsPipeline(material.graphicsPipeline.get());
}

void RenderCommand::drawPrimitive(uint32_t baseVertexIndex, uint32_t numPrimitives, uint32_t numInstances) {
    rhi::gDynamicRhi->immediateContext->drawPrimitive(baseVertexIndex, numPrimitives, numInstances);
}

void RenderCommand::drawIndexedPrimitive(rhi::Buffer const& indexBuffer, int32_t baseVertexIndex, uint32_t firstInstance, uint32_t startIndex, uint32_t numPrimitives, uint32_t numInstances) {
    rhi::gDynamicRhi->immediateContext->drawIndexedPrimitive(indexBuffer, baseVertexIndex, firstInstance, startIndex, numPrimitives, numInstances);
}

void RenderCommand::beginRenderPass(rhi::RenderPassInfo const& rpInfo) {
    rhi::gDynamicRhi->immediateContext->beginRenderPass(rpInfo);
}

void RenderCommand::endRenderPass() {
    rhi::gDynamicRhi->immediateContext->endRenderPass();
}

void RenderCommand::submitMesh(Mesh const& mesh, Material const& material) {
    RenderCommand::setStreamSource(0, *mesh.vertexBuffer);
    RenderCommand::setGraphicsPipeline(material);

    for (Submesh const& submesh : mesh.submeshes) {
        // TODO: should have primitive count
        RenderCommand::drawIndexedPrimitive(*mesh.indexBuffer, 0, 0, 0, submesh.indexCount / 3, 1);
    }
}

void RenderCommand::waitIdle() {
    rhi::gDynamicRhi->immediateContext->waitIdle();
}

} // namespace bvd::renderer
