#include "Material.hpp"

#include "DynamicRhi.hpp"
#include "Logger.hpp"
#include "UiNode.hpp"
#include "FileManager.hpp"
#include "RenderCommand.hpp"

namespace bvd::renderer {

void Material::prepareShaders(rhi::GraphicsPipelineStateInit const& init) {
    graphicsPipeline = rhi::createGraphicsPipeline();

    graphicsPipeline->load(init);
}

void Material::destroy() {
    graphicsPipeline->destroy();
    graphicsPipeline.reset();
}

void Material::setTexture(uint32_t set, uint32_t binding, uint32_t arrayElement, rhi::Texture const& texture) {
    uint32_t textureArraySize = graphicsPipeline->getInfo().descriptors.at(set).bindings.at(binding).count;
    if (arrayElement >= textureArraySize) {
        BVD_LOG(LogGroup::gen, BE, "Setting texture with incorrect set={} binding={} id={} size={}", set, binding, arrayElement, textureArraySize);
    }
    graphicsPipeline->setTexture(set, binding, arrayElement, texture);
}

void Material::processTextures(ui::Node const& node, HashCounter<std::string>& textureIds) {
    if (node.getTexture() != "none" && not textureIds.containts(node.getTexture())) {
        // TODO: whenever new texture appears in ui I have to wait because of vkUpdateDescriptorSets
        // even if this command is unavoidable this is not the right place
        RenderCommand::waitIdle();

        textureIds.update(node.getTexture());
        setTexture(0, 1, textureIds.get(node.getTexture()), *fs::FileManager::getTexture(node.getTexture()));
    }

    for (ui::Node const& child : node.getChildren()) {
        processTextures(child, textureIds);
    }
}

} // namespace bvd::renderer
