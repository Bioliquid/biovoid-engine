#include "Mesh.hpp"

#include "RenderCommand.hpp"
#include "Logger.hpp"
#include "Exception.hpp"

#include "UiNode.hpp"
#include "Font.hpp"
#include "FileManager.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>

namespace bvd::renderer {

void Mesh::destroy() {
    vertexBuffer->destroy();
    vertexBuffer.reset();
    indexBuffer->destroy();
    indexBuffer.reset();
}

void Mesh::resetSubmeshes() {
    Submesh& submesh = submeshes.back();
    submesh.vertexCount = 0;
    submesh.indexCount = 0;
}

void Mesh3D::load(char const* path, Mesh& mesh) {
    BVD_LOG(LogGroup::render, BD, "Loading mesh (path={})", path);

    Assimp::Importer importer;

    aiScene const* scene = importer.ReadFile(path, meshImportFlags);

    if (!scene || !scene->HasMeshes()) {
        BVD_THROW("Failed to load mesh (path={})", path);
    }

    if (scene->mAnimations != nullptr) {
        BVD_LOG(LogGroup::render, BE, "Engine does not support animations (path={})", path);
    }

    uint32_t vertexCount = 0;
    uint32_t indexCount = 0;

    mesh.submeshes.reserve(scene->mNumMeshes);
    for (uint32_t meshIndex = 0; meshIndex < scene->mNumMeshes; ++meshIndex) {
        aiMesh* inmesh = scene->mMeshes[meshIndex];

        Submesh& submesh = mesh.submeshes.emplace_back();
        submesh.name = inmesh->mName.C_Str();
        submesh.vertexStart = vertexCount;
        submesh.indexStart = indexCount;
        submesh.vertexCount = inmesh->mNumVertices;
        submesh.indexCount = inmesh->mNumFaces * 3; // TODO: remove hardcode
        submesh.materialIndex = inmesh->mMaterialIndex;

        vertexCount += submesh.vertexCount;
        indexCount += submesh.indexCount;

        for (size_t vertexIndex = 0; vertexIndex < inmesh->mNumVertices; ++vertexIndex) {
            Vertex vertex;
            vertex.position = {inmesh->mVertices[vertexIndex].x, inmesh->mVertices[vertexIndex].y, inmesh->mVertices[vertexIndex].z};
            vertex.normal = {inmesh->mNormals[vertexIndex].x, inmesh->mNormals[vertexIndex].y, inmesh->mNormals[vertexIndex].z};

            if (inmesh->HasTangentsAndBitangents()) {
                vertex.tangent = {inmesh->mTangents[vertexIndex].x, inmesh->mTangents[vertexIndex].y, inmesh->mTangents[vertexIndex].z};
                vertex.binormal = {inmesh->mBitangents[vertexIndex].x, inmesh->mBitangents[vertexIndex].y, inmesh->mBitangents[vertexIndex].z};
            }

            if (inmesh->HasTextureCoords(0))
                vertex.texcoord = {inmesh->mTextureCoords[0][vertexIndex].x, inmesh->mTextureCoords[0][vertexIndex].y};

            vertices.push_back(vertex);
        }

        for (size_t faceIndex = 0; faceIndex < inmesh->mNumFaces; ++faceIndex) {
            if (inmesh->mFaces[faceIndex].mNumIndices != 3) {
                BVD_LOG(LogGroup::render, BE, "Only triangle meshes are supported");
            }

            for (size_t indexIndex = 0; indexIndex < inmesh->mFaces[faceIndex].mNumIndices; ++indexIndex) {
                indices.push_back(inmesh->mFaces[faceIndex].mIndices[indexIndex]);
            }
        }
    }

    // creation of vertex and index buffers
}

void Mesh2D::fromQuad(glm::vec2 const& pos, glm::vec2 const& size, Mesh& mesh) {
    const Vertex2D customVertices[] = {
        {{pos.x, pos.y}, {0.0f, 0.0f}},
        {{pos.x, pos.y + size.y}, {0.0f, 1.0f}},
        {{pos.x + size.x, pos.y + size.y}, {1.0f, 1.0f}},
        {{pos.x + size.x, pos.y}, {1.0f, 0.0f}},
    };

    const uint32_t customIndices[] = {
        0, 1, 2, 2, 3, 0
    };

    // TODO: make something like Rhi::createVertexBuffer
    mesh.vertexBuffer = rhi::createVertexBuffer(sizeof(customVertices));
    mesh.vertexBuffer->copy(customVertices, sizeof(customVertices));

    mesh.indexBuffer = rhi::createIndexBuffer(sizeof(customIndices));
    mesh.indexBuffer->copy(customIndices, sizeof(customIndices));

    Submesh& submesh = mesh.submeshes.emplace_back();
    submesh.name = "main";
    submesh.vertexStart = 0;
    submesh.vertexCount = 4;
    submesh.indexStart = 0;
    submesh.indexCount = 6;
}

void MeshUi::fromUi(uint8_t* vBuffer, uint32_t vCount, uint32_t* iBuffer, uint32_t iCount, Mesh& mesh) {
    uint32_t vSize = vCount * sizeof(UiVertex);
    uint32_t iSize = iCount * sizeof(uint32_t);

    vertices.resize(vCount);

    mesh.vertexBuffer = rhi::createVertexBuffer(vSize);
    if (vBuffer != nullptr) {
        mesh.vertexBuffer->copy(vBuffer, vSize);
    }

    mesh.indexBuffer = rhi::createIndexBuffer(iSize);
    if (iBuffer != nullptr) {
        mesh.indexBuffer->copy(iBuffer, iSize);
    }

    Submesh& submesh = mesh.submeshes.emplace_back();
    submesh.name = "main";
    submesh.vertexStart = 0;
    submesh.vertexCount = 0;
    submesh.indexStart = 0;
    submesh.indexCount = 0;
}

void MeshUi::fromUi(ui::Node const& node, Mesh& mesh, HashCounter<std::string> const& textureIds) {
    glm::vec2 pos(0.0f, 0.0f);
    glm::vec2 maxNegPos(0.0f, 0.0f);

    Submesh& submesh = mesh.submeshes.back();
    fromUi(node, pos, maxNegPos, submesh, textureIds);
    submesh.indexCount = submesh.vertexCount * 3 / 2;
}

void MeshUi::fromUi(ui::Node const& node, glm::vec2 pos, glm::vec2 maxNegPos, Submesh& submesh, HashCounter<std::string> const& textureIds) {
    if (not node.isEnabled()) {
        return;
    }

    if (node.shouldRender) {
        glm::vec2 const& position = node.getPos();
        glm::vec2 const& size = node.getSize();

        ui::NodeStyle const& style = node.getStyle();
        Color const& color = style.getColor();

        if (node.isText) {
            float maxWidth;
            // TODO: should get size of parent
            if (node.getSize().x == 0.0f) {
                // TODO: magic constant
                maxWidth = 10000.0f;
            } else {
                maxWidth = node.getSize().x / node.getFontSize();
            }
            auto [screenWidth, screenHeight] = Input::getScreenSize();

            TextParams params{
                  .text = node.getText().data()
                , .font = node.textureName
                , .scale = node.getFontSize()
                , .maxWidth = maxWidth
                , .maxHeight = screenHeight
                , .startIdx = node.subtextStartIdx
                , .startY = node.subtextY
            };

            fromText(glm::vec3{pos.x + position.x, -pos.y + position.y, 0.0f}, maxNegPos, color, submesh, params, textureIds);
        } else {
            glm::mat4 transform = glm::translate(glm::mat4(1.0f), glm::vec3{pos.x + position.x + size.x / 2.0f, pos.y - position.y - size.y / 2.0f, 0.0f});
            transform = glm::rotate(transform, glm::radians(float(node.rotateAngle)), glm::vec3{0.0f, 0.0f, 1.0f});
            transform = glm::scale(transform, glm::vec3(size.x, size.y, 0.0f));

            uint32_t textureId = textureIds.get(node.textureName);

            Color borderColor = node.getBorderColor();
            float borderWidth = node.getBorderWidth();

            vertices[submesh.vertexCount + 0].position = transform * glm::vec4(-0.5f, -0.5f, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 0].texCoord = {0.0f, 0.0f};
            vertices[submesh.vertexCount + 0].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 0].borderColor = glm::vec4{ borderColor.r, borderColor.g, borderColor.b, borderColor.a };
            vertices[submesh.vertexCount + 0].center = pos + glm::vec2(position.x, -position.y);
            vertices[submesh.vertexCount + 0].size = size;
            vertices[submesh.vertexCount + 0].borderRadius = node.getBorderRadius().getVec();
            vertices[submesh.vertexCount + 0].localPosition = maxNegPos + position + size;
            vertices[submesh.vertexCount + 0].borderWidth = borderWidth;
            vertices[submesh.vertexCount + 0].textureId = textureId;
            vertices[submesh.vertexCount + 0].isText = 0;

            vertices[submesh.vertexCount + 1].position = transform * glm::vec4(-0.5f, 0.5f, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 1].texCoord = {0.0f, 1.0f};
            vertices[submesh.vertexCount + 1].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 1].borderColor = glm::vec4{ borderColor.r, borderColor.g, borderColor.b, borderColor.a };
            vertices[submesh.vertexCount + 1].center = pos + glm::vec2(position.x, -position.y);
            vertices[submesh.vertexCount + 1].size = size;
            vertices[submesh.vertexCount + 1].borderRadius = node.getBorderRadius().getVec();
            vertices[submesh.vertexCount + 1].localPosition = maxNegPos + position + glm::vec2{size.x, 0.0f};
            vertices[submesh.vertexCount + 1].borderWidth = borderWidth;
            vertices[submesh.vertexCount + 1].textureId = textureId;
            vertices[submesh.vertexCount + 1].isText = 0;

            vertices[submesh.vertexCount + 2].position = transform * glm::vec4(0.5f, 0.5f, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 2].texCoord = {1.0f, 1.0f};
            vertices[submesh.vertexCount + 2].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 2].borderColor = glm::vec4{ borderColor.r, borderColor.g, borderColor.b, borderColor.a };
            vertices[submesh.vertexCount + 2].center = pos + glm::vec2(position.x, -position.y);
            vertices[submesh.vertexCount + 2].size = size;
            vertices[submesh.vertexCount + 2].borderRadius = node.getBorderRadius().getVec();
            vertices[submesh.vertexCount + 2].localPosition = maxNegPos + position;
            vertices[submesh.vertexCount + 2].borderWidth = borderWidth;
            vertices[submesh.vertexCount + 2].textureId = textureId;
            vertices[submesh.vertexCount + 2].isText = 0;

            vertices[submesh.vertexCount + 3].position = transform * glm::vec4(0.5f, -0.5f, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 3].texCoord = {1.0f, 0.0f};
            vertices[submesh.vertexCount + 3].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 3].borderColor = glm::vec4{ borderColor.r, borderColor.g, borderColor.b, borderColor.a };
            vertices[submesh.vertexCount + 3].center = pos + glm::vec2(position.x, -position.y);
            vertices[submesh.vertexCount + 3].size = size;
            vertices[submesh.vertexCount + 3].borderRadius = node.getBorderRadius().getVec();
            vertices[submesh.vertexCount + 3].localPosition = maxNegPos + position + glm::vec2{0.0f, size.y};
            vertices[submesh.vertexCount + 3].borderWidth = borderWidth;
            vertices[submesh.vertexCount + 3].textureId = textureId;
            vertices[submesh.vertexCount + 3].isText = 0;

            submesh.vertexCount += 4;
        }
    }

    for (ui::Node const& child : node.getChildren()) {
        glm::vec2 const& rootPos = node.getPos();
        glm::vec2 newMaxNegPos = maxNegPos + (node.id != "Root" && node.id != "OverlayRoot" ? rootPos : glm::vec2{0.0f, 0.0f});
        fromUi(child, pos + glm::vec2{rootPos.x, -rootPos.y}, glm::vec2{std::min(newMaxNegPos.x, 0.0f), std::min(newMaxNegPos.y, 0.0f)}, submesh, textureIds);
    }
}

static bool NextLine(int index, const std::vector<int>& lines) {
    for (int line : lines) {
        if (line == index)
            return true;
    }
    return false;
}

void MeshUi::fromText(glm::vec3 const& pos, glm::vec2 maxNegPos, Color color, Submesh& submesh, TextParams const& params, HashCounter<std::string> const& textureIds) {
    Ref<Font> font = fs::FileManager::getFont(params.font);
    Ref<rhi::Texture2D> fontAtlas = font->getFontAtlas();

    uint32_t textureId = textureIds.get(params.font);

    auto& fontGeometry = font->getMsdfData()->fontGeometry;
    const auto& metrics = fontGeometry.getMetrics();

    std::string_view view(params.text);

    float lineHeightOffset = 0.0f;
    float kerningOffset = 0.01f;
    double fsScale = 1.0 / (metrics.ascenderY - metrics.descenderY);

    const float lineHeightTemp = float(fsScale * metrics.lineHeight + lineHeightOffset);

    std::vector<int> nextLines;
    {
        double x = 0.0;
        int lastSpace = -1;
        float y = params.startY / params.scale;
        for (uint32_t i = params.startIdx; i < view.size() && (-y) < (params.maxHeight - pos.y) / params.scale; ++i) {
            char character = view[i];
            if (character == '\n') {
                x = 0;
                y -= lineHeightTemp;
                lastSpace = -1;
                continue;
            }

            auto glyph = fontGeometry.getGlyph(character);
            if (!glyph)
                glyph = fontGeometry.getGlyph('?');
            if (!glyph)
                continue;

            if (character != ' ') {
                // Calc geo
                double pl, pb, pr, pt;
                glyph->getQuadPlaneBounds(pl, pb, pr, pt);
                glm::vec2 quadMax((float)pr, (float)pt);

                quadMax *= fsScale;
                quadMax += glm::vec2(x, y);

                if (quadMax.x > params.maxWidth) {
                    if (lastSpace != -1) {
                        i = lastSpace;
                        nextLines.emplace_back(lastSpace);
                        lastSpace = -1;
                    } else {
                        nextLines.emplace_back(i);
                        i = i - 1;
                    }
                    x = 0;
                    y -= lineHeightTemp;
                    continue;
                }
            } else {
                lastSpace = i;
            }

            if (i != view.size() - 1) {
                double advance = glyph->getAdvance();
                fontGeometry.getAdvance(advance, character, view[i + 1]);
                x += fsScale * advance + kerningOffset;
            }
        }
    }

    float ptOfFirstChar = 0.0f;
    {
        float y = 0.0f;
        for (uint32_t i = 0; i < view.size(); ++i) {
            char character = view[i];
            if (character == '\n' || NextLine(i, nextLines)) {
                break;
            }

            auto glyph = fontGeometry.getGlyph(character);
            if (glyph) {
                double pl, pb, pr, pt;
                glyph->getQuadPlaneBounds(pl, pb, pr, pt);

                pt *= fsScale;
                pt += y;

                ptOfFirstChar = std::max(ptOfFirstChar, (float)pt);
            }
        }
    }

    glm::mat4 transform = glm::translate(glm::mat4(1.0f), glm::vec3{pos.x, -pos.y - ptOfFirstChar * params.scale, pos.z});
    transform = glm::scale(transform, glm::vec3{params.scale, params.scale, 0.0f});

    {
        float x = 0.0f;
        float y = params.startY / params.scale;
        for (uint32_t i = params.startIdx; i < view.size() && (-y) < (params.maxHeight - pos.y) / params.scale; ++i) {
            char character = view[i];
            if (character == '\n' || NextLine(i, nextLines)) {
                x = 0;
                y -= lineHeightTemp;
                if (character == '\n') {
                    continue;
                }
            }

            auto glyph = fontGeometry.getGlyph(character);
            if (!glyph) {
                glyph = fontGeometry.getGlyph('?');
            }

            double l, b, r, t;
            glyph->getQuadAtlasBounds(l, b, r, t);

            double pl, pb, pr, pt;
            glyph->getQuadPlaneBounds(pl, pb, pr, pt);

            pl *= fsScale, pb *= fsScale, pr *= fsScale, pt *= fsScale;
            pl += x, pb += y, pr += x, pt += y;

            // ptOfFirstChar = std::max(ptOfFirstChar, (float)pt);

            double texelWidth = 1. / fontAtlas->getWidth();
            double texelHeight = 1. / fontAtlas->getHeight();
            l *= texelWidth, b *= texelHeight, r *= texelWidth, t *= texelHeight;

            glm::vec2 position{pl * params.scale, -pb * params.scale};
            glm::vec2 size{(pr - pl) * params.scale, float(fsScale * metrics.lineHeight + lineHeightOffset) * params.scale};


            vertices[submesh.vertexCount + 0].position = transform * glm::vec4(pl, pb, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 0].texCoord = {l, b};
            vertices[submesh.vertexCount + 0].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 0].localPosition = glm::vec2{0.0f, 0.0f};
            vertices[submesh.vertexCount + 0].localPosition = maxNegPos + position + size;
            vertices[submesh.vertexCount + 0].textureId = textureId;
            vertices[submesh.vertexCount + 0].isText = 1;

            vertices[submesh.vertexCount + 1].position = transform * glm::vec4(pl, pt, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 1].texCoord = {l, t};
            vertices[submesh.vertexCount + 1].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 1].localPosition = glm::vec2{0.0f, 0.0f};
            vertices[submesh.vertexCount + 1].localPosition = maxNegPos + position + glm::vec2{size.x, 0.0f};
            vertices[submesh.vertexCount + 1].textureId = textureId;
            vertices[submesh.vertexCount + 1].isText = 1;

            vertices[submesh.vertexCount + 2].position = transform * glm::vec4(pr, pt, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 2].texCoord = {r, t};
            vertices[submesh.vertexCount + 2].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 2].localPosition = glm::vec2{0.0f, 0.0f};
            vertices[submesh.vertexCount + 2].localPosition = maxNegPos + position;
            vertices[submesh.vertexCount + 2].textureId = textureId;
            vertices[submesh.vertexCount + 2].isText = 1;

            vertices[submesh.vertexCount + 3].position = transform * glm::vec4(pr, pb, 0.0f, 1.0f);
            vertices[submesh.vertexCount + 3].texCoord = {r, b};
            vertices[submesh.vertexCount + 3].color = glm::vec4{color.r, color.g, color.b, color.a};
            vertices[submesh.vertexCount + 3].localPosition = glm::vec2{0.0f, 0.0f};
            vertices[submesh.vertexCount + 3].localPosition = maxNegPos + position + glm::vec2{0.0f, size.y};
            vertices[submesh.vertexCount + 3].textureId = textureId;
            vertices[submesh.vertexCount + 3].isText = 1;

            submesh.vertexCount += 4;

            if (i != view.size() - 1) {
                double advance = glyph->getAdvance();
                fontGeometry.getAdvance(advance, character, view[i + 1]);
                x += float(fsScale * advance + kerningOffset);
            }
        }
    }
}

} // namespace bvd::renderer
