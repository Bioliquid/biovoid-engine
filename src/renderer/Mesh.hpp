#pragma once

#include "Rhi.hpp"
#include "Ref.hpp"
#include "HashCounter.hpp"

#include <glm/glm.hpp>
#include <assimp/postprocess.h>

#include <vector>
#include <cstdint>

namespace bvd {

namespace ui {

class Node;

} // namespace ui

namespace renderer {

class RenderCommand;

struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 tangent;
    glm::vec3 binormal;
    glm::vec2 texcoord;
};

struct Vertex2D {
    glm::vec2 pos;
    glm::vec2 texCoord;
};

struct UiVertex {
    glm::vec2 position;
    glm::vec2 texCoord;
    glm::vec4 color;
    glm::vec4 borderColor;
    glm::vec2 center;
    glm::vec2 size;
    glm::vec4 borderRadius;
    glm::vec2 localPosition;
    float     borderWidth;
    uint32_t  textureId;
    uint32_t  isText;
};

struct Submesh {
    char const* name;

    uint32_t vertexStart = 0;
    uint32_t vertexCount;
    uint32_t indexStart = 0;
    uint32_t indexCount;

    uint32_t materialIndex;
};

class Mesh {
    friend class RenderCommand;

public:
    void destroy();

    void resetSubmeshes();

public:
    std::vector<Submesh> submeshes;
    Ref<rhi::Buffer>     vertexBuffer;
    Ref<rhi::Buffer>     indexBuffer;
};

class Mesh3D {
    static constexpr uint32_t meshImportFlags =
        aiProcess_CalcTangentSpace |        // Create binormals/tangents just in case
        aiProcess_Triangulate |             // Make sure we're triangles
        aiProcess_SortByPType |             // Split meshes by primitive type
        aiProcess_GenNormals |              // Make sure we have legit normals
        aiProcess_GenUVCoords |             // Convert UVs if required 
        aiProcess_OptimizeMeshes |          // Batch draws where possible
        aiProcess_JoinIdenticalVertices |
        aiProcess_ValidateDataStructure;    // Validation

public:
    void load(char const*, Mesh&);

private:
    std::vector<Vertex>   vertices;
    std::vector<uint32_t> indices;
};

class Mesh2D {
public:
    void fromQuad(glm::vec2 const&, glm::vec2 const&, Mesh&);

public:
    std::vector<Vertex2D> vertices;
    std::vector<uint32_t> indices;
};

class MeshUi {
    struct TextParams {
        char const* text;
        std::string const& font;
        float       scale;
        float       maxWidth;
        float       maxHeight;
        uint32_t    startIdx;
        float       startY;
    };

public:
    void fromUi(uint8_t*, uint32_t, uint32_t*, uint32_t, Mesh&);
    void fromUi(ui::Node const&, Mesh&, HashCounter<std::string> const&);

private:
    void fromUi(ui::Node const&, glm::vec2, glm::vec2, Submesh&, HashCounter<std::string> const&);
    void fromText(glm::vec3 const&, glm::vec2, Color, Submesh&, TextParams const&, HashCounter<std::string> const&);

public:
    std::vector<UiVertex> vertices;
    std::vector<uint32_t> indices;
};

} // namespace renderer

} // namespace bvd
