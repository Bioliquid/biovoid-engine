#pragma once

#include "Messages.hpp"
#include "WindowMode.hpp"
#include "Scope.hpp"

namespace bvd {

namespace renderer {

class Viewport;
class DummyViewport;
class RenderPassGraph;

class Component
{
public:
    Component(srvm::Router&);
    ~Component();

    void receive(RenderSetupReq const&);
    void receive(WindowSurfaceInfoRsp const&);
    void receive(FrameTickInd const&);
    void receive(WindowMinimizeInd const&);
    void receive(WindowResizeInd const&);
    void receive(WindowRefreshInd const&);
    void receive(KeyPressInd const&);

    inline Viewport& getViewport() { return *viewport; }

private:
    void destroy();

    srvm::Sender      sender;
    srvm::Receiver    receiver;
    Scope<Viewport> viewport;

    bool isMinimized = false;
};

} // namespace renderer

} // namespace bvd
