#pragma once

#include "Api.hpp"
#include "RtcDelta.hpp"
#include "RendererComponent.hpp"
#include "WsiComponent.hpp"
#include "Ui.hpp"
#include "Scope.hpp"

#include "Rhi.hpp"
#include "Messages.hpp"
#include "WsiComponent.hpp"

#include <vector>
#include <string>
#include <memory>

namespace bvd {

struct IEngineClient;

struct AppWindow {
    int32_t     width;
    int32_t     height;
    WindowMode  mode;
    std::string title;
};

struct AppInit {
    std::vector<AppWindow> windows;
};

class App {
public:
    App(std::string const&);
    ~App();

    void init(IEngineClient&, AppInit const&);
    void run();
    bool shouldClose() const;

    srvm::Router& getRouter() { return router; }

private:
    std::string appRootPath;
    rtc::Delta  timeDelta;

    Scope<renderer::Component> rendererComponent;
    Scope<wsi::Component>      wsiComponent;
    Scope<ui::UiComponent>     uiComponent;

    Rhi rhi;
    srvm::Router router;
    srvm::Sender sender;
};

} // namespace bvd
