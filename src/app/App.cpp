#include "App.hpp"

#include "FileManager.hpp"
#include "GlobalConfig.hpp"
#include "WindowManager.hpp"
#include "Messages.hpp"
#include "Logger.hpp"
#include "RtcTime.hpp"
#include "RenderCommand.hpp"
#include "Timer.hpp"
#include "IEngineClient.hpp"

#include <boost/assert.hpp>

namespace boost
{

void assertion_failed(char const* expr, char const*, char const* file, long line) {
    BVD_LOG(bvd::LogGroup::gen, bvd::BE, "{}, file {}, line {}", expr, file, line);
}

} // namespace boost

namespace bvd {

App::App(std::string const& path)
    : appRootPath(path)
    , sender(router)
{
    fs::FileManager::initialize(FilePath{appRootPath}.append("assets"));

    // Loading all configs needed for initialization
    GlobalConfig::load("config.toml");

    // Selecting graphics API
    // Important note: window creation require information about graphics API
    rhi.selectGraphicsAPI();
}

void App::init(IEngineClient& client, AppInit const& init) {
    // Initializing wsi
    windowManager.init(rhi.currentApi);

    for (AppWindow const& window : init.windows) {
        wsiComponent.reset(new wsi::Component(router));

        WsiSetupReq req{
              .width = window.width
            , .height = window.height
            , .mode = window.mode
            , .title = window.title
        };

        wsiComponent->receive(req);
    }

    // Initializing rhi
    rhi.init();

    fs::FileManager::initializeDefaultResources();

    for (AppWindow const& window : init.windows) {
        rendererComponent.reset(new renderer::Component(router));

        RenderSetupReq req {
              .width = window.width
            , .height = window.height
        };

        rendererComponent->receive(req);
    }

    uiComponent.reset(new ui::UiComponent(router));

    client.initialize(*rendererComponent.get(), *uiComponent.get());
}

App::~App() {
    renderer::RenderCommand::waitIdle();

    fs::FileManager::destroyDefaultResources();

    fs::FileManager::destroy();

    uiComponent.reset();
    wsiComponent.reset();
    rendererComponent.reset();

    rhi.destroy();

    windowManager.destroy();
}

void App::run() {
    while (!shouldClose()) {
        FrameTickInd ind;
        ind.ms = timeDelta.getMilliseconds();
        rtc::timerPool.processTick((uint32_t)timeDelta.getMilliseconds());

        sender.send(ind);
        sender.send(FrameEndInd{});

        timeDelta.advance();
    }
}

bool App::shouldClose() const {
    return windowManager.shouldClose();
}

} // namespace bvd
