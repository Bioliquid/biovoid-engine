include(CommonOptions)

create_library(ecs
    SRCS
        Entity.cpp
        Scene.cpp
    DIRS
        .
        components
    LIBS
        EnTT::EnTT
        inc::core
        inc::rhi
        # make ids for meshes, etc
        renderer
    PVT_LIBS
        wsi
        ui
)
