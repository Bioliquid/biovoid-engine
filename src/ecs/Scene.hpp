#pragma once

#include "Resource.hpp"

#include "PixelFormat.hpp"
#include "Texture.hpp"

#include <entt/entt.hpp>
#include <glm/glm.hpp>

namespace bvd {

namespace ui { class Node; }

namespace ecs {

struct UiPageComponent;
struct MaterialComponent;
class Entity;

// TODO: probably should be changed
struct SceneCreateInfo {
    PixelFormat                   format;
    rhi::TextureCreateFlags::Type flags;
    uint32_t                      numSamples;
};

class Scene : public Resource {
    friend class Entity;

    struct Transforms {
        glm::mat4 viewProjection;
    };

public:
    Scene();
    ~Scene();

    void destroy();

    Entity createEntity(std::string const&);
    Entity getEntity(std::string const&);

    void onUpdate();
    void render();

    void updateAllPrimitiveSceneInfos(glm::mat4 const&);

    void deserialize(SceneCreateInfo const&);
    void deserializeUi(SceneCreateInfo const&);

    entt::registry registry;
    Transforms     transforms;
};

} // namespace ecs

} // namespace bvd
