#pragma once

#include <string>

namespace bvd {

namespace ecs {

struct IdComponent {
    std::string tag;
};

} // namespace ecs

} // namespace bvd
