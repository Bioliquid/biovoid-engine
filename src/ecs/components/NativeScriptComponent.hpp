#pragma once

namespace bvd {

namespace ecs {

struct INativeScript {
    virtual ~INativeScript() = default;

    virtual void onUpdate() {}
};

struct NativeScriptComponent {
    INativeScript* script;

    ~NativeScriptComponent() {
        if (script) {
            delete script;
        }
    }

    template<typename T, typename... Args>
    void init(Args&&... args) {
        script = new T(std::forward<Args>(args)...);
    }

    void onUpdate() {
        script->onUpdate();
    }
};

} // namespace ecs

} // namespace bvd
