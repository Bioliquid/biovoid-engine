#pragma once

#include "HashCounter.hpp"

namespace bvd {

namespace ecs {

struct UiPageComponent {
    HashCounter<std::string> textures;

    UiPageComponent() {
        textures.update("none");
    }
};

} // namespace ecs

} // namespace bvd
