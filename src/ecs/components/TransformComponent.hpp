#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace bvd {

namespace ecs {

struct TransformComponent {
    glm::vec3 translation = {0.0f, 0.0f, 0.0f};
    glm::vec3 rotation = {0.0f, 0.0f, 0.0f};
    glm::vec3 scale = {1.0f, 1.0f, 1.0f};

    // glm::vec3 up = {0.0f, 1.0f, 0.0f};
    // glm::vec3 right = {1.0f, 0.0f, 0.0f};
    // glm::vec3 forward = {0.0f, 0.0f, -1.0f};

    TransformComponent() = default;
    TransformComponent(TransformComponent const&) = default;
    TransformComponent(glm::vec3 const& t) : translation(t) {}

    glm::mat4 getTransform() const {
        return glm::translate(glm::mat4(1.0f), translation)
            * glm::toMat4(glm::quat(glm::radians(rotation)))
            * glm::scale(glm::mat4(1.0f), scale);
    }
};

} // namespace ecs

} // namespace bvd
