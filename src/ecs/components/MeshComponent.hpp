#pragma once

#include "Mesh.hpp"

namespace bvd {

namespace ecs {

struct MeshComponent {
    renderer::Mesh mesh;
};

struct MeshUiComponent {
    renderer::MeshUi mesh;
};

struct Mesh2DComponent {
    renderer::Mesh2D mesh;
};

struct Mesh3DComponent {
    renderer::Mesh3D mesh;
};

} // namespace ecs

} // namespace bvd
