#pragma once

#include "Material.hpp"

namespace bvd {

namespace ecs {

struct MaterialComponent {
    renderer::Material material;
};

} // namespace ecs

} // namespace bvd
