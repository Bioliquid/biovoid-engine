#include "Scene.hpp"

#include "Entity.hpp"
#include "IdComponent.hpp"
#include "UiPageComponent.hpp"

#include "RenderCommand.hpp"
#include "MeshComponent.hpp"
#include "MaterialComponent.hpp"
#include "TransformComponent.hpp"

#include "Input.hpp"
#include "Font.hpp"
#include "FileManager.hpp"
#include "GlobalConfig.hpp"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "UiNodeManager.hpp"

namespace bvd::ecs {

Scene::Scene() {}

Scene::~Scene() {
    registry.clear();
}

void Scene::destroy() {
    auto view = registry.view<MeshComponent, MaterialComponent>().each();
    for (auto [entity, meshComponent, materialComponent] : view) {
        meshComponent.mesh.destroy();
        materialComponent.material.destroy();
    };
}

Entity Scene::createEntity(std::string const& entityId) {
    Entity entity = EntityInit{registry.create(), this};
    entity.addComponent<IdComponent>(entityId);
    return entity;
}

Entity Scene::getEntity(std::string const& entityId) {
    auto view = registry.view<IdComponent>().each();
    for (auto [e, idComponent] : view) {
        if (idComponent.tag == entityId) {
            return ecs::Entity(ecs::EntityInit{e, this});
        }
    }
    BVD_THROW("No entity found id={}", entityId);
}

void Scene::onUpdate() {
    registry.view<NativeScriptComponent>().each([this](auto entity, auto& nsc) {
        nsc.onUpdate();
        (void)entity;
    });
}

void Scene::updateAllPrimitiveSceneInfos(glm::mat4 const& viewProjection) {
    transforms.viewProjection = viewProjection;
}

void Scene::render() {
    {
        auto view = registry.view<MaterialComponent, UiPageComponent>().each();
        for (auto [entity, materialComponent, uiPage] : view) {
            materialComponent.material.processTextures(ui::NodeManager::get().getMainLayer(), uiPage.textures);
            materialComponent.material.processTextures(ui::NodeManager::get().getOverlayLayer(), uiPage.textures);
        }
    }

    {
        auto view = registry.view<MeshComponent, MeshUiComponent, UiPageComponent>().each();
        for (auto [entity, mesh, meshUi, uiPage] : view) {
            mesh.mesh.resetSubmeshes();
            meshUi.mesh.fromUi(ui::NodeManager::get().getMainLayer(), mesh.mesh, uiPage.textures);
            meshUi.mesh.fromUi(ui::NodeManager::get().getOverlayLayer(), mesh.mesh, uiPage.textures);

            mesh.mesh.vertexBuffer->copy(meshUi.mesh.vertices.data(), sizeof(renderer::UiVertex) * mesh.mesh.submeshes.back().vertexCount);
        }
    }
    {
        auto view = registry.view<NativeScriptComponent>().each();
        for (auto [entity, script] : view) {
            script.onUpdate();
        }
    }

    auto view = registry.view<MeshComponent, MaterialComponent, TransformComponent>().each();
    for (auto [entity, meshComponent, materialComponent, transformComponent] : view) {
        materialComponent.material.setUniformBuffer("viewProjection", 0, 0, transforms.viewProjection * transformComponent.getTransform());
        renderer::RenderCommand::submitMesh(meshComponent.mesh, materialComponent.material);
    };
}

void Scene::deserialize(SceneCreateInfo const& info) {
    Entity mainObject = createEntity("myPicture");
    {
        mainObject.addComponent<TransformComponent>();
    }
    {
        MeshComponent& component = mainObject.addComponent<MeshComponent>();
        Mesh2DComponent& component2d = mainObject.addComponent<Mesh2DComponent>();
        component2d.mesh.fromQuad({-300.0f, -300.0f}, {600.0f, 600.0f}, component.mesh);
    }
    {
        MaterialComponent& component = mainObject.addComponent<MaterialComponent>();

        rhi::ShaderSource gp_vertShader(ShaderStage::vertex, "base.vert");
        rhi::ShaderSource gp_fragShader(ShaderStage::fragment, "base.frag");
        rhi::GraphicsPipelineStateInit gp_materialInit({gp_vertShader, gp_fragShader});
        gp_materialInit.numColorRenderTargets = 1;
        gp_materialInit.renderTargetFormats[0] = info.format;
        gp_materialInit.renderTargetFlags[0] = info.flags;
        gp_materialInit.primitiveTopology = PrimitiveTopology::triangles;
        gp_materialInit.numSamples = info.numSamples;

        component.material.prepareShaders(gp_materialInit);
        component.material.setTexture(0, 1, 0, *fs::FileManager::initializeTexture("texture.jpg"));
    }
}

void Scene::deserializeUi(SceneCreateInfo const& info) {
    constexpr uint32_t maxQuads = 100000;
    constexpr uint32_t maxVertices = maxQuads * 4;
    constexpr uint32_t maxIndices = maxQuads * 6;

    Entity mainObject = createEntity("mainObject");
    {
        mainObject.addComponent<TransformComponent>();
    }
    {
        uint32_t* uiQuadIndices = new uint32_t[maxIndices];

        uint32_t offset = 0;
        for (uint32_t i = 0; i < maxIndices; i += 6) {
            uiQuadIndices[i + 0] = offset + 0;
            uiQuadIndices[i + 1] = offset + 1;
            uiQuadIndices[i + 2] = offset + 2;

            uiQuadIndices[i + 3] = offset + 2;
            uiQuadIndices[i + 4] = offset + 3;
            uiQuadIndices[i + 5] = offset + 0;

            offset += 4;
        }

        MeshComponent& component = mainObject.addComponent<MeshComponent>();
        MeshUiComponent& componentUi = mainObject.addComponent<MeshUiComponent>();
        componentUi.mesh.fromUi(nullptr, maxVertices, uiQuadIndices, maxIndices, component.mesh);

        delete[] uiQuadIndices;
    }
    {
        MaterialComponent& component = mainObject.addComponent<MaterialComponent>();

        rhi::ShaderSource gp_vertShader(ShaderStage::vertex, "UiElement.vert");
        rhi::ShaderSource gp_fragShader(ShaderStage::fragment, "UiElement.frag");
        rhi::GraphicsPipelineStateInit gp_materialInit({gp_vertShader, gp_fragShader});
        gp_materialInit.numColorRenderTargets = 1;
        gp_materialInit.renderTargetFormats[0] = info.format;
        gp_materialInit.renderTargetFlags[0] = info.flags;
        gp_materialInit.primitiveTopology = PrimitiveTopology::triangles;
        gp_materialInit.numSamples = info.numSamples;

        component.material.prepareShaders(gp_materialInit);

        constexpr uint32_t atlasSet = 0;
        constexpr uint32_t atlasBinding = 1;

        for (uint32_t texId = 0; texId < component.material.getInfo().descriptors[atlasSet].bindings[atlasBinding].count; ++texId) {
            component.material.setTexture(atlasSet, atlasBinding, texId, *fs::FileManager::getTexture("dummy"));
        }
    }
    {
        mainObject.addComponent<UiPageComponent>();
    }
}

} // namespace bvd::ecs
