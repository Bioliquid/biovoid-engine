#pragma once

#include "Scene.hpp"
#include "NativeScriptComponent.hpp"

#include <entt/entt.hpp>

namespace bvd {

namespace ecs {

class Scene;

struct EntityInit {
    entt::entity    handle;
    Scene*          scene;
};

class Entity {
public:
    Entity() = default;
    Entity(EntityInit const&);

    void destroy();

    template<typename T, typename... Args>
    T& addComponent(Args&&... args) {
        return scene->registry.emplace<T>(handle, std::forward<Args>(args)...);
    }

    template<typename T, typename... Args>
    NativeScriptComponent& addScriptComponent(Args&&... args) {
        NativeScriptComponent& component = scene->registry.emplace<NativeScriptComponent>(handle, std::forward<Args>(args)...);
        component.init<T>();
        return component;
    }

    template<typename T>
    void removeComponent() {
        scene->registry.remove<T>(handle);
    }

    template<typename T>
    T& getComponent() {
        return scene->registry.get<T>(handle);
    }

    template<typename T>
    bool hasComponent() const {
        return scene->registry.all_of<T>(handle);
    }

private:
    entt::entity    handle = entt::null;
    Scene*          scene = nullptr;
};

} // namespace ecs

} // namespace bvd
