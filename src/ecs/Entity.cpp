#include "Entity.hpp"

namespace bvd::ecs {

Entity::Entity(EntityInit const& init)
    : handle(init.handle)
    , scene(init.scene)
{}

void Entity::destroy() {
    scene->registry.destroy(handle);
}

} // namespace bvd::ecs
