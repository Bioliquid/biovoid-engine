#pragma once

#include "UiNode.hpp"
#include "Messages.hpp"
#include "Logger.hpp"
#include "UiScript.hpp"

#include <list>
#include <map>
#include <tuple>

namespace bvd {

namespace ui {

struct NodeStateManager {
    virtual void notifyDeletedNode(Node&) = 0;
    virtual Node& getMainLayer() = 0;
    virtual Node& getOverlayLayer() = 0;
};

class NodeManager {
    struct UiServer {
        UiServer() : sender(router) {}

        srvm::Router router;
        srvm::Sender sender;
    };

    // TODO: shrink script size or make it dynamic
    static constexpr uint32_t handlerBufferSize = 256 + sizeof(srvm::Receiver);

    using HandlerBuffer = std::array<uint8_t, handlerBufferSize>;

    struct HandlerStorage
    {
        HandlerBuffer buffer;
        std::function<srvm::Receiver&(HandlerBuffer&)> getReceiver;
        std::function<void(HandlerBuffer&)> destroyer;
    };

    template<typename T>
    void initializeNodeStorage(Node& node) {
        nodeBuffers[&node].getReceiver = [](HandlerBuffer& buffer) -> srvm::Receiver& {
            return reinterpret_cast<T*>(buffer.data())->receiver;
        };

        nodeBuffers[&node].destroyer = [](HandlerBuffer& buffer) {
            reinterpret_cast<T*>(buffer.data())->~T();
        };
    }

public:
    NodeManager(NodeStateManager& sm, srvm::Sender& inSender) : stateManager(sm), sender(inSender) {}

    NodeManager(NodeManager const&) = delete;
    NodeManager& operator=(NodeManager const&) = delete;

    ~NodeManager()
    {
        auto& mainChildren = stateManager.getMainLayer().getChildren();
        auto& overlayChildren = stateManager.getOverlayLayer().getChildren();
        while (not mainChildren.empty()) {
            removeElement(mainChildren.front());
        }
        while (not overlayChildren.empty()) {
            removeElement(overlayChildren.front());
        }
        processDeletedNodes();
        for (auto& [node, storage] : nodeBuffers)
        {
            storage.destroyer(storage.buffer);
        }
    }

    Node& emplace() {
        Node& node = nodes.emplace_back(*this);
        return node;
    }

    void removeElement(Node& node) {
        stateManager.notifyDeletedNode(node);
        node.erase();
        removeNode(node);
    }

    void removeNode(Node& node) {
        if (nodeBuffers.contains(&node)) {
            auto& storage = nodeBuffers.at(&node);
            storage.getReceiver(storage.buffer).disable();
        }

        nodesToDelete.push_back(node);
    }

    void resizeElement(Node& node) {
        if (not node.NodeExternalHook::is_linked()) {
            nodesToResize.push_back(node);
        }
    }

    void processDeletedNodes() {
        for (auto it = nodesToDelete.begin(); it != nodesToDelete.end();) {
            Node& node = *it;
            if (node.NodeExternalHook::is_linked()) {
                nodesToResize.erase(nodesToResize.iterator_to(node));
            }
            if (nodeBuffers.contains(&node)) {
                nodeBuffers.at(&node).destroyer(nodeBuffers.at(&node).buffer);
                nodeBuffers.erase(&node);
            }
            it = nodesToDelete.erase(it);
            nodes.remove_if([&node](Node& lhs) { return &lhs == &node; });
        }
    }

    void processResizeNodes() {
        for (Node& node : nodesToResize) {
            node.scale();
        }
        nodesToResize.clear();
    }

    template<std::derived_from<Script> T> requires(sizeof(T) <= handlerBufferSize)
    void addHandler(Node& node, T::Init const& init) {
        new (nodeBuffers[&node].buffer.data()) T{node, init};
        initializeNodeStorage<T>(node);
    }

    template<std::derived_from<Script> T> requires(sizeof(T) <= handlerBufferSize)
    void addHandler(Node& node) {
        new (nodeBuffers[&node].buffer.data()) T{node};
        initializeNodeStorage<T>(node);
    }

    template<typename MsgType>
    void receive(MsgType msg) {
        server.sender.send(msg);
    }

    template<typename MsgType>
    void receive(Node& node, MsgType msg) {
        if (not nodeBuffers.contains(&node)) {
            return;
        }

        auto& storage = nodeBuffers.at(&node);
        server.sender.send(msg, storage.getReceiver(storage.buffer).getContainerId());
    }

    template<typename MsgType>
    void send(MsgType& msg) {
        sender.send(msg);
    }

    template<typename MsgType>
    void internalSend(MsgType const& msg) {
        receive(msg);
    }

    Node& getMainLayer() { return stateManager.getMainLayer(); }
    Node& getOverlayLayer() { return stateManager.getOverlayLayer(); }

    static void set(NodeManager& nodeManager) { manager = &nodeManager; }
    static NodeManager& get() { return *manager; }

    srvm::Router& getRouter() { return server.router; }

    uint64_t getNodeCount() { return nodes.size(); }

private:
    static inline NodeManager* manager = nullptr;

    NodeStateManager& stateManager;
    srvm::Sender&     sender;
    std::list<Node> nodes;
    std::map<Node*, HandlerStorage> nodeBuffers;

    NodeChildrenList nodesToDelete;
    NodeExternalList nodesToResize;
    UiServer         server;
};

} // namespace ui

} // namespace bvd
