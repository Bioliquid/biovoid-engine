#pragma once

#include "UiOptionHandler.hpp"
#include "UiOptionHandlerCt.hpp"
#include "RefWrapper.hpp"

#include <hml/algo/concat.hpp>

namespace bvd {

namespace ui {

template <typename T, typename Tuple>
struct has_type;

template <typename T, typename... Us>
struct has_type<T, std::tuple<Us...>> : std::disjunction<std::is_same<T, Us>...> {};

template<typename, typename>
class ComponentHelper;

template<typename... Params, typename... Args>
class ComponentHelper<hml::List<Params...>, hml::List<Args...>> {
    using ArgumentStorage = std::tuple<Args...>;

    template<typename T = hml::Placeholder>
    struct IsGroup {
        using type = hml::False;
    };

    template<typename... T>
    struct IsGroup<group::ComponentClass<T...>> {
        using type = hml::True;
    };

    using Groups = hml::concat<typename hml::filter<IsGroup<>, hml::List<Params...>>::type>::type;

    template<typename, typename = hml::Placeholder>
    struct IsGroupParam {
        using type = hml::False;
    };

    template<typename T, auto x>
    struct IsGroupParam<T, group::Property<T, x>> {
        using type = hml::True;
    };

public:
    ComponentHelper(Node& self, Node& parent, Args... args)
        : selfNode(&self)
        , parentNode(&parent)
        , arguments(args...)
    {
        OptionHandler<Args...>::configure(self, parent, args...);
        ct::OptionHandler<Params...>::configure(self, parent);
    }

    template<typename Param>
    Param::type arg() {
        if constexpr (hasArg<Param>()) {
            return std::get<Param>(arguments).value;
        } else {
            return group<Param>().value;
        }
    }

    template<typename Param>
    Param::type argOr(Param::type defValue) {
        if constexpr (hasArg<Param>() or hasGroup<Param>()) {
            return arg<Param>();
        } else {
            return defValue;
        }
    }

    template<typename Param>
    Param param() {
        if constexpr (hasArg<Param>()) {
            return std::get<Param>(arguments);
        } else {
            return group<Param>();
        }
    }

    template<typename Param>
    Param paramOr(Param defValue) {
        if constexpr (hasArg<Param>() or hasGroup<Param>()) {
            return param<Param>();
        } else {
            return defValue;
        }
    }

private:
    template<typename Param>
    static constexpr bool hasArg() {
        return has_type<Param, ArgumentStorage>::value;
    }

    template<typename Group>
    static constexpr bool hasGroup() {
        return not std::is_same_v<hml::find<IsGroupParam<Group>, Groups>::type, hml::Nothing>;
    }

    template<typename Group>
    static constexpr Group group() {
        return hml::fromJust<hml::find<IsGroupParam<Group>, Groups>::type>::type::param;
    }

private:
    Node* selfNode;
    Node* parentNode;
    ArgumentStorage arguments;
};

} // namespace ui

} // namespace bvd
