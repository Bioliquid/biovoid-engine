#pragma once

#include "DivComponent.hpp"

namespace bvd {

namespace ui {

namespace components {

template<typename... Params>
struct DockPlace : public Component<DockPlace, Params...>, ComponentInfo {
    void render(auto) {
        setParams(Texture{"DockPlace.png"});
        setGroups<group::DockPlace>();
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
