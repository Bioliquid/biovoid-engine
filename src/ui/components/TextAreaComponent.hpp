#pragma once

#include "DivComponent.hpp"
#include "FileManager.hpp"

namespace bvd {

namespace ui {

namespace components {

template<typename... Params>
struct TextArea : public Component<TextArea, Params...>, ComponentInfo {
    void render(auto helper) {
        setParams(helper.paramOr<Color>(colors::transparent));
        setParams(helper.paramOr<Width>(autoWidth), helper.paramOr<Height>(autoHeight));
        // setGroups<group::Text>();

        Color     textColor = helper.template argOr<TextColor>(colors::white);
        Alignment alignment = helper.template argOr<TextAlignment>(Alignment::none);

        fragment(
            Div<>::blank(Text{helper.template arg<Title>()}, Width{autoFit}, Height{autoFit}, textColor, Id{"textArea"}, alignment)()
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
