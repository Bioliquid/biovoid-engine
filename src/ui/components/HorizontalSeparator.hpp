#pragma once

#include "DivComponent.hpp"
#include "SeparatorHandler.hpp"

namespace bvd {

namespace ui {

namespace components {

template<typename... Params>
struct HorizontalSeparator : public Component<HorizontalSeparator, Params...>, ComponentInfo {
    void render(auto) {
        setGroups<group::HorizontalSeparator, Handler<handlers::HorizontalSeparator>>();
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
