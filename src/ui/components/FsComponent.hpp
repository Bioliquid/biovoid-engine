#pragma once

#include "DivComponent.hpp"
#include "TextAreaComponent.hpp"
#include "FileEntry.hpp"
#include "FileIterator.hpp"
#include "UiScript.hpp"

namespace bvd {

namespace ui {

namespace components {

struct DirectoryScript : public Script {
    struct Init {
        FilePath path;
    };

    DirectoryScript(Node& n, Init const& init) : Script(n), path(init.path) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const&);

    FilePath path;
};

template<typename... Params>
struct Filesystem : public Component<Filesystem, Params...>, ComponentInfo {
    void render(auto helper) {
        constexpr float padding = 5.0f;

        setParams(Width{100.0_pct}, colors::transparent, Padding{glm::vec4{padding}});

        constexpr auto fileWidth = 75.0_px;

        FilePath path = helper.template arg<Path>();

        DirectoryScript::Init init{path};

        std::vector<NodeCreator> proxies;
        proxies.reserve(10);
        for (FileEntry const& entry : FileIterator{path}) {
            if (entry.is_directory()) {
                proxies.push_back(
                    Div<>::left(Width{fileWidth}, Height{autoFit}, Render{false})(
                        Div<>::top(Width{fileWidth}, Height{fileWidth}, Texture{"folder.png"}, Alignment::x, Handler<DirectoryScript>{init})(),
                        TextArea<>::top(Id{"name"}, Title{entry.path().filename().string()}, Width{fileWidth}, Height{autoFit}, TextAlignment{Alignment::x})()
                    )
                );
            }
        }

        for (FileEntry const& entry : FileIterator{path}) {
            if (entry.is_regular_file()) {
                proxies.push_back(
                    Div<>::left(Width{fileWidth}, Height{autoFit}, Render{false})(
                        Div<>::top(Width{fileWidth}, Height{fileWidth}, Texture{"file.png"}, Alignment::x)(),
                        TextArea<>::top(Title{entry.path().filename().string()}, Width{fileWidth}, Height{autoFit}, TextAlignment{Alignment::x})()
                    )
                );
            }
        }

        fragment(
            proxies
        );
        /*fragment(
            TextArea<>::top(Title{path.string()}, Width{100.0_pct}, Height{autoFit})(),
            TDiv<>::vmiddle(Width{100.0_pct})(
                proxies
            )
        );*/
    }
};

void DirectoryScript::receive(MousePressInd const& msg) {
    if (node.isHovered() && msg.isDoubleClick) {
        NodeManager& nodeManager = node.getManager();
        Node& file = node.getParent();
        Node& files = file.getParent();
        Node& parent = files.getParent();

        FilePath newPath = path / file["name"]["textArea"].getText();

        nodeManager.removeElement(files);
        Filesystem<>::blank(Id{"Filesystem"}, Path{newPath}, Height{autoFit})().inPlace(parent).bindLeft(parent);
    }
}

} // namespace components

} // namespace ui

} // namespace bvd
