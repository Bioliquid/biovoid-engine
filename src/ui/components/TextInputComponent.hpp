#pragma once

#include "UiScript.hpp"
#include "DivComponent.hpp"
#include "FileManager.hpp"
#include "Timer.hpp"

namespace bvd {

namespace ui {

namespace components {

class TextInputScript : public Script {
    using TextValidation = std::function<bool(std::string_view)>;

public:
    struct Init {
        OnTextChange::type onChange;
        TextValidation     isValid;
    };

    TextInputScript(Node& n, Init const& init) : Script(n), onChange(init.onChange), isValid(init.isValid) {
        receiver.init<MessageMap>(0, *this);

        borderColor = node.getBorderColor();

        lastValue.reserve(50);
        timer = rtc::timerPool.create([this](rtc::Timer&) {
            timeout();
        });
    }

    void receive(FrameTickInd const&) {
        // Refreshing value if it changes from outside
        // Node& textField = node["textField"];
        /*if (not editingField && std::stof(textField.text) != *output) {
            textField.setText(fmt::format("{}", *output));
            node.getParent().scale();
        }*/
    }

    void receive(MousePressInd const& msg) {
        if (editingField) {
            stopEditing();
        }

        Node& textField = node["textField"];

        if (node.isHovered()) {
            editingField = true;
            node.setBorderColor(colors::blue);

            // TODO: it probably should be a constant from font maxHeight
            // float height = node.size.y - (node.padding.top + node.padding.bottom) / 2.0f;
            float height = textField.getSize().y * 1.25f;
            float margin = (node.getSize().y - height) / 2.0f;

            float mousePosX = msg.pos.x - textField.getAbsolutePos().x;

            Ref<Font> font = fs::FileManager::getFont(textField.getTexture());
            auto [marginX, cpos] = font->getCursorPos(textField.getText(), textField.getFontSize(), mousePosX);
            cursorPos = cpos;
            marginX += textField.getMargin().left;

            components::Div<>::blank(Id{"caret"}, Width{1.5_px}, Height{px(height)}, colors::white, Margin{glm::vec4{ margin, 0.0f, 0.0f, marginX }})().inPlace(node);
            caretBlinks = 11; // 5 * 2 + 1
            timer->start(500);
        } else if (editingField) {
            editingField = false;
            if (not isValid(textField.getText())) {
                textField.setText(lastValue);
            }
        }
    }

    void receive(KeyPressInd const& msg) {
        if (not editingField) {
            return;
        }

        if (msg.keyCode == keys::enter) {
            stopEditing();
            editingField = false;
            return;
        }

        Node& textField = node["textField"];
        Node& caret = node["caret"];

        Ref<Font> font = fs::FileManager::getFont(textField.getTexture());

        bool isNotFirst = cursorPos != 0;
        bool isNotLast = cursorPos != textField.getText().size();
        bool shouldChange = false;

        if (isValid(textField.getText())) {
            lastValue = textField.getText();
        }
        if (msg.keyCode == keys::left && isNotFirst) {
            caret.addMarginLeft(-font->getAdvance(textField.getText().data(), cursorPos - 1, textField.getFontSize()));
            --cursorPos;
        } else if (msg.keyCode == keys::right && isNotLast) {
            caret.addMarginLeft(font->getAdvance(textField.getText().data(), cursorPos, textField.getFontSize()));
            ++cursorPos;
        } else if (msg.keyCode == keys::backspace && isNotFirst) {
            caret.addMarginLeft(-font->getAdvance(textField.getText().data(), cursorPos - 1, textField.getFontSize()));
            textField.updateText().erase(cursorPos - 1, 1);
            --cursorPos;
            shouldChange = true;
        } else if (msg.keyCode == keys::del && isNotLast) {
            textField.updateText().erase(cursorPos, 1);
            shouldChange = true;
        } else if (std::isprint(msg.keyCode)) {
            textField.updateText().insert(cursorPos, 1, char(msg.keyCode));
            caret.addMarginLeft(font->getAdvance(textField.getText().data(), cursorPos, textField.getFontSize()));
            ++cursorPos;
            shouldChange = true;
        }

        if (isValid(textField.getText())) {
            node.setBorderColor(colors::blue);
            if (shouldChange) {
                onChange(textField.getText());
            }
        } else {
            node.setBorderColor(colors::red);
        }

        resetCaretTimer();
    }

    void timeout() {
        --caretBlinks;
        if (caretBlinks) {
            Node& caret = node["caret"];
            caret.setToRender(not caret.isRendered());
            timer->restart();
        } else {
            timer->stop();
        }
    }

    void resetCaretTimer() {
        Node& caret = node["caret"];
        caret.setToRender(true);
        caretBlinks = 11; // 5 * 2 + 1
        timer->restart();
    }

    void stopEditing() {
        if (timer->isRunning) {
            timer->stop();
        }
        node.getManager().removeElement(node["caret"]);
        node.setBorderColor(borderColor);
    }

    bool               editingField{ false };
    OnTextChange::type onChange;
    TextValidation     isValid;
    std::string        lastValue;
    uint32_t           cursorPos{0};
    rtc::TimerPtr      timer;
    uint32_t           caretBlinks;
    Color              borderColor;
};

template<typename... Params>
struct TextInput : public Component<TextInput, Params...>, ComponentInfo {
    void render(auto helper) {
        setGroups<group::TextInput>();

        float* floatValue = helper.argOr<Float>(nullptr);
        std::string* stringValue = helper.argOr<String>(nullptr);
        auto callback = helper.argOr<OnTextChange>({});

        Text text;
        TextInputScript::Init init{};
        if (floatValue) {
            init.onChange = [floatValue](std::string_view input) {
                *floatValue = std::stof(std::string(input));
            };
            init.isValid = [](std::string_view str) {
                float result;
                auto [ptr, ec] = std::from_chars(str.data(), str.data() + str.size(), result);
                return ec == std::errc() and std::strlen(ptr) == 0;
            };
            text.value = std::to_string(*floatValue);
        } else if (stringValue) {
            init.onChange = [stringValue](std::string_view input) {
                *stringValue = input;
            };
            init.isValid = [](std::string_view) { return true; };
            text.value = *stringValue;
        } else if (callback) {
            init.onChange = callback;
            init.isValid = [] (std::string_view) { return true; };
            text.value = "";
        }
        Color textColor = helper.template argOr<TextColor>(colors::white);

        setParams(Handler<TextInputScript>{init});

        // TODO: support right aligned text (Caret is broken)
        fragment(
            Div<>::left(Id{"textField"}, text, Width{autoFit}, Height{autoFit}, textColor, Margin{glm::vec4{5.0f, 5.0f, 5.0f, 5.0f}})()
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
