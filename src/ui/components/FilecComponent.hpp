#pragma once

#include "DivComponent.hpp"
#include "TextAreaComponent.hpp"
#include "FileManager.hpp"
#include "UiScript.hpp"

namespace bvd {

namespace ui {

namespace components {

struct FpsScript : public Script {
    FpsScript(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(FrameTickInd const& msg) {
        if (msg.ms == 0) {
            return;
        }

        if (n == 10000) {
            averageTime = 0.0f;
            n = 0;
            maxTime = 0.0f;
            minTime = 10000000.0f;
        }

        ++n;
        averageTime += msg.ms;
        maxTime = std::max(maxTime, msg.ms);
        minTime = std::min(minTime, msg.ms);

        Node& textField = node["textArea"];
        textField.setText(fmt::format("Frame time:\navg={:.2f}\nmax={:.2f}\nmin={:.2f}\nnodes={}\nnode size={}", averageTime / n, maxTime, minTime, node.getManager().getNodeCount(), sizeof(Node)));
    }

    float averageTime{0.0f};
    uint32_t n{0};
    float maxTime{0.0f};
    float minTime{10000000.0f};
};

template<typename... Params>
struct Filec : public Component<Filec, Params...>, ComponentInfo {
    void render(auto) {
        setParams(Width{100.0_pct}, Height{100.0_pct}, colors::transparent, Padding{glm::vec4{5.0f, 5.0f, 5.0f, 5.0f}});

        // std::string text = fs::FileManager::readFile(fs::FileManager::getAbsolutePath("text.txt"));

        fragment(
            // TextArea<>::top(Title{text}, Width{100.0_pct}, Height{autoFit})()
            TextArea<Handler<FpsScript>>::top(Title{"Frame time: "}, Width{100.0_pct}, Height{autoFit})()
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
