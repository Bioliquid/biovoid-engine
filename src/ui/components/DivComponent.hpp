#pragma once

#include "UiNodeProxy.hpp"
#include "UiComponent.hpp"

namespace bvd {

namespace ui {

namespace components {

template<typename... Params>
struct Div : public Component<Div, Params...>, ComponentInfo {
    template<typename... Components>
    void render(auto, Components... children) {
        fragment(
            children...
        );
    }
};

template<typename... Params>
struct TDiv : public Component<TDiv, Params...>, ComponentInfo {
    template<typename... Components>
    void render(auto, Components... children) {
        setParams(Render{false});
        fragment(
            children...
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
