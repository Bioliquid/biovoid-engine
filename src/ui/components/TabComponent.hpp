#pragma once

#include "DivComponent.hpp"
#include "TextAreaComponent.hpp"
#include "TabHandler.hpp"
#include "ScrollableComponent.hpp"

namespace bvd {

namespace ui {

namespace components {

template<typename... Params>
struct Tab : public Component<Tab, Params...>, ComponentInfo {
    template<typename... Components>
    void render(auto helper, Components... children) {
        setParams(colors::darkGrey);

        fragment(
            TextArea<group::Tab, Handler<handlers::Tab>>::top(Id{"title"}, Title{helper.template arg<Id>()}, Width{autoFit}, Height{autoFit})(),
            Div<>::vmiddle(Id{"content"}, Width{100.0_pct}, colors::grey)(
                children...
            )
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
