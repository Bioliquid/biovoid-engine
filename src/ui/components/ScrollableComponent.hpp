#pragma once

#include "DivComponent.hpp"
#include "UiScript.hpp"

namespace bvd {

namespace ui {

namespace components {

struct ContentScript : public Script {
    ContentScript(Node& n) : Script(n) {
        static uint16_t cid = 26;
        receiver.init<MessageMap>(cid, *this);
        ++cid;
    }

    void receive(UiNodeResizeInd const&) {
        Node& scrollable = node.getParent();
        Node& scroll = scrollable["scroll"];

        float frameHeight = scrollable.getContentSize().y;
        float fcRatio = frameHeight / node.getSize().y;

        float newScrollSize = (node.getSize().y > frameHeight) ? std::max(frameHeight * fcRatio, 10.0f) : 0.0f;
        scroll.setMarginTop((-node.getMargin().top) * fcRatio);
        scroll.setSizeY(px(newScrollSize));
    }
};

struct ScrollScript : public Script {
    ScrollScript(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const&) {
        if (node.isHovered()) {
            affected = true;
        }
    }

    void receive(MouseReleaseInd const&) {
        affected = false;
    }

    void receive(MouseMoveInd const& msg) {
        if (not affected) {
            return;
        }

        Node& scrollable = node.getParent();
        Node& content = scrollable["content"];

        float delta = msg.delta.y;
        float frameHeight = scrollable.getContentSize().y;

        // TODO: node.margin
        if (node.getPos().y + delta < scrollable.getPadding().top) {
            delta = scrollable.getPadding().top - node.getPos().y;
        }
        if (node.getPos().y + node.getSize().y + delta > frameHeight) {
            delta = frameHeight - node.getPos().y - node.getSize().y;
        }

        node.addMarginTop(delta);
        content.addMarginTop(-delta * (content.getSize().y - frameHeight) / (frameHeight - node.getSize().y));

        // TODO: this is just hardcode for logViewer
        if (delta > 0.0f) {
            for (Node& textArea : content.getChildren()) {
                Node& text = textArea.getChildren().front();
                while (content.getPos().y + text.getLineHeight() < text.getSubtextY() /*check for text size*/) {
                    if (text.getText()[text.getSubtextStartIdx()] == '\n') {
                        text.decSubtextY();
                    }
                    text.incSubtextStartIdx();
                }
            }
        } else {
            for (Node& textArea : content.getChildren()) {
                Node& text = textArea.getChildren().front();
                while (content.getPos().y + text.getLineHeight() > text.getSubtextY() && text.getSubtextStartIdx() != 0) {
                    if (text.getText()[text.getSubtextStartIdx()] == '\n') {
                        text.incSubtextY();
                    }
                    text.decSubtextStartIdx();
                }
            }
        }
    }

    void receive(MouseScrollInd const& msg) {
        Node& scrollable = node.getParent();
        Node& content = scrollable["content"];

        if (not scrollable.isHovered()) {
            return;
        }

        float delta = msg.yOffset * 15;
        float contentPos = -content.getPos().y;
        float contentHeight = content.getSize().y;
        float frameHeight = scrollable.getContentSize().y;

        // TODO: node.margin
        if (delta > contentPos + scrollable.getPadding().top) {
            delta = contentPos + scrollable.getPadding().top;
        } else if (delta < contentPos + frameHeight - contentHeight) {
            delta = contentPos + frameHeight - contentHeight;
        }

        node.addMarginTop(-delta * node.getSize().y / frameHeight);
        content.addMarginTop(delta);

        // TODO: this is just hardcode for logViewer
        if (delta < 0.0f) {
            for (Node& textArea : content.getChildren()) {
                Node& text = textArea.getChildren().front();
                while (content.getPos().y + text.getLineHeight() < text.getSubtextY() /*check for text size*/) {
                    if (text.getText()[text.getSubtextStartIdx()] == '\n') {
                        text.decSubtextY();
                    }
                    text.incSubtextStartIdx();
                }
            }
        } else {
            for (Node& textArea : content.getChildren()) {
                Node& text = textArea.getChildren().front();
                while (content.getPos().y + text.getLineHeight() > text.getSubtextY() && text.getSubtextStartIdx() != 0) {
                    if (text.getText()[text.getSubtextStartIdx()] == '\n') {
                        text.incSubtextY();
                    }
                    text.decSubtextStartIdx();
                }
            }
        }
    }

    bool affected{false};
};

template<typename... Params>
struct Scrollable : public Component<Scrollable, Params...>, ComponentInfo {
    template<typename... Components>
    void render(auto, Components... children) {
        if (self.isAutoHeight()) {
            BVD_LOG(LogGroup::ui, BE, "Scrollable cannot have auto height property");
        }

        setParams(Id{"Scrollable"});

        fragment(
            Div<Handler<ContentScript>>::hmiddle(Id{"content"}, Height{autoFit}, colors::transparent)(
                children...
            ),
            Div<Handler<ScrollScript>>::right(Id{"scroll"}, Width{6.0_px}, colors::lightGrey, Border{glm::vec4{3.0f, 3.0f, 3.0f, 3.0f}}, Margin{glm::vec4{0.0f, 0.0f, 0.0f, 5.0f}})()
        );
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
