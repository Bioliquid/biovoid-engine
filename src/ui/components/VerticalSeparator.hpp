#pragma once

#include "DivComponent.hpp"
#include "SeparatorHandler.hpp"

namespace bvd {

namespace ui {

namespace components {

template<typename... Params>
struct VerticalSeparator : public Component<VerticalSeparator, Params...>, ComponentInfo {
    void render(auto) {
        setGroups<group::VerticalSeparator, Handler<handlers::VerticalSeparator>>();
    }
};

} // namespace components

} // namespace ui

} // namespace bvd
