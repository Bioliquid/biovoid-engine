#pragma once

#include "Messages.hpp"
#include "UiNodeManager.hpp"
#include "DivComponent.hpp"

namespace bvd {

namespace ui {

class UiComponent : public NodeStateManager {
    struct Init {
        int32_t width;
        int32_t height;
    };

public:
    UiComponent(srvm::Router&);
 
    void receive(FrameTickInd const&);
    void receive(FrameEndInd const&);
    void receive(MouseMoveInd const&);
    void receive(MousePressInd const&);
    void receive(KeyPressInd const&);
    void receive(MouseReleaseInd const&);
    void receive(MouseScrollInd const&);
    void receive(FileDropInd const&);
    void receive(WindowResizeInd const&);

    void notifyDeletedNode(Node&) final;
    Node& getMainLayer() final;
    Node& getOverlayLayer() final;

    template<template<typename> class T>
    void create(Init init) {
        layers[0] = &render<T>(init).inPlace();
        layers[1] = &components::TDiv<>::blank(Id{"OverlayRoot"})().inPlace();
        postInit(init);
    }

private:
    template<template<typename...> class T>
    auto render(Init const&) {
        return components::TDiv<>::blank(Id{"Root"})(
            T<>::hmiddle(Id{"Subroot"}, Height{100.0_pct}, Render{false})()
        );
    }

    void postInit(Init const&);

    Node* getHoveredNode(Node& node, glm::vec2 const& uiPos, glm::vec2 const& pos);

    void changeState(Node&, Node::State, bool);

private:
    srvm::Sender   sender;
    srvm::Receiver receiver;
    NodeManager  nodeManager;
    std::array<Node*, 2> layers = {nullptr, nullptr};
    std::array<Node*, 2> hoveredNodes = {nullptr, nullptr};
};

} // namespace ui

} // namespace bvd
