#pragma once

#include "UiParams.hpp"
#include "UiStyle.hpp"

#include <hml/data/List.hpp>

namespace bvd::ui::group {

using Tab = ComponentClass<
      Property<Color, colors::grey>
    , Property<Padding, glm::vec4{5.0f, 7.0f, 5.0f, 7.0f}>
    , Property<Border, glm::vec4{7.0f, 0.0f, 7.0f, 0.0f}>
    , OnHover<
          Property<Color, colors::lightGrey>
        , Property<Cursor, CursorType::hand>
    >
>;

using Text = ComponentClass<
      Property<Color, colors::transparent>
>;

using TextInput = ComponentClass<
      Property<Border, glm::vec4{5.0f, 5.0f, 5.0f, 5.0f}>
    , Property<Color, colors::darkGrey>
    , OnHover<
        Property<Cursor, CursorType::beam>
    >
>;

using VerticalSeparator = ComponentClass<
      Property<Color, colors::darkGrey>
    , Property<Width, 4.0_px>
    , Property<Height, 100.0_pct>
    , OnHover<
          Property<Color, colors::darkGrey>
        , Property<Cursor, CursorType::hResize>
    >
>;

using HorizontalSeparator = ComponentClass<
      Property<Color, colors::darkGrey>
    , Property<Width, 100.0_pct>
    , Property<Height, 4.0_px>
    , OnHover<
          Property<Color, colors::darkGrey>
        , Property<Cursor, CursorType::vResize>
    >
>;

using DockPlace = ComponentClass<
      Property<Width, 40.0_px>
    , Property<Height, 40.0_px>
    , Property<Color, Color{1.0f, 1.0f, 1.0f, 0.8f}>
    , OnHover<
        Property<Cursor, CursorType::hand>
    >
>;

using SelectableItem = ComponentClass<
    Property<Width, 100.0_pct>,
    Property<Color, colors::darkGrey>,
    Property<PaddingTop, 5.0f>,
    Property<PaddingBottom, 5.0f>,
    OnHover<
        Property<Color, colors::grey>
    >
>;

} // namespace bvd::ui::group
