#pragma once

#include <hml/data/List.hpp>

namespace bvd::ui::group {

namespace detail {

class Group;
class OnHover;

} // namespace detail

template<typename Param, Param::type v>
struct Property {
    static constexpr Param::type value = v;
    static constexpr Param param = Param{v};
};

template<typename... T>
using ComponentClass = hml::List<detail::Group, T...>;

template<typename... T>
using OnHover = hml::List<detail::OnHover, T...>;

} // namespace bvd::ui::group
