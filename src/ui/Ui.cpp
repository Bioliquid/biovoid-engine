#include "Ui.hpp"

#include "ContainerIds.hpp"
#include "Collision.hpp"
#include "UiNodeProxy.hpp"
#include "Input.hpp"

#include <boost/range/adaptor/reversed.hpp>

namespace bvd::ui {

UiComponent::UiComponent(srvm::Router& router)
    : sender(router)
    , receiver(router)
    , nodeManager(*this, sender)
{
    receiver.init<MessageMap>(containerId::any, *this);

    NodeManager::set(nodeManager);
}

void UiComponent::receive(FrameTickInd const& msg) {
    nodeManager.receive(msg);
    nodeManager.processDeletedNodes();
    nodeManager.processResizeNodes();
}

void UiComponent::receive(FrameEndInd const&) {
    // nodeManager.receive(msg);
    // nodeManager.processDeletedNodes();
    // nodeManager.processResizeNodes();
}

void UiComponent::receive(MouseMoveInd const& msg) {
    nodeManager.receive(msg);

    for (int32_t layerId = 1; layerId >= 0; --layerId) {
        if (layers[layerId] == nullptr) {
            continue;
        }

        Node& root = *layers[layerId];

        Node* node = getHoveredNode(root, -root.getPos(), msg.pos);

        if (node != nullptr && node != hoveredNodes[layerId]) {
            if (hoveredNodes[layerId] != nullptr) {
                changeState(*hoveredNodes[layerId], Node::State::regular, false);
                hoveredNodes[layerId]->setLastHoverable(false);
            }

            changeState(*node, Node::State::onHover, true);
            node->setLastHoverable(true);
            hoveredNodes[layerId] = node;
        }
    }

    for (int32_t layerId = 1; layerId >= 0; --layerId) {
        if (hoveredNodes[layerId]) {
            nodeManager.receive(*hoveredNodes[1], UiNodeHoverInd{ .isEnter = true });
            break;
        }
    }
}

void UiComponent::changeState(Node& node, Node::State state, bool isEnter) {
    node.changeState(state);
    // TODO
    if (not isEnter) {
        nodeManager.receive(node, UiNodeHoverInd{ .isEnter = isEnter });
    }
    if (node.hasParent()) {
        changeState(node.getParent(), state, isEnter);
    }
}

void UiComponent::receive(MousePressInd const& msg) {
    Node* node = hoveredNodes[0];

    while (node != nullptr && node->hasParent()) {
        node->prioritize();
        node = &node->getParent();
    }

    nodeManager.receive(msg);
}

void UiComponent::receive(KeyPressInd const& msg) {
    nodeManager.receive(msg);
}

void UiComponent::receive(MouseReleaseInd const& msg) {
    // TODO: this might be not just a covered node
    nodeManager.receive(msg);
}

void UiComponent::receive(MouseScrollInd const& msg) {
    nodeManager.receive(msg);
}

void UiComponent::receive(FileDropInd const& msg) {
    nodeManager.receive(msg);
}

void UiComponent::receive(WindowResizeInd const& msg) {
    for (int32_t layerId = 1; layerId >= 0; --layerId) {
        if (layers[layerId] == nullptr) {
            continue;
        }

        Node& node = *layers[layerId];
        node.setPosX(px(-msg.width / 2.0f));
        node.setPosY(px(-msg.height / 2.0f));
        node.setSizeX(px((float)msg.width));
        node.setSizeY(px((float)msg.height));
        nodeManager.processResizeNodes();
    }
}

void UiComponent::notifyDeletedNode(Node& deletedNode) {
    for (Node*& node : hoveredNodes) {
        if (node == &deletedNode) {
            node = node->hasParent() ? &node->getParent() : nullptr;
        }
    }
}

void UiComponent::postInit(Init const& init) {
    using namespace bvd::ui;

    for (int32_t layerId = 1; layerId >= 0; --layerId) {
        Node& node = *layers[layerId];
        node.setPosX(px(float(-init.width) / 2.0f));
        node.setPosY(px(float(-init.height) / 2.0f));
        node.setSizeX(px(float(init.width)));
        node.setSizeY(px(float(init.height)));
        nodeManager.processResizeNodes();
    }
}

Node* UiComponent::getHoveredNode(Node& node, glm::vec2 const& uiPos, glm::vec2 const& pos) {
    glm::vec2 newUiPos = uiPos + node.getPos();

    if (!node.isRegular() || !math::testDotInRect(pos, newUiPos, node.getSize())) {
        return nullptr;
    }

    for (Node& child : boost::adaptors::reverse(node.getChildren())) {
        Node* hoveredChild = getHoveredNode(child, newUiPos, pos);
        if (hoveredChild) {
            return hoveredChild;
        }
    }

    return &node;
}

Node& UiComponent::getMainLayer() {
    if (layers[0] == nullptr) {
        BVD_THROW("Main ui layer is nullptr");
    }
    return *layers[0];
}

Node& UiComponent::getOverlayLayer() {
    if (layers[1] == nullptr) {
        BVD_THROW("Overlay ui layer is nullptr");
    }
    return *layers[1];
}

} // namespace bvd::ui
