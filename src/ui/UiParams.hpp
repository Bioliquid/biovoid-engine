#pragma once

#include "UiNode.hpp"
#include "UiNodeProxy.hpp"
#include "FilePath.hpp"

namespace bvd {

namespace ui {

// template<typename T>
// struct ParamValue {
//     constexpr ParamValue() = default;
//     constexpr ParamValue(T v) : value(v), shouldUse(true) {}

//     T    value{};
//     bool shouldUse{false};
// };

struct Id {
    using type = char const*;
    type value;
};
struct Render {
    using type = bool;
    type value;
};

constexpr Render noRender{false};

// template<ParamValue<Val> val>
struct Width {
    using type = Val;

    // Width() : value(val.value) {}
    constexpr Width(type v) : value(v) {}

    type value;
};

inline constexpr Width autoWidth{autoFit};

// TODO: make _wpx/_wpct and _hpx/_hpct literals

// template<Val val>
// Width() -> Width<val>;

// Width(Val) -> Width<ParamValue<Val>{}>;

struct Height {
    using type = Val;
    type value;
};

inline constexpr Height autoHeight{autoFit};

struct Text {
    using type = std::string;
    type value;
};
struct Texture {
    using type = std::string_view;
    type value;
};
struct Border {
    using type = glm::vec4;
    type value;
};
struct FontSize {
    using type = float;
    type value;
};
struct Padding {
    using type = glm::vec4;
    type value;
}; // top-right-bottom-left
struct PaddingTop {
    using type = float;
    type value;
};
struct PaddingBottom {
    using type = float;
    type value;
};
struct PaddingLeft {
    using type = float;
    type value;
};
struct PaddingRight {
    using type = float;
    type value;
};
struct Margin {
    using type = glm::vec4;
    type value;
};
struct MarginTop {
    using type = float;
    type value;
};
struct MarginBottom {
    using type = float;
    type value;
};
struct MarginLeft {
    using type = float;
    type value;
};
struct MarginRight {
    using type = float;
    type value;
};
struct Cursor {
    using type = CursorType;
    type value;
};
struct Title {
    using type = std::string;
    type value;
};
struct RotateAngle {
    using type = int32_t;
    type value;
};
struct SceneId {
    using type = std::string;
    type value;
};
struct EntityId {
    using type = std::string;
    type value;
};
struct TextColor {
    using type = Color;
    type value;
};
struct Float {
    using type = float*;
    type value;
};
struct String {
    using type = std::string*;
    type value;
};
struct OnTextChange {
    using type = std::function<void(std::string_view)>;
    type value;
};
struct TextAlignment {
    using type = Alignment;
    type value;
};
struct Path {
    using type = FilePath;
    type value;
};
struct BorderColor {
    using type = Color;
    type value;
};

struct BorderWidth {
    using type = float;
    type value;
};

template<typename T>
concept HasInit = requires {
    typename T::Init;
};

template<typename T>
struct Handler {
    using type = T;
};

template<HasInit T>
struct Handler<T> {
    using type = typename T::Init;
    type value;
};

namespace detail {

template<typename Param>
void optHandler(NodeStyle&, Node&, Node&, Param) {}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Render param) {
    self.setToRender(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Width param) {
    self.setSizeX(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Height param) {
    self.setSizeY(param.value);
}

template<>
inline void optHandler(NodeStyle& style, Node&, Node&, Color param) {
    style.setColor(param);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Text param) {
    self.setText(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Texture param) {
    self.setTexutre(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Border param) {
    self.setBorderRadius(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, FontSize param) {
    self.setFontSize(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Alignment param) {
    self.setAlignment(param);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Padding param) {
    self.setPadding(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, PaddingTop param) {
    self.setPaddingTop(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, PaddingBottom param) {
    self.setPaddingBottom(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, PaddingLeft param) {
    self.setPaddingLeft(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, PaddingRight param) {
    self.setPaddingRight(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Margin param) {
    self.setMargin(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, MarginTop param) {
    self.setMarginTop(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, MarginBottom param) {
    self.setMarginBottom(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, MarginLeft param) {
    self.setMarginLeft(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, MarginRight param) {
    self.setMarginRight(param.value);
}

template<>
inline void optHandler(NodeStyle& style, Node&, Node&, Cursor param) {
    style.setCursor(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, Id param) {
    self.setId(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, RotateAngle param) {
    self.setRotateAngle(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, BorderColor param) {
    self.setBorderColor(param.value);
}

template<>
inline void optHandler(NodeStyle&, Node& self, Node&, BorderWidth param) {
    self.setBorderWidth(param.value);
}

} // namespace detail

} // namespace ui

} // namespace bvd
