#pragma once

#include "UiNodeManager.hpp"
#include "traits/Indices.hpp"
#include "RefWrapper.hpp"

#include <functional>

namespace bvd {

namespace ui {

namespace pages {

template<typename, bool>
class Page;

} // namespace pages

class Node;

using NodeProxy = std::function<void(Node&, Node&)>;

class NodeCreator {
public:
    NodeCreator(NodeProxy p)
        : proxy(p) {}

    void operator()(Node& parent) {
        proxy(*node, parent);
    }

    Node& emplace() {
        node = &NodeManager::get().emplace();
        return *node;
    }

    Node& inPlace(Node& parent) {
        Node& newNode = emplace();
        parent.emplace(newNode);
        proxy(*node, parent);
        return newNode;
    }

    Node& inPlace() {
        Node& newNode = emplace();
        Node fakeNode(NodeManager::get());
        proxy(*node, fakeNode);
        return newNode;
    }

private:
    NodeProxy proxy;
    Node* node = nullptr;
};

template<typename Handler>
class NodeHandler {
    template<typename, bool>
    friend class pages::Page;

public:
    NodeHandler(Handler h) : handler(h) {}

    NodeCreator operator()() {
        return NodeCreator(handler);
    }

    template<typename... T>
    NodeCreator operator()(T... children) { // NodeHandler::Proxy children
        using namespace std::placeholders;

        NodeProxy proxy = std::bind(handler, _1, _2, children...);

        return NodeCreator(proxy);
    }

    NodeCreator operator()(std::vector<NodeCreator>& children) { // NodeHandler::Proxy children
        using namespace std::placeholders;

        NodeProxy proxy = std::bind(handler, _1, _2, RefWrapper(children));

        return NodeCreator(proxy);
    }

private:
    Handler handler;
};

template<typename T>
NodeHandler(T) -> NodeHandler<T>;

} // namespace ui

} // namespace bvd
