#pragma once

#include "UiParams.hpp"
#include "CommonStyle.hpp"
#include "UiNodeManager.hpp"

namespace bvd {

namespace ui {

namespace ct {

template<typename... Params>
struct OnHoverOptionHandler;

template<>
struct OnHoverOptionHandler<> {
    static void configure(Node& self, Node&) {
        self.setToHoverable();
    }
};

template<typename Param, Param::type value, typename... Params>
struct OnHoverOptionHandler<group::Property<Param, value>, Params...> {
    static void configure(Node& self, Node& parent) {
        detail::optHandler<Param>(self.getOnHoverStyle(), self, parent, Param{value});

        OnHoverOptionHandler<Params...>::configure(self, parent);
    }
};

template<typename... Params>
struct OptionHandler;

template<>
struct OptionHandler<> {
    static void configure(Node&, Node&) {}
};

// Passthrough parameters
template<typename T, typename... Params>
struct OptionHandler<T, Params...> {
    static void configure(Node& self, Node& parent) {
        OptionHandler<Params...>::configure(self, parent);
    }
};

template<typename T, typename... Params>
struct OptionHandler<Handler<T>, Params...> {
    static void configure(Node& self, Node& parent) {
        NodeManager::get().addHandler<T>(self);

        OptionHandler<Params...>::configure(self, parent);
    }
};

template<typename Param, Param::type value, typename... Params>
struct OptionHandler<group::Property<Param, value>, Params...> {
    static void configure(Node& self, Node& parent) {
        detail::optHandler(self.getRegularStyle(), self, parent, Param{value});

        OptionHandler<Params...>::configure(self, parent);
    }
};

template<typename... InnerParams, typename... Params>
struct OptionHandler<group::ComponentClass<InnerParams...>, Params...> {
    static void configure(Node& self, Node& parent) {
        OptionHandler<InnerParams..., Params...>::configure(self, parent);
    }
};

template<typename... InnerParams, typename... Params>
struct OptionHandler<group::OnHover<InnerParams...>, Params...> {
    static void configure(Node& self, Node& parent) {
        OptionHandler<Params...>::configure(self, parent);
        self.hardCodeOnHoverStyle();
        OnHoverOptionHandler<InnerParams...>::configure(self, parent);
    }
};

} // namespace ct

} // namespace ui

} // namespace bvd
