#include "UiNode.hpp"

#include "FileManager.hpp"
#include "UiNodeManager.hpp"
#include "UiNodeResizeInd.hpp"
#include "Logger.hpp"

namespace bvd::ui {

#define UI_DBG(tempstr, ...)                                           \
    if (nodesToDebug.contains(id)) {                                   \
        BVD_LOG(LogGroup::ui, BD, tempstr __VA_OPT__ (,) __VA_ARGS__); \
    }

Node::Node(NodeManager& nm) : manager(nm) {}

void Node::bindLt(Node* bindNode, NodeGrid newGrid) {
    grid = newGrid;

    if (rbBind) {
        auto& dep = newGrid == NodeGrid::horizontal ? xDep : yDep;
        dep = SizeDependency::self;
    }

    if (bindNode) {
        ltBind = bindNode;
        if (NodeBindHook::is_linked() and rbBind == nullptr) {
            if ((bindNode == parent and &parent->binds.front() != this) or (bindNode != parent and &*(++parent->binds.iterator_to(*bindNode)) != this)) {
                BVD_LOG(LogGroup::ui, BE, "node is already linked to binds");
                parent->binds.erase(binds.iterator_to(*this));
            }
        }

        Node* oldBind = nullptr;

        // TODO: if node is vmiddle or hmiddle its going to be inserted twice
        if (bindNode == parent) {
            if (not NodeBindHook::is_linked()) {
                parent->binds.push_front(*this);
            }
            oldBind = bindNode->innerLt;
            bindNode->innerLt = this;
        } else {
            if (not NodeBindHook::is_linked()) {
                parent->binds.insert(++parent->binds.iterator_to(*bindNode), *this);
            }
            oldBind = bindNode->outerLt;
            bindNode->outerLt = this;
        }

        if (oldBind) {
            oldBind->bindLt(this, newGrid);
        }
        return;
    }

    if (this == &parent->children.front()) {
        ltBind = parent;
        parent->innerLt = this;
    } else {
        Node* node = prevChild();
        ltBind = node;
        node->outerLt = this;
    }
}
void Node::bindRb(Node* bindNode, NodeGrid newGrid) {
    grid = newGrid;

    if (ltBind) {
        auto& dep = newGrid == NodeGrid::horizontal ? xDep : yDep;
        dep = SizeDependency::self;
    }

    if (bindNode) {
        rbBind = bindNode;
        if (NodeBindHook::is_linked() and ltBind == nullptr) {
            if ((bindNode == parent and &parent->binds.back() != this) or (bindNode != parent and &*(--parent->binds.iterator_to(*bindNode)) != this)) {
                BVD_LOG(LogGroup::ui, BE, "node is already linked to binds");
                parent->binds.erase(binds.iterator_to(*this));
            }
        }

        Node* oldBind = nullptr;

        // TODO: if node is vmiddle or hmiddle its going to be inserted twice
        if (bindNode == parent) {
            if (not NodeBindHook::is_linked()) {
                parent->binds.push_back(*this);
            }
            oldBind = bindNode->innerRb;
            bindNode->innerRb = this;
        } else {
            if (not NodeBindHook::is_linked()) {
                parent->binds.insert(parent->binds.iterator_to(*bindNode), *this);
            }
            oldBind = bindNode->outerRb;
            bindNode->outerRb = this;
        }

        if (oldBind) {
            oldBind->bindRb(this, newGrid);
        }
        return;
    }

    if (this == &parent->children.back()) {
        rbBind = parent;
        parent->innerRb = this;
    } else {
        Node* node = nextChild();
        rbBind = node;
        node->outerRb = this;
    }
}

void Node::unbind() {
    if (bothBound()) {
        auto& dep = grid == NodeGrid::horizontal ? xDep : yDep;
        dep = SizeDependency::none;
    }

    if (parent) {
        if (NodeBindHook::is_linked()) {
            parent->binds.erase(parent->binds.iterator_to(*this));
        }
    }

    if (outerLt) {
        outerLt->ltBind = ltBind;
    }

    if (outerRb) {
        outerRb->rbBind = rbBind;
    }

    if (ltBind) {
        if (ltBind == parent) {
            ltBind->innerLt = outerLt;
        } else {
            ltBind->outerLt = outerLt;
        }
    }

    if (rbBind) {
        if (rbBind == parent) {
            rbBind->innerRb = outerRb;
        } else {
            rbBind->outerRb = outerRb;
        }
    }

    ltBind = nullptr;
    rbBind = nullptr;
    outerLt = nullptr;
    outerRb = nullptr;

    grid = NodeGrid::blank;
}

NodeChildrenList::iterator Node::removeFromParent() {
    if (parent) {
        return parent->children.erase(NodeChildrenList::s_iterator_to(*this));
    }
    return {};
}

void Node::prioritize() {
    parent->children.erase(NodeChildrenList::s_iterator_to(*this));
    parent->children.push_back(*this);
}

void Node::emplace(Node& node) {
    node.unbind();
    node.removeFromParent();
    children.push_back(node);
    node.parent = this;

    submitToResize();
}

void Node::erase() {
    unbind();
    eraseChildren();
    removeFromParent();

    submitToResize();
}

void Node::eraseChildren() {
    for (auto it = children.begin(); it != children.end();) {
        Node& node = *it;
        node.eraseChildren();
        if (node.NodeBindHook::is_linked()) {
            binds.erase(NodeBindList::s_iterator_to(node));
        }
        it = node.removeFromParent();
        manager.removeNode(node);
    }
}

void Node::scale() {
    if (not nodesToDebug.empty()) {
        BVD_LOG(LogGroup::ui, BD, "======================Resize======================");
    }

    if (parent) {
        BVD_ICHECK(sizeVec.pctVal.x != 0.0f && parent->sizeVec.autoWidth, "Cannot calculate pct width of node (parent width is auto) id={} parentId={}", id, parent->id);
        BVD_ICHECK(sizeVec.pctVal.y != 0.0f && parent->sizeVec.autoHeight, "Cannot calculate pct height of node (parent height is auto) id={} parentId={}", id, parent->id);

        BVD_ICHECK(parent->sizeVec.autoHeight && grid == NodeGrid::blank && isYCentered(), "Parent height is dependent on child's pos and vice versa id={} parentId={}", id, parent->id);
        BVD_ICHECK(parent->sizeVec.autoWidth && grid == NodeGrid::blank && isXCentered(), "Parent width is dependent on child's pos and vice versa id={} parentId={}", id, parent->id);
    }

    BVD_ICHECK((sizeVec.pctVal.x != 0.0f || sizeVec.val.x != 0.0f) && sizeVec.autoWidth, "Both auto-width and width are set id={}", id);
    BVD_ICHECK((sizeVec.pctVal.y != 0.0f || sizeVec.val.y != 0.0f) && sizeVec.autoHeight, "Both auto-height and height are set id={}", id);

    if (rbBind == parent && parent != nullptr) {
        BVD_ICHECK(parent->sizeVec.autoWidth && grid == NodeGrid::horizontal, "Cannot calculate pct width of node (parent width is auto) id={}", id);
        BVD_ICHECK(parent->sizeVec.autoHeight && grid == NodeGrid::vertical, "Cannot calculate pct height of node (parent height is auto) id={}", id);
    }

    BVD_ICHECK(grid == NodeGrid::horizontal && isXCentered(), "Cannot be both left/right bound and aligned id={}", id);
    BVD_ICHECK(grid == NodeGrid::vertical && isYCentered(), "Cannot be both top/bottom bound and aligned id={}", id);

    BVD_ICHECK(bothBound() && grid == NodeGrid::horizontal && (sizeVec.pctVal.x != 0.0f || sizeVec.val.x != 0.0f || sizeVec.autoWidth), "Cannot be both hmiddle and have size id={}", id);
    BVD_ICHECK(bothBound() && grid == NodeGrid::vertical && (sizeVec.pctVal.y != 0.0f || sizeVec.val.y != 0.0f || sizeVec.autoHeight), "Cannot be both vmiddle and have size id={}", id);

    BVD_ICHECK(parent == nullptr && (sizeVec.autoWidth || sizeVec.pctVal.x != 0.0f), "Root node should have fixed width id={}", id);
    BVD_ICHECK(parent == nullptr && (sizeVec.autoHeight || sizeVec.pctVal.y != 0.0f), "Root node should have fixed height id={}", id);

    BVD_ICHECK(isText && not children.empty(), "Text node cannot have children id={}", id);

    NodeResizeList resizeList;
    if (parent == nullptr) {
        // position and size have to be fixed
        size.x = sizeVec.val.x;
        size.y = sizeVec.val.y;

        for (Node& child : children) {
            if (not child.NodeResizeHook::is_linked()) {
                resizeList.push_back(child);
                child.processPreResize(resizeList);
            }
        }

        resizeList.clear();

        for (Node& child : children) {
            if (not child.NodeResizeHook::is_linked()) {
                resizeList.push_back(child);
                child.processResize(resizeList);
            }
        }

        resizeList.clear();

        for (Node& child : children) {
            if (not child.NodeResizeHook::is_linked()) {
                resizeList.push_back(child);
                child.processPostResize(resizeList);
            }
        }
    } else {
        processPreResize(resizeList);

        resizeList.clear();
        processResize(resizeList);

        resizeList.clear();
        processPostResize(resizeList);
    }

    if (not nodesToDebug.empty()) {
        BVD_LOG(LogGroup::ui, BD, "==================================================");
    }
}

void Node::processPreResize(NodeResizeList& curUsedNodes) {
    if (not (isText && sizeXCalced && sizeYCalced)) {
        // reseting size
        size = glm::vec2{ 0.0f, 0.0f };
    }

    // processing node dependencies
    if (ltBind != nullptr && ltBind != parent && not ltBind->NodeResizeHook::is_linked()) {
        curUsedNodes.push_back(*ltBind);
        ltBind->processPreResize(curUsedNodes);
    }

    if (rbBind != nullptr && rbBind != parent && not rbBind->NodeResizeHook::is_linked()) {
        curUsedNodes.push_back(*rbBind);
        rbBind->processPreResize(curUsedNodes);
    }

    UI_DBG("PreResize {}", *this);

    if (isText) {
        processTextSize();
    }

    if (grid != NodeGrid::horizontal && not isXCentered()) {
        pos.x = margin.left + parent->padding.left;
    }

    if (grid != NodeGrid::vertical && not isYCentered()) {
        pos.y = margin.top + parent->padding.top;
    }

    glm::vec2 parentContentSize = parent->getContentSize();

    if ((parent->hasSizeX() || sizeVec.pctVal.x == 0.0f) && not sizeVec.autoWidth && (not bothBound() || grid == NodeGrid::vertical)) {
        sizeVec.updateX(size, parentContentSize);
    }

    if ((parent->hasSizeY() || sizeVec.pctVal.y == 0.0f) && not sizeVec.autoHeight && (not bothBound() || grid == NodeGrid::horizontal)) {
        sizeVec.updateY(size, parentContentSize);
    }

    if (parent->hasSizeX() && hasSizeX())
    {
        processXCenteredPos(parentContentSize);
    }

    if (parent->hasSizeY() && hasSizeY())
    {
        processYCenteredPos(parentContentSize);
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (ltBind != nullptr) {
        if (grid == NodeGrid::horizontal) {
            if (ltBind == parent) {
                pos.x = parent->padding.left + margin.left;
            } else {
                pos.x = ltBind->pos.x + ltBind->size.x + ltBind->margin.right + margin.left;
            }
        }

        if (grid == NodeGrid::vertical) {
            if (ltBind == parent) {
                pos.y = parent->padding.top + margin.top;
            } else {
                pos.y = ltBind->pos.y + ltBind->size.y + ltBind->margin.bottom + margin.top;
            }
        }
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (bothBound()) {
        if (grid == NodeGrid::horizontal) {
            if (rbBind == parent) {
                if (parent->hasSizeX())
                {
                    size.x = parent->size.x - pos.x - parent->padding.right - margin.right;
                }
            } else {
                size.x = rbBind->pos.x - pos.x - rbBind->margin.left - margin.right;
            }
        } else if (grid == NodeGrid::vertical) {
            if (rbBind == parent) {
                if (parent->hasSizeY())
                {
                    size.y = parent->size.y - pos.y - parent->padding.bottom - margin.bottom;
                }
            } else {
                size.y = rbBind->pos.y - pos.y - rbBind->margin.top - margin.bottom;
            }
        }
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (ltBind == nullptr && rbBind != nullptr) {
        if (grid == NodeGrid::horizontal && hasSizeX()) {
            if (rbBind == parent) {
                if (parent->hasSizeX())
                {
                    pos.x = parent->size.x - size.x - parent->padding.right - margin.right;
                }
            } else {
                pos.x = rbBind->pos.x - size.x - rbBind->margin.left - margin.right;
            }
        }

        if (grid == NodeGrid::vertical && not sizeVec.autoHeight) {
            if (rbBind == parent) {
                if (parent->hasSizeY())
                {
                    pos.y = parent->size.y - size.y - parent->padding.bottom - margin.bottom;
                }
            } else {
                pos.y = rbBind->pos.y - size.y - rbBind->margin.top - margin.bottom;
            }
        }
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    // process children
    for (Node& child : children) {
        if (not child.NodeResizeHook::is_linked()) {
            curUsedNodes.push_back(child);
            child.processPreResize(curUsedNodes);
        }
    }
}

void Node::processResize(NodeResizeList& curUsedNodes) {
    // process children
    for (Node& child : children) {
        if (not child.NodeResizeHook::is_linked()) {
            curUsedNodes.push_back(child);
            child.processResize(curUsedNodes);
        }
    }

    // processing node dependencies
    if (ltBind != nullptr && ltBind != parent && not ltBind->NodeResizeHook::is_linked()) {
        curUsedNodes.push_back(*ltBind);
        ltBind->processResize(curUsedNodes);
    }

    if (rbBind != nullptr && rbBind != parent && not rbBind->NodeResizeHook::is_linked()) {
        curUsedNodes.push_back(*rbBind);
        rbBind->processResize(curUsedNodes);
    }

    UI_DBG("Resize {}", *this);

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (sizeVec.autoWidth && not isText && not children.empty()) {
        float width = 0.0f;

        float blankMaxWidth = 0.0f;
        for (Node& child : children) {
            if (child.grid == NodeGrid::blank) {
                blankMaxWidth = std::max(blankMaxWidth, child.pos.x + child.size.x + child.margin.right);
            }
        }

        if (children.front().grid == NodeGrid::horizontal) {
            for (Node& child : children) {
                if (child.grid != NodeGrid::blank) {
                    width += child.margin.left + child.size.x + child.margin.right;
                }
            }
        }
        else if (children.front().grid == NodeGrid::vertical) {
            for (Node& child : children) {
                if (child.grid != NodeGrid::blank) {
                    width = std::max(width, child.margin.left + child.size.x + child.margin.right);
                }
            }
        }
        // pos of blank entities already contains padding.left
        size.x = std::max(blankMaxWidth, width + padding.left) + padding.right;
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (sizeVec.autoHeight && not isText && not children.empty()) {
        float height = 0.0f;

        float blankMaxHeight = 0.0f;
        for (Node& child : children) {
            if (child.grid == NodeGrid::blank) {
                blankMaxHeight = std::max(blankMaxHeight, child.pos.y + child.size.y + child.margin.bottom);
            }
        }

        if (children.front().grid == NodeGrid::vertical) {
            for (Node& child : children) {
                if (child.grid != NodeGrid::blank) {
                    height += child.margin.top + child.size.y + child.margin.bottom;
                }
            }
        }
        else if (children.front().grid == NodeGrid::horizontal) {
            for (Node& child : children) {
                if (child.grid != NodeGrid::blank) {
                    height = std::max(height, child.margin.top + child.size.y + child.margin.bottom);
                }
            }
        }
        // pos of blank entities already contains padding.top
        size.y = std::max(blankMaxHeight, height + padding.top) + padding.bottom;
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);
}

void Node::processPostResize(NodeResizeList& curUsedNodes) {
    // processing node dependencies
    if (ltBind != nullptr && ltBind != parent && not ltBind->NodeResizeHook::is_linked()) {
        curUsedNodes.push_back(*ltBind);
        ltBind->processPostResize(curUsedNodes);
    }

    if (rbBind != nullptr && rbBind != parent && not rbBind->NodeResizeHook::is_linked()) {
        curUsedNodes.push_back(*rbBind);
        rbBind->processPostResize(curUsedNodes);
    }

    UI_DBG("PostResize {}", *this);

    glm::vec2 parentContentSize = parent->getContentSize();

    if (ltBind != nullptr) {
        if (grid == NodeGrid::horizontal) {
            if (ltBind != parent) {
                pos.x = ltBind->pos.x + ltBind->size.x + ltBind->margin.right + margin.left;
            }
        }

        if (grid == NodeGrid::vertical) {
            if (ltBind != parent) {
                pos.y = ltBind->pos.y + ltBind->size.y + ltBind->margin.bottom + margin.top;
            }
        }
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (bothBound()) {
        if (grid == NodeGrid::horizontal) {
            if (rbBind == parent) {
                if (parent->hasSizeX())
                {
                    size.x = parent->size.x - pos.x - parent->padding.right - margin.right;
                }
            } else {
                size.x = rbBind->pos.x - pos.x - rbBind->margin.left - margin.right;
            }
        } else if (grid == NodeGrid::vertical) {
            if (rbBind == parent) {
                if (parent->hasSizeY())
                {
                    size.y = parent->size.y - pos.y - parent->padding.bottom - margin.bottom;
                }
            } else {
                size.y = rbBind->pos.y - pos.y - rbBind->margin.top - margin.bottom;
            }
        }
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (ltBind == nullptr && rbBind != nullptr) {
        if (grid == NodeGrid::horizontal && not sizeVec.autoWidth) {
            if (rbBind == parent) {
                pos.x = parent->size.x - size.x - parent->padding.right - margin.right;
            } else {
                pos.x = rbBind->pos.x - size.x - rbBind->margin.left - margin.right;
            }
        }

        // if (grid == NodeGrid::vertical && hasSizeY()) {
        if (grid == NodeGrid::vertical && not sizeVec.autoHeight) {
            if (rbBind == parent) {
                // if (parent->hasSizeY())
                {
                    pos.y = parent->size.y - size.y - parent->padding.bottom - margin.bottom;
                }
            } else {
                pos.y = rbBind->pos.y - size.y - rbBind->margin.top - margin.bottom;
            }
        }
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (not isText) {
        // TODO: revisit outer if statement
        if (sizeVec.autoWidth and not children.empty()) {
            if (grid == NodeGrid::horizontal && rbBind != nullptr) {
                processRightBoundPos();
            }
        }
        processXCenteredPos(parentContentSize);
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    if (not isText) {
        // TODO: revisit outer if statement
        if (sizeVec.autoHeight and not children.empty()) {
            if (grid == NodeGrid::vertical && rbBind != nullptr) {
                processBottomBoundPos();
            }
        }

        processYCenteredPos(parentContentSize);
    }

    UI_DBG("Position({}px); Size=({}px)", pos, size);

    // process children
    for (Node& child : children) {
        if (not child.NodeResizeHook::is_linked()) {
            curUsedNodes.push_back(child);
            child.processPostResize(curUsedNodes);
        }
    }

    UI_DBG("End of {}", *this);

    UiNodeResizeInd ind;
    ind.width = size.x;
    ind.height = size.y;

    manager.receive(*this, ind);
}

void Node::submitToResize() {
    struct Candidate {
        Node*    node;
        uint32_t parentLevel;
    };

    Candidate x{this, 0};
    // parent::down and this::up is illegal
    while (x.node != nullptr and x.node->parent != nullptr and (x.node->parent->xDep == SizeDependency::down or x.node->xDep == SizeDependency::up)) {
        x.node = x.node->parent;
        ++x.parentLevel;
    }

    Candidate y{this, 0};
    while (y.node != nullptr and y.node->parent != nullptr and (y.node->parent->yDep == SizeDependency::down or y.node->yDep == SizeDependency::up)) {
        y.node = y.node->parent;
        ++y.parentLevel;
    }
    manager.resizeElement(x.parentLevel > y.parentLevel ? *x.node : *y.node);
}

void Node::processTextSize() {
    if (sizeVec.autoWidth && !sizeXCalced) {
        float width = fs::FileManager::getFont(getTexture())->getTextWidth(text, fontSize, 10000.0f);
        if (parent->size.x != 0.0f) {
            // TODO: is it correct to shink text by its parent
            // change 10000.0f in getTextWidth
            width = std::min(width, parent->size.x);
        }
        size.x = width + padding.left + padding.right;
        sizeXCalced = true;
    }

    if (sizeVec.autoHeight && !sizeYCalced) {
        float height = fs::FileManager::getFont(getTexture())->getTextHeight(text, fontSize, 10000.0f);
        if (parent->size.y != 0.0f) {
            // TODO: is it correct to shink text by its parent
            // change 10000.0f in getTextWidth
            height = std::min(height, parent->size.y);
        }
        size.y = height + padding.top + padding.bottom;
        sizeYCalced = true;
    }
}

void Node::processRightBoundPos() {
    if (rbBind == parent) {
        pos.x = parent->size.x - size.x - parent->padding.right - margin.right;
    } else {
        pos.x = rbBind->pos.x - size.x - rbBind->margin.left - margin.right;
    }
}
void Node::processBottomBoundPos() {
    if (rbBind == parent) {
        pos.y = parent->size.y - size.y - parent->padding.bottom - margin.bottom;
    } else {
        pos.y = rbBind->pos.y - size.y - rbBind->margin.top - margin.bottom;
    }
}

void Node::processXCenteredPos(glm::vec2 const& parentContentSize) {
    if (isXCentered()) {
        pos.x = parent->padding.left + (parentContentSize.x - size.x) / 2.0f;
    }
}

void Node::processYCenteredPos(glm::vec2 const& parentContentSize) {
    if (isYCentered()) {
        pos.y = parent->padding.top + (parentContentSize.y - size.y) / 2.0f;
    }
}

void Node::setFixedSize() {
    setFixedWidth();
    setFixedHeight();
}

void Node::setFixedWidth() {
    xDep = SizeDependency::none;
    glm::vec2 parentContentSize = parent ? parent->getContentSize() : glm::vec2{};
    sizeVec.updateX(sizeVec.val, parentContentSize);
    sizeVec.pctVal.x = 0.0f;

    submitToResize();
}

void Node::setFixedHeight() {
    yDep = SizeDependency::none;
    glm::vec2 parentContentSize = parent ? parent->getContentSize() : glm::vec2{};
    sizeVec.updateY(sizeVec.val, parentContentSize);
    sizeVec.pctVal.y = 0.0f;

    submitToResize();
}

void Node::setId(std::string_view newId) { id = newId; }

void Node::setTexutre(std::string_view newTextureName) { textureName = newTextureName; }

void Node::setText(std::string content) {
    text = content;
    isText = true;
    xDep = SizeDependency::none;
    yDep = SizeDependency::none;
    sizeXCalced = false;
    sizeYCalced = false;
    textureName = "Roboto/Roboto-Regular.ttf";

    submitToResize();
}

std::string& Node::updateText() {
    if (not isText) {
        BVD_THROW("Updating text of non-text node is prohibited");
    }

    xDep = SizeDependency::none;
    yDep = SizeDependency::none;
    sizeXCalced = false;
    sizeYCalced = false;
    textureName = "Roboto/Roboto-Regular.ttf";

    submitToResize();

    return text;
}

void Node::setSizeX(Val x) {
    if (x.isAuto) {
        xDep = SizeDependency::down;
        sizeVec.autoWidth = true;
    } else if (x.isFixed) {
        xDep = SizeDependency::none;
        sizeVec.val.x = x.value;
        size.x = x.value;
    } else {
        xDep = SizeDependency::up;
        sizeVec.pctVal.x = x.value;
    }

    submitToResize();
}

void Node::setSizeY(Val y) {
    if (y.isAuto) {
        yDep = SizeDependency::down;
        sizeVec.autoHeight = true;
    } else if (y.isFixed) {
        yDep = SizeDependency::none;
        sizeVec.val.y = y.value;
        size.y = y.value;
    } else {
        yDep = SizeDependency::up;
        sizeVec.pctVal.y = y.value;
    }

    submitToResize();
}

void Node::setPosX(Val x) {
    pos.x = x.value;
}

void Node::setPosY(Val y) {
    pos.y = y.value;
}

void Node::setPadding(glm::vec4 const& newPadding) {
    setPaddingTop(newPadding[0]);
    setPaddingRight(newPadding[1]);
    setPaddingBottom(newPadding[2]);
    setPaddingLeft(newPadding[3]);
}

// TODO: this resize can be just position without size
void Node::setPaddingTop(float newPadding) { padding.top = newPadding; submitToResize(); }

void Node::setPaddingBottom(float newPadding) { padding.bottom = newPadding; submitToResize(); }

void Node::setPaddingLeft(float newPadding) { padding.left = newPadding; submitToResize(); }

void Node::setPaddingRight(float newPadding) { padding.right = newPadding; submitToResize(); }

void Node::setMargin(glm::vec4 const& newMargin) {
    setMarginTop(newMargin[0]);
    setMarginRight(newMargin[1]);
    setMarginBottom(newMargin[2]);
    setMarginLeft(newMargin[3]);
}

// TODO: this resize can be just position without size
void Node::setMarginTop(float newMargin) { margin.top = newMargin; submitToResize(); }

void Node::setMarginBottom(float newMargin) { margin.bottom = newMargin; submitToResize(); }

void Node::setMarginLeft(float newMargin) { margin.left = newMargin; submitToResize(); }

void Node::setMarginRight(float newMargin) { margin.right = newMargin; submitToResize(); }

void Node::addSizeX(float addSize) {
    setFixedWidth();
    sizeVec.val.x += addSize;
}

void Node::addSizeY(float addSize) {
    setFixedHeight();
    sizeVec.val.y += addSize;
}

void Node::addMarginTop(float addMargin) { setMarginTop(margin.top + addMargin); }

void Node::addMarginBottom(float addMargin) { setMarginBottom(margin.bottom + addMargin); }

void Node::addMarginLeft(float addMargin) { setMarginLeft(margin.left + addMargin); }

void Node::addMarginRight(float addMargin) { setMarginRight(margin.right + addMargin); }

void Node::setBorderRadius(glm::vec4 const& newBorderRadius) {
    borderRadius.top = newBorderRadius[0];
    borderRadius.right = newBorderRadius[1];
    borderRadius.bottom = newBorderRadius[2];
    borderRadius.left = newBorderRadius[3];
}

void Node::setBorderColor(Color const& newBorderColor) {
    borderColor = newBorderColor;
}

void Node::setBorderWidth(float newBorderWidth) {
    borderWidth = newBorderWidth;
}

void Node::setRotateAngle(int32_t angle) { rotateAngle = angle; }

void Node::setAlignment(Alignment inAlignment) { alignment = inAlignment; }

void Node::setFontSize(float font) { fontSize = font; }

void Node::changeState(State s) {
    state = s;
    if (s == State::onHover) {
        if (!isHoverable) {
            return;
        }

        Input::changeCursor(onHoverStyle.cursor);
    } else {
        Input::changeCursor(CursorType::arrow);
    }
}

glm::vec2 Node::getAbsolutePos() const {
    glm::vec2 absPos = pos;
    Node* node = parent;
    if (node) {
        while (node->parent != nullptr) {
            absPos += node->pos;
            node = node->parent;
        }
    }
    return absPos;
}

glm::vec2 Node::getContentSize() const {
    return glm::vec2{size.x - padding.left - padding.right, size.y - padding.top - padding.bottom};
}

Node& Node::getParent() {
    if (not hasParent()) {
        BVD_THROW("Node {} has no parent", id);
    }
    return *parent;
}

Node& Node::getLt() {
    if (not hasLt()) {
        BVD_THROW("Node {} has no lt", id);
    }
    return *ltBind;
}

Node& Node::getRb() {
    if (not hasRb()) {
        BVD_THROW("Node {} has no rb", id);
    }
    return *rbBind;
}

Node& Node::getOuterLt() {
    if (not hasOuterLt()) {
        BVD_THROW("Node {} has no outer lt", id);
    }
    return *outerLt;
}

Node& Node::getOuterRb() {
    if (not hasOuterRb()) {
        BVD_THROW("Node {} has no outer rb", id);
    }
    return *outerRb;
}

Node& Node::getInnerLt() {
    if (not hasInnerLt()) {
        BVD_THROW("Node {} has no inner lt", id);
    }
    return *innerLt;
}

Node& Node::getInnerRb() {
    if (not hasInnerRb()) {
        BVD_THROW("Node {} has no inner rb", id);
    }
    return *innerRb;
}

Node* Node::getLtIf() {
    return ltBind;
}

Node* Node::getRbIf() {
    return rbBind;
}

Node* Node::getOuterLtIf() {
    return outerLt;
}

Node* Node::getOuterRbIf() {
    return outerRb;
}

Node* Node::getInnerLtIf() {
    return innerLt;
}

Node* Node::getInnerRbIf() {
    return innerRb;
}

} // namespace bvd::ui

auto fmt::formatter<bvd::ui::Node>::format(bvd::ui::Node const& node, format_context& ctx) -> decltype(ctx.out()) {
    return fmt::format_to(ctx.out(), "Node(id={})", node.getId());
}
