#pragma once

#include "UiVec.hpp"
#include "Color.hpp"
#include "CursorType.hpp"
#include "Input.hpp"
#include "Exception.hpp"

#include <boost/intrusive/list.hpp>
#include <boost/intrusive/list_hook.hpp>
#include <glm/glm.hpp>

#include <concepts>
#include <string>
#include <vector>
#include <functional>
#include <set>
#include <ranges>

namespace bvd {

namespace renderer {

class MeshUi;

} // namespace renderer

namespace ui {

struct Size {
    void updateX(glm::vec2& size, glm::vec2 parent) {
        if (pctVal.x != 0.0f) {
            size.x = pctVal.x * parent.x / 100.0f;
        } else {
            size.x = val.x;
        }
    }

    void updateY(glm::vec2& size, glm::vec2 parent) {
        if (pctVal.y != 0.0f) {
            size.y = pctVal.y * parent.y / 100.0f;
        } else {
            size.y = val.y;
        }
    }

    glm::vec2 val{};
    glm::vec2 pctVal{};
    bool      autoWidth{false};
    bool      autoHeight{false};
};

enum class Alignment : uint8_t {
      none
    , x
    , y
    , both
};

enum class NodeGrid : uint8_t {
      blank
    , horizontal
    , vertical
};

struct Box {
    glm::vec4 getVec() const { return glm::vec4{top, right, bottom, left}; }

    float top;
    float bottom;
    float left;
    float right;
};

class Node;
class NodeManager;

struct NodeChildrenTag;
struct NodeBindTag;
struct NodeResizeTag;
struct NodeExternalTag;

using NodeChildrenHook = boost::intrusive::list_base_hook<boost::intrusive::tag<NodeChildrenTag>>;
using NodeBindHook = boost::intrusive::list_base_hook<boost::intrusive::tag<NodeBindTag>>;
using NodeResizeHook = boost::intrusive::list_base_hook<boost::intrusive::tag<NodeResizeTag>>;
using NodeExternalHook = boost::intrusive::list_base_hook<boost::intrusive::tag<NodeExternalTag>>;

using NodeChildrenList = boost::intrusive::list<Node, boost::intrusive::base_hook<NodeChildrenHook>>;
using NodeBindList = boost::intrusive::list<Node, boost::intrusive::base_hook<NodeBindHook>>;
using NodeResizeList = boost::intrusive::list<Node, boost::intrusive::base_hook<NodeResizeHook>>;
using NodeExternalList = boost::intrusive::list<Node, boost::intrusive::base_hook<NodeExternalHook>>;

struct NodeStyle {
    // setters
    void setColor(Color const& newcColor) { color = newcColor; }
    void setCursor(CursorType const& newCursor) { cursor = newCursor; }

    // getters
    Color const&      getColor() const { return color; }
    CursorType const& getCursor() const { return cursor; }

    Color      color{ colors::white };
    CursorType cursor{ CursorType::arrow };
};

class Node : public NodeChildrenHook
           , public NodeBindHook
           , public NodeResizeHook
           , public NodeExternalHook
{
    // TODO: delete
    friend class renderer::MeshUi;
    friend class NodeManager;

    enum class SizeDependency : uint8_t {
        none,
        up,
        self,
        down
    };

public:
    enum class State : uint8_t {
          regular
        , onHover
    };

public:
    Node(NodeManager&);
    Node(Node const&) = delete;
    Node& operator=(Node const&) = delete;

    inline Node* nextChild() { return &*++NodeChildrenList::s_iterator_to(*this); }
    inline Node* prevChild() { return &*--NodeChildrenList::s_iterator_to(*this); }

    template<typename... Nodes>
    void initialize(Nodes&... nodes) {
        (children.push_back(nodes), ...);

        for (Node& child : children) {
            child.parent = this;
        }
    }

    // TODO: temp solution
    void setBinds() {
        for (Node& child : children) {
            binds.push_back(child);
        }
    }

    // setters
    void setId(std::string_view);
    void setTexutre(std::string_view);
    void setText(std::string);
    void setSizeX(Val);
    void setSizeY(Val);
    void setPosX(Val);
    void setPosY(Val);
    void setPadding(glm::vec4 const&);
    void setPaddingTop(float);
    void setPaddingBottom(float);
    void setPaddingLeft(float);
    void setPaddingRight(float);
    void setMargin(glm::vec4 const&);
    void setMarginTop(float);
    void setMarginBottom(float);
    void setMarginLeft(float);
    void setMarginRight(float);
    void setBorderRadius(glm::vec4 const&);
    void setBorderColor(Color const&);
    void setBorderWidth(float);
    void setRotateAngle(int32_t);
    void setAlignment(Alignment);
    void setFontSize(float);
    void changeState(State s);

    // changers
    void addSizeX(float);
    void addSizeY(float);
    void addMarginTop(float);
    void addMarginBottom(float);
    void addMarginLeft(float);
    void addMarginRight(float);
    void incSubtextY() { subtextY += lineHeight; }
    void decSubtextY() { subtextY -= lineHeight; }
    void incSubtextStartIdx() { ++subtextStartIdx; }
    void decSubtextStartIdx() { --subtextStartIdx; }

    std::string& updateText();

    // getters
    inline NodeManager&     getManager() const { return manager; }
    inline std::string_view getId() const { return id.c_str(); }
    // inline std::string_view getTexture() const { return textureName.c_str(); }
    inline std::string const& getTexture() const { return textureName; }
    inline std::string_view getText() const { return text.c_str(); }
    inline glm::vec2 const& getSize() const { return size; }
    inline glm::vec2 const& getPos() const { return pos; }
    inline Box const&       getPadding() const { return padding; }
    inline Box const&       getMargin() const { return margin; }
    inline Box const&       getBorderRadius() const { return borderRadius; }
    inline Color const&     getBorderColor() const { return borderColor; }
    inline float            getBorderWidth() const { return borderWidth; }
    inline int32_t          getRotateAngle() const { return rotateAngle; }
    inline float            getFontSize() const { return fontSize; }
    inline NodeStyle&       getRegularStyle() { return style; }
    inline NodeStyle const& getRegularStyle() const { return style; }
    inline NodeStyle&       getOnHoverStyle() { return onHoverStyle; }
    inline NodeStyle const& getOnHoverStyle() const { return onHoverStyle; }
    inline NodeStyle const& getStyle() const { return isHoverable && state == State::onHover ? onHoverStyle : style; }
    inline uint32_t         getSubtextStartIdx() const { return subtextStartIdx; }
    inline float            getSubtextY() const { return subtextY; }
    inline float            getLineHeight() const { return lineHeight; }
    inline NodeGrid         getGrid() const { return grid; }

    inline NodeChildrenList&       getChildren() { return children; }
    inline NodeChildrenList const& getChildren() const { return children; }
    inline auto                    view() { return std::views::all(binds); }

    // properties
    inline bool isRegular() const { return !isText; }
    inline bool hasParent() const { return parent != nullptr; }
    inline bool hasLt() const { return ltBind != nullptr; }
    inline bool hasRb() const { return rbBind != nullptr; }
    inline bool hasOuterLt() const { return outerLt != nullptr; }
    inline bool hasOuterRb() const { return outerRb != nullptr; }
    inline bool hasInnerLt() const { return innerLt != nullptr; }
    inline bool hasInnerRb() const { return innerRb != nullptr; }
    inline bool isHovered() const { return state == State::onHover; }
    inline bool isLastHovered() const { return isHovered() and isLastHoverable; }
    inline bool isRendered() const { return shouldRender; }
    inline bool isEnabled() const { return shouldEnable; }
    inline bool isBound() const { return ltBind || rbBind; }
    inline bool isHorizontalGrid() const { return grid == NodeGrid::horizontal; }
    inline bool isVerticalGrid() const { return grid == NodeGrid::vertical; }
    inline bool isLtBind(Node& node) const { return ltBind == &node; }
    inline bool isRbBind(Node& node) const { return rbBind == &node; }
    inline bool isParent(Node& node) const { return parent == &node; }
    inline bool isAutoWidth() const { return sizeVec.autoWidth; }
    inline bool isAutoHeight() const { return sizeVec.autoHeight; }

    // quick settings
    inline void setToHoverable() { isHoverable = true; }
    inline void setToRender(bool toRender = true) { shouldRender = toRender; }
    inline void disable() { shouldEnable = false; }
    inline void enable() { shouldEnable = true; }
    inline void setLastHoverable(bool b) { isLastHoverable = b; }

    // binds related
    void  bindLt(Node*, NodeGrid);
    void  bindRb(Node*, NodeGrid);
    // TODO: bind -> insert
    void  bindLeft(Node* node) { bindLt(node, NodeGrid::horizontal); }
    void  bindRight(Node* node) { bindRb(node, NodeGrid::horizontal); }
    void  bindTop(Node* node) { bindLt(node, NodeGrid::vertical); }
    void  bindBottom(Node* node) { bindRb(node, NodeGrid::vertical); }
    void  bindLeft(Node& node) { bindLeft(&node); }
    void  bindRight(Node& node) { bindRight(&node); }
    void  bindTop(Node& node) { bindTop(&node); }
    void  bindBottom(Node& node) { bindBottom(&node); }

    // control
    void prioritize();
    void emplace(Node&);
    void erase(); // complete node erasure 

    void setFixedSize();
    void setFixedWidth();
    void setFixedHeight();

    glm::vec2 getAbsolutePos() const;
    glm::vec2 getContentSize() const;
    Node&     getParent();
    Node&     getLt();
    Node&     getRb();
    Node&     getOuterLt();
    Node&     getOuterRb();
    Node&     getInnerLt();
    Node&     getInnerRb();
    Node*     getLtIf();
    Node*     getRbIf();
    Node*     getOuterLtIf();
    Node*     getOuterRbIf();
    Node*     getInnerLtIf();
    Node*     getInnerRbIf();

    // TODO: onHoverStyle should have all params optional
    void hardCodeOnHoverStyle() {
        onHoverStyle = style;
    }

    Node& operator[](std::string_view childName) {
        for (Node& child : children) {
            if (child.id == childName) {
                return child;
            }
        }
        BVD_THROW("No subnode {} inside node {}", childName, id);
    }

private:
    // properties
    inline bool hasSizeX() const { return size.x != 0.0f; }
    inline bool hasSizeY() const { return size.y != 0.0f; }
    inline bool isXCentered() const { return alignment == Alignment::x || alignment == Alignment::both; }
    inline bool isYCentered() const { return alignment == Alignment::y || alignment == Alignment::both; }
    inline bool bothBound() const { return ltBind != nullptr && rbBind != nullptr; }

    // control
    void unbind();
    void eraseChildren();
    NodeChildrenList::iterator removeFromParent();

private:
    void scale();
    void processPreResize(NodeResizeList&);
    void processResize(NodeResizeList&);
    void processPostResize(NodeResizeList&);

    void submitToResize();

    void processTextSize();
    void processRightBoundPos();
    void processBottomBoundPos();
    void processXCenteredPos(glm::vec2 const&);
    void processYCenteredPos(glm::vec2 const&);

    std::set<std::string> nodesToDebug = {};

private:
    NodeManager& manager;

    std::string id = "none";
    bool        shouldRender = true;
    bool        shouldEnable = true;
    std::string textureName = "none";

    // relations
    Node*            parent = nullptr;
    Node*            ltBind = nullptr;
    Node*            rbBind = nullptr;
    Node*            outerLt = nullptr;
    Node*            outerRb = nullptr;
    Node*            innerLt = nullptr;
    Node*            innerRb = nullptr;
    NodeGrid         grid = NodeGrid::blank;
    NodeChildrenList children;
    NodeBindList     binds;

    // text
    std::string text;
    bool        isText = false;
    float       fontSize = 18.0f;
    // parameters to track where text is needed for large text rendering optimization
    uint32_t    subtextStartIdx = 0;
    float       subtextY = 0;
    float       lineHeight = 1.0f * 18.0f; // hardcoded fsScale * metrics.lineHeight + lineHeightOffset
    // uint32_t subtextSize;

    // size
    glm::vec2      size;
    Size           sizeVec;
    bool           sizeXCalced = false;
    bool           sizeYCalced = false;
    SizeDependency xDep = SizeDependency::none;
    SizeDependency yDep = SizeDependency::none;

    // style
    bool       isHoverable = false;
    State      state = State::regular;
    NodeStyle  style;
    NodeStyle  onHoverStyle;
    bool       isLastHoverable{false}; // TODO: scrollable inside scrollable

    // border
    Box       borderRadius = {};
    Color     borderColor = colors::transparent;
    float     borderWidth = 2.0f;

    glm::vec2 pos;
    Box       padding = {};
    Box       margin = {};
    Alignment alignment = Alignment::none;
    int32_t   rotateAngle = 0;
};

} // namespace ui

} // namespace bvd
