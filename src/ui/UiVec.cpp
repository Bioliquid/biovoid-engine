#include "UiVec.hpp"
#include "Logger.hpp"

namespace bvd::ui {

} // namespace bvd::ui

auto fmt::formatter<bvd::ui::Val>::format(bvd::ui::Val const& val, format_context& ctx) -> decltype(ctx.out()) {
    return fmt::format_to(ctx.out(), "{}{}", val.value, val.isFixed ? "px" : "%");
}
