#pragma once

#include "Messages.hpp"

namespace bvd {

namespace ui {

class Node;

struct Script {
    Script(Node&);

    Node&        node;
    srvm::Receiver receiver;
};

} // namespace ui

} // namespace bvd
