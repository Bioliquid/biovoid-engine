#include "UiScript.hpp"

#include "UiNodeManager.hpp"

namespace bvd::ui {

Script::Script(Node& n) : node(n), receiver(n.getManager().getRouter()) {}

} // namespace bvd::ui
