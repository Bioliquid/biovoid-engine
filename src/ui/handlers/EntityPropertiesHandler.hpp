#pragma once

#include "UiScript.hpp"
#include "UiNodeManager.hpp"
#include "Messages.hpp"
#include "Entity.hpp"
#include "InspectorComponent.hpp"

namespace bvd {

namespace ui {

namespace handlers {

struct EntityProperties : public Script {
    EntityProperties(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(UiEntitySelectedInd const& msg) {
        Node& content = node["content"];
        if (activeInspector) {
            node.getManager().removeElement(content["inspector"]);
        }

        components::Inspector<>::blank(Id{"inspector"}, SceneId{std::string(msg.scene)}, EntityId{std::string(msg.name)})().inPlace(content).bindTop(content);

        activeInspector = true;
    }

    bool activeInspector{false};
};

} // namespace handlers

} // namespace ui

} // namespace bvd
