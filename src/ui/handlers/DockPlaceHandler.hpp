#pragma once

#include "UiScript.hpp"

namespace bvd {

namespace ui {

namespace handlers {

struct DockPlace : public Script {
    DockPlace(Node& n) : Script(n) {
        receiver.init(0, *this);
    }

    void receive(MouseReleaseInd const& msg) {
        
    }
};

} // namespace handlers

} // namespace ui

} // namespace bvd
