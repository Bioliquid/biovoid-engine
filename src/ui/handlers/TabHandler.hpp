#pragma once

#include "UiScript.hpp"
#include "DockPlaceComponent.hpp"
#include "VerticalSeparator.hpp"
#include "HorizontalSeparator.hpp"

namespace bvd {

namespace ui {

namespace handlers {

struct Tab : public Script {
    Tab(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const&) {
        if (not node.isHovered()) {
            return;
        }
        affected = true;

        Node& root = node.getManager().getOverlayLayer();

        Node& top = components::DockPlace<>::blank(Id{"DockPlaceGlobalTop"}, Alignment::x, Margin{glm::vec4{10.0f, 0.0f, 0.0f, 0.0f}}, RotateAngle{270})().inPlace(root);
        Node& bottom = components::DockPlace<>::blank(Id{"DockPlaceGlobalBottom"}, Alignment::x, Margin{ glm::vec4{0.0f, 0.0f, 10.0f, 0.0f} }, RotateAngle{ 90 })().inPlace(root);
        Node& middle = components::Div<>::blank(Render{false}, Id{"node1"}, Width{100.0_pct})(
            components::DockPlace<>::left(Id{ "DockPlaceGlobalLeft" }, Alignment::y, Margin{ glm::vec4{0.0f, 0.0f, 0.0f, 10.0f} })(),
            components::DockPlace<>::right(Id{ "DockPlaceGlobalRight" }, Alignment::y, Margin{ glm::vec4{0.0f, 10.0f, 0.0f, 0.0f} }, RotateAngle{ 180 })()
        ).inPlace(root);

        top.bindTop(root);
        bottom.bindBottom(root);
        middle.bindTop(top);
        middle.bindBottom(bottom);
    }

    void receive(MouseReleaseInd const&) {
        if (not affected) {
            return;
        }
        affected = false;

        Node& overlayRoot = node.getManager().getOverlayLayer();
        Node& root = node.getManager().getMainLayer();

        Node& tab = node.getParent();
        Node& node1 = overlayRoot["node1"];
        Node& dockPlaceGlobalLeft = node1["DockPlaceGlobalLeft"];
        Node& dockPlaceGlobalRight = node1["DockPlaceGlobalRight"];
        Node& dockPlaceGlobalTop = overlayRoot["DockPlaceGlobalTop"];
        Node& dockPlaceGlobalBottom = overlayRoot["DockPlaceGlobalBottom"];

        auto adoptInner = [&root](Node& adopter) {
            NodeGrid grid = root.view().front().getGrid();
            Node* innerRb = root.getInnerRbIf();
            Node* nodeToAdopt = root.getInnerLtIf();
            Node* bindNode = &adopter;
            Node* savedNode = nullptr;
            while (nodeToAdopt != nullptr) {
                Node* nextNode = nodeToAdopt->getOuterLtIf();
                if (nodeToAdopt->hasRb()) {
                    savedNode = nodeToAdopt;
                }
                adopter.emplace(*nodeToAdopt);
                nodeToAdopt->bindLt(bindNode, grid);
                bindNode = nodeToAdopt;
                nodeToAdopt = nextNode;
            }
            nodeToAdopt = innerRb;
            bindNode = &adopter;
            while (nodeToAdopt != nullptr and savedNode != nodeToAdopt) {
                Node* nextNode = nodeToAdopt->getOuterRbIf();
                adopter.emplace(*nodeToAdopt);
                nodeToAdopt->bindRb(bindNode, grid);
                bindNode = nodeToAdopt;
                nodeToAdopt = nextNode;
            }
            if (savedNode) {
                savedNode->bindRb(bindNode, grid);
            }
        };

        if (dockPlaceGlobalLeft.isHovered()) {
            root.emplace(tab);
            if (root.getChildren().front().isHorizontalGrid()) {
                if (root.getChildren().size() == 1 && &root.getChildren().front() == &tab) {
                    tab.bindLeft(root);
                    tab.bindRight(root);
                } else {
                    if (root.hasInnerLt()) {
                        tab.bindLeft(root);
                        components::VerticalSeparator<>::blank()().inPlace(root).bindLeft(tab);

                        root.getInnerLt().setMarginTop(0.0f);
                        root.getInnerLt().setMarginLeft(0.0f);
                    }
                }
            } else {
                Node& adopter = components::Div<>::blank(Id{"fakeLeft"}, Render{ false }, Height{ 100.0_pct })().inPlace(root);
                Node& separator = components::VerticalSeparator<>::blank()().inPlace(root);
                adoptInner(adopter);
                adopter.bindLeft(root);
                adopter.bindRight(root);
                tab.bindLeft(root);
                separator.bindLeft(tab);
            }

            // TODO: what if horizontal but root does not have inner lt
            tab.setMarginTop(0.0f);
            tab.setMarginLeft(0.0f);
            tab.setSizeY(100.0_pct);
        }

        if (dockPlaceGlobalRight.isHovered()) {
            root.emplace(tab);
            if (root.getChildren().front().isHorizontalGrid()) {
                if (root.getChildren().size() == 1 && &root.getChildren().front() == &tab) {
                    tab.bindLeft(root);
                    tab.bindRight(root);
                } else {
                    if (root.hasInnerRb()) {
                        tab.bindRight(root);
                        components::VerticalSeparator<>::blank()().inPlace(root).bindRight(tab);

                        root.getInnerRb().setMarginTop(0.0f);
                        root.getInnerRb().setMarginLeft(0.0f);
                    }
                }
            } else {
                Node& adopter = components::Div<>::blank(Render{ false }, Height{ 100.0_pct })().inPlace(root);
                Node& separator = components::VerticalSeparator<>::blank()().inPlace(root);
                adoptInner(adopter);
                adopter.bindLeft(root);
                adopter.bindRight(root);
                tab.bindRight(root);
                separator.bindRight(tab);
            }

            tab.setMarginTop(0.0f);
            tab.setMarginLeft(0.0f);
            tab.setSizeY(100.0_pct);
        }

        if (dockPlaceGlobalTop.isHovered()) {
            root.emplace(tab);
            if (root.getChildren().front().isVerticalGrid()) {
                if (root.getChildren().size() == 1 && &root.getChildren().front() == &tab) {
                    tab.bindTop(root);
                    tab.bindBottom(root);
                } else {
                    if (root.hasInnerLt()) {
                        tab.bindTop(root);
                        components::HorizontalSeparator<>::blank()().inPlace(root).bindTop(tab);

                        root.getInnerLt().setMarginTop(0.0f);
                        root.getInnerLt().setMarginLeft(0.0f);
                    }
                }
            } else {
                Node& adopter = components::Div<>::blank(Id{"fakeTop"}, Render{false}, Width{100.0_pct})().inPlace(root);
                Node& separator = components::HorizontalSeparator<>::blank()().inPlace(root);
                adoptInner(adopter);
                adopter.bindTop(root);
                adopter.bindBottom(root);
                tab.bindTop(root);
                separator.bindTop(tab);
            }

            tab.setMarginTop(0.0f);
            tab.setMarginLeft(0.0f);
            tab.setSizeX(100.0_pct);
        }

        if (dockPlaceGlobalBottom.isHovered()) {
            root.emplace(tab);
            if (root.getChildren().front().isVerticalGrid()) {
                if (root.getChildren().size() == 1 && &root.getChildren().front() == &tab) {
                    tab.bindTop(root);
                    tab.bindBottom(root);
                } else {
                    if (root.hasInnerRb()) {
                        tab.bindBottom(root);
                        components::HorizontalSeparator<>::blank()().inPlace(root).bindBottom(tab);

                        root.getInnerRb().setMarginTop(0.0f);
                        root.getInnerRb().setMarginLeft(0.0f);
                    }
                }
            } else {
                Node& adopter = components::Div<>::blank(Render{false}, Width{100.0_pct})().inPlace(root);
                Node& separator = components::HorizontalSeparator<>::blank()().inPlace(root);
                adoptInner(adopter);
                adopter.bindTop(root);
                adopter.bindBottom(root);
                tab.bindBottom(root);
                separator.bindBottom(tab);
            }

            tab.setMarginTop(0.0f);
            tab.setMarginLeft(0.0f);
            tab.setSizeX(100.0_pct);
        }

        node.getManager().removeElement(dockPlaceGlobalLeft);
        node.getManager().removeElement(dockPlaceGlobalRight);
        node.getManager().removeElement(dockPlaceGlobalTop);
        node.getManager().removeElement(dockPlaceGlobalBottom);
        node.getManager().removeElement(node1);
    }

    void receive(MouseMoveInd const& msg) {
        // if (Input::isMouseButtonPressed(buttons::left) && node.isHovered()) {
        if (affected) {
            Node& root = node.getManager().getMainLayer();
            Node& tab = node.getParent();

            if (not tab.isBound()) {
                tab.addMarginLeft(msg.delta.x);
                tab.addMarginTop(msg.delta.y);
                return;
            }

            Node* lt = tab.getOuterLtIf();
            if (lt and not tab.isParent(*lt)) {
                tab.getManager().removeElement(*lt);
            }
            Node* rb = tab.getOuterRbIf();
            if (rb and not tab.isParent(*rb)) {
                tab.getManager().removeElement(*rb);
            }

            glm::vec2 absPos = tab.getAbsolutePos();
            tab.setMarginLeft(absPos.x + msg.delta.x);
            tab.setMarginTop(absPos.y + msg.delta.y);
            tab.setFixedSize();

            root.emplace(tab);
        }
    }

    bool affected{false};
};

} // namespace handlers

} // namespace ui

} // namespace bvd
