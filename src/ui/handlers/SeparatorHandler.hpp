#pragma once

#include "UiScript.hpp"

namespace bvd {

namespace ui {

namespace handlers {

struct VerticalSeparator : public Script {
    VerticalSeparator(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const&) {
        if (node.isHovered()) {
            affected = true;
        }
    }

    void receive(MouseReleaseInd const&) {
        affected = false;
    }

    void receive(MouseMoveInd const& msg) {
        if (affected) {
            if (node.hasLt()) {
                node.getLt().addSizeX(msg.delta.x);
            } else if (node.hasRb()) {
                node.getRb().addSizeX(-msg.delta.x);
            }
        }
    }

    bool affected{false};
};

struct HorizontalSeparator : public Script {
    HorizontalSeparator(Node& n) : Script(n) {
        receiver.init<MessageMap>(0, *this);
    }

    void receive(MousePressInd const&) {
        if (node.isHovered()) {
            affected = true;
        }
    }

    void receive(MouseReleaseInd const&) {
        affected = false;
    }

    void receive(MouseMoveInd const& msg) {
        if (affected) {
            if (node.hasLt()) {
                node.getLt().addSizeY(msg.delta.y);
            } else if (node.hasRb()) {
                node.getRb().addSizeY(-msg.delta.y);
            }
        }
    }

    bool affected{false};
};

} // namespace handlers

} // namespace ui

} // namespace bvd
