#pragma once

#include "UiComponentHelper.hpp"

namespace bvd {

namespace ui {

struct ComponentInfo {
    ComponentInfo(Node& s, Node& p) : self(s), parent(p) {}

    template<typename... R>
    void fragment(R... children) {
        self.initialize(children.emplace()...);
        self.setBinds();
        (children(self), ...);
    }

    void fragment(std::vector<NodeCreator>& children) {
        for (NodeCreator& child : children) {
            self.initialize(child.emplace());
        }

        self.setBinds();

        for (NodeCreator& child : children) {
            child(self);
        }
    }

    void fragment(RefWrapper<std::vector<NodeCreator>> childrenRef) {
        std::vector<NodeCreator>& children = childrenRef.get();
        for (NodeCreator& child : children) {
            self.initialize(child.emplace());
        }

        self.setBinds();

        for (NodeCreator& child : children) {
            child(self);
        }
    }

    template<typename... R>
    void setParams(R... params) {
        OptionHandler<R...>::configure(self, parent, params...);
    }

    template<typename... R>
    void setGroups() {
        ct::OptionHandler<R...>::configure(self, parent);
    }

    Node& self;
    Node& parent;
};

template<template<typename...> class T, typename... NodeParams>
class Component {
protected:
    using ComponentType = T<NodeParams...>;
    using ClassTypes = hml::List<NodeParams...>;

public:
    template<typename... R>
    static auto blank(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);
        });
    }

    // Horizontal modifiers as if content layout was Row
    template<typename... R>
    static auto left(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);

            selfNode.bindLeft(nullptr);
        });
    }

    template<typename... R>
    static auto hmiddle(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);

            selfNode.bindLeft(nullptr);
            selfNode.bindRight(nullptr);
        });
    }

    template<typename... R>
    static auto right(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);

            selfNode.bindRight(nullptr);
        });
    }

    // Vertical modifiers as if content layout was Column
    template<typename... R>
    static auto top(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);

            selfNode.bindTop(nullptr);
        });
    }

    template<typename... R>
    static auto vmiddle(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);

            selfNode.bindTop(nullptr);
            selfNode.bindBottom(nullptr);
        });
    }

    template<typename... R>
    static auto bottom(R... params) {
        return NodeHandler([params...]<typename... Components>(Node& selfNode, Node& parentNode, Components... children) {
            static_assert(std::derived_from<ComponentType, ComponentInfo>, "UI Component is not derived from ComponentInfo");

            ComponentHelper<ClassTypes, hml::List<R...>> helper{selfNode, parentNode, params...};
            ComponentType component{ {}, {selfNode, parentNode} };
            component.render(helper, children...);

            selfNode.bindBottom(nullptr);
        });
    }
};

} // namespace ui

} // namespace bvd
