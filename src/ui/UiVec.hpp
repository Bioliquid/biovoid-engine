#pragma once

#include <cstdint>

namespace bvd {

namespace ui {

class Node;

struct Val {
    constexpr Val() = default;
    constexpr Val(char const*) : isAuto(true) {}
    constexpr Val(float v, uint8_t f) : value(v), isFixed(f) {}

    float value{0.0f};
    bool  isFixed{false};
    bool  isAuto{false};
};

inline Val pct(float value) { return Val(value, false); }
inline Val px(float value) { return Val(value, true); }

inline constexpr Val autoFit("auto");

} // namespace ui

} // namespace bvd

inline constexpr bvd::ui::Val operator"" _pct(long double value) {
    return bvd::ui::Val{(float)value, false};
}

inline constexpr bvd::ui::Val operator"" _px(long double value) {
    return bvd::ui::Val{(float)value, true};
}
