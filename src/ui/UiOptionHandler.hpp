#pragma once

#include "UiParams.hpp"
#include "CommonStyle.hpp"
#include "UiNodeManager.hpp"

namespace bvd {

namespace ui {

template<typename... Params>
struct OptionHandler;

template<>
struct OptionHandler<> {
    static void configure(Node&, Node&) {}
};

template<typename T, typename... Params>
struct OptionHandler<Handler<T>, Params...> {
    static void configure(Node& self, Node& parent, Handler<T> param, Params... params) {
        NodeManager::get().addHandler<T>(self, param.value);

        OptionHandler<Params...>::configure(self, parent);
    }
};

template<typename Param, typename... Params>
struct OptionHandler<Param, Params...> {
    static void configure(Node& self, Node& parent, Param param, Params... params) {
        detail::optHandler<Param>(self.getRegularStyle(), self, parent, param);

        OptionHandler<Params...>::configure(self, parent, params...);
    }
};

} // namespace ui

} // namespace bvd
