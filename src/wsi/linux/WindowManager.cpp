#include "WindowManager.hpp"

#include "Logger.hpp"
#include "Exception.hpp"

#include <glfw/glfw3.h>

namespace bvd {

static void glfwErrorCallback(int err, const char* description) {
    BVD_LOG(LogGroup::wcs, BE, "glfw error ({}): {}", err, description);
}

void WindowManager::init(RhiApi api) {
    rhiApi = api;

    if (glfwInit() == GLFW_FALSE) {
        BVD_THROW("Failed to initialize glfw");
    }

    glfwSetErrorCallback(glfwErrorCallback);

    if (selectedVulkan()) {
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    }

    if (selectedOpengl()) {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    }

    monitors = glfwGetMonitors(&numMonitors);

    for (uint8_t index = 0; index < numMonitors; ++index) {
        BVD_LOG(LogGroup::wcs, BI, "Monitor {}: {}", index, glfwGetMonitorName(monitors[index]));
    }

    cursors[CursorType::arrow] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    cursors[CursorType::beam] = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
    cursors[CursorType::crosshair] = glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);
    cursors[CursorType::hand] = glfwCreateStandardCursor(GLFW_HAND_CURSOR);
    cursors[CursorType::hResize] = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
    cursors[CursorType::vResize] = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
}

void WindowManager::destroy() {
    // Window manager doesn't own windows so clear is enough
    windowList.clear();

    glfwTerminate();
}

void WindowManager::pollEvents() const {
    glfwPollEvents();
}

bool WindowManager::shouldClose() const {
    if (windowList.empty()) {
        return true;
    }

    bool noActiveWindows = true;
    for (wsi::Window const& window : windowList) {
        if (!window.shouldClose()) {
            noActiveWindows = false;
        }
    }

    return noActiveWindows;
}

void WindowManager::addWindow(wsi::Window& window) {
    windowList.push_back(window);

    if (windowList.size() == 1) {
        singleWindow = true;
        currentWindow = &window;
        if (selectedOpengl()) {
            glfwMakeContextCurrent(window.window);
        }
    } else {
        singleWindow = false;
    }
}

void WindowManager::removeWindow(wsi::Window& window) {
    windowList.erase(windowList.iterator_to(window));
}

void WindowManager::makeWindowCurrent(wsi::Window& window) {
    currentWindow = &window;

    if (!singleWindow) {
        if (selectedOpengl()) {
            glfwMakeContextCurrent(window.window);
        }
    }
}

void WindowManager::changeCursor(CursorType cursorType) {
    glfwSetCursor(currentWindow->window, cursors[cursorType]);
}

bool WindowManager::isKeyPressed(int keycode) {
    int state = glfwGetKey(currentWindow->window, keycode);
    return state == GLFW_PRESS || state == GLFW_REPEAT;
}

bool WindowManager::isMouseButtonPressed(int button) {
    int state = glfwGetMouseButton(currentWindow->window, button);
    return state == GLFW_PRESS;
}

float WindowManager::getMouseX() {
    double xpos, ypos;
    glfwGetCursorPos(currentWindow->window, &xpos, &ypos);

    return (float)xpos;
}

float WindowManager::getMouseY() {
    double xpos, ypos;
    glfwGetCursorPos(currentWindow->window, &xpos, &ypos);

    return (float)ypos;
}

std::pair<float, float> WindowManager::getMousePos() {
    double xpos, ypos;
    glfwGetCursorPos(currentWindow->window, &xpos, &ypos);

    return std::make_pair((float)xpos, (float)ypos);
}

std::pair<float, float> WindowManager::getScreenSize() {
    int width, height;
    glfwGetWindowSize(currentWindow->window, &width, &height);

    return std::make_pair((float)width, (float)height);
}

std::pair<float, float> WindowManager::getFramebufferSize() {
    int width, height;
    glfwGetFramebufferSize(currentWindow->window, &width, &height);

    return std::make_pair((float)width, (float)height);
}

void WindowManager::waitForNonZeroFramebuffer() {
    int width = 0, height = 0;
    glfwGetFramebufferSize(currentWindow->window, &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(currentWindow->window, &width, &height);
        glfwWaitEvents();
    }
}

} // namespace bvd
