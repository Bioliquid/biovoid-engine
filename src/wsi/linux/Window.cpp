#include "Window.hpp"

#include "WindowManager.hpp"
#include "Messages.hpp"
#include "RhiApi.hpp"
#include "Logger.hpp"
#include "Exception.hpp"
#include "ContainerIds.hpp"
#include "Input.hpp"
#include "RtcTime.hpp"

#include <glfw/glfw3.h>

namespace bvd::wsi {

Window::Window(SrvmSender& inSender)
    : sender(inSender)
{}

void Window::init(WindowInit const& init) {
    if (windowManager.selectedOpengl()) {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
    }

    if (init.mode == WindowMode::windowed) {
        window = glfwCreateWindow(init.width, init.height, init.title.data(), nullptr, nullptr);
    } else if (init.mode == WindowMode::fullscreen) {
        window = glfwCreateWindow(init.width, init.height, init.title.data(), windowManager.getMonitor(preferredMonitor), nullptr);
    } else if (init.mode == WindowMode::borderless) {
        GLFWmonitor* monitor = windowManager.getMonitor(preferredMonitor);
        GLFWvidmode const* mode = glfwGetVideoMode(monitor);

        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

        window = glfwCreateWindow(mode->width, mode->height, init.title.data(), monitor, nullptr);
    }

    if (!window) {
        BVD_THROW("Failed to create a window");
    }

    if (windowManager.selectedOpengl()) {
        glfwMakeContextCurrent(window);
    }

    glfwSetWindowUserPointer(window, this);

    glfwSetWindowSizeCallback(window, [](GLFWwindow* nativeWindow, int width, int height) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        if (width == 0 || height == 0) {
            WindowMinimizeInd msg;
            window.sender.send(msg);
        } else {
            WindowResizeInd msg{
                  .width = width
                , .height = height
            };
            window.sender.send(msg);
        }
    });

    glfwSetWindowCloseCallback(window, [](GLFWwindow* nativeWindow) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        WindowCloseInd msg;
        window.sender.send(msg);
    });

    glfwSetKeyCallback(window, [](GLFWwindow* nativeWindow, int key, int, int action, int) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        switch (action) {
            case GLFW_PRESS:
            {
                KeyPressInd msg{
                      .keyCode = key
                    , .repeatCount = 0
                };

                window.sender.send(msg);
                break;
            }
            case GLFW_RELEASE:
            {
                KeyReleaseInd msg{
                    .keyCode = key
                };
                window.sender.send(msg);
                break;
            }
            case GLFW_REPEAT:
            {
                KeyPressInd msg{
                      .keyCode = key
                    , .repeatCount = 1
                };
                window.sender.send(msg);
                break;
            }
        }
    });

    glfwSetCharCallback(window, [](GLFWwindow* nativeWindow, uint32_t codepoint) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        KeyTypeInd msg{
            .keyCode = (int)codepoint
        };
        window.sender.send(msg);
    });

    glfwSetMouseButtonCallback(window, [](GLFWwindow* nativeWindow, int button, int action, int) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        switch (action) {
            case GLFW_PRESS:
            {
                float now = rtc::getTime();
                bool isDoubleClick = window.prevClickButtom == button && (now - window.prevClickTime) <= 0.5;

                MousePressInd msg{
                      .btnKey = button
                    , .isDoubleClick = isDoubleClick
                    , .pos = glm::vec2{Input::getMouseX(), Input::getMouseY()}
                };

                window.prevClickButtom = button;
                window.prevClickTime = now;

                window.sender.send(msg);
                break;
            }
            case GLFW_RELEASE:
            {
                MouseReleaseInd msg{
                    .btnKey = button
                };

                window.sender.send(msg);
                break;
            }
        }
    });

    glfwSetScrollCallback(window, [](GLFWwindow* nativeWindow, double xOffset, double yOffset) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));
        
        MouseScrollInd msg{
              .xOffset = (float)xOffset
            , .yOffset = (float)yOffset
        };
        window.sender.send(msg);
    });

    glfwSetCursorPosCallback(window, [](GLFWwindow* nativeWindow, double x, double y) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        glm::vec2 pos{x, y};
        glm::vec2 delta = pos - window.prevMousePos;
        
        MouseMoveInd msg{
              .pos = pos
            , .delta = delta
        };

        window.prevMousePos = pos;

        window.sender.send(msg);
    });

    glfwSetWindowRefreshCallback(window, [](GLFWwindow* nativeWindow) {
        Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(nativeWindow));

        WindowRefreshInd msg;
        window.sender.send(msg);
    });

    BVD_LOG(LogGroup::wcs, BI, "Window (title=\"{}\", width={}, height={}) has been initialized", init.title, init.width, init.height);
}

void Window::destroy() {
    glfwDestroyWindow(window);
}

bool Window::shouldClose() const {
    return forcedClose || glfwWindowShouldClose(window);
}

void Window::swapBuffers() const {
    glfwSwapBuffers(window);
}

} // namespace bvd::wsi
