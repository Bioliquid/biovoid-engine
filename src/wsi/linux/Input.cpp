#include "Input.hpp"

#include "WindowManager.hpp"

namespace bvd {

bool Input::isKeyPressed(int keycode) {
    return windowManager.isKeyPressed(keycode);
}

bool Input::isMouseButtonPressed(int button) {
    return windowManager.isMouseButtonPressed(button);
}

float Input::getMouseX() {
    return windowManager.getMouseX();
}

float Input::getMouseY() {
    return windowManager.getMouseY();
}

std::pair<float, float> Input::getMousePos() {
    return windowManager.getMousePos();
}

std::pair<float, float> Input::getScreenSize() {
    return windowManager.getScreenSize();
}

std::pair<float, float> Input::getFramebufferSize() {
    return windowManager.getFramebufferSize();
}

void Input::waitForNonZeroFramebuffer() {
    windowManager.waitForNonZeroFramebuffer();
}

void Input::changeCursor(CursorType type) {
    windowManager.changeCursor(type);
}

} // namespace bvd
