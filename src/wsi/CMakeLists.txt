include(CommonOptions)

create_library(wsi
    SRCS
        WsiComponent.cpp
        ${BuildPlatform}/Input.cpp
        ${BuildPlatform}/Window.cpp
        ${BuildPlatform}/WindowManager.cpp
    DIRS
        .
    LIBS
        inc::core
)
