#pragma once

#include "Window.hpp"
#include "CursorType.hpp"
#include "RhiApi.hpp"

#include <boost/intrusive/list.hpp>
#include <tuple>
#include <map>

namespace bvd {

struct Input;

class WindowManager {
    using Container = boost::intrusive::list<wsi::Window>;
    using Cursors = std::map<CursorType, GLFWcursor*>;

    friend struct Input;

public:
    WindowManager() = default;
    WindowManager(WindowManager const&) = delete;
    WindowManager& operator=(WindowManager const&) = delete;

    void init(RhiApi);
    void destroy();

    void pollEvents() const;

    bool shouldClose() const;

    Container& getAllActiveWindows() { return windowList; }
    
    void makeWindowCurrent(wsi::Window&);
    
    void addWindow(wsi::Window&);
    void removeWindow(wsi::Window&);

    GLFWmonitor* getPrimaryMonitor() { return monitors[0]; }
    GLFWmonitor* getMonitor(uint8_t index) { return monitors[index]; }

    void changeCursor(CursorType);

    inline bool selectedVulkan() { return rhiApi == RhiApi::vulkan; }
    inline bool selectedOpengl() { return rhiApi == RhiApi::opengl; }

private:
    bool isKeyPressed(int);
    bool isMouseButtonPressed(int);
    float getMouseX();
    float getMouseY();
    std::pair<float, float> getMousePos();
    std::pair<float, float> getScreenSize();
    std::pair<float, float> getFramebufferSize();
    void waitForNonZeroFramebuffer();

    Container     windowList;
    wsi::Window*  currentWindow = nullptr;

    bool          singleWindow = false;

    int           numMonitors;
    GLFWmonitor** monitors;

    Cursors       cursors;

    RhiApi rhiApi;
};

/*
* Initialized and destroyed in App
*/
inline WindowManager windowManager;

} // namespace bvd
