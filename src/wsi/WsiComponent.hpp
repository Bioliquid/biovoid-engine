#pragma once

#include "WsiSetupReq.hpp"
#include "WindowActionReq.hpp"

#include "Messages.hpp"
#include "Scope.hpp"

namespace bvd {

namespace wsi {

class Window;

class Component
{

public:
    Component(srvm::Router&);
    ~Component();

    void receive(WsiSetupReq const&);
    void receive(FrameTickInd const&);
    void receive(WindowActionReq const&);
    void receive(WindowSurfaceInfoReq const&);

private:
    srvm::Sender    sender;
    srvm::Receiver  receiver;
    Scope<Window> window;
};

} // namespace wsi

} // namespace bvd
