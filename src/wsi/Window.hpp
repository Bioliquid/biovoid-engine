#pragma once

#include "WindowMode.hpp"
#include "SrvmEndpoint.hpp"
#include "Messages.hpp"

#include <boost/intrusive/list_hook.hpp>
#include <glm/glm.hpp>
#include <cstdint>
#include <tuple>

struct GLFWwindow;
struct GLFWmonitor;
struct GLFWcursor;

namespace bvd {

class WindowManager;

struct WindowInit {
    int32_t          width;
    int32_t          height;
    WindowMode       mode;
    std::string_view title;
};

namespace wsi {

class Window : public boost::intrusive::list_base_hook<>
{
    friend class WindowManager;

public:
    Window(srvm::Sender&);
    Window(Window const&) = delete;
    Window& operator=(Window const&) = delete;

    void init(WindowInit const&);
    void destroy();

    bool shouldClose() const;
    void swapBuffers() const;

    void* getHandle() const { return window; }

    void setForcedClosed() { forcedClose = true; }

    srvm::Sender& sender;
    float prevClickTime{0.0f};
    int   prevClickButtom{0};

private:
    GLFWwindow* window;

    glm::vec2 prevMousePos;
    bool      forcedClose = false;
    uint8_t   preferredMonitor = 0;

};

} // namespace wsi

} // namespace bvd
