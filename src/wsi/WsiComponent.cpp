#include "WsiComponent.hpp"

#include "Window.hpp"
#include "WindowManager.hpp"
#include "Input.hpp"
#include "ContainerIds.hpp"

namespace bvd::wsi {

Component::Component(srvm::Router& inRouter)
    : sender(inRouter)
    , receiver(inRouter)
{
    receiver.init<MessageMap>(containerId::any, *this);
    window.reset(new Window(sender));
}

Component::~Component() {
    windowManager.removeWindow(*window);
    window->destroy();
}

void Component::receive(WsiSetupReq const& req) {
    WindowInit windowInit{
          .width = req.width
        , .height = req.height
        , .mode = req.mode
        , .title = req.title
    };

    window->init(windowInit);

    windowManager.addWindow(*window);
}

void Component::receive(FrameTickInd const&) {
    windowManager.pollEvents();
}

void Component::receive(WindowActionReq const& req) {
    if (req.action == WindowAction::makeActive) {
        windowManager.makeWindowCurrent(*window);
    } else if (req.action == WindowAction::swapBuffers) {
        if (windowManager.selectedOpengl()) {
            window->swapBuffers();
        }
    } else if (req.action == WindowAction::close) {
        window->setForcedClosed();
    }
}

void Component::receive(WindowSurfaceInfoReq const&) {
    auto [width, height] = Input::getScreenSize();
    
    WindowSurfaceInfoRsp rsp;
    rsp.width = (uint32_t)width;
    rsp.height = (uint32_t)height;
    rsp.surfaceHandle = window->getHandle();
    sender.send(rsp);
}

} // namespace bvd::wsi
