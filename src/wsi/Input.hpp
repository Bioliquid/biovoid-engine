#pragma once

#include "KeyCodes.hpp"
#include "MouseCodes.hpp"
#include "CursorType.hpp"

#include <tuple>

namespace bvd {

struct Input {
    static bool isKeyPressed(int);
    static bool isMouseButtonPressed(int);
    static float getMouseX();
    static float getMouseY();
    static std::pair<float, float> getMousePos();
    static std::pair<float, float> getScreenSize();
    static std::pair<float, float> getFramebufferSize();
    static void waitForNonZeroFramebuffer();

    static void changeCursor(CursorType);
};

} // namespace bvd
