#version 450

layout(location = 0) in vec2  vPosition;
layout(location = 1) in vec2  vTexCoord;
layout(location = 2) in vec4  vColor;
layout(location = 3) in vec4  vBorderColor;
layout(location = 4) in vec2  vCenter;
layout(location = 5) in vec2  vSize;
layout(location = 6) in vec4  vBorderRadius;
layout(location = 7) in vec2  vLocalPosition;
layout(location = 8) in float vBorderWidth;
layout(location = 9) in uint  vTextureId;
layout(location = 10) in uint vIsText;

layout(location = 0) out vec2 fragTexCoord;
layout(location = 1) out vec4 color;
layout(location = 2) out vec4 borderColor;
layout(location = 3) out vec2 position;
layout(location = 4) out vec2 center;
layout(location = 5) out vec2 size;
layout(location = 6) out vec4 borderRadius;
layout(location = 7) out vec2 localPosition;
layout(location = 8) flat out float borderWidth;
layout(location = 9) flat out uint textureId;
layout(location = 10) flat out uint isText;

layout (std140, binding = 0) uniform Camera
{
    mat4 viewProjection;
} camera;

void main() {
    fragTexCoord = vTexCoord;
    color = vColor;
    borderColor = vBorderColor;
    textureId = vTextureId;
    position = vPosition;
    center = vCenter;
    size = vSize;
    borderRadius = vBorderRadius;
    localPosition = vLocalPosition;
    borderWidth = vBorderWidth;
    isText = vIsText;
    gl_Position = camera.viewProjection * vec4(vPosition, 0.0, 1.0);
}
