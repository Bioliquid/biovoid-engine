#version 450

layout(location = 0) in vec2 vPosition;
layout(location = 1) in vec2 vTexCoord;

layout(location = 0) out vec2 fragTexCoord;

layout (std140, binding = 0) uniform Camera
{
    mat4 viewProjection;
} camera;

void main() {
    gl_Position = camera.viewProjection * vec4(vPosition, 0.0, 1.0);
    fragTexCoord = vTexCoord;
}
