#version 450

layout(location = 0) in vec2 fragTexCoord;
layout(location = 1) in vec4 color;
layout(location = 2) in vec4 borderColor;
layout(location = 3) in vec2 position;
layout(location = 4) in vec2 center;
layout(location = 5) in vec2 size;
layout(location = 6) in vec4 borderRadius;
layout(location = 7) in vec2 localPosition;
layout(location = 8) flat in float borderWidth;
layout(location = 9) flat in uint textureId;
layout(location = 10) flat in uint isText;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler[10];

float roundedBoxSDF(vec2 CenterPosition, vec2 Size, vec4 Radius) {
    Radius.xy = (CenterPosition.x > 0.0) ? Radius.xy : Radius.zw;
    Radius.x  = (CenterPosition.y > 0.0) ? Radius.x  : Radius.y;

    vec2 q = abs(CenterPosition) - Size + Radius.x;
    return min(max(q.x, q.y), 0.0) + length(max(q, 0.0)) - Radius.x;
}

float median(float r, float g, float b) {
    return max(min(r, g), min(max(r, g), b));
}

float screenPxRange() {
    float pxRange = 2.5f;
    vec2 unitRange = vec2(pxRange) / vec2(textureSize(texSampler[textureId], 0));
    vec2 screenTexSize = vec2(1.0) / fwidth(fragTexCoord);
    return max(0.5 * dot(unitRange, screenTexSize), 1.0);
}

void main() {
    float temp = step(0.0, localPosition.y);
    vec4 tmpv = vec4(temp);

    if (!bool(isText)) {
        // How soft the edges should be (in pixels). Higher values could be used to simulate a drop shadow.
        float edgeSoftness  = 0.0f;

        // Calculate distance to edge
        vec2 centerPosition = position - center - vec2(size.x, -size.y) / 2;
        float distance = roundedBoxSDF(centerPosition, size / 2, borderRadius);

        // Smooth the result (free antialiasing).
        float smoothedAlpha =  1.0f - smoothstep(0.0f, edgeSoftness, distance);

        // Border
        float borderSoftness  = 2.0f;
        float borderAlpha     = 1.0f - smoothstep(borderWidth - borderSoftness, borderWidth, abs(distance));

        // Shadow
        // float shadowSoftness = 0.0;            // The (half) shadow radius (in pixels)
        // vec2  shadowOffset   = vec2(0.0, 0.0); // The pixel-space shadow offset from rectangle center

        // Colors
        vec4 rectColor = color;
        // vec4 bgColor = color;
        vec4 bgColor = vec4(1.0f, 1.0f, 1.0f, 0.0f);
        vec4 shadowColor = color;

        // Apply a drop shadow effect.
        // float shadowDistance  = roundedBoxSDF(centerPosition + shadowOffset, size / 2, borderRadius);
        // float shadowAlpha = 1.0 - smoothstep(-shadowSoftness, shadowSoftness, shadowDistance);
        float shadowAlpha = 0.0f;

        // Apply colors layer-by-layer: background <- shadow <- rect <- border.
        // Blend background with shadow
        vec4 res_shadow_color = mix(bgColor, vec4(shadowColor.rgb, shadowAlpha), shadowAlpha);

        // Blend (background+shadow) with rect
        //   Note:
        //     - Used 'min(u_colorRect.a, smoothedAlpha)' instead of 'smoothedAlpha'
        //       to enable rectangle color transparency
        vec4 res_shadow_with_rect_color = mix(res_shadow_color, rectColor, min(rectColor.a, smoothedAlpha));

        // Blend (background+shadow+rect) with border
        //   Note:
        //     - Used 'min(borderAlpha, smoothedAlpha)' instead of 'borderAlpha'
        //       to make border 'internal'
        //     - Used 'min(u_colorBorder.a, alpha)' instead of 'alpha' to enable
        //       border color transparency
        vec4 res_shadow_with_rect_with_border = mix(res_shadow_with_rect_color, borderColor, min(borderColor.a, min(borderAlpha, smoothedAlpha)));

        outColor = texture(texSampler[textureId], fragTexCoord) * res_shadow_with_rect_with_border * tmpv;
    } else {
        vec4 bgColor = vec4(color.rgb, 0.0);
        vec4 fgColor = color;

        vec3 msd = texture(texSampler[textureId], fragTexCoord).rgb;
        float sd = median(msd.r, msd.g, msd.b);

        float screenPxDistance = screenPxRange() * (sd - 0.5f);
        float opacity = clamp(screenPxDistance + 0.75, 0.0, 1.0);
        outColor = mix(bgColor, fgColor, opacity) * tmpv;
        
        if (opacity == 0.0) {
            discard;
        }
    }
}
