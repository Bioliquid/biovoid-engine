﻿cmake_minimum_required(VERSION 3.22)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake/modules)

project(biovoid)

include(FetchContent)

set(CMAKE_CXX_STANDARD 23)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)

# General variables
set(RootDir ${CMAKE_CURRENT_SOURCE_DIR})
set(SrcDir ${RootDir}/src)
set(ExtDir ${RootDir}/externals/biovoid-sdk/build)
set(SDKDir ${RootDir}/externals/biovoid-sdk/)
set(glad_DIR ${SDKDir}/glad/build/glad)

if(${CMAKE_BUILD_TYPE} STREQUAL "Release" OR ${CMAKE_BUILD_TYPE} STREQUAL "RelWithDebInfo")
    set(BuildType release)
elseif(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    set(BuildType debug)
endif()

if(WIN32)
    # for Windows operating system in general
    set(BuildPlatform windows)
    set(WindowsPlatform TRUE)
    set(ApplePlatform FALSE)
    set(LinuxPlatform FALSE)
    set(CMAKE_SHARED_LINKER_FLAGS /MANIFEST:NO)
    set(CMAKE_EXE_LINKER_FLAGS /MANIFEST:NO)
elseif(APPLE)
    # for MacOS X or iOS, watchOS, tvOS (since 3.10.3)
    set(BuildPlatform macos)
    set(WindowsPlatform FALSE)
    set(ApplePlatform TRUE)
    set(LinuxPlatform FALSE)
elseif(UNIX AND NOT APPLE)
    # for Linux, BSD, Solaris, Minix
    set(BuildPlatform linux)
    set(WindowsPlatform FALSE)
    set(ApplePlatform FALSE)
    set(LinuxPlatform TRUE)
endif()

if(${WindowsPlatform})
    add_compile_options(
       -W4
       #-Wall
       -WX
       -wd4061 # enumerator in switch of enum is not explicitly handled by a case label
       -wd4514 # unreferenced inline function has been removed
       -wd4820 # n bytes padding added after data member
       -wd4251 # "needs to have dll-interface to be used by clients of class"
       -wd4710 # function not inlined
       -wd4711 # selected for automatic inline expansion
       -wd5045 # Compiler will insert Spectre mitigation for memory load
       -wd4701 # Potentially uninitialized local variable
       -wd4996
        )
elseif(${LinuxPlatform})
    add_compile_options(
       -Wall
       -Wextra
       )
endif()

add_compile_definitions(
    $<$<CONFIG:Debug>:ENABLE_MEMORY_LEAK_DETECTION>
    USE_VALIDATION_LAYERS
    VULKAN_USE_IMAGE_ACQUIRE_FENCES=false
    DEVELOPMENT_GUIDES_CHECK
    ENABLE_INTERNAL_DEBUG=true
    BOOST_ENABLE_ASSERT_HANDLER
)

# Attach externals
include_directories(SYSTEM ${ExtDir}/${BuildType}/include)
link_directories(${ExtDir}/${BuildType}/lib)

set(CMAKE_PREFIX_PATH
    ${CMAKE_PREFIX_PATH}
    ${ExtDir}/${BuildType}/glad
    ${ExtDir}/${BuildType}/lib/cmake/
    ${ExtDir}/${BuildType}/lib/EnTT/cmake
)

find_package(Vulkan REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Boost REQUIRED)
find_package(assimp REQUIRED)
find_package(EnTT REQUIRED)
find_package(fmt REQUIRED)
find_package(glfw3 REQUIRED)
find_package(glm REQUIRED)
find_package(glad REQUIRED)
find_package(RapidJSON)

FetchContent_Declare(hml
    GIT_REPOSITORY https://gitlab.com/Bioliquid/hml
    GIT_TAG master
)
FetchContent_MakeAvailable(hml)


# Project directories
add_subdirectory(src)
add_subdirectory(editor)
