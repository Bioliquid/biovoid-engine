# Biovoid Engine

## Build procedure

```
$ git clone https://gitlab.com/Bioliquid/biovoid-engine.git --recursive
$ cd external/biovoid-sdk/
$ python setup.py
```

## Cmake dependency graph

```
$ mkdir tmp
$ cd tmp
$ cmake --graphviz=cmakedeps.dot -DCMAKE_BUILD_TYPE=Release ..
$ dot -Tpng cmakedeps.dot -o cmakedeps.png
```
